﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 12/17/2014
'Description:
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using System.IO;
using WrapperService.BusinessEntities;

namespace WrapperService.DataAccessRepository.Interfaces
{
    public interface IInvoiceRepository
    {
        /// <summary>
        /// Create Invoice
        /// </summary>
        /// <returns></returns>
        ValidationDetail CreateInvoice(Invoice invoice);

        /// <summary>
        /// Get Customer Invoices
        /// </summary>
        /// <returns></returns>
        List<InvoiceDetails> GetInvoices(string accountNumber,string status=null);

        /// <summary>
        /// Get Customer Invoice documents
        /// </summary>
        /// <returns></returns>
        InvoiceDocumentsList GetInvoiceDocument(string folderpath,string customerId, List<string> invoiceIDs = null);

        ///// <summary>
        ///// Get invoice folder
        ///// </summary>
        ///// <returns></returns>
        //string GetInvoiceFolder();

        /// <summary>
        /// Cancell Invoice
        /// </summary>
        /// <returns></returns>
        ValidationDetail CancelInvoice(InvoiceCancellation InvoiceCancellation);


        /// <summary>
        /// Get Customer Invoice details based on billable items id
        /// </summary>
        /// <returns></returns>
        List<CustomerInvoiceListDetails> GetInvoiceDetails(List<string> BillabaleItemIDList);


    }
}
