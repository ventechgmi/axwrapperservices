﻿/******************************************************************************************
'Created By: ANANTHAKUMAR S
'Created Date: 12/03/2014
'Description:
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using WrapperService.BusinessEntities;

namespace WrapperService.DataAccessRepository.Interfaces
{
    public interface ICustomerRepository
    {
        ///// <summary>
        ///// Chek if the account number is existing or not
        ///// </summary>
        ///// <returns></returns>
       // bool IsCustomerExists(string accountNumner);

        ///// <summary>
        ///// Chek if the account number is existing or not
        ///// </summary>
        ///// <returns></returns>

        ValidationDetail IsExistingCustomer(string accountNumner);

        /// <summary>
        /// Insert Customer
        /// </summary>
        /// <returns></returns>
        ValidationDetail InsertCustomer(Customer customer);
        /// <summary>
        /// Update the existing customer
        /// </summary>
        /// <returns></returns>
        ValidationDetail UpdateCustomer(Customer customer);
        /// <summary>
        /// Delete a customer 
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <returns></returns>
        ValidationDetail DisableCustomer(string accountNumber);

        string GetCustomer(string accountNumber);
  
    }

}
