﻿/******************************************************************************************
'Created By: ANANTHAKUMAR S
'Created Date: 09/17/2015
'Description:
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using WrapperService.BusinessEntities;

namespace WrapperService.DataAccessRepository.Interfaces
{
    public interface IVendorRepository
    {
          ///// <summary>
        ///// Chek if the account number is existing or not
        ///// </summary>
        ///// <returns></returns>

        ValidationDetail IsExistingVendor(string accountNumner);

        /// <summary>
        /// Insert Vendor
        /// </summary>
        /// <returns></returns>
        ValidationDetail InsertVendor(Vendor Vendor);
        /// <summary>
        /// Update the existing Vendor
        /// </summary>
        /// <returns></returns>
        ValidationDetail UpdateVendor(Vendor Vendor);
  
    }

}
