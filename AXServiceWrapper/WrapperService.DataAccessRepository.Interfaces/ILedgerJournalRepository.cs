﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 12/19/2014
'Description:
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using WrapperService.BusinessEntities;

namespace WrapperService.DataAccessRepository.Interfaces
{
    public interface ILedgerJournalRepository
    {
        /// <summary>
        /// Create Payment
        /// </summary>
        /// <returns></returns>
        ValidationDetail CreatePayment(LedgerJournal ledgerJournal);
    }
}
