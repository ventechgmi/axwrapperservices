﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 12/19/2014
'Description: Ledger Journal Transaction properties
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
namespace WrapperService.BusinessEntities.Interfaces
{
    public interface ILedgerJournalTransaction
    {
        /// <summary>
        /// Transaction date
        /// </summary>
        DateTime? TransactionDate { get; set; }

        //  CustPaymJournalWCF.AxdEnum_LedgerJournalACType? AccountType { get; set; }
        /// <summary>
        /// Account Type 
        /// </summary>
        string AccountType { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Debit 
        /// </summary>
        decimal? Debit { get; set; }

        /// <summary>
        /// Credit
        /// </summary>
        decimal? Credit { get; set; }

        //   CustPaymJournalWCF.AxdEnum_LedgerJournalACType? OffsetAccountType { get; set; }
        /// <summary>
        /// Offset Account Type
        /// </summary>
        string OffsetAccountType { get; set; }


        /// <summary>
        /// Invoice Id
        /// </summary>
        string InvoiceId { get; set; }

        // DateTime? DueDate { get; set; } //Not needed
        //  List<DefaultDimension> DefaultDimension { get; set; } //Not needed
        //  List<DefaultDimension> OffsetDefaultDimension { get; set; } //Not needed

        /// <summary>
        /// Company Name
        /// </summary>
        string Company { get; set; } //SFM

        /// <summary>
        /// Offset Company Name
        /// </summary>
        string OffsetCompany { get; set; } //SMF

        /// <summary>
        /// Payment Mode
        /// </summary>
        string PaymentMode { get; set; } //CreditCard

        /// <summary>
        /// Payment Id
        /// </summary>
        string PaymentId { get; set; }

        /// <summary>
        /// Payment Notes
        /// </summary>
        string PaymentNotes { get; set; } //Random note
        
        //Added ARCode in ledger creation - Jan-22-2015 - starts here 
        /// <summary>
        /// ARCode
        /// </summary>
          string ARCode { get; set; }
        //Added ARCode in ledger creation - Jan-22-2015 - ends here 
    }
}
