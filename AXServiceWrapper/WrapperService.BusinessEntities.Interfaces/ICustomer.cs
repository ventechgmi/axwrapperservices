﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/03/2014
'Description: Customer interface
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

namespace WrapperService.BusinessEntities.Interfaces
{
    public interface ICustomer
    {
        /// <summary>
        /// Handles running unique account number
        /// </summary>
        string AccountNumber { get; set; }
        /// <summary>
        /// Customer first name
        /// </summary>
        string FirstName { get; set; }
        /// <summary>
        /// Customer middle Name
        /// </summary>
        string MiddleName { get; set; }
        /// Customer Last Name
        string LastName { get; set; }
        /// <summary>
        /// Customer Name Sequence which means to show the first name last name combination  like  - FirstLast, LastFirst - Its a configurable item in the web.config
        /// </summary>
        string NameSequence { get; set; }
        /// <summary>
        /// Customer Alais Name
        /// </summary>
        string NameAlias { get; set; }

        /// <summary>
        /// Curreny Type 
        /// </summary>
        string Currency { get; set; }

        /// <summary>
        /// Customer Group
        /// </summary>
        string Group { get; set; }
        // Commented By Ananthakumar - Dec-09-2014 - starts
        // //Added Ananthakumar-Dec-06-2016 , for status  --starts here
        // /// <summary>
        // /// Customer Status 
        // /// </summary>
        //   string Status { get; set; }

        ////Added Ananthakumar-Dec-06-2016 , for status  --starts here
        // Commented By Ananthakumar - Dec-09-2014 - ends starts


        //Added for enable or disable the customer - Jan-23-2015
        /// <summary>
        /// IsDisable
        /// </summary>
        bool IsDisable { get; set; }

        //Added By Ananthakumar - Feb-23-2015 Reason: Customertype , Suffix - starts here
        /// <summary>
        /// Customertype is like Person , Organization etc
        /// </summary>
        string CustomerType { get; set; }
        /// <summary>
        /// Suffix
        /// </summary>
        string Suffix { get; set; }
        /// <summary>
        /// OrganizationName
        /// </summary>
        string OrganizationName { get; set; }

        //Added By Ananthakumar - Feb-23-2015 Reason: Customertype , Suffix - ends here       

    }
}
