﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 12/19/2014
'Description: Ledger related properties
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
namespace WrapperService.BusinessEntities.Interfaces
{
    public interface ILedgerDimension
    {
        /// <summary>
        /// Account Name
        /// </summary>
          string Account { get; set; }

        /// <summary>
        /// Value
        /// </summary>
          string DisplayValue { get; set; }
        //  public List<DefaultDimension> Values { get; set; }
    }
}