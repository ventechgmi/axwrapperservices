﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/03/2014
'Description: Fiscal Officeer interface
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrapperService.BusinessEntities.Interfaces
{
   public interface IFiscalOfficer
    {
        /// <summary>
        /// Fiscal Officer Account ID
        /// </summary>
         string AccountID { get; set; }
        /// <summary>
        /// Fiscal officer First Name
        /// </summary>
         string FirstName { get; set; }
        /// <summary>
        /// Fiscal officer Last name
        /// </summary>
         string LastName { get; set; }
        /// <summary>
        /// Fiscal officer Middle name
        /// </summary>
         string MiddleName { get; set; }
       
       /// <summary>
        /// Fiscal officer  name
        /// </summary>
       
       string Name { get; set; }
        /// <summary>
        /// Fiscal officer Suffix
        /// </summary>
        string Suffix { get; set; }

         /// <summary>
         /// Type is either Secretary or Fiscal
         /// </summary>
        string Type { get; set; }

    }
}