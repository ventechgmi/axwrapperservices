﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 09/17/2015
'Description: Vendor interface
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

namespace WrapperService.BusinessEntities.Interfaces
{
    public interface IVendor
    {
        /// <summary>
        /// Handles running unique account number
        /// </summary>
         string AccountNumber { get; set; }
        /// <summary>
        /// Vendor first name
        /// </summary>
         string FirstName { get; set; }
        /// <summary>
        /// Vendor middle Name
        /// </summary>
         string MiddleName { get; set; }
        /// Vendor Last Name
         string LastName { get; set; }
       
        /// <summary>
        /// Curreny Type 
        /// </summary>
         string Currency { get; set; }

        /// <summary>
        /// Vendor Group
        /// </summary>
         string Group { get; set; }
      
    }
}
