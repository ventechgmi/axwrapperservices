﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 01/07/2015
'Description: Invoice Details
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
namespace WrapperService.BusinessEntities.Interfaces
{
    public interface IInvoiceDetails
    {

          string InvoiceAccount { get; set; }
          string InvoiceAmount { get; set; }
          string InvoiceId { get; set; }
          string InvoiceDate { get; set; }
          string DueDate { get; set; }
          string RemainingAmount { get; set; }
       
          /// <summary>
          /// Paymnet Status
          /// </summary>
          string PaymentStatus { get; set; }
      
        //Commented By Ananthakumar --Feb-20-2015 Reason: No need to bring all fields from AX - starts here
        //string CustGroup { get; set; }
        //string RefNum { get; set; }
        //string SalesId { get; set; }
        //string OrderAccount { get; set; }
        //string InvoiceAccount { get; set; }
        //string InvoiceDate { get; set; }
        //string DueDate { get; set; }
        //string CashDisc { get; set; }
        //string CashDiscDate { get; set; }
        //string Qty { get; set; }
        //string Volume { get; set; }
        //string Weight { get; set; }
        //string SumLineDisc { get; set; }
        //string SalesBalance { get; set; }
        //string EndDisc { get; set; }
        //string InvoiceAmount { get; set; }
        //string CurrencyCode { get; set; }
        //string ExchRate { get; set; }
        //string InvoiceId { get; set; }
        //string LedgerVoucher { get; set; }
        //string Updated { get; set; }
        //string OnAccountAmount { get; set; }
        //string TaxPrintOnInvoice { get; set; }
        //string Listcode { get; set; }
        //string DocumentNum { get; set; }
        //string DocumentDate { get; set; }
        //string CashDiscPercent { get; set; }
        //string IntrastatDispatch { get; set; }
        //string DeliveryName { get; set; }
        //string EnterpriseNumber { get; set; }
        //string PurchaseOrder { get; set; }
        //string DlvTerm { get; set; }
        //string DlvMode { get; set; }
        //string Payment { get; set; }
        //string CashDiscCode { get; set; }
        //string InvoiceRoundOff { get; set; }
        //string SumMarkup { get; set; }
        //string CovStatus { get; set; }
        //string ReturnItemNum { get; set; }
        //string PostingProfile { get; set; }
        //string Backorder { get; set; }
        //string Prepayment { get; set; }
        //string TaxGroup { get; set; }
        //string TaxItemGroup { get; set; }
        //string TaxSpecifyByLine { get; set; }
        //string EInvoiceLineSpecific { get; set; }
        //string OneTimeCustomer { get; set; }
        //string PaymentSched { get; set; }
        //string SumTax { get; set; }
        //string SalesType { get; set; }
        //string EInvoiceAccountCode { get; set; }
        //string InterCompanyPosted { get; set; }
        //string ParmId { get; set; }
        //string ReturnReasonCodeId { get; set; }
        //string EUSalesList { get; set; }
        //string ExchRateSecondary { get; set; }
        //string Triangulation { get; set; }
        //string CustomerRef { get; set; }
        //string VATNum { get; set; }
        //string numberSequenceGroup { get; set; }
        //string InclTax { get; set; }
        //string Log { get; set; }
        //string PaymDayId { get; set; }
        //string InvoicingName { get; set; }
        //string GiroType { get; set; }
        //string ContactPersonId { get; set; }
        //string SalesOriginId { get; set; }
        //string BillofLadingId { get; set; }
        //string inventLocationId { get; set; }
        //string FixedDueDate { get; set; }
        //string InvoiceAmountMST { get; set; }
        //string InvoiceRoundOffMST { get; set; }
        //string SumMarkupMST { get; set; }
        //string SumLineDiscMST { get; set; }
        //string EndDiscMST { get; set; }
        //string SalesBalanceMST { get; set; }
        //string SUmTaxMST { get; set; }
        //string PrintMgmtSiteId { get; set; }
        //string ReturnStatus { get; set; }
        //string InterCompanyCompanyId { get; set; }
        //string InterCompanyPurchId { get; set; }
        //string PrintedOriginals { get; set; }
        //string Proforma { get; set; }
        //string RCSalesList_UK { get; set; }
        //string ReverseCharge_UK { get; set; }
        //string DeliveryPostalAddress { get; set; }
        //string InvoicePostalAddress { get; set; }
        //string SourceDocumentHeader { get; set; }
        //string DefaultDimension { get; set; }
        //string BankLCExportLine { get; set; }
        //string WorkerSalesTaker { get; set; }
        //string ReversedRecId { get; set; }
        //string ReceiptDateConfirmed_ES { get; set; }
        //string PaymId { get; set; }
        //string TaxInvoiceSalesId { get; set; }
        //string IntrastatFulfillmentDate_HU { get; set; }
        //string CashDiscBaseDate { get; set; }
        //string IsCorrection { get; set; }
        //string ReasonTableRef { get; set; }
        //string SourceDocumentLine { get; set; }
        //string TransportationDocument { get; set; }
        //string DirectDebitmandate { get; set; }
        //string modifiedDateTime { get; set; }
        //string createdDateTime { get; set; }
        //string dEL_createdTime { get; set; }
        //string createdBy { get; set; }
        //string dataAreaId { get; set; }
        //string recVersion { get; set; }
        //string Partition { get; set; }
        //string RecId { get; set; }

        //Commented By Ananthakumar --Feb-20-2015 Reason: No need to bring all fields from AX - ends here
    }
}