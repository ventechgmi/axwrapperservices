﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 12/17/2014
'Description: Invoice related properties
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
namespace WrapperService.BusinessEntities.Interfaces
{
     public interface IDefaultDimension
    {
        /// <summary>
        /// Dimension name
        /// </summary>
          string Name { get; set; }
        /// <summary>
        /// value
        /// </summary>
          string Value { get; set; } 
    }
}