﻿/******************************************************************************************
'Created By: ANANTHAKUMAR S
'Created Date: 12/03/2014
'Description:
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace WrapperService.BusinessEntities.Interfaces
{
    public interface IValidationDetail
    {
        //This is to store the resultant id out of insertion
        Int32 Id { get; set; }
        bool Result { get; set; }
        //This would contain business validations
        List<string> ErrorList { get; set; }
        Int32 ErrorType { get; set; }
        string Number { get; set; }
    }
}
