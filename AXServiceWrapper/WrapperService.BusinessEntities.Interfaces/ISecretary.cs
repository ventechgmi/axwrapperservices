﻿/******************************************************************************************
'Created By: Ananthakumar Subramani
'Created Date: 12/03/2014
'Description: Fiscal Officeer interface
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrapperService.BusinessEntities.Interfaces
{
    public interface ISecretary
    {
        /// <summary>
        ///  Account ID
        /// </summary>
        string AccountID { get; set; }
        /// <summary>
        ///  First Name
        /// </summary>
        string FirstName { get; set; }
        /// <summary>
        ///   Last name
        /// </summary>
        string LastName { get; set; }
        /// <summary>
        ///   Middle name
        /// </summary>
        string MiddleName { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Fiscal officer Suffix
        /// </summary>
        string Suffix { get; set; }

        /// <summary>
        /// Type is either Secretary or Fiscal
        /// </summary>
          string Type { get; set; }

    }
}