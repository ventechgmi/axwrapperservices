﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 12/17/2014
'Description: Invoice related properties
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
namespace WrapperService.BusinessEntities.Interfaces
{
    public interface IInvoiceLine
    {
        /// <summary>
        /// invoice line description
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Invoice amount
        /// </summary>
        decimal? Amount { get; set; }

        /// <summary>
        /// Invoice main account
        /// </summary>
        string MainAccount { get; set; }
        /// <summary>
        /// Invoice ARCode
        /// </summary>
        string ARCode { get; set; }

        /// <summary>
        /// Transaction
        /// </summary>
        int? ARTrans { get; set; }

        /// <summary>
        /// INvoice comments
        /// </summary>
        string InvoiceComments { get; set; }

        // Adding two fields for unit price,quantity - Starts here

        /// <summary>
        /// Total Amount
        /// </summary>
        decimal? UnitPrice { get; set; }

        /// <summary>
        /// Quantity
        /// </summary>
        int Quantity { get; set; }
        // Adding two fields for unit price,quantity - ends here

        //Added billable id for cancellation
        /// <summary>
        /// billable id for cancellation
        /// </summary>
        string VEN_BillableItemId { get; set; }
    }
}