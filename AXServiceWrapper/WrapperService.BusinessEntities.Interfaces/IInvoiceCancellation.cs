﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 04/07/2015
'Description: To cancell invoices
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
namespace WrapperService.BusinessEntities.Interfaces
{
    public interface IInvoiceCancellation
    {
        /// <summary>
        /// Billable Item ID
        /// </summary>
        string BillableItemID { get; set; }

        /// <summary>
        /// Cancellation Date
        /// </summary>
        DateTime CancellationDate { get; set; }

        /// <summary>
        /// Cancellation Id
        /// </summary>
        string CancellationID { get; set; }

        /// <summary>
        /// Reason for cancellation
        /// </summary>
        string Reason { get; set; }

    }
}