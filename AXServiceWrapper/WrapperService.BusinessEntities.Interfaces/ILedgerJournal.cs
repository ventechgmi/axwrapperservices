﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 12/19/2014
'Description:  Ledger Journal properties
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

namespace WrapperService.BusinessEntities.Interfaces
{
    public interface ILedgerJournal
    {
        /// <summary>
        /// Journal Name from COnfig file
        /// </summary>
          string JournalName { get; set; }

        /// <summary>
        /// Description 
        /// </summary>
          string Description { get; set; }
    }
}
