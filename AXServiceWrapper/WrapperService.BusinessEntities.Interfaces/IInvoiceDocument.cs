﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 01/06/2014
'Description: Invoice document
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
namespace WrapperService.BusinessEntities.Interfaces
{
    public interface IInvoiceDocument
    {
        /// <summary>
        /// Invoice ID
        /// </summary>
          string InvoiceID { get; set; }
        /// <summary>
        /// FileName
        /// </summary>
          string FileName { get; set; }
        ///// <summary>
        ///// File Extension
        ///// </summary>
        //  string FileExtension { get; set; }

        /// <summary>
        /// Server Path
        /// </summary>
          string FileServerPath { get; set; }
        ///// <summary>
        ///// Size of the file
        ///// </summary>
        //  Int32 FileSize { get; set; }

        ///// <summary>
        //  /// File Contents Base 64
        ///// </summary>
        //  string FileContentsBase64 { get; set; }
        /// <summary>
        /// Total Bytes
        /// </summary>
          byte[] FileContentsByte { get; set; }
    }
}