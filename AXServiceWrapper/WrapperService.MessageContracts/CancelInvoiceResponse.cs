﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 04/07/2015
'Description: cancellation  invoice response
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.ServiceModel;
using WrapperService.DataContracts;
using WCF = System.Runtime.Serialization;
namespace WrapperService.MessageContracts
{
    [MessageContract()]
    public class CancelInvoiceResponse
    {
        [MessageBodyMember()]
        public ValidationDetail ValidationDetail { get; set; }
    }
}


 