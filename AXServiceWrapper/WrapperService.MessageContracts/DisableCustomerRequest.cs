﻿/******************************************************************************************
'Created By: ANANTHAKUMAR S
'Created Date: 12/03/2014
'Description:
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using System.ServiceModel;
using WCF = System.Runtime.Serialization;
namespace WrapperService.MessageContracts
{
    [MessageContract()]
    public class DisableCustomerRequest
    {
        [MessageBodyMember()]
        public List<string> AccountNumbers
        {
            get;   set;
        }
       // public Int32 ModifiedBy { get; set; }
    }
}


 