﻿/******************************************************************************************
'Created By: ANANTHAKUMAR S
'Created Date: 12/19/2014
'Description:
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.ServiceModel;
using WrapperService.DataContracts;
using WCF = System.Runtime.Serialization;
namespace WrapperService.MessageContracts
{
    [MessageContract()]
    public class CreatePaymentResponse
    {
        [MessageBodyMember()]
        public ValidationDetail ValidationDetail { get; set; }
    }
}


 