﻿/******************************************************************************************
'Created By: ANANTHAKUMAR S
'Created Date: 12/17/2014
'Description:
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using System.ServiceModel;
using WrapperService.DataContracts;
using WCF = System.Runtime.Serialization;
namespace WrapperService.MessageContracts
{
    [MessageContract()]
    public class GetInvoiceDetailsResponse
    {
         
        [MessageBodyMember()]
        public List<CustomerInvoiceListDetails> InvoiceDetailsList { get; set; }
    }
}


 