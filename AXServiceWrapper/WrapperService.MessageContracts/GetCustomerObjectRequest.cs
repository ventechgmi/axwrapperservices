﻿/******************************************************************************************
'Created By: ANANTHAKUMAR S
'Created Date: 01/02/2014
'Description:
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using System.ServiceModel;
using WCF = System.Runtime.Serialization;
namespace WrapperService.MessageContracts
{
    [MessageContract()]
    public class GetCustomerObjectRequest
    {
        [MessageBodyMember()]
        public List<string> AccountNumbersList
        {
            get;
            set;
        }
    }
}


