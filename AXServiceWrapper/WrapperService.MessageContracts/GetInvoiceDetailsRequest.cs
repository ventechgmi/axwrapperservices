﻿/******************************************************************************************
'Created By: ANANTHAKUMAR S
'Created Date: 12/31/2014
'Description:
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using System.ServiceModel;
using WCF = System.Runtime.Serialization;
namespace WrapperService.MessageContracts
{
    [MessageContract()]
    public class GetInvoiceDetailsRequest
    {
        [MessageBodyMember()]
        public List<string> BillabaleItemIDList
        {
            get;
            set;
        }
        //[MessageBodyMember()]
        //public string Status
        //{
        //    get;
        //    set;
        //}
    }
}


