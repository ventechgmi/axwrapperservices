﻿/******************************************************************************************
'Created By: ANANTHAKUMAR S
'Created Date: 09/17/2015
'Description:
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.ServiceModel;
using WrapperService.DataContracts;
using WCF = System.Runtime.Serialization;
namespace WrapperService.MessageContracts
{
    [MessageContract()]
    public class IsVendorExistsResponse
    {
        [MessageBodyMember()]
        public ValidationDetail ValidationDetail { get; set; }
    }
}


 