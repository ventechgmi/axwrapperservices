﻿/******************************************************************************************
'Created By: ANANTHAKUMAR S
'Created Date: 01/26/2014
'Description:
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using System.ServiceModel;
using WrapperService.DataContracts;
using WCF = System.Runtime.Serialization;
namespace WrapperService.MessageContracts
{
    [MessageContract()]
    public class GetCustomerObjectResponse
    {
        [MessageBodyMember()]
        public Customer Customer { get; set; }

        [MessageBodyMember()]
        public bool Result { get; set; }
    }
}


 