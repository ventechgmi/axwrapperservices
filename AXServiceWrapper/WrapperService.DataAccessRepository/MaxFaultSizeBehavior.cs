﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 12/17/2014
'Description: 
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using log4net;
using log4net.Config;
using WrapperService.BusinessEntities;
using WrapperService.DataAccessRepository.Interfaces;
using WrapperService.DataAccessRepository.InvoiceServiceReference;
using SystemSvcModel = System.ServiceModel;
// Uses to access the AXD tables.

//[assembly: XmlConfigurator(Watch = true)] // uses to track the actions into the logfile / DB based on the config settings

namespace WrapperService.DataAccessRepository
{
    public class MaxFaultSizeBehavior : IEndpointBehavior
    {
        private readonly int _size;

        public MaxFaultSizeBehavior(int size)
        {
            _size = size;
        }

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MaxFaultSize = _size;
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }
    }

}