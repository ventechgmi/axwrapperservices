﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/03/2014
'Description: 
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using log4net;
using log4net.Config;
using WrapperService.BusinessEntities;
using WrapperService.DataAccessRepository.CustomerQueryServiceReference;
using WrapperService.DataAccessRepository.Interfaces;
using WrapperService.DataAccessRepository.CustomerServiceReference;
using WrapperService.DataAccessRepository.InitialPremiumService;
using AifFault = WrapperService.DataAccessRepository.CustomerServiceReference.AifFault;
using CallContext = WrapperService.DataAccessRepository.CustomerServiceReference.CallContext;

// Uses to access the AXD tables.
//using WrapperService.DataAccessRepository.InvoiceQueryServiceReference;

//[assembly: XmlConfigurator(Watch = true)] // uses to track the actions into the logfile / DB based on the config settings

namespace WrapperService.DataAccessRepository
{
    public class CustomerRepository : ICustomerRepository
    {
        //Uses to tracking the actions 
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        const string Business = "Business";
        const string language = "en-us";
        //const string language = "";

        const string USA = "USA";
        //private const string ServiceUserid = "dynamic@VentechSolutions.com";
        //private const string ServicePassword = "welcome@123";

        private ICustomerRepository _customerRepository;
        public CustomerRepository(ICustomerRepository iCustomerRepository)
        {
            XmlConfigurator.Configure();
            _customerRepository = iCustomerRepository;
        }
        public CustomerRepository()
        {
            XmlConfigurator.Configure();
        }
        #region "General insert/update/delete methods"

        /// <summary>
        /// Check new customer
        /// </summary>
        /// <param name="accountNumner"></param>
        /// <returns></returns>
        public ValidationDetail IsExistingCustomer(string accountNumner)
        {
            log.Info("Enter IsExistingCustomer(" + accountNumner + ")");
            ValidationDetail validationDetail = new ValidationDetail();
            try
            {
                // just pass the "AccountNum" field and value to rad customer
                AxdCustomer axdCustomer = ReadCustomer("AccountNum", accountNumner);

                if (axdCustomer != null)
                {
                    //Check whether if the customer is exists or not.
                    if (axdCustomer.CustTable != null) // New Customer
                    {
                        log.Info("Enter IsExistingCustomer(" + accountNumner + ") :" + " Yes");
                        validationDetail.Result = true;
                        return validationDetail; // If the customer is exists then return it
                    }
                }
                log.Info("Exit IsExistingCustomer(" + accountNumner + ") :" + "No, New Customer");
                log.Debug("Exit IsExistingCustomer(" + accountNumner + ") :" + "No, New Customer");
                validationDetail.Result = false;
            }
            catch (Exception ex)
            {
                log.Error("Exit IsExistingCustomer(" + accountNumner + ") :" + ex.Message.ToString());
                validationDetail.Result = false;
                validationDetail.ErrorList = new List<string> { ex.Message.ToString() };
            }
            return validationDetail;
            //call AX - ends here
        }

        // Read Customer 
        private AxdCustomer ReadCustomer(string fieldName, string value)
        {
            log.Info("Enter ReadCustomer");
            AxdCustomer retAxdCustomer = null;
            //try
            {
                EntityKey[] customerKeys = null;
                //You can look-up N customers in AX. In this case I'm fetching one customer, but you can fill the array with as many customer's keys as you want.
                customerKeys = new EntityKey[1];
                customerKeys[0] = new EntityKey();

                //Definig the Key field (Primary Key on Customer table) and it's value to be searched inside AX
                customerKeys[0].KeyData = new KeyField[1] { CreateKeyField(fieldName, value) };
                log.Info("ReadCustomer before client object intiates starts");

                //Commented by Ananthakumar - implemented Call context
                CustomerServiceClient client = new CustomerServiceClient();
                try
                {
                    // implemented credentials starts
                    string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                    string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));


                    if (client.ChannelFactory.Credentials != null)
                    {
                        client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
                        client.ChannelFactory.Credentials.UserName.Password = servicePassword;
                        log.Info("ReadCustomer() entered into credentials");
                    }
                    // implemented credentials ends

                    log.Debug("ReadCustomer: Before read from client object");
                    log.Debug("ReadCustomer: client.InnerChannel.State :" + client.InnerChannel.State.ToString());

                    if (client.InnerChannel.State != CommunicationState.Faulted)
                    {
                        // retAxdCustomer = client.read(new CallContext { Company = "SFM", LogonAsUser = "vsusa\dynamic" }, customerKeys);
                        retAxdCustomer = client.read(null, customerKeys);
                        client.Close();
                    }
                    else
                    {
                        // channel faulted - re-create your client and then try again
                        log.Debug("ReadCustomer: else client.InnerChannel.State : " + client.InnerChannel.State.ToString());
                    }
                    log.Debug("ReadCustomer: after read from client object");
                }
                catch (TimeoutException exception)
                {
                    log.Error(string.Format("Error ReadCustomer(): {0}", exception.Message));
                    log.Error("-------------------------------------------------------------------------------------------");
                    client.Abort();
                }
                catch (CommunicationException exception)
                {
                    log.Error(string.Format("Error ReadCustomer(): {0}", exception.Message));
                    log.Error("-------------------------------------------------------------------------------------------");
                    client.Abort();
                }
                catch (Exception ex)
                {
                    log.Error(string.Format("Error: ReadCustomer() : {0}", ex.Message));
                    log.Error("-------------------------------------------------------------------------------------------");
                    client.Abort();
                }
                // log.Info("ReadCustomer before client object intiates ends");

                log.Info("Exit ReadCustomer");

            }
            //catch (Exception ex)
            //{

            //}
            return retAxdCustomer;
        }

        /// <summary>
        /// This creates the Keys used to fetch records from AX. For the customers, you pass the field "AccountNum" and the Customer ID
        /// </summary>
        private KeyField CreateKeyField(string fieldName, string value)
        {
            KeyField KeyField = new KeyField();

            KeyField.Field = fieldName;
            KeyField.Value = value;
            return KeyField;
        }

        /// <summary>
        /// Create the EntityKeys used to indicate which customers will be updated on the update call. You can add N customers in the key
        /// </summary>
        private EntityKey[] CreateEntityKeys(string fieldName, string accountNum)
        {
            EntityKey[] keys = new EntityKey[1];
            keys[0] = new EntityKey();
            keys[0].KeyData = new KeyField[1];

            keys[0].KeyData = new KeyField[1] { CreateKeyField(fieldName, accountNum) };

            return keys;
        }

        /// <summary>
        /// Insert a customer , call AX server to fetch
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public ValidationDetail InsertCustomer(Customer customer)
        {
            //XmlConfigurator.Configure();
            // Validationdetail object is used to return final output from this method, 
            ValidationDetail validationDetail = new ValidationDetail();
            List<string> errorList = new List<string>();
            try
            {
                log.Info("Entering InsertCustomer()");

                //call AX - starts here
                errorList = CreateCustomer(customer);

                if (errorList.Any())
                {
                    validationDetail.ErrorList = errorList.ToList();
                    validationDetail.Result = false;
                }
                else
                {
                    validationDetail.Result = true;
                    //if customer is participating VFFD then create intial premium - starts
                    if (customer.IsParticipating)
                    {
                        string result = CreateIntialPremium(customer.AccountNumber, customer.PropertyValuation); // Added PropertyValuation in Create initial premium 
                        if (result == "success")
                            validationDetail.Result = true;
                        else
                        {
                            validationDetail.ErrorList = errorList.ToList();
                            validationDetail.Result = false;
                        }
                    }
                    // //if customer is participating VFFD then create intial premium - ends
                }
                //call AX - ends here

                log.Info("Exit InsertCustomer()");
            }
            catch (Exception ex)
            {
                log.Error("Exit InsertCustomer:" + ex.Message.ToString());
                validationDetail.Result = false;
                validationDetail.ErrorList = new List<string> { ex.Message.ToString() };
            }
            return validationDetail;
        }

        /// <summary>
        /// Update customer
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public ValidationDetail UpdateCustomer(Customer customer)
        {
            log.Info("Entering into UpdateCustomer().");
            ValidationDetail validationDetail = new ValidationDetail();
            List<string> errorList = new List<string>();
            try
            {
                // To-Do Update customer logic is here
                errorList = UpdateCustomerinAx(customer);

                if (errorList.Any())
                {
                    validationDetail.ErrorList = errorList.ToList();
                    validationDetail.Result = false;
                }
                else
                {
                    validationDetail.Result = true;

                    //if customer is participating VFFD then create intial premium - starts
                    //if (customer.IsParticipating)
                    //{
                    //    string result = CreateIntialPremium(customer.AccountNumber);
                    //    if (result == "success")
                    //        validationDetail.Result = true;
                    //    else
                    //    {
                    //        validationDetail.ErrorList = errorList.ToList();
                    //        validationDetail.Result = false;
                    //    }
                    //}
                    // //if customer is participating VFFD then create intial premium - ends
                }
                //call AX - ends here

                log.Info("Exit UpdateCustomer().");
            }
            catch (Exception ex)
            {
                log.Error("Exit UpdateCustomer:" + ex.Message.ToString());
                validationDetail.Result = false;
                validationDetail.ErrorList = new List<string> { ex.Message.ToString() };
            }
            return validationDetail;
        }


        /// Disable Customer - to call Block Customer method and Changing tne status alone...AxdEntity_CustTable.Blocked = AxdExtType_CustBlocked.All
        ///This will block the client inside AX to any transaction.
        ///To unblock it, set the field to = AxdExtType_CustBlocked.No
        public ValidationDetail DisableCustomer(string accountNumber)
        {
            log.Info("Enter DisableCustomer()");
            var validationDetail = new ValidationDetail();
            List<string> errorList = new List<string>();
            try
            {
                // To-Do Update customer logic is here

                errorList = BlockCustomer(accountNumber);

                //if (errorList != null)
                if (errorList.Any())
                {
                    validationDetail.ErrorList = errorList.ToList();
                    validationDetail.Result = false;
                }
                else
                {
                    validationDetail.Result = true;
                }
                //call AX - ends here

                log.Info("Exit DisableCustomer()");
            }
            catch (Exception ex)
            {
                log.Error("Exit DisableCustomer(" + accountNumber + "):" + ex.Message.ToString());
                validationDetail.Result = false;
                validationDetail.ErrorList = new List<string> { ex.Message.ToString() };
            }
            return validationDetail;
        }

        ///// Disable Customer - to call Block Customer method and Changing tne status alone...AxdEntity_CustTable.Blocked = AxdExtType_CustBlocked.All
        /////This will block the client inside AX to any transaction.
        /////To unblock it, set the field to = AxdExtType_CustBlocked.No
        //public ValidationDetail disableCustomerPermanantly(string accountNumber)
        //{
        //    log.Info("Entering into disableCustomer().");
        //    ValidationDetail validationDetail = new ValidationDetail();
        //    List<string> errorList = new List<string>();

        //    // To-Do Update customer logic is here

        //    errorList = CustomerPermanentDelete(accountNumber);

        //    //if (errorList != null)
        //    if (errorList.Any())
        //    {
        //        validationDetail.ErrorList = errorList.ToList();
        //        validationDetail.Result = false;
        //    }
        //    else
        //    {
        //        validationDetail.Result = true;
        //    }
        //    //call AX - ends here

        //    //  log.Info("Exit into disableCustomer().");
        //    return validationDetail;
        //}

        /// <summary>
        /// Make the customer status to inactive.
        /// </summary>
        /// <returns></returns>
        private List<string> BlockCustomer(string accountNumber)
        {
            log.Info("Enter BlockCustomer()");
            var retList = new List<string>();
            AxdCustomer axdCustomers = null;
            // log.Info("Enter BlockCustomer()");
            try
            {
                axdCustomers = ReadCustomer("AccountNum", accountNumber);

                if (axdCustomers != null && axdCustomers.CustTable != null)
                {
                    if (axdCustomers.CustTable.Length > 0)
                    {
                        axdCustomers.CustTable[0].Blocked = AxdExtType_CustBlocked.All;
                        axdCustomers.CustTable[0].BlockedSpecified = true;
                        axdCustomers.CustTable[0].action = AxdEnum_AxdEntityAction.update;
                        axdCustomers.CustTable[0].actionSpecified = true;

                        axdCustomers.CustTable[0].DirParty = null;

                        var client = new CustomerServiceClient();
                        try
                        {
                            // implemented credentials starts
                            // implemented credentials starts
                            string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                            string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));


                            if (client.ChannelFactory.Credentials != null)
                            {
                                client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
                                client.ChannelFactory.Credentials.UserName.Password = servicePassword;
                                log.Info("ReadCustomer() entered into credentials");
                            }
                            // implemented credentials ends

                            log.Debug("Before call ax service BlockCustomer()");
                            client.update(null, CreateEntityKeys("AccountNum", accountNumber), axdCustomers);
                            client.Close();
                            log.Debug("After call ax service BlockCustomer()");

                        }
                        catch (TimeoutException exception)
                        {
                            log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                            log.Error("-------------------------------------------------------------------------------------------");
                            client.Abort();
                        }
                        catch (CommunicationException exception)
                        {
                            log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                            log.Error("-------------------------------------------------------------------------------------------");
                            client.Abort();
                        }
                        catch (Exception exception)
                        {
                            log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                            log.Error("-------------------------------------------------------------------------------------------");
                            client.Abort();
                        }
                    }
                    else
                        retList.Add("No Customer data retrieved");
                }
                else
                    retList.Add("No Customer data retrieved");
            }
            catch (FaultException<AifFault> aifExc)
            {
                // retList.Add(string.Join(System.Environment.NewLine, aifExc.Detail.InfologMessageList.Select(i => i.Message)));

                //1  retList.Add(string.Join(Environment.NewLine, aifExc.Detail.InfologMessageList[0].Message.ToString()));
                // log.Info("BlockCustomer(): " + aifExc.Detail.InfologMessageList[0].Message.ToString());
                retList.Add(string.Join(Environment.NewLine, aifExc.Message));
                log.Error("ERROR from AX: BlockCustomer(): " + string.Join(Environment.NewLine, aifExc.Message));
            }
            catch (Exception e)
            {
                log.Error("ERROR from AX: BlockCustomer(): " + e.Message.ToString());
                retList.Add(e.Message);

                while (e.InnerException != null)
                {
                    e = e.InnerException;
                    retList.Add(e.Message);
                }
            }
            log.Info("Exit BlockCustomer()");

            return retList;
        }

        /// <summary>
        /// Make the customer status to inactive.
        /// </summary>
        /// <returns></returns>
        private List<string> CustomerPermanentDelete(string accountNumber)
        {
            List<string> retList = new List<string>();
            AxdCustomer axdCustomers = null;
            log.Info("Enter CustomerPermanentDelete()");
            CustomerServiceClient client = new CustomerServiceClient();

            try
            {
                axdCustomers = ReadCustomer("AccountNum", accountNumber);

                if (axdCustomers != null && axdCustomers.CustTable != null)
                {
                    if (axdCustomers.CustTable.Length > 0)
                    {
                        axdCustomers.CustTable[0].Blocked = AxdExtType_CustBlocked.All;
                        axdCustomers.CustTable[0].BlockedSpecified = true;
                        //axdCustomers.CustTable[0].action = AxdEnum_AxdEntityAction.delete;
                        //axdCustomers.CustTable[0].actionSpecified = true;
                        axdCustomers.CustTable[0].DirParty = null;


                        {
                            // implemented credentials starts
                            // implemented credentials starts
                            string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                            string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

                            if (client.ChannelFactory.Credentials != null)
                            {
                                client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
                                client.ChannelFactory.Credentials.UserName.Password = servicePassword;
                                log.Info("ReadCustomer() entered into credentials");
                            }
                            // implemented credentials ends

                            log.Info("Before call ax service BlockCustomer()");
                            client.update(null, CreateEntityKeys("AccountNum", accountNumber), axdCustomers); client.Close();
                            log.Info("After call ax service BlockCustomer()");
                        }
                    }
                    else
                    {
                        retList.Add("No Customer data retrieved");
                    }
                }
                else
                {
                    retList.Add("No Customer data retrieved");
                }
            }
            catch (FaultException<AifFault> aifExc)
            {
                // retList.Add(string.Join(System.Environment.NewLine, aifExc.Detail.InfologMessageList.Select(i => i.Message)));

                //  retList.Add(string.Join(Environment.NewLine, aifExc.Detail.InfologMessageList[0].Message.ToString()));
                //   log.Info("CustomerPermanentDelete(): " + aifExc.Detail.InfologMessageList[0].Message.ToString());

                retList.Add(string.Join(Environment.NewLine, aifExc.Message));
                log.Error("ERROR from AX: CustomerPermanentDelete(): " + aifExc.Message);
                client.Abort();
            }

            catch (TimeoutException exception)
            {
                log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }
            catch (CommunicationException exception)
            {
                log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }

            catch (Exception e)
            {
                log.Error("ERROR from AX: CustomerPermanentDelete(): " + e.Message.ToString());
                retList.Add(e.Message);
                client.Abort();
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                    retList.Add(e.Message);
                }
            }
            return retList;
        }

        #endregion "General insert/update/delete methods"

        #region "Creating Customer"

        /// <summary>
        /// Customer creation
        /// </summary>
        /// <returns></returns>
        List<string> CreateCustomer(Customer customer)
        {
            log.Info("CreateCustomer()");

            //we get the key elements 
            List<string> retVal = new List<string>();

            #region Objects for Creation
            AxdCustomer axdCustomer = new AxdCustomer(); //Only one per call. You can use the same for successive calls.
            List<AxdEntity_CustTable> axdEntityCustTable = new List<AxdEntity_CustTable>(); //List of customers to create and/or update
            #endregion

            axdEntityCustTable.Add(SetCustomerTable(AxdEnum_AxdEntityAction.create, customer, true));
            axdCustomer.CustTable = axdEntityCustTable.ToArray();

            // axdCustomer.CustTable[0].AS400_ID = customer.AS400ID;

            axdCustomer.CustTable[0].Blocked = (customer.IsDisable == true) ? AxdExtType_CustBlocked.All : AxdExtType_CustBlocked.No;
            axdCustomer.CustTable[0].BlockedSpecified = true;
            //Setting System Document properties
            axdCustomer.ValidTimeStateType = AxdEnum_AxdValidTimeStateType.AsOf;
            axdCustomer.ValidTimeStateTypeSpecified = true;

            CustomerServiceClient client = new CustomerServiceClient();
            try
            {
                log.Info("CreateCustomer(): before create call");
                // implemented credentials starts
                // implemented credentials starts
                string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

                if (client.ChannelFactory.Credentials != null)
                {
                    client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
                    client.ChannelFactory.Credentials.UserName.Password = servicePassword;
                    log.Info("ReadCustomer() entered into credentials");
                }
                // implemented credentials ends

                client.create(null, axdCustomer); client.Close();
                log.Info("CreateCustomer(): after create call");

            }
            catch (FaultException<AifFault> aifFault)
            {
                //1retVal.Add(string.Join(Environment.NewLine, aifFault.Detail.InfologMessageList.Select(i => i.Message)));
                retVal.Add(string.Join(Environment.NewLine, aifFault.Message));
                log.Error("ERROR from AX: CreateCustomer(): " + aifFault.Message);
                client.Abort();
            }
            catch (TimeoutException exception)
            {
                log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                retVal.Add("ERROR from AX: " + exception.Message);
                log.Error("-------------------------------------------------------------------------------------------");
                retVal.Add(exception.Message);
                client.Abort();
            }
            catch (CommunicationException exception)
            {
                log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                retVal.Add("ERROR from AX: " + exception.Message);
                log.Error("-------------------------------------------------------------------------------------------");
                retVal.Add(exception.Message);
                client.Abort();
            }
            catch (Exception exception)
            {
                log.Error("ERROR from AX: CreateCustomer(): Error: " + exception.Message);
                retVal.Add("ERROR from AX: " + exception.Message);
                client.Abort();
                //while (exception.InnerException != null)
                //{
                //    exception = exception.InnerException;
                //    retVal.Add(exception.Message);
                //}
            }

            return retVal;
        }

        /// <summary>
        /// Set neccessary AX fields and wrapper service fields to AX temop customer object.
        /// </summary>
        /// <returns></returns>
        private List<string> UpdateCustomerinAx(Customer customer)
        {
            List<string> retList = new List<string>();
            AxdCustomer axdCustomer = new AxdCustomer();
            log.Info("Enter UpdateCustomerinAx()");
            CustomerServiceClient client = new CustomerServiceClient();
            try
            {
                // get customer data from AX by passing customer account number 
                axdCustomer = ReadCustomer("AccountNum", customer.AccountNumber);

                // if customer table is not empty
                if (axdCustomer.CustTable != null)
                {
                    // if customer table has data
                    if (axdCustomer.CustTable.Length > 0)
                    {
                        axdCustomer.CustTable[0] = PrepareCustomerUpdate(axdCustomer.CustTable[0], customer);
                        axdCustomer.CustTable[0].action = AxdEnum_AxdEntityAction.update;
                        axdCustomer.CustTable[0].actionSpecified = true;

                        axdCustomer.CustTable[0].Blocked = customer.IsDisable ? AxdExtType_CustBlocked.All : AxdExtType_CustBlocked.No;
                        axdCustomer.CustTable[0].BlockedSpecified = true;
                        // implemented credentials starts
                        string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                        string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

                        if (client.ChannelFactory.Credentials != null)
                        {
                            client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
                            client.ChannelFactory.Credentials.UserName.Password = servicePassword;
                            log.Info("ReadCustomer() entered into credentials");
                        }
                        // implemented credentials ends
                        client.update(null, CreateEntityKeys("AccountNum", customer.AccountNumber), axdCustomer);
                        client.Close();
                    }
                    else
                    {
                        retList.Add("Customer table is null for this account number");
                    }
                    //retList = axdCustomers.CustTable.Select(c => c.AccountNum).ToList();
                }
            }
            catch (FaultException<AifFault> aifExc)
            {
                //1 string error = string.Join(Environment.NewLine, aifExc.Detail.InfologMessageList.Select(i => i.Message));
                string error = string.Join(Environment.NewLine, aifExc.Message);
                retList.Add(error);
                log.Error("ERROR from AX: UpdateCustomerinAX()" + error);
                client.Abort();
            }
            catch (TimeoutException exception)
            {
                log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                retList.Add("ERROR from AX: " + exception.Message);
                log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }
            catch (CommunicationException exception)
            {
                log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                retList.Add("ERROR from AX: " + exception.Message);
                log.Error("-------------------------------------------------------------------------------------------");
                retList.Add(exception.Message);
                client.Abort();
            }

            catch (Exception e)
            {
                string error = e.Message.ToString();

                retList.Add(error);
                // Looping inner exception
                while (e.InnerException != null)
                {
                    string innerException = e.InnerException.Message.ToString();
                    e = e.InnerException;
                    retList.Add(innerException);

                    log.Debug("Error UpdateCustomerinAX()" + innerException);

                }
                log.Error("ERROR from AX: UpdateCustomerinAX()  " + e.Message);
            }
            client.Abort();
            log.Info("Exit UpdateCustomerinAX()");
            return retList;
        }

        /// <summary>
        /// intial Premium for VFFD customer
        /// </summary>
        /// <param name="accountNummber"></param>
        private string CreateIntialPremium(string accountNummber, decimal? PropertyValuation)
        {
            string result = "success";
            log.Info(string.Format("Enter CreateIntialPremium for accountNummber {0}", accountNummber));
            VEN_AssessmentServiceClient client = new VEN_AssessmentServiceClient();
            try
            {
                // implemented credentials starts
                string serviceUserid =
                    EncryptandDecrypt.GeneralFunctions.Decrypt(
                        Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                string servicePassword =
                    EncryptandDecrypt.GeneralFunctions.Decrypt(
                        Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

                if (client.ChannelFactory.Credentials != null)
                {
                    client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
                    client.ChannelFactory.Credentials.UserName.Password = servicePassword;

                }
                // implemented credentials ends

                log.Debug(string.Format("CreateIntialPremium: client.InnerChannel.State : {0} for accountNummber {1}",
                    accountNummber, client.InnerChannel.State));

                if (client.InnerChannel.State != CommunicationState.Faulted)
                {

                    VEN_AssessmentInitialPremiumContract contract = new VEN_AssessmentInitialPremiumContract();
                    contract.parmaccountNum = accountNummber;
                    contract.parmpropertyValue = Convert.ToDecimal(PropertyValuation);

                    contract.parmmainAccount = Convert.ToString(ConfigurationManager.AppSettings["PaymentMainAccount"]);
                    client.createInitialAssessment(null, contract);

                    client.Close();
                }
                else
                {
                    // channel faulted - re-create your client and then try again
                    log.Debug("CreateIntialPremium: else client.InnerChannel.State : " +
                              client.InnerChannel.State.ToString());
                }
                log.Debug("CreateIntialPremium: after read from client object");
            }
            catch (TimeoutException ex)
            {
                result = string.Format("Error: CreateIntialPremium() : {0}", ex.Message);
                log.Error(result);
                log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }
            catch (CommunicationException ex)
            {
                result = string.Format("Error: CreateIntialPremium() : {0}", ex.Message);
                log.Error(result);
                log.Error("-------------------------------------------------------------------------------------------");
                result = string.Format("Error: CreateIntialPremium() : {0}", ex.Message);
                client.Abort();
            }
            catch (Exception ex)
            {
                result = string.Format("Error: CreateIntialPremium() : {0}", ex.Message);
                log.Error(result);
                log.Error("-------------------------------------------------------------------------------------------");
                result = string.Format("Error: CreateIntialPremium() : {0}", ex.Message);
                client.Abort();
            }
            // log.Info("ReadCustomer before client object intiates ends");

            log.Info("Exit CreateIntialPremium");
            return result;
        }

        /// <summary>
        /// Create Customer Header information
        /// </summary>
        private AxdEntity_CustTable SetCustomerTable(AxdEnum_AxdEntityAction action, Customer customer, bool isCustomerCreate)
        {
            log.Info("Enter SetCustomerTable() ");

            AxdEntity_CustTable retCustTable = new AxdEntity_CustTable();
            //Check either create or update or other actions 
            if (action != AxdEnum_AxdEntityAction.create)
            {
                retCustTable.action = action;
                retCustTable.actionSpecified = true;
            }
            //  retCustTable.actionSpecified = true;

            retCustTable.AccountNum = customer.AccountNumber; //Assign Customer ID
            retCustTable.Currency = customer.Currency; //Assign Currency here
            //    retCustTable.CustGroup = customer.Group; //"Assign group name here
            retCustTable.CustGroup = (customer.CustomerType == "Organization") ? "FD" : customer.Group; //"Assign group name here
            retCustTable.AS400_ID = customer.AS400ID;
            //    retCustTable.VFFDPropertyValuation = 0;
            //  retCustTable.VFFDPropertyValuationSpecified = true;
            retCustTable.CustClassificationId = "No";


            retCustTable.FiscalOfficerAccountID = string.Empty;
            retCustTable.FiscalOfficerFirstName = string.Empty;
            retCustTable.FiscalOfficerLastName = string.Empty;
            retCustTable.FiscalOfficerMiddleName = string.Empty;
            retCustTable.FiscalOfficerName = string.Empty;
            retCustTable.FiscalOfficerSuffix = string.Empty;

            retCustTable.SecretaryAccountID = string.Empty;
            retCustTable.SecretaryFirstName = string.Empty;
            retCustTable.SecretaryLastName = string.Empty;
            retCustTable.SecretaryMiddleName = string.Empty;
            retCustTable.SecretaryName = string.Empty;
            retCustTable.SecretarySuffix = string.Empty;

            if (customer.CustomerType == "Organization" && customer.IsParticipating)
            {
                retCustTable.VFFDPropertyValuation = customer.PropertyValuation;
                retCustTable.VFFDPropertyValuationSpecified = true;

                if (customer.Secretary != null)
                {
                    retCustTable.SecretaryAccountID = customer.Secretary.AccountID;
                    retCustTable.SecretaryFirstName = customer.Secretary.FirstName;
                    retCustTable.SecretaryLastName = customer.Secretary.LastName;
                    retCustTable.SecretaryMiddleName = customer.Secretary.MiddleName;
                    retCustTable.SecretaryName = customer.Secretary.Name;
                    retCustTable.SecretarySuffix = customer.Secretary.Suffix;
                    retCustTable.CustClassificationId = "Yes";

                }

                if (customer.FiscalOfficer != null)
                {
                    retCustTable.FiscalOfficerAccountID = customer.FiscalOfficer.AccountID;
                    retCustTable.FiscalOfficerFirstName = customer.FiscalOfficer.FirstName;
                    retCustTable.FiscalOfficerLastName = customer.FiscalOfficer.LastName;
                    retCustTable.FiscalOfficerMiddleName = customer.FiscalOfficer.MiddleName;
                    retCustTable.FiscalOfficerName = customer.FiscalOfficer.Name;
                    retCustTable.FiscalOfficerSuffix = customer.FiscalOfficer.Suffix;
                    retCustTable.CustClassificationId = "Yes";

                }


            }

            if (customer.CustomerType == "Person") //   person 
            {
                retCustTable.DirParty = new AxdEntity_DirParty_DirPartyTable[] { CreateDirPerson(AxdEnum_AxdEntityAction.create, customer, isCustomerCreate) };
            }
            else //if (customer.CustomerType == "Organization") // botjh org   and Grant
            {
                retCustTable.DirParty = new AxdEntity_DirParty_DirPartyTable[] { CreateDirOrganization(AxdEnum_AxdEntityAction.create, customer, isCustomerCreate) };

            }

            log.Info("Exit SetCustomerTable() ");

            return retCustTable;
        }

        /// <summary>
        /// DirPerson represents Personal information of CUSTOMERS OF TYPE PERSON ONLY.
        /// </summary>
        private AxdEntity_DirParty_DirPerson CreateDirPerson(AxdEnum_AxdEntityAction action, Customer customer, bool _isCustomerCreate)
        {
            log.Info("Enter CreateDirPerson() ");

            AxdEntity_DirParty_DirPerson retPerson = new AxdEntity_DirParty_DirPerson();
            // Will review when update heppens - once it's ready - 
            if (action != AxdEnum_AxdEntityAction.create)
            {
                //This property says what to do with this information. If you use it as update, you need reference data from the original record INSIDE AX.
                retPerson.action = action; //This is the default value. Dont need to set it if you are going to create records.

            }

            if (!_isCustomerCreate)
                retPerson.actionSpecified = true; //Same rule applies to all Data Objects in terms of the Action property.
            retPerson.NameAlias = customer.NameAlias;

            retPerson.PersonSuffix = customer.Suffix; // added by ananthakumar - Feb-23-2015
            // if  name is less >20 then get 20 chars only
            if (customer.NameAlias != null)
                if (customer.NameAlias.Length > 20)
                    retPerson.NameAlias = customer.NameAlias.Substring(0, 20);
            //Comment - No need
            //retPerson.NameSequence = customer.NameSequence;
            retPerson.PersonName = new AxdEntity_PersonName[1] { SetPersonName(action, customer, _isCustomerCreate) }; //There can be only one instance of Person Name

            retPerson.LanguageId = language;

            // retPerson.DirPartyPostalAddressView = new AxdEntity_DirPartyPostalAddressView[] { CreateAddress(_action, _Customer).ToArray() };
            retPerson.DirPartyPostalAddressView = CreateAddress(customer).ToArray();


            //Adding contact info
            List<AxdEntity_DirPartyContactInfoView> axdEntityDirPartyContactInfoViewList = new List<AxdEntity_DirPartyContactInfoView>();

            foreach (var eachContact in customer.ContactList)
            {
                AxdEntity_DirPartyContactInfoView contactInfoView = new AxdEntity_DirPartyContactInfoView();

                contactInfoView = CreateContactInfo(AxdEnum_AxdEntityAction.create, eachContact);
                axdEntityDirPartyContactInfoViewList.Add(contactInfoView);
            }
            retPerson.DirPartyContactInfoView = axdEntityDirPartyContactInfoViewList.ToArray();
            log.Info("Exit CreateDirPerson() ");

            return retPerson;
        }

        /// <summary>
        /// DirPerson represents Personal information of CUSTOMERS OF TYPE PERSON ONLY.
        /// </summary>
        private string CreateVFFDContact(Customer customer)
        {
            log.Debug("Enter CreateVFFDContact() ");

            string message = string.Empty;
            ContactPersonsServiceClient Contactsclient = new ContactPersonsServiceClient();
            //CallContext context = new CallContext();
            //context.Company = "SFM";

            AxdContactPersons contactPersons = new AxdContactPersons();

            AxdEntity_ContactPerson[] ContactPersonList = new AxdEntity_ContactPerson[1];

            AxdEntity_ContactPerson ContactPerson = new AxdEntity_ContactPerson();

            ContactPerson.ContactForPartyCustAccount = customer.AccountNumber;

            AxdEntity_Person_DirPerson DirPerson = new AxdEntity_Person_DirPerson();

            AxdEntity_PersonName1 personName = new AxdEntity_PersonName1();
            personName.FirstName = customer.FiscalOfficer.FirstName;
            personName.MiddleName = customer.FiscalOfficer.MiddleName;
            personName.LastName = customer.FiscalOfficer.LastName;

            DirPerson.PersonName = new AxdEntity_PersonName1[1] { personName };


            DirPerson.PersonTitle = customer.FiscalOfficer.Type;

            ContactPerson.Person = new AxdEntity_Person_DirPerson[1] { DirPerson };

            AxdEntity_PostalAddressView PostalAddressView = new AxdEntity_PostalAddressView();

            if (customer.FundBillingAddress != null)
            {
                PostalAddressView.LocationName = customer.FundBillingAddress.LocationName;
                PostalAddressView.Street = customer.FundBillingAddress.Street + " " + customer.FundBillingAddress.Street2;
                PostalAddressView.City = customer.FundBillingAddress.City;
                PostalAddressView.State = customer.FundBillingAddress.State;
                PostalAddressView.CountryRegionId = customer.FundBillingAddress.CountryRegionID;
                PostalAddressView.ZipCode = customer.FundBillingAddress.ZipCode;
                PostalAddressView.Roles = customer.FundBillingAddress.Roles;
                PostalAddressView.IsPrimary = (customer.FundBillingAddress.IsPrimaryAddress) ? AxdExtType_LogisticsIsPrimaryAddress.Yes : AxdExtType_LogisticsIsPrimaryAddress.No;
                PostalAddressView.IsPrimarySpecified = true;
            }

            ContactPerson.Person[0].PostalAddressView = new AxdEntity_PostalAddressView[1] { PostalAddressView };

            // ContactPerson.Person[0].ContactInfoView = new AxdEntity_ContactInfoView[2] { email, cellPhone };

            ContactPersonList[0] = ContactPerson;

            contactPersons.ContactPerson = ContactPersonList;

            try
            {
                EntityKey[] a;//= new EntityKey[]();
                // a = Contactsclient.create(context, contactPersons);
                a = Contactsclient.create(null, contactPersons);
                message = "success";
            }
            catch (Exception e)
            {
                message = e.Message;
                log.Error(string.Format("ERROR from AX: UpdateCustomerinAX() {0} ", message));
            }
            log.Debug("Exit CreateVFFDContact() ");
            return message;
        }
        /// <summary>
        /// Create Any Postal address through this instance
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="_isPrimaryAddress"></param>
        private List<AxdEntity_DirPartyPostalAddressView> CreateAddress(Customer customer, AxdExtType_LogisticsIsPrimaryAddress _isPrimaryAddress = AxdExtType_LogisticsIsPrimaryAddress.Yes)
        {

            log.Info("Enter CreateAddress()  Account Number : " + customer.AccountNumber);

            //Create a postal address
            List<AxdEntity_DirPartyPostalAddressView> AddressList = new List<AxdEntity_DirPartyPostalAddressView>();
            //Check each address
            foreach (var eachAddress in customer.AddressList)
            {
                log.Info("CreateAddress() : Foreach  in  Account Number : " + customer.AccountNumber);

                AxdEntity_DirPartyPostalAddressView address = new AxdEntity_DirPartyPostalAddressView();

                //if (_action != AxdEnum_AxdEntityAction.create)
                //{
                //    address.action = _action;
                //    address.actionSpecified = true; : 
                //}

                address.IsPrimary = _isPrimaryAddress;
                address.IsPrimarySpecified = eachAddress.IsPrimaryAddress;
                address.LocationName = eachAddress.LocationName;
                address.Street = eachAddress.Street;
                //address.StreetNumber = eachAddress.Street2;
                address.City = eachAddress.City;
                address.State = eachAddress.State;
                address.CountryRegionId = eachAddress.CountryRegionID;
                address.ZipCode = FormatZipCode(eachAddress.ZipCode);
                address.Roles = eachAddress.Roles;
                //Add each address into address list
                AddressList.Add(address);
            }
            //return address list 
            log.Info("Exit CreateAddress() Account Number : " + customer.AccountNumber);

            return AddressList;
        }

        private string FormatZipCode(string ZipCode)
        {
            if (ZipCode.Length == 9)
                ZipCode = ZipCode.Substring(0, 5) + "-" + ZipCode.Substring(5);
            return (ZipCode);
        }
        private AxdEntity_DirParty_DirOrganization CreateDirOrganization(AxdEnum_AxdEntityAction _action, Customer customer, bool _isCustomerCreate)
        {
            AxdEntity_DirParty_DirOrganization retOrganization = new AxdEntity_DirParty_DirOrganization();

            if (_action != AxdEnum_AxdEntityAction.create)
            {
                //This property says what to do with this information. If you use it as update, you need reference data from the original record INSIDE AX.
                retOrganization.action = _action; //This is the default value. Dont need to set it if you are going to create records.
            }
            if (!_isCustomerCreate)
                retOrganization.actionSpecified = true; //Same rule applies to all Data Objects in terms of the Action property.                

            retOrganization.Name = customer.OrganizationName;
            retOrganization.OrganizationName = new AxdEntity_OrganizationName[1] { this.CreateDirOrganizationName(_action, customer.OrganizationName, _isCustomerCreate) };
            retOrganization.LanguageId = language;

            //In this case, I'm creating ONE postal address. But you can create as many as you want.
            // retOrganization.DirPartyPostalAddressView = new AxdEntity_DirPartyPostalAddressView[1] { this.crea(_action) };

            retOrganization.DirPartyPostalAddressView = CreateAddress(customer).ToArray();
            //2 contact info
            //retOrganization.DirPartyContactInfoView = new AxdEntity_DirPartyContactInfoView[2] { this.createDirContactInfo(_action, AxdEnum_LogisticsElectronicAddressMethodType.Email, "gbagno@madeup.com", AxdEnum_NoYes.Yes),
            //                                                                               this.createDirContactInfo(_action, AxdEnum_LogisticsElectronicAddressMethodType.Phone, "2259874696", AxdEnum_NoYes.Yes) };

            //Adding contact info
            List<AxdEntity_DirPartyContactInfoView> axdEntityDirPartyContactInfoViewList = new List<AxdEntity_DirPartyContactInfoView>();

            foreach (var eachContact in customer.ContactList)
            {
                AxdEntity_DirPartyContactInfoView contactInfoView = new AxdEntity_DirPartyContactInfoView();

                contactInfoView = CreateContactInfo(AxdEnum_AxdEntityAction.create, eachContact);
                axdEntityDirPartyContactInfoViewList.Add(contactInfoView);
            }
            retOrganization.DirPartyContactInfoView = axdEntityDirPartyContactInfoViewList.ToArray();

            return retOrganization;
        }
        /// <summary>
        /// Creates Names for Organizations
        /// </summary>
        /// <param name="_action"></param>
        /// <returns></returns>
        private AxdEntity_OrganizationName CreateDirOrganizationName(AxdEnum_AxdEntityAction _action, string OrganizationName, bool _isCustomerCreate)
        {
            AxdEntity_OrganizationName retOrganizationName = new AxdEntity_OrganizationName();

            if (_action != AxdEnum_AxdEntityAction.create)
            {
                retOrganizationName.action = _action;
                retOrganizationName.updateMode = AxdEnum_ValidTimeStateUpdate.CreateNewTimePeriod;
                retOrganizationName.updateModeSpecified = true;
            }
            if (!_isCustomerCreate)
                retOrganizationName.actionSpecified = true;

            retOrganizationName.Name = OrganizationName;

            return retOrganizationName;
        }

        /// <summary>
        /// Update address with rec id
        /// </summary>
        /// <param name="axdEntityDirPartyDirPerson"></param>
        /// <param name="customer"></param>
        /// <returns></returns>
        private List<AxdEntity_DirPartyPostalAddressView> UpdateAddress(AxdEntity_DirParty_DirPerson axdEntityDirPartyDirPerson, Customer customer)
        {
            log.Info("Enter UpdateAddress()  Account Number : " + customer.AccountNumber);

            //local variable uses to store the customer address details - once it is identified as create or update then put the action and store it in this object.
            List<AxdEntity_DirPartyPostalAddressView> addressList = new List<AxdEntity_DirPartyPostalAddressView>();

            // Local object uses to loop customer addresses
            List<AxdEntity_DirPartyPostalAddressView> addressListFromAxService = new List<AxdEntity_DirPartyPostalAddressView>();
            if (axdEntityDirPartyDirPerson != null)
            {
                //Checking if existing address there are not, if there just all addresses to our local variable
                if (axdEntityDirPartyDirPerson.DirPartyPostalAddressView != null)
                {
                    addressListFromAxService.AddRange(axdEntityDirPartyDirPerson.DirPartyPostalAddressView);

                    #region "Update Address"

                    if (customer.AddressList.Count() == 0)
                    {
                        AxdEntity_DirPartyPostalAddressView addressTemp = new AxdEntity_DirPartyPostalAddressView();
                        log.Info("Enter UpdateAddress(): No address from ACADIS and AX has " + addressListFromAxService.Count() + " address. So updating empty details in the address since we cannot delete shared address in AX.");

                        var query = from customerAddress in addressListFromAxService
                                    select customerAddress;

                        var enumerable = query.ToList();
                        int count = enumerable.Count;
                        #region "if only one address iin ax "
                        if (count == 1) // if count is 1 then update the record
                        {

                            addressTemp.action = AxdEnum_AxdEntityAction.update;
                            addressTemp.actionSpecified = true;

                            addressTemp.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                            addressTemp.updateModeSpecified = true;

                            addressTemp.RecId = enumerable[0].RecId;
                            addressTemp.RecIdSpecified = true;
                            addressTemp.RecVersion = enumerable[0].RecVersion;
                            addressTemp.RecVersionSpecified = true;

                            addressTemp.LocationName = string.Empty;
                            addressTemp.Street = string.Empty;
                            addressTemp.City = string.Empty;
                            addressTemp.State = string.Empty;
                            string coutry = (enumerable[0].CountryRegionId != null) ? enumerable[0].CountryRegionId : USA;

                            addressTemp.CountryRegionId = coutry;
                            addressTemp.ZipCode = string.Empty; ;

                            string roles = (enumerable[0].Roles != null) ? enumerable[0].Roles : Business;
                            addressTemp.Roles = roles;

                            //Add each address into address list
                            addressList.Add(addressTemp);
                            log.Info("Exit UpdateAddress()");
                            return addressList;
                        }
                        #endregion "if only one address in ax "
                        else
                        {
                            #region "if only one address in ax "

                            var querySelecttemp = from customerAddress in addressListFromAxService
                                                  where customerAddress.IsPrimary == AxdExtType_LogisticsIsPrimaryAddress.Yes
                                                  select customerAddress;

                            var enumerable1 = querySelecttemp.ToList();

                            #region "if No primary address select first and update with empty record"
                            if (enumerable1.Count == 0)
                            {
                                #region "take first address to update "

                                query = (from customerAddress in addressListFromAxService
                                         select customerAddress).Take(1);

                                if (query.Count() == 1) // if count is 1 then update the record
                                {

                                    addressTemp.action = AxdEnum_AxdEntityAction.update;
                                    addressTemp.actionSpecified = true;

                                    addressTemp.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                                    addressTemp.updateModeSpecified = true;

                                    addressTemp.RecId = enumerable[0].RecId;
                                    addressTemp.RecIdSpecified = true;
                                    addressTemp.RecVersion = enumerable[0].RecVersion;
                                    addressTemp.RecVersionSpecified = true;

                                    addressTemp.LocationName = string.Empty;
                                    addressTemp.Street = string.Empty;
                                    addressTemp.City = string.Empty;
                                    addressTemp.State = string.Empty;
                                    string coutry = (enumerable[0].CountryRegionId != null) ? enumerable[0].CountryRegionId : USA;

                                    addressTemp.CountryRegionId = coutry;
                                    addressTemp.ZipCode = string.Empty; ;

                                    string roles = (enumerable[0].Roles != null) ? enumerable[0].Roles : Business;
                                    addressTemp.Roles = roles;

                                    //Add each address into address list
                                    addressList.Add(addressTemp);

                                }
                                #endregion "take first address to update "

                                #region "skip first address and delete other address "
                                foreach (var eachPostalAddress in addressListFromAxService.Skip(1))
                                {
                                    AxdEntity_DirPartyPostalAddressView address = new AxdEntity_DirPartyPostalAddressView();

                                    log.Info("Enter UpdateAddress(): For each address");
                                    address.action = AxdEnum_AxdEntityAction.delete;
                                    address.actionSpecified = true;
                                    address.RecId = eachPostalAddress.RecId;
                                    address.RecIdSpecified = true;
                                    address.RecVersion = eachPostalAddress.RecVersion;
                                    address.RecVersionSpecified = true;

                                    //Added the address in the address list
                                    addressList.Add(address);
                                    log.Info("Exit UpdateAddress()");
                                    return addressList;
                                }
                                #endregion "skip first address and delete other address "
                            }
                            #endregion "if No primary address select first and update with empty record"
                            #region "if primary address found"
                            else
                            {
                                #region "if  primary address select - update address"

                                addressTemp.action = AxdEnum_AxdEntityAction.update;
                                addressTemp.actionSpecified = true;

                                addressTemp.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                                addressTemp.updateModeSpecified = true;

                                addressTemp.RecId = enumerable1[0].RecId;
                                addressTemp.RecIdSpecified = true;
                                addressTemp.RecVersion = enumerable1[0].RecVersion;
                                addressTemp.RecVersionSpecified = true;

                                addressTemp.LocationName = string.Empty;
                                addressTemp.Street = string.Empty;
                                addressTemp.City = string.Empty;
                                addressTemp.State = string.Empty;
                                string coutry = (enumerable1[0].CountryRegionId != null) ? enumerable1[0].CountryRegionId : USA;

                                addressTemp.CountryRegionId = coutry;
                                addressTemp.ZipCode = string.Empty; ;

                                string roles = (enumerable1[0].Roles != null) ? enumerable1[0].Roles : Business;
                                addressTemp.Roles = roles;

                                //Add each address into address list
                                addressList.Add(addressTemp);
                                #endregion "if  primary address select - update address"

                                #region "if Update other records"

                                var queryUnmatchedRecords = (from eachaddressListFromAxService in addressListFromAxService
                                                             join eachAddressList in querySelecttemp.ToList() on eachaddressListFromAxService.LocationName equals
                                                                 eachAddressList.LocationName
                                                                 into combinedResult
                                                             from f in combinedResult.DefaultIfEmpty()

                                                             select new
                                                             {
                                                                 IsPrimary = eachaddressListFromAxService.IsPrimary,
                                                                 LocationName = eachaddressListFromAxService.LocationName,
                                                                 RecId = eachaddressListFromAxService.RecId,
                                                                 RecVersion = eachaddressListFromAxService.RecVersion,
                                                                 MissingProd = (f == null ? string.Empty : f.LocationName)

                                                             }).Where(a => a.MissingProd == string.Empty);

                                var unmatachedAddress = queryUnmatchedRecords.ToList();


                                foreach (var eachPostalAddress in unmatachedAddress.ToList())
                                {
                                    AxdEntity_DirPartyPostalAddressView address = new AxdEntity_DirPartyPostalAddressView();

                                    log.Info("Enter UpdateAddress(): For each address");
                                    address.action = AxdEnum_AxdEntityAction.delete;
                                    address.actionSpecified = true;
                                    address.RecId = eachPostalAddress.RecId;
                                    address.RecIdSpecified = true;
                                    address.RecVersion = eachPostalAddress.RecVersion;
                                    address.RecVersionSpecified = true;

                                    //Added the address in the address list
                                    addressList.Add(address);
                                }
                                log.Info("Exit UpdateAddress()");
                                return addressList;
                                #endregion "if Update other records"
                            }
                            #endregion "if primary address found"

                            #endregion "if only one address in ax "
                        }

                    }
                    #endregion "Update address"
                }
                #region "unmatached Address"

                //var queryUnmatchedRecords = (from eachaddressListFromAxService in addressListFromAxService
                //                             join eachAddressList in customer.AddressList on eachaddressListFromAxService.LocationName equals
                //                                 eachAddressList.LocationName
                //                                 into combinedResult
                //                             from f in combinedResult.DefaultIfEmpty()

                //                             select new
                //                             {
                //                                 IsPrimary = eachaddressListFromAxService.IsPrimary,
                //                                 LocationName = eachaddressListFromAxService.LocationName,
                //                                 RecId = eachaddressListFromAxService.RecId,
                //                                 RecVersion = eachaddressListFromAxService.RecVersion,
                //                                 MissingProd = (f == null ? string.Empty : f.LocationName)

                //                             }).Where(a => a.MissingProd == string.Empty);

                //var unmatachedAddress = queryUnmatchedRecords.ToList();


                //foreach (var eachPostalAddress in unmatachedAddress.ToList())
                //{
                //    AxdEntity_DirPartyPostalAddressView address = new AxdEntity_DirPartyPostalAddressView();

                //    log.Info("Enter UpdateAddress(): For each address");
                //    address.action = AxdEnum_AxdEntityAction.delete;
                //    address.actionSpecified = true;

                //    //address.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                //    //address.updateModeSpecified = true;

                //    //
                //    address.RecId = eachPostalAddress.RecId;
                //    address.RecIdSpecified = true;
                //    address.RecVersion = eachPostalAddress.RecVersion;
                //    address.RecVersionSpecified = true;

                //    //Added the address in the address list
                //    addressList.Add(address);
                //}

                #endregion "unmatached Address"

                #region " Looping Customer addresses"
                // Looping all addresses

                ////To-Do Use ACADIS-ID to update the info.
                var querySelect = from customerAddress in addressListFromAxService
                                  orderby customerAddress.IsPrimary descending
                                  select customerAddress;

                var enumerableQuery = querySelect.ToList();
                int i = 0;
                int addresscount = enumerableQuery.Count();

                foreach (var eachPostalAddress in customer.AddressList)
                {
                    AxdEntity_DirPartyPostalAddressView address = new AxdEntity_DirPartyPostalAddressView();


                    if (i < addresscount) // if count is 1 then update the record
                    {

                        if (enumerableQuery[i].RecId == null)
                        {
                            #region "Create address"

                            log.Info("Enter Create addres: For each address");

                            address.action = AxdEnum_AxdEntityAction.create;
                            address.actionSpecified = true;

                            //address.RecId = enumerable[0].RecId;
                            //address.RecIdSpecified = true;
                            //address.RecVersion = enumerable[0].RecVersion;
                            //address.RecVersionSpecified = true;

                            #endregion "Create address"
                        }
                        else
                        {
                            #region "Update Address"

                            log.Info("Enter UpdateAddress(): For each address");
                            address.action = AxdEnum_AxdEntityAction.update;
                            address.actionSpecified = true;

                            address.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                            address.updateModeSpecified = true;

                            address.RecId = enumerableQuery[i].RecId;
                            address.RecIdSpecified = true;
                            address.RecVersion = enumerableQuery[i].RecVersion;
                            address.RecVersionSpecified = true;

                            #endregion "Update address"
                        }
                    }
                    else
                    {
                        #region "Create address"

                        log.Info("Enter Create addres: For each address");

                        address.action = AxdEnum_AxdEntityAction.create;
                        address.actionSpecified = true;

                        #endregion "Create address"
                    }
                    i++;
                    #region "Address data"

                    // Checking is it is logistics primary address or not
                    AxdExtType_LogisticsIsPrimaryAddress isPrimary = (eachPostalAddress.IsPrimaryAddress)
                        ? AxdExtType_LogisticsIsPrimaryAddress.Yes
                        : AxdExtType_LogisticsIsPrimaryAddress.No;
                    address.IsPrimary = isPrimary;
                    address.IsPrimarySpecified = true;

                    address.LocationName = eachPostalAddress.LocationName;
                    address.Street = eachPostalAddress.Street;
                    address.City = eachPostalAddress.City;
                    address.State = eachPostalAddress.State;
                    address.CountryRegionId = eachPostalAddress.CountryRegionID;
                    address.ZipCode = FormatZipCode(eachPostalAddress.ZipCode);
                    address.Roles = eachPostalAddress.Roles;

                    //Add each address into address list
                    addressList.Add(address);

                    #endregion " address"
                    log.Info("Exit UpdateAddress()");
                }
                #endregion " Loo[ping Customer addresses"

                if (i <= addresscount)
                {
                    #region "unmatached Address"

                    var queryUnmatchedRecords = (from eachaddressListFromAxService in addressListFromAxService
                                                 orderby eachaddressListFromAxService.IsPrimary descending
                                                 select eachaddressListFromAxService).Skip(i);

                    var unmatachedAddress = queryUnmatchedRecords.ToList();


                    foreach (var eachPostalAddress in unmatachedAddress.ToList())
                    {
                        AxdEntity_DirPartyPostalAddressView address = new AxdEntity_DirPartyPostalAddressView();

                        log.Info("Enter UpdateAddress(): For each address");
                        address.action = AxdEnum_AxdEntityAction.delete;
                        address.actionSpecified = true;

                        //address.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                        //address.updateModeSpecified = true;

                        //
                        address.RecId = eachPostalAddress.RecId;
                        address.RecIdSpecified = true;
                        address.RecVersion = eachPostalAddress.RecVersion;
                        address.RecVersionSpecified = true;

                        //Added the address in the address list
                        addressList.Add(address);
                    }

                    #endregion "unmatached Address"
                }
            }
            log.Info("Exit UpdateAddress()  Account Number : " + customer.AccountNumber);
            return addressList;

        }

        //private List<AxdEntity_DirPartyPostalAddressView> UpdateAddress(AxdEntity_DirParty_DirPerson axdEntityDirPartyDirPerson, Customer customer)
        //{
        //    log.Info("Enter UpdateAddress()  Account Number : " + customer.AccountNumber);

        //    //local variable uses to store the customer address details - once it is identified as create or update then put the action and store it in this object.
        //    List<AxdEntity_DirPartyPostalAddressView> addressList = new List<AxdEntity_DirPartyPostalAddressView>();
        //    // Local object uses to loop customer addresses
        //    List<AxdEntity_DirPartyPostalAddressView> addressListFromAxService = new List<AxdEntity_DirPartyPostalAddressView>();
        //    if (axdEntityDirPartyDirPerson != null)
        //    {
        //        //Checking if existing address there are not, if there just all addresses to our local variable
        //        if (axdEntityDirPartyDirPerson.DirPartyPostalAddressView != null)
        //            addressListFromAxService.AddRange(axdEntityDirPartyDirPerson.DirPartyPostalAddressView);
        //        #region "unmatached Address"

        //        var queryUnmatchedRecords = (from eachaddressListFromAxService in addressListFromAxService
        //                                     join eachAddressList in customer.AddressList on eachaddressListFromAxService.LocationName equals
        //                                         eachAddressList.LocationName
        //                                         into combinedResult
        //                                     from f in combinedResult.DefaultIfEmpty()

        //                                     select new
        //                                     {
        //                                         IsPrimary = eachaddressListFromAxService.IsPrimary,
        //                                         LocationName = eachaddressListFromAxService.LocationName,
        //                                         RecId = eachaddressListFromAxService.RecId,
        //                                         RecVersion = eachaddressListFromAxService.RecVersion,
        //                                         MissingProd = (f == null ? string.Empty : f.LocationName)

        //                                     }).Where(a => a.MissingProd == string.Empty);

        //        var unmatachedAddress = queryUnmatchedRecords.ToList();


        //        foreach (var eachPostalAddress in unmatachedAddress.ToList())
        //        {
        //            AxdEntity_DirPartyPostalAddressView address = new AxdEntity_DirPartyPostalAddressView();

        //            log.Info("Enter UpdateAddress(): For each address");
        //            address.action = AxdEnum_AxdEntityAction.delete;
        //            address.actionSpecified = true;

        //            //address.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
        //            //address.updateModeSpecified = true;

        //            //
        //            address.RecId = eachPostalAddress.RecId;
        //            address.RecIdSpecified = true;
        //            address.RecVersion = eachPostalAddress.RecVersion;
        //            address.RecVersionSpecified = true;

        //            //Added the address in the address list
        //            addressList.Add(address);
        //        }

        //        #endregion "unmatached Address"

        //        #region " Looping Customer addresses"
        //        // Looping all addresses
        //        foreach (var eachPostalAddress in customer.AddressList)
        //        {
        //            AxdEntity_DirPartyPostalAddressView address = new AxdEntity_DirPartyPostalAddressView();

        //            //To-Do Use ACADIS-ID to update the info.
        //            var query = from customerAddress in addressListFromAxService
        //                        where eachPostalAddress != null && eachPostalAddress.LocationName == customerAddress.LocationName
        //                        select customerAddress;

        //            var enumerable = query.ToList();
        //            if (enumerable.Count() == 1) // if count is 1 then update the record
        //            {

        //                if (enumerable[0].RecId == null)
        //                {
        //                    #region "Create address"

        //                    log.Info("Enter Create addres: For each address");

        //                    address.action = AxdEnum_AxdEntityAction.create;
        //                    address.actionSpecified = true;

        //                    //address.RecId = enumerable[0].RecId;
        //                    //address.RecIdSpecified = true;
        //                    //address.RecVersion = enumerable[0].RecVersion;
        //                    //address.RecVersionSpecified = true;

        //                    #endregion "Create address"
        //                }
        //                else
        //                {
        //                    #region "Update Address"

        //                    log.Info("Enter UpdateAddress(): For each address");
        //                    address.action = AxdEnum_AxdEntityAction.update;
        //                    address.actionSpecified = true;

        //                    address.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
        //                    address.updateModeSpecified = true;

        //                    address.RecId = enumerable[0].RecId;
        //                    address.RecIdSpecified = true;
        //                    address.RecVersion = enumerable[0].RecVersion;
        //                    address.RecVersionSpecified = true;

        //                    #endregion "Update address"
        //                }
        //            }
        //            else
        //            {
        //                #region "Create address"

        //                log.Info("Enter Create addres: For each address");

        //                address.action = AxdEnum_AxdEntityAction.create;
        //                address.actionSpecified = true;

        //                #endregion "Create address"
        //            }
        //            #region "Address data"

        //            // Checking is it is logistics primary address or not
        //            AxdExtType_LogisticsIsPrimaryAddress isPrimary = (eachPostalAddress.IsPrimaryAddress)
        //                ? AxdExtType_LogisticsIsPrimaryAddress.Yes
        //                : AxdExtType_LogisticsIsPrimaryAddress.No;
        //            address.IsPrimary = isPrimary;
        //            address.IsPrimarySpecified = true;

        //            address.LocationName = eachPostalAddress.LocationName;
        //            address.Street = eachPostalAddress.Street;
        //            address.City = eachPostalAddress.City;
        //            address.State = eachPostalAddress.State;
        //            address.CountryRegionId = eachPostalAddress.CountryRegionID;
        //            address.ZipCode = eachPostalAddress.ZipCode;
        //            address.Roles = eachPostalAddress.Roles;
        //            //Add each address into address list
        //            addressList.Add(address);

        //            #endregion " address"
        //            log.Info("Exit UpdateAddress()");
        //        }
        //        #endregion " Loo[ping Customer addresses"
        //    }
        //    log.Info("Exit UpdateAddress()  Account Number : " + customer.AccountNumber);
        //    return addressList;

        //}



        /// <summary>
        /// Update address with rec id
        /// </summary>
        /// <param name="axdEntityDirPartyDirOrganization"></param>
        /// <param name="customer"></param>
        /// <returns></returns>
        private List<AxdEntity_DirPartyPostalAddressView> UpdateAddressOrganization(AxdEntity_DirParty_DirOrganization axdEntityDirPartyDirOrganization, Customer customer)
        {
            log.Info("Enter UpdateAddressOrganization()  Account Number : " + customer.AccountNumber);

            //local variable uses to store the customer address details - once it is identified as create or update then put the action and store it in this object.
            List<AxdEntity_DirPartyPostalAddressView> addressList = new List<AxdEntity_DirPartyPostalAddressView>();

            // Local object uses to loop customer addresses
            List<AxdEntity_DirPartyPostalAddressView> addressListFromAxService = new List<AxdEntity_DirPartyPostalAddressView>();
            if (axdEntityDirPartyDirOrganization != null)
            {
                //Checking if existing address there are not, if there just all addresses to our local variable
                if (axdEntityDirPartyDirOrganization.DirPartyPostalAddressView != null)
                {
                    addressListFromAxService.AddRange(axdEntityDirPartyDirOrganization.DirPartyPostalAddressView);

                    #region "Update Address"

                    if (customer.AddressList.Count() == 0)
                    {
                        AxdEntity_DirPartyPostalAddressView addressTemp = new AxdEntity_DirPartyPostalAddressView();
                        log.Info("Enter UpdateAddressOrganization(): No address from ACADIS and AX has " + addressListFromAxService.Count() + " address. So updating empty detaiols in the address since we cannot delete shared address in AX.");

                        var query = from customerAddress in addressListFromAxService
                                    select customerAddress;

                        var enumerable = query.ToList();
                        int count = enumerable.Count;
                        #region "if only one address iin ax "
                        if (count == 1) // if count is 1 then update the record
                        {

                            addressTemp.action = AxdEnum_AxdEntityAction.update;
                            addressTemp.actionSpecified = true;

                            addressTemp.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                            addressTemp.updateModeSpecified = true;

                            addressTemp.RecId = enumerable[0].RecId;
                            addressTemp.RecIdSpecified = true;
                            addressTemp.RecVersion = enumerable[0].RecVersion;
                            addressTemp.RecVersionSpecified = true;

                            addressTemp.LocationName = string.Empty;
                            addressTemp.Street = string.Empty;
                            addressTemp.City = string.Empty;
                            addressTemp.State = string.Empty;
                            string coutry = (enumerable[0].CountryRegionId != null) ? enumerable[0].CountryRegionId : USA;

                            addressTemp.CountryRegionId = coutry;
                            addressTemp.ZipCode = string.Empty; ;

                            string roles = (enumerable[0].Roles != null) ? enumerable[0].Roles : Business;
                            addressTemp.Roles = roles;

                            //Add each address into address list
                            addressList.Add(addressTemp);
                            log.Info("Exit UpdateAddressOrganization()");
                            return addressList;
                        }
                        #endregion "if only one address in ax "
                        else
                        {
                            #region "if only one address in ax "

                            var querySelecttemp = from customerAddress in addressListFromAxService
                                                  where customerAddress.IsPrimary == AxdExtType_LogisticsIsPrimaryAddress.Yes
                                                  select customerAddress;

                            var enumerable1 = querySelecttemp.ToList();

                            #region "if No primary address select first and update with empty record"
                            if (enumerable1.Count == 0)
                            {
                                #region "take first address to update "

                                query = (from customerAddress in addressListFromAxService
                                         select customerAddress).Take(1);

                                if (query.Count() == 1) // if count is 1 then update the record
                                {

                                    addressTemp.action = AxdEnum_AxdEntityAction.update;
                                    addressTemp.actionSpecified = true;

                                    addressTemp.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                                    addressTemp.updateModeSpecified = true;

                                    addressTemp.RecId = enumerable[0].RecId;
                                    addressTemp.RecIdSpecified = true;
                                    addressTemp.RecVersion = enumerable[0].RecVersion;
                                    addressTemp.RecVersionSpecified = true;

                                    addressTemp.LocationName = string.Empty;
                                    addressTemp.Street = string.Empty;
                                    addressTemp.City = string.Empty;
                                    addressTemp.State = string.Empty;
                                    string coutry = (enumerable[0].CountryRegionId != null) ? enumerable[0].CountryRegionId : USA;

                                    addressTemp.CountryRegionId = coutry;
                                    addressTemp.ZipCode = string.Empty; ;

                                    string roles = (enumerable[0].Roles != null) ? enumerable[0].Roles : Business;
                                    addressTemp.Roles = roles;

                                    //Add each address into address list
                                    addressList.Add(addressTemp);

                                }
                                #endregion "take first address to update "

                                #region "skip first address and delete other address "
                                foreach (var eachPostalAddress in addressListFromAxService.Skip(1))
                                {
                                    AxdEntity_DirPartyPostalAddressView address = new AxdEntity_DirPartyPostalAddressView();

                                    log.Info("Enter UpdateAddressOrganization(): For each address");
                                    address.action = AxdEnum_AxdEntityAction.delete;
                                    address.actionSpecified = true;
                                    address.RecId = eachPostalAddress.RecId;
                                    address.RecIdSpecified = true;
                                    address.RecVersion = eachPostalAddress.RecVersion;
                                    address.RecVersionSpecified = true;

                                    //Added the address in the address list
                                    addressList.Add(address);
                                    log.Info("Exit UpdateAddressOrganization()");
                                    return addressList;
                                }
                                #endregion "skip first address and delete other address "
                            }
                            #endregion "if No primary address select first and update with empty record"
                            #region "if primary address found"
                            else
                            {
                                #region "if  primary address select - update address"

                                addressTemp.action = AxdEnum_AxdEntityAction.update;
                                addressTemp.actionSpecified = true;

                                addressTemp.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                                addressTemp.updateModeSpecified = true;

                                addressTemp.RecId = enumerable1[0].RecId;
                                addressTemp.RecIdSpecified = true;
                                addressTemp.RecVersion = enumerable1[0].RecVersion;
                                addressTemp.RecVersionSpecified = true;

                                addressTemp.LocationName = string.Empty;
                                addressTemp.Street = string.Empty;
                                addressTemp.City = string.Empty;
                                addressTemp.State = string.Empty;
                                string coutry = (enumerable1[0].CountryRegionId != null) ? enumerable1[0].CountryRegionId : USA;

                                addressTemp.CountryRegionId = coutry;
                                addressTemp.ZipCode = string.Empty; ;

                                string roles = (enumerable1[0].Roles != null) ? enumerable1[0].Roles : Business;
                                addressTemp.Roles = roles;

                                //Add each address into address list
                                addressList.Add(addressTemp);
                                #endregion "if  primary address select - update address"

                                #region "if Update other records"

                                var queryUnmatchedRecords = (from eachaddressListFromAxService in addressListFromAxService
                                                             join eachAddressList in querySelecttemp.ToList() on eachaddressListFromAxService.LocationName equals
                                                                 eachAddressList.LocationName
                                                                 into combinedResult
                                                             from f in combinedResult.DefaultIfEmpty()

                                                             select new
                                                             {
                                                                 IsPrimary = eachaddressListFromAxService.IsPrimary,
                                                                 LocationName = eachaddressListFromAxService.LocationName,
                                                                 RecId = eachaddressListFromAxService.RecId,
                                                                 RecVersion = eachaddressListFromAxService.RecVersion,
                                                                 MissingProd = (f == null ? string.Empty : f.LocationName)

                                                             }).Where(a => a.MissingProd == string.Empty);

                                var unmatachedAddress = queryUnmatchedRecords.ToList();


                                foreach (var eachPostalAddress in unmatachedAddress.ToList())
                                {
                                    AxdEntity_DirPartyPostalAddressView address = new AxdEntity_DirPartyPostalAddressView();

                                    log.Info("Enter UpdateAddressOrganization(): For each address");
                                    address.action = AxdEnum_AxdEntityAction.delete;
                                    address.actionSpecified = true;
                                    address.RecId = eachPostalAddress.RecId;
                                    address.RecIdSpecified = true;
                                    address.RecVersion = eachPostalAddress.RecVersion;
                                    address.RecVersionSpecified = true;

                                    //Added the address in the address list
                                    addressList.Add(address);
                                }
                                log.Info("Exit UpdateAddressOrganization()");
                                return addressList;
                                #endregion "if Update other records"
                            }
                            #endregion "if primary address found"

                            #endregion "if only one address in ax "
                        }

                    }
                    #endregion "Update address"
                }
                #region "unmatached Address"

                //var queryUnmatchedRecords = (from eachaddressListFromAxService in addressListFromAxService
                //                             join eachAddressList in customer.AddressList on eachaddressListFromAxService.LocationName equals
                //                                 eachAddressList.LocationName
                //                                 into combinedResult
                //                             from f in combinedResult.DefaultIfEmpty()

                //                             select new
                //                             {
                //                                 IsPrimary = eachaddressListFromAxService.IsPrimary,
                //                                 LocationName = eachaddressListFromAxService.LocationName,
                //                                 RecId = eachaddressListFromAxService.RecId,
                //                                 RecVersion = eachaddressListFromAxService.RecVersion,
                //                                 MissingProd = (f == null ? string.Empty : f.LocationName)

                //                             }).Where(a => a.MissingProd == string.Empty);

                //var unmatachedAddress = queryUnmatchedRecords.ToList();


                //foreach (var eachPostalAddress in unmatachedAddress.ToList())
                //{
                //    AxdEntity_DirPartyPostalAddressView address = new AxdEntity_DirPartyPostalAddressView();

                //    log.Info("Enter UpdateAddressOrganization(): For each address");
                //    address.action = AxdEnum_AxdEntityAction.delete;
                //    address.actionSpecified = true;

                //    //address.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                //    //address.updateModeSpecified = true;

                //    //
                //    address.RecId = eachPostalAddress.RecId;
                //    address.RecIdSpecified = true;
                //    address.RecVersion = eachPostalAddress.RecVersion;
                //    address.RecVersionSpecified = true;

                //    //Added the address in the address list
                //    addressList.Add(address);
                //}

                #endregion "unmatached Address"

                #region " Looping Customer addresses"
                // Looping all addresses

                ////To-Do Use ACADIS-ID to update the info.
                var querySelect = from customerAddress in addressListFromAxService
                                  orderby customerAddress.IsPrimary descending
                                  select customerAddress;

                var enumerableQuery = querySelect.ToList();
                int i = 0;
                int addresscount = enumerableQuery.Count();

                foreach (var eachPostalAddress in customer.AddressList)
                {
                    AxdEntity_DirPartyPostalAddressView address = new AxdEntity_DirPartyPostalAddressView();


                    if (i < addresscount) // if count is 1 then update the record
                    {

                        if (enumerableQuery[i].RecId == null)
                        {
                            #region "Create address"

                            log.Info("Enter Create addres: For each address");

                            address.action = AxdEnum_AxdEntityAction.create;
                            address.actionSpecified = true;

                            //address.RecId = enumerable[0].RecId;
                            //address.RecIdSpecified = true;
                            //address.RecVersion = enumerable[0].RecVersion;
                            //address.RecVersionSpecified = true;

                            #endregion "Create address"
                        }
                        else
                        {
                            #region "Update Address"

                            log.Info("Enter UpdateAddressOrganization(): For each address");
                            address.action = AxdEnum_AxdEntityAction.update;
                            address.actionSpecified = true;

                            address.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                            address.updateModeSpecified = true;

                            address.RecId = enumerableQuery[i].RecId;
                            address.RecIdSpecified = true;
                            address.RecVersion = enumerableQuery[i].RecVersion;
                            address.RecVersionSpecified = true;

                            #endregion "Update address"
                        }
                    }
                    else
                    {
                        #region "Create address"

                        log.Info("Enter Create addres: For each address");

                        address.action = AxdEnum_AxdEntityAction.create;
                        address.actionSpecified = true;

                        #endregion "Create address"
                    }
                    i++;
                    #region "Address data"

                    // Checking is it is logistics primary address or not
                    AxdExtType_LogisticsIsPrimaryAddress isPrimary = (eachPostalAddress.IsPrimaryAddress)
                        ? AxdExtType_LogisticsIsPrimaryAddress.Yes
                        : AxdExtType_LogisticsIsPrimaryAddress.No;
                    address.IsPrimary = isPrimary;
                    address.IsPrimarySpecified = true;

                    address.LocationName = eachPostalAddress.LocationName;
                    address.Street = eachPostalAddress.Street;
                    address.City = eachPostalAddress.City;
                    address.State = eachPostalAddress.State;
                    address.CountryRegionId = eachPostalAddress.CountryRegionID;
                    //  address.ZipCode = eachPostalAddress.ZipCode;
                    address.ZipCode = FormatZipCode(eachPostalAddress.ZipCode);

                    address.Roles = eachPostalAddress.Roles;

                    //Add each address into address list
                    addressList.Add(address);

                    #endregion " address"
                    log.Info("Exit UpdateAddressOrganization()");
                }
                #endregion " Loo[ping Customer addresses"

                if (i <= addresscount)
                {
                    #region "unmatached Address"

                    var queryUnmatchedRecords = (from eachaddressListFromAxService in addressListFromAxService
                                                 orderby eachaddressListFromAxService.IsPrimary descending
                                                 select eachaddressListFromAxService).Skip(i);

                    var unmatachedAddress = queryUnmatchedRecords.ToList();


                    foreach (var eachPostalAddress in unmatachedAddress.ToList())
                    {
                        AxdEntity_DirPartyPostalAddressView address = new AxdEntity_DirPartyPostalAddressView();

                        log.Info("Enter UpdateAddressOrganization(): For each address");
                        address.action = AxdEnum_AxdEntityAction.delete;
                        address.actionSpecified = true;

                        //address.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                        //address.updateModeSpecified = true;

                        //
                        address.RecId = eachPostalAddress.RecId;
                        address.RecIdSpecified = true;
                        address.RecVersion = eachPostalAddress.RecVersion;
                        address.RecVersionSpecified = true;

                        //Added the address in the address list
                        addressList.Add(address);
                    }

                    #endregion "unmatached Address"
                }
            }
            log.Info("Exit UpdateAddressOrganization()  Account Number : " + customer.AccountNumber);
            return addressList;

        }
        /// <summary>
        /// Update address with rec id
        /// </summary>
        /// <param name="axdEntityDirPartyDirOrganization"></param>
        /// <param name="customer"></param>
        /// <returns></returns>
        private List<AxdEntity_DirPartyPostalAddressView> UpdateAddressOrganizationOld(AxdEntity_DirParty_DirOrganization axdEntityDirPartyDirOrganization, Customer customer)
        {
            log.Info("Enter UpdateAddressOrganization()  Account Number : " + customer.AccountNumber);

            //local variable uses to store the customer address details - once it is identified as create or update then put the action and store it in this object.
            List<AxdEntity_DirPartyPostalAddressView> addressList = new List<AxdEntity_DirPartyPostalAddressView>();
            // Local object uses to loop customer addresses
            List<AxdEntity_DirPartyPostalAddressView> addressListFromAxService = new List<AxdEntity_DirPartyPostalAddressView>();

            if (axdEntityDirPartyDirOrganization != null)
            {
                //Checking if existing address there are not, if there just all addresses to our local variable
                if (axdEntityDirPartyDirOrganization.DirPartyPostalAddressView != null)
                {
                    addressListFromAxService.AddRange(axdEntityDirPartyDirOrganization.DirPartyPostalAddressView);
                    #region "Update Address"

                    if (customer.AddressList.Count() == 0)
                    {
                        AxdEntity_DirPartyPostalAddressView addressTemp = new AxdEntity_DirPartyPostalAddressView();
                        log.Info("Enter UpdateAddressOrganization(): No address from ACADIS and AX has " + addressListFromAxService.Count() + " address. So updating empty detaiols in the address since we cannot delete shared address in AX.");

                        var query = from customerAddress in addressListFromAxService
                                    select customerAddress;

                        var enumerable = query.ToList();

                        if (enumerable.Any()) // if count is 1 then update the record
                        {

                            addressTemp.action = AxdEnum_AxdEntityAction.update;
                            addressTemp.actionSpecified = true;

                            addressTemp.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                            addressTemp.updateModeSpecified = true;

                            addressTemp.RecId = enumerable[0].RecId;
                            addressTemp.RecIdSpecified = true;
                            addressTemp.RecVersion = enumerable[0].RecVersion;
                            addressTemp.RecVersionSpecified = true;

                            addressTemp.LocationName = string.Empty;
                            addressTemp.Street = string.Empty;
                            addressTemp.City = string.Empty;
                            addressTemp.State = string.Empty;
                            string coutry = (enumerable[0].CountryRegionId != null) ? enumerable[0].CountryRegionId : Business;

                            addressTemp.CountryRegionId = coutry;
                            addressTemp.ZipCode = string.Empty; ;

                            string roles = (enumerable[0].Roles != null) ? enumerable[0].Roles : Business;
                            addressTemp.Roles = roles;

                            //Add each address into address list
                            addressList.Add(addressTemp);
                            log.Info("Exit UpdateAddress()");
                            return addressList;
                        }
                    }
                    #endregion "Update address"
                }
                #region "unmatached Address"

                //var queryUnmatchedRecords = (from eachaddressListFromAxService in addressListFromAxService
                //                             join eachAddressList in customer.AddressList on eachaddressListFromAxService.LocationName equals
                //                                 eachAddressList.LocationName
                //                                 into combinedResult
                //                             from f in combinedResult.DefaultIfEmpty()

                //                             select new
                //                             {
                //                                 IsPrimary = eachaddressListFromAxService.IsPrimary,
                //                                 LocationName = eachaddressListFromAxService.LocationName,
                //                                 RecId = eachaddressListFromAxService.RecId,
                //                                 RecVersion = eachaddressListFromAxService.RecVersion,
                //                                 MissingProd = (f == null ? string.Empty : f.LocationName)

                //                             }).Where(a => a.MissingProd == string.Empty);

                //var unmatachedAddress = queryUnmatchedRecords.ToList();


                //foreach (var eachPostalAddress in unmatachedAddress.ToList())
                //{
                //    AxdEntity_DirPartyPostalAddressView address = new AxdEntity_DirPartyPostalAddressView();

                //    log.Info("Enter UpdateAddressOrganization(): For each address");
                //    address.action = AxdEnum_AxdEntityAction.delete;
                //    address.actionSpecified = true;

                //    //address.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                //    //address.updateModeSpecified = true;

                //    //
                //    address.RecId = eachPostalAddress.RecId;
                //    address.RecIdSpecified = true;
                //    address.RecVersion = eachPostalAddress.RecVersion;
                //    address.RecVersionSpecified = true;

                //    //Added the address in the address list
                //    addressList.Add(address);
                //}

                #endregion "unmatached Address"

                #region " Looping Customer addresses"

                int i = 0;
                // Looping all addresses
                foreach (var eachPostalAddress in customer.AddressList)
                {
                    AxdEntity_DirPartyPostalAddressView address = new AxdEntity_DirPartyPostalAddressView();

                    //To-Do Use ACADIS-ID to update the info.
                    var query = from customerAddress in addressListFromAxService
                                where eachPostalAddress != null //&& eachPostalAddress.LocationName == customerAddress.LocationName
                                select customerAddress;

                    var enumerable = query.ToList();
                    if (enumerable.Count() == (i + 1)) // if count is 1 then update the record
                    {

                        if (enumerable[i].RecId == null)
                        {
                            #region "Create address"

                            log.Info("Enter UpdateAddressOrganization: For each address");

                            address.action = AxdEnum_AxdEntityAction.create;
                            address.actionSpecified = true;

                            //address.RecId = enumerable[0].RecId;
                            //address.RecIdSpecified = true;
                            //address.RecVersion = enumerable[0].RecVersion;
                            //address.RecVersionSpecified = true;

                            #endregion "Create address"
                        }
                        else
                        {
                            #region "Update Address"

                            log.Info("Enter UpdateAddressOrganization(): For each address");
                            address.action = AxdEnum_AxdEntityAction.update;
                            address.actionSpecified = true;

                            address.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                            address.updateModeSpecified = true;

                            address.RecId = enumerable[i].RecId;
                            address.RecIdSpecified = true;
                            address.RecVersion = enumerable[i].RecVersion;
                            address.RecVersionSpecified = true;

                            #endregion "Update address"
                        }
                    }
                    else
                    {
                        #region "Create address"

                        log.Info("Enter CreateAddressOrganization address: For each address");

                        address.action = AxdEnum_AxdEntityAction.create;
                        address.actionSpecified = true;

                        #endregion "Create address"
                    }
                    #region "Address data"

                    // Checking is it is logistics primary address or not
                    AxdExtType_LogisticsIsPrimaryAddress isPrimary = (eachPostalAddress.IsPrimaryAddress)
                        ? AxdExtType_LogisticsIsPrimaryAddress.Yes
                        : AxdExtType_LogisticsIsPrimaryAddress.No;
                    address.IsPrimary = isPrimary;
                    address.IsPrimarySpecified = true;

                    address.LocationName = eachPostalAddress.LocationName;
                    address.Street = eachPostalAddress.Street;
                    address.City = eachPostalAddress.City;
                    address.State = eachPostalAddress.State;
                    address.CountryRegionId = eachPostalAddress.CountryRegionID;
                    //  address.ZipCode = eachPostalAddress.ZipCode;
                    address.ZipCode = FormatZipCode(eachPostalAddress.ZipCode);

                    address.Roles = eachPostalAddress.Roles;
                    //Add each address into address list
                    addressList.Add(address);

                    #endregion " address"

                    i++;
                }
                #endregion " Loo[ping Customer addresses"
            }
            log.Info("Exit UpdateAddressOrganization()");
            return addressList;

        }

        /// <summary>
        /// Update address  with rec id
        /// </summary>
        /// <param name="AxdEntity_DirParty_DirOrganization"></param>
        /// <param name="customer"></param>
        /// <returns></returns>
        private List<AxdEntity_DirPartyContactInfoView> UpdateContactInfoOrganization_Aug08(AxdEntity_DirParty_DirOrganization AxdEntity_DirParty_DirOrganization, Customer customer)
        {
            log.Info("Enter UpdateContact()  Account Number : " + customer.AccountNumber);

            List<AxdEntity_DirPartyContactInfoView> ContactList = new List<AxdEntity_DirPartyContactInfoView>();
            List<AxdEntity_DirPartyContactInfoView> ContactListFromAXService = new List<AxdEntity_DirPartyContactInfoView>();
            if (AxdEntity_DirParty_DirOrganization != null)
            {
                if (AxdEntity_DirParty_DirOrganization.DirPartyContactInfoView != null)
                    ContactListFromAXService.AddRange(AxdEntity_DirParty_DirOrganization.DirPartyContactInfoView);

                #region "unmatached Address"

                var queryUnmatchedRecords = (from eachaddressListFromAxService in ContactListFromAXService
                                             join eachAddressList in customer.ContactList on eachaddressListFromAxService.LocationName equals
                                                 eachAddressList.ContactName
                                                 into combinedResult
                                             from f in combinedResult.DefaultIfEmpty()

                                             select new
                                             {
                                                 Primary = eachaddressListFromAxService.IsPrimary,
                                                 LocationName = eachaddressListFromAxService.LocationName,
                                                 RecId = eachaddressListFromAxService.RecId,
                                                 RecVersion = eachaddressListFromAxService.RecVersion,
                                                 MissingProd = (f == null ? string.Empty : f.ContactName)

                                             }).Where(a => a.MissingProd == string.Empty && a.Primary == AxdEnum_NoYes.No);

                var unmatachedAddress = queryUnmatchedRecords.ToList();


                foreach (var eachPostalAddress in unmatachedAddress.ToList())
                {
                    AxdEntity_DirPartyContactInfoView contactInfo = new AxdEntity_DirPartyContactInfoView();

                    log.Info("Enter UpdateContactInfo(): For each contact");
                    contactInfo.action = AxdEnum_AxdEntityAction.delete;
                    contactInfo.actionSpecified = true;

                    //address.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                    //address.updateModeSpecified = true;

                    //
                    contactInfo.RecId = eachPostalAddress.RecId;
                    contactInfo.RecIdSpecified = true;
                    contactInfo.RecVersion = eachPostalAddress.RecVersion;
                    contactInfo.RecVersionSpecified = true;

                    //Added the address in the address list
                    ContactList.Add(contactInfo);
                }

                #endregion "unmatached Address"
                // Looping all Contactes
                foreach (var eachPostalContact in customer.ContactList)
                {
                    AxdEntity_DirPartyContactInfoView Contact = new AxdEntity_DirPartyContactInfoView();


                    //To-Do Use ACADIS-ID to update the info.
                    var query = from customerContact in ContactListFromAXService
                                where eachPostalContact != null && eachPostalContact.ContactName == customerContact.LocationName
                                select customerContact;

                    var enumerable = query.ToList();

                    if (enumerable.Count() == 1) // if count is 1 then update the record
                    {

                        if (enumerable[0].RecId == null)
                        {
                            #region "Create Contact"

                            log.Info("Enter Create contact: For each Contact");

                            Contact.action = AxdEnum_AxdEntityAction.create;
                            Contact.actionSpecified = true;

                            #endregion "Create Contact"
                        }
                        else
                        {
                            #region "Update Contact"

                            log.Info("Enter UpdateContact(): For each Contact");
                            Contact.action = AxdEnum_AxdEntityAction.update;
                            Contact.actionSpecified = true;

                            //Contact.updateModeSpecified = AxdEnum_ValidTimeStateUpdate.Correction;
                            //Contact.updateModeSpecified = true;

                            Contact.RecId = enumerable[0].RecId;
                            Contact.RecIdSpecified = true;
                            Contact.RecVersion = enumerable[0].RecVersion;
                            Contact.RecVersionSpecified = true;

                            #endregion "Update Contact"
                        }
                    }
                    else
                    {
                        #region "Create Contact"

                        log.Info("Enter Create addres: For each Contact");

                        Contact.action = AxdEnum_AxdEntityAction.create;
                        Contact.actionSpecified = true;

                        #endregion "Create Contact"
                    }

                    #region "Contact data"

                    AxdEnum_NoYes isPrimaryContact = (eachPostalContact.IsPrimary) ? AxdEnum_NoYes.Yes : AxdEnum_NoYes.No;
                    Contact.IsPrimary = isPrimaryContact;
                    Contact.IsPrimarySpecified = true;

                    //get address type enum value 
                    Contact.Type = GetAddressType(eachPostalContact.Type);
                    Contact.TypeSpecified = true;

                    Contact.LocationName = eachPostalContact.ContactName; // contact name 
                    Contact.Locator = eachPostalContact.Locator;
                    Contact.LocatorExtension = eachPostalContact.Extension;
                    Contact.Roles = eachPostalContact.Roles;

                    //Add each Contact into Contact list
                    ContactList.Add(Contact);

                    #endregion " Contact data"


                }
            }
            log.Info("Exit UpdateContact()");
            return ContactList;

        }

        /// <summary>
        /// Update address with rec id
        /// </summary>
        /// <param name="axdEntityDirPartyDirPerson"></param>
        /// <param name="customer"></param>
        /// <returns></returns>
        private List<AxdEntity_DirPartyContactInfoView> UpdateContactInfo(AxdEntity_DirParty_DirPerson axdEntityDirPartyDirPerson, Customer customer)
        {
            log.Info("Enter UpdateContact()  Account Number : " + customer.AccountNumber);

            //local variable uses to store the customer Contact details - once it is identified as create or update then put the action and store it in this object.
            List<AxdEntity_DirPartyContactInfoView> ContactList = new List<AxdEntity_DirPartyContactInfoView>();

            // Local object uses to loop customer Contactes
            List<AxdEntity_DirPartyContactInfoView> ContactListFromAxService = new List<AxdEntity_DirPartyContactInfoView>();
            if (axdEntityDirPartyDirPerson != null)
            {
                //Checking if existing Contact there are not, if there just all Contactes to our local variable
                if (axdEntityDirPartyDirPerson.DirPartyContactInfoView != null)
                {
                    //ContactListFromAxService.AddRange(axdEntityDirPartyDirPerson.DirPartyContactInfoView);

                    #region "Delete existing Contact"
                    foreach (var eachPostalContact in axdEntityDirPartyDirPerson.DirPartyContactInfoView.ToList())
                    {
                        AxdEntity_DirPartyContactInfoView Contact = new AxdEntity_DirPartyContactInfoView();

                        log.Info("Enter UpdateContact(): For each Contact");
                        Contact.action = AxdEnum_AxdEntityAction.delete;
                        Contact.actionSpecified = true;

                        //Contact.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                        //Contact.updateModeSpecified = true;

                        //
                        Contact.RecId = eachPostalContact.RecId;
                        Contact.RecIdSpecified = true;
                        Contact.RecVersion = eachPostalContact.RecVersion;
                        Contact.RecVersionSpecified = true;

                        //Added the Contact in the Contact list
                        ContactList.Add(Contact);
                    }

                    #endregion "Delete existing Contact"
                }

                #region "Create Contact"

                if (customer.ContactList.Count() > 0)
                {

                    foreach (var eachPostalContact in customer.ContactList)
                    {
                        AxdEntity_DirPartyContactInfoView ContactTemp = new AxdEntity_DirPartyContactInfoView();

                        // Checking is it is logistics primary Contact or not
                        AxdEnum_NoYes isPrimary = (eachPostalContact.IsPrimary)
                            ? AxdEnum_NoYes.Yes
                            : AxdEnum_NoYes.No;

                        ContactTemp.action = AxdEnum_AxdEntityAction.create;
                        ContactTemp.actionSpecified = true;
                        ContactTemp.IsPrimary = isPrimary;
                        ContactTemp.IsPrimarySpecified = true;

                        ContactTemp.Type = GetAddressType(eachPostalContact.Type);
                        ContactTemp.TypeSpecified = true;

                        ContactTemp.LocationName = eachPostalContact.ContactName;
                        ContactTemp.Locator = eachPostalContact.Locator;
                        ContactTemp.LocatorExtension = eachPostalContact.Extension; //Added for phone extension - Feb-23-2015
                        ContactTemp.Roles = eachPostalContact.Roles;
                        //Add each Contact into Contact list
                        ContactList.Add(ContactTemp);
                    }
                }

                #endregion "Create Contact"
            }
            log.Info("Exit UpdateContact()  Account Number : " + customer.AccountNumber);
            return ContactList;

        }

        /// <summary>
        /// Update address with rec id
        /// </summary>
        /// <param name="axdEntityDirPartyDirPerson"></param>
        /// <param name="customer"></param>
        /// <returns></returns>
        private List<AxdEntity_DirPartyContactInfoView> UpdateContactInfoOrganization(AxdEntity_DirParty_DirOrganization axdEntityDirPartyDirOrganization, Customer customer)
        {
            log.Info("Enter UpdateContact()  Account Number : " + customer.AccountNumber);

            //local variable uses to store the customer Contact details - once it is identified as create or update then put the action and store it in this object.
            List<AxdEntity_DirPartyContactInfoView> ContactList = new List<AxdEntity_DirPartyContactInfoView>();

            // Local object uses to loop customer Contactes
            List<AxdEntity_DirPartyContactInfoView> ContactListFromAxService = new List<AxdEntity_DirPartyContactInfoView>();
            if (axdEntityDirPartyDirOrganization != null)
            {
                //Checking if existing Contact there are not, if there just all Contactes to our local variable
                if (axdEntityDirPartyDirOrganization.DirPartyContactInfoView != null)
                {
                    //ContactListFromAxService.AddRange(axdEntityDirPartyDirOrganization.DirPartyContactInfoView);

                    #region "Delete existing Contact"
                    foreach (var eachPostalContact in axdEntityDirPartyDirOrganization.DirPartyContactInfoView.ToList())
                    {
                        AxdEntity_DirPartyContactInfoView Contact = new AxdEntity_DirPartyContactInfoView();

                        log.Info("Enter UpdateContact(): For each Contact");
                        Contact.action = AxdEnum_AxdEntityAction.delete;
                        Contact.actionSpecified = true;

                        //Contact.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                        //Contact.updateModeSpecified = true;

                        //
                        Contact.RecId = eachPostalContact.RecId;
                        Contact.RecIdSpecified = true;
                        Contact.RecVersion = eachPostalContact.RecVersion;
                        Contact.RecVersionSpecified = true;

                        //Added the Contact in the Contact list
                        ContactList.Add(Contact);
                    }

                    #endregion "Delete existing Contact"


                }
                #region "Create Contact"

                if (customer.ContactList.Count() > 0)
                {

                    foreach (var eachPostalContact in customer.ContactList)
                    {
                        AxdEntity_DirPartyContactInfoView ContactTemp = new AxdEntity_DirPartyContactInfoView();

                        // Checking is it is logistics primary Contact or not
                        AxdEnum_NoYes isPrimary = (eachPostalContact.IsPrimary)
                            ? AxdEnum_NoYes.Yes
                            : AxdEnum_NoYes.No;

                        ContactTemp.action = AxdEnum_AxdEntityAction.create;
                        ContactTemp.actionSpecified = true;
                        ContactTemp.IsPrimary = isPrimary;
                        ContactTemp.IsPrimarySpecified = true;

                        ContactTemp.Type = GetAddressType(eachPostalContact.Type);
                        ContactTemp.TypeSpecified = true;

                        ContactTemp.LocationName = eachPostalContact.ContactName;
                        ContactTemp.Locator = eachPostalContact.Locator;
                        ContactTemp.LocatorExtension = eachPostalContact.Extension; //Added for phone extension - Feb-23-2015
                        ContactTemp.Roles = eachPostalContact.Roles;
                        //Add each Contact into Contact list
                        ContactList.Add(ContactTemp);
                    }
                }

                #endregion " Contact"
            }
            log.Info("Exit UpdateContact()  Account Number : " + customer.AccountNumber);
            return ContactList;

        }

        /// <summary>
        /// Update address with rec id
        /// </summary>
        /// <param name="axdEntityDirPartyDirPerson"></param>
        /// <param name="customer"></param>
        /// <returns></returns>
        private List<AxdEntity_DirPartyContactInfoView> UpdateContactInfo1(AxdEntity_DirParty_DirPerson axdEntityDirPartyDirPerson, Customer customer)
        {
            log.Info("Enter UpdateContact()  Account Number : " + customer.AccountNumber);

            //local variable uses to store the customer Contact details - once it is identified as create or update then put the action and store it in this object.
            List<AxdEntity_DirPartyContactInfoView> ContactList = new List<AxdEntity_DirPartyContactInfoView>();

            // Local object uses to loop customer Contactes
            List<AxdEntity_DirPartyContactInfoView> ContactListFromAxService = new List<AxdEntity_DirPartyContactInfoView>();
            if (axdEntityDirPartyDirPerson != null)
            {
                //Checking if existing Contact there are not, if there just all Contactes to our local variable
                if (axdEntityDirPartyDirPerson.DirPartyContactInfoView != null)
                {
                    ContactListFromAxService.AddRange(axdEntityDirPartyDirPerson.DirPartyContactInfoView);

                    #region "Update Contact"

                    if (customer.ContactList.Count() == 0)
                    {
                        AxdEntity_DirPartyContactInfoView ContactTemp = new AxdEntity_DirPartyContactInfoView();
                        log.Info("Enter UpdateContact(): No Contact from ACADIS and AX has " + ContactListFromAxService.Count() + " Contact. So updating empty details in the Contact since we cannot delete shared Contact in AX.");

                        var query = from customerContact in ContactListFromAxService
                                    select customerContact;

                        var enumerable = query.ToList();
                        int count = enumerable.Count;
                        #region "if only one Contact in ax "
                        if (count == 1) // if count is 1 then update the record
                        {

                            ContactTemp.action = AxdEnum_AxdEntityAction.update;
                            ContactTemp.actionSpecified = true;

                            //ContactTemp.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                            //ContactTemp.updateModeSpecified = true;

                            ContactTemp.RecId = enumerable[0].RecId;
                            ContactTemp.RecIdSpecified = true;
                            ContactTemp.RecVersion = enumerable[0].RecVersion;
                            ContactTemp.RecVersionSpecified = true;

                            ContactTemp.LocationName = string.Empty;

                            string roles = (enumerable[0].Roles != null) ? enumerable[0].Roles : Business;
                            ContactTemp.Roles = roles;

                            //Add each Contact into Contact list
                            ContactList.Add(ContactTemp);
                            log.Info("Exit UpdateContact()");
                            return ContactList;
                        }
                        #endregion "if only one Contact in ax "
                        else
                        {
                            #region "if only one Contact in ax "

                            var querySelecttemp = from customerContact in ContactListFromAxService
                                                  where customerContact.IsPrimary == AxdEnum_NoYes.Yes
                                                  select customerContact;

                            var enumerable1 = querySelecttemp.ToList();

                            #region "if No primary Contact select first and update with empty record"
                            if (enumerable1.Count == 0)
                            {
                                #region "take first Contact to update "

                                query = (from customerContact in ContactListFromAxService
                                         select customerContact).Take(1);

                                if (query.Count() == 1) // if count is 1 then update the record
                                {

                                    ContactTemp.action = AxdEnum_AxdEntityAction.update;
                                    ContactTemp.actionSpecified = true;

                                    //ContactTemp.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                                    //ContactTemp.updateModeSpecified = true;

                                    ContactTemp.RecId = enumerable[0].RecId;
                                    ContactTemp.RecIdSpecified = true;
                                    ContactTemp.RecVersion = enumerable[0].RecVersion;
                                    ContactTemp.RecVersionSpecified = true;

                                    ContactTemp.LocationName = string.Empty;

                                    string roles = (enumerable[0].Roles != null) ? enumerable[0].Roles : Business;
                                    ContactTemp.Roles = roles;

                                    //Add each Contact into Contact list
                                    ContactList.Add(ContactTemp);

                                }
                                #endregion "take first Contact to update "

                                #region "skip first Contact and delete other Contact "
                                foreach (var eachPostalContact in ContactListFromAxService.Skip(1))
                                {
                                    AxdEntity_DirPartyContactInfoView Contact = new AxdEntity_DirPartyContactInfoView();

                                    log.Info("Enter UpdateContact(): For each Contact");
                                    Contact.action = AxdEnum_AxdEntityAction.delete;
                                    Contact.actionSpecified = true;
                                    Contact.RecId = eachPostalContact.RecId;
                                    Contact.RecIdSpecified = true;
                                    Contact.RecVersion = eachPostalContact.RecVersion;
                                    Contact.RecVersionSpecified = true;

                                    //Added the Contact in the Contact list
                                    ContactList.Add(Contact);
                                    log.Info("Exit UpdateContact()");
                                    return ContactList;
                                }
                                #endregion "skip first Contact and delete other Contact "
                            }
                            #endregion "if No primary Contact select first and update with empty record"
                            #region "if primary Contact found"
                            else
                            {
                                #region "if  primary Contact select - update Contact"

                                ContactTemp.action = AxdEnum_AxdEntityAction.update;
                                ContactTemp.actionSpecified = true;

                                //ContactTemp.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                                //ContactTemp.updateModeSpecified = true;

                                ContactTemp.RecId = enumerable1[0].RecId;
                                ContactTemp.RecIdSpecified = true;
                                ContactTemp.RecVersion = enumerable1[0].RecVersion;
                                ContactTemp.RecVersionSpecified = true;

                                ContactTemp.LocationName = string.Empty;

                                string roles = (enumerable1[0].Roles != null) ? enumerable1[0].Roles : Business;
                                ContactTemp.Roles = roles;

                                //Add each Contact into Contact list
                                ContactList.Add(ContactTemp);
                                #endregion "if  primary Contact select - update Contact"

                                #region "if Update other records"

                                var queryUnmatchedRecords = (from eachContactListFromAxService in ContactListFromAxService
                                                             join eachContactList in querySelecttemp.ToList() on eachContactListFromAxService.LocationName equals
                                                                 eachContactList.LocationName
                                                                 into combinedResult
                                                             from f in combinedResult.DefaultIfEmpty()

                                                             select new
                                                             {
                                                                 IsPrimary = eachContactListFromAxService.IsPrimary,
                                                                 LocationName = eachContactListFromAxService.LocationName,
                                                                 RecId = eachContactListFromAxService.RecId,
                                                                 RecVersion = eachContactListFromAxService.RecVersion,
                                                                 MissingProd = (f == null ? string.Empty : f.LocationName)

                                                             }).Where(a => a.MissingProd == string.Empty);

                                var unmatachedContact = queryUnmatchedRecords.ToList();

                                foreach (var eachPostalContact in unmatachedContact.ToList())
                                {
                                    AxdEntity_DirPartyContactInfoView Contact = new AxdEntity_DirPartyContactInfoView();

                                    log.Info("Enter UpdateContact(): For each Contact");
                                    Contact.action = AxdEnum_AxdEntityAction.delete;
                                    Contact.actionSpecified = true;
                                    Contact.RecId = eachPostalContact.RecId;
                                    Contact.RecIdSpecified = true;
                                    Contact.RecVersion = eachPostalContact.RecVersion;
                                    Contact.RecVersionSpecified = true;

                                    //Added the Contact in the Contact list
                                    ContactList.Add(Contact);
                                }
                                log.Info("Exit UpdateContact()");
                                return ContactList;
                                #endregion "if Update other records"
                            }
                            #endregion "if primary Contact found"

                            #endregion "if only one Contact in ax "
                        }

                    }
                    #endregion "Update Contact"
                }


                #region " Looping Customer Contactes"
                // Looping all Contactes

                var querySelect = from customerContact in ContactListFromAxService
                                  orderby customerContact.IsPrimary descending
                                  select customerContact;

                var enumerableQuery = querySelect.ToList();
                int i = 0;
                int Contactcount = enumerableQuery.Count();

                foreach (var eachPostalContact in customer.ContactList)
                {
                    AxdEntity_DirPartyContactInfoView Contact = new AxdEntity_DirPartyContactInfoView();


                    if (i < Contactcount) // if count is 1 then update the record
                    {

                        if (enumerableQuery[i].RecId == null)
                        {
                            #region "Create Contact"

                            log.Info("Enter Create addres: For each Contact");

                            Contact.action = AxdEnum_AxdEntityAction.create;
                            Contact.actionSpecified = true;

                            #endregion "Create Contact"
                        }
                        else
                        {
                            #region "Update Contact"

                            log.Info("Enter UpdateContact(): For each Contact");
                            Contact.action = AxdEnum_AxdEntityAction.update;
                            Contact.actionSpecified = true;

                            //Contact.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                            //Contact.updateModeSpecified = true;

                            Contact.RecId = enumerableQuery[i].RecId;
                            Contact.RecIdSpecified = true;
                            Contact.RecVersion = enumerableQuery[i].RecVersion;
                            Contact.RecVersionSpecified = true;

                            #endregion "Update Contact"
                        }
                    }
                    else
                    {
                        #region "Create Contact"

                        log.Info("Enter Create addres: For each Contact");

                        Contact.action = AxdEnum_AxdEntityAction.create;
                        Contact.actionSpecified = true;

                        #endregion "Create Contact"
                    }
                    i++;
                    #region "Contact data"

                    // Checking is it is logistics primary Contact or not
                    AxdEnum_NoYes isPrimary = (eachPostalContact.IsPrimary)
                        ? AxdEnum_NoYes.Yes
                        : AxdEnum_NoYes.No;
                    Contact.IsPrimary = isPrimary;
                    Contact.IsPrimarySpecified = true;

                    Contact.Type = GetAddressType(eachPostalContact.Type);
                    Contact.TypeSpecified = true;

                    Contact.LocationName = eachPostalContact.ContactName;
                    Contact.Locator = eachPostalContact.Locator;
                    Contact.LocatorExtension = eachPostalContact.Extension; //Added for phone extension - Feb-23-2015
                    Contact.Roles = eachPostalContact.Roles;
                    //Add each Contact into Contact list
                    ContactList.Add(Contact);

                    #endregion " Contact"
                    log.Info("Exit UpdateContact()");
                }
                #endregion " Looping Customer Contactes"

                if (i <= Contactcount)
                {
                    #region "unmatached Contact"

                    var queryUnmatchedRecords = (from eachContactListFromAxService in ContactListFromAxService
                                                 orderby eachContactListFromAxService.IsPrimary descending
                                                 select eachContactListFromAxService).Skip(i);

                    var unmatachedContact = queryUnmatchedRecords.ToList();


                    foreach (var eachPostalContact in unmatachedContact.ToList())
                    {
                        AxdEntity_DirPartyContactInfoView Contact = new AxdEntity_DirPartyContactInfoView();

                        log.Info("Enter UpdateContact(): For each Contact");
                        Contact.action = AxdEnum_AxdEntityAction.delete;
                        Contact.actionSpecified = true;

                        //Contact.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                        //Contact.updateModeSpecified = true;

                        //
                        Contact.RecId = eachPostalContact.RecId;
                        Contact.RecIdSpecified = true;
                        Contact.RecVersion = eachPostalContact.RecVersion;
                        Contact.RecVersionSpecified = true;

                        //Added the Contact in the Contact list
                        ContactList.Add(Contact);
                    }

                    #endregion "unmatached Contact"
                }
            }
            log.Info("Exit UpdateContact()  Account Number : " + customer.AccountNumber);
            return ContactList;

        }

        /// <summary>
        /// Update address  with rec id
        /// </summary>
        /// <param name="axdEntityDirPartyDirPerson"></param>
        /// <param name="customer"></param>
        /// <returns></returns>
        private List<AxdEntity_DirPartyContactInfoView> UpdateContactInfo_Aug_08(AxdEntity_DirParty_DirPerson axdEntityDirPartyDirPerson, Customer customer)
        {
            log.Info("Enter UpdateContact()  Account Number : " + customer.AccountNumber);

            List<AxdEntity_DirPartyContactInfoView> ContactList = new List<AxdEntity_DirPartyContactInfoView>();
            List<AxdEntity_DirPartyContactInfoView> ContactListFromAXService = new List<AxdEntity_DirPartyContactInfoView>();
            if (axdEntityDirPartyDirPerson != null)
            {
                if (axdEntityDirPartyDirPerson.DirPartyContactInfoView != null)
                    ContactListFromAXService.AddRange(axdEntityDirPartyDirPerson.DirPartyContactInfoView);


                #region "unmatached Address"

                var queryUnmatchedRecords = (from eachaddressListFromAxService in ContactListFromAXService
                                             join eachAddressList in customer.ContactList on eachaddressListFromAxService.LocationName equals
                                                 eachAddressList.ContactName
                                                 into combinedResult
                                             from f in combinedResult.DefaultIfEmpty()

                                             select new
                                             {
                                                 Primary = eachaddressListFromAxService.IsPrimary,
                                                 LocationName = eachaddressListFromAxService.LocationName,
                                                 RecId = eachaddressListFromAxService.RecId,
                                                 RecVersion = eachaddressListFromAxService.RecVersion,
                                                 MissingProd = (f == null ? string.Empty : f.ContactName)

                                             }).Where(a => a.MissingProd == string.Empty && a.Primary == AxdEnum_NoYes.No);

                var unmatachedAddress = queryUnmatchedRecords.ToList();


                foreach (var eachPostalAddress in unmatachedAddress.ToList())
                {
                    AxdEntity_DirPartyContactInfoView contactInfo = new AxdEntity_DirPartyContactInfoView();

                    log.Info("Enter UpdateContactInfo(): For each contact");
                    contactInfo.action = AxdEnum_AxdEntityAction.delete;
                    contactInfo.actionSpecified = true;

                    //address.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                    //address.updateModeSpecified = true;

                    //
                    contactInfo.RecId = eachPostalAddress.RecId;
                    contactInfo.RecIdSpecified = true;
                    contactInfo.RecVersion = eachPostalAddress.RecVersion;
                    contactInfo.RecVersionSpecified = true;

                    //Added the address in the address list
                    ContactList.Add(contactInfo);
                }

                #endregion "unmatached Address"
                // Looping all Contactes
                foreach (var eachPostalContact in customer.ContactList)
                {
                    AxdEntity_DirPartyContactInfoView Contact = new AxdEntity_DirPartyContactInfoView();


                    //To-Do Use ACADIS-ID to update the info.
                    var query = from customerContact in ContactListFromAXService
                                where eachPostalContact != null && eachPostalContact.ContactName == customerContact.LocationName
                                select customerContact;

                    var enumerable = query.ToList();

                    if (enumerable.Count() == 1) // if count is 1 then update the record
                    {

                        if (enumerable[0].RecId == null)
                        {
                            #region "Create Contact"

                            log.Info("Enter Create contact: For each Contact");

                            Contact.action = AxdEnum_AxdEntityAction.create;
                            Contact.actionSpecified = true;

                            #endregion "Create Contact"
                        }
                        else
                        {
                            #region "Update Contact"

                            log.Info("Enter UpdateContact(): For each Contact");
                            Contact.action = AxdEnum_AxdEntityAction.update;
                            Contact.actionSpecified = true;

                            //Contact.updateModeSpecified = AxdEnum_ValidTimeStateUpdate.Correction;
                            //Contact.updateModeSpecified = true;

                            Contact.RecId = enumerable[0].RecId;
                            Contact.RecIdSpecified = true;
                            Contact.RecVersion = enumerable[0].RecVersion;
                            Contact.RecVersionSpecified = true;

                            #endregion "Update Contact"
                        }
                    }
                    else
                    {
                        #region "Create Contact"

                        log.Info("Enter Create addres: For each Contact");

                        Contact.action = AxdEnum_AxdEntityAction.create;
                        Contact.actionSpecified = true;

                        #endregion "Create Contact"
                    }
                    //if (enumerable.Count() == 1) // if count is 1 then update the record
                    //{

                    //    if (enumerable[0].RecId == null)
                    //    {
                    //        #region "Create Contact"

                    //        log.Info("Enter Create contact: For each Contact");

                    //        Contact.action = AxdEnum_AxdEntityAction.create;
                    //        Contact.actionSpecified = true;

                    //        #endregion "Create Contact"
                    //    }
                    //    else
                    //    {
                    //        #region "Update Contact"

                    //        log.Info("Enter UpdateContact(): For each Contact");
                    //        Contact.action = AxdEnum_AxdEntityAction.update;
                    //        Contact.actionSpecified = true;

                    //        //Contact.updateModeSpecified = AxdEnum_ValidTimeStateUpdate.Correction;
                    //        //Contact.updateModeSpecified = true;

                    //        Contact.RecId = enumerable[0].RecId;
                    //        Contact.RecIdSpecified = true;
                    //        Contact.RecVersion = enumerable[0].RecVersion;
                    //        Contact.RecVersionSpecified = true;

                    //        #endregion "Update Contact"
                    //    }
                    //}
                    //else
                    //{
                    //    #region "Create Contact"

                    //    log.Info("Enter Create addres: For each Contact");

                    //    Contact.action = AxdEnum_AxdEntityAction.create;
                    //    Contact.actionSpecified = true;

                    //    #endregion "Create Contact"
                    //}

                    #region "Contact data"

                    AxdEnum_NoYes isPrimaryContact = (eachPostalContact.IsPrimary) ? AxdEnum_NoYes.Yes : AxdEnum_NoYes.No;
                    Contact.IsPrimary = isPrimaryContact;
                    Contact.IsPrimarySpecified = true;

                    //get address type enum value 
                    Contact.Type = GetAddressType(eachPostalContact.Type);
                    Contact.TypeSpecified = true;

                    Contact.LocationName = eachPostalContact.ContactName; // contact name 
                    Contact.Locator = eachPostalContact.Locator;
                    Contact.LocatorExtension = eachPostalContact.Extension;
                    Contact.Roles = eachPostalContact.Roles;

                    //Add each Contact into Contact list
                    ContactList.Add(Contact);

                    #endregion " Contact data"


                }
            }
            log.Info("Exit UpdateContact()");
            return ContactList;

        }

        /// <summary>
        /// Create any electronic contact (phone, mobile, fax, email, etc.)
        /// </summary>
        private AxdEntity_DirPartyContactInfoView CreateContactInfo(AxdEnum_AxdEntityAction action, Contact contact)
        {
            //Create an electronic address
            AxdEntity_DirPartyContactInfoView electronicAddress = new AxdEntity_DirPartyContactInfoView();

            if (action != AxdEnum_AxdEntityAction.create)
            {
                electronicAddress.action = action;
                electronicAddress.actionSpecified = true;
            }

            // check if primary address
            AxdEnum_NoYes isPrimaryContact = (contact.IsPrimary) ? AxdEnum_NoYes.Yes : AxdEnum_NoYes.No;
            electronicAddress.IsPrimary = isPrimaryContact;
            electronicAddress.IsPrimarySpecified = true;

            //get address type enum value 
            electronicAddress.Type = GetAddressType(contact.Type);
            electronicAddress.TypeSpecified = true;

            electronicAddress.LocationName = contact.ContactName;
            electronicAddress.Locator = contact.Locator;
            electronicAddress.LocatorExtension = contact.Extension; //Added for phone extension - Feb-23-2015
            electronicAddress.Roles = contact.Roles;
            //electronicAddress.LocatorExtension = contact.Roles;

            return electronicAddress;
        }
        ///Getting address type
        AxdEnum_LogisticsElectronicAddressMethodType GetAddressType(string type)
        {

            AxdEnum_LogisticsElectronicAddressMethodType addressMethodType;
            if (type == null)
                type = string.Empty;

            switch (Convert.ToString(type).ToUpper())
            {
                case "EMAIL":
                    addressMethodType = AxdEnum_LogisticsElectronicAddressMethodType.Email;
                    break;

                case "PHONE":
                    addressMethodType = AxdEnum_LogisticsElectronicAddressMethodType.Phone;
                    break;
                case "FAX":
                    addressMethodType = AxdEnum_LogisticsElectronicAddressMethodType.Fax;
                    break;
                case "TELEX":
                    addressMethodType = AxdEnum_LogisticsElectronicAddressMethodType.Telex;
                    break;
                case "URL":
                    addressMethodType = AxdEnum_LogisticsElectronicAddressMethodType.URL;
                    break;
                default:
                    addressMethodType = AxdEnum_LogisticsElectronicAddressMethodType.None;
                    break;
            }
            return addressMethodType;
        }
        /// <summary>
        /// PersonName is the Data Object that holds the Name of CUSTOMERS OF TYPE PERSON ONLY
        /// </summary>        
        private AxdEntity_PersonName SetPersonName(AxdEnum_AxdEntityAction action, Customer customer, bool _isCustomerCreate)
        {
            AxdEntity_PersonName retPersonName = new AxdEntity_PersonName();

            //for update we have to set the action  
            if (action != AxdEnum_AxdEntityAction.create)
            {
                retPersonName.action = action;
            }
            if (!_isCustomerCreate)
                retPersonName.actionSpecified = true;
            //Set First name,middle name and last name
            retPersonName.FirstName = customer.FirstName;
            if (string.IsNullOrEmpty(customer.MiddleName))
                retPersonName.MiddleName = string.Empty;
            else
                retPersonName.MiddleName = customer.MiddleName;

            retPersonName.LastName = customer.LastName;
            return retPersonName;
        }
        #endregion "Creating Customer"

        #region "Update Customer"
        private AxdEntity_CustTable PrepareCustomerUpdate(AxdEntity_CustTable custTable, Customer customer)
        {
            AxdEntity_CustTable copyCustomer = null;
            AxdEntity_DirParty_DirPerson copyDirPerson = null;
            AxdEntity_DirParty_DirOrganization copyDirOrganization = null;
            AxdEntity_DirParty_DirPerson dirPerson = null;
            AxdEntity_DirParty_DirOrganization dirOrg = null;
            #region "Person"
            if (customer.CustomerType == "Person")
            {

                dirPerson = custTable.DirParty[0] as AxdEntity_DirParty_DirPerson;
                //Original DirParty. Retrieved from Read

                //Running the same method as if I were creating a new customer
                //Unless we set the properties to Update, the customer will be duplicated and an error will be thrown
                copyCustomer = SetCustomerTable(AxdEnum_AxdEntityAction.update, customer, false);

                copyDirPerson = copyCustomer.DirParty[0] as AxdEntity_DirParty_DirPerson;

                //Now I'm setting the system fields that will allow AX to know which record its going to Update
                copyCustomer._DocumentHash = custTable._DocumentHash;
                copyCustomer.RecId = custTable.RecId;
                copyCustomer.RecIdSpecified = true;
                copyCustomer.RecVersion = custTable.RecVersion;
                copyCustomer.RecVersionSpecified = true;

                //Setting update action
                copyCustomer.action = AxdEnum_AxdEntityAction.update;
                copyCustomer.actionSpecified = true;
                if (copyCustomer.CustClassificationId == "No")
                    //For Addresses, Names and Contacts too
                    if (copyDirPerson != null)
                    {
                        #region "If person data is not null"

                        if (dirPerson != null)
                        {
                            copyDirPerson.RecId = dirPerson.RecId;
                            copyDirPerson.RecIdSpecified = true;
                            copyDirPerson.RecVersion = dirPerson.RecVersion;
                            copyDirPerson.RecVersionSpecified = true;


                            dirPerson.LanguageId = language;

                            //Setting update action
                            copyDirPerson.action = AxdEnum_AxdEntityAction.update;
                            copyDirPerson.actionSpecified = true;


                            #region "User first and last name here. Cannot EXCEED 20 CHARACTERS  "

                            copyDirPerson.NameAlias = customer.NameAlias;
                            // if  name is less >20 then get 20 chars only
                            if (customer.NameAlias != null)
                                if (customer.NameAlias.Length > 20)
                                    copyDirPerson.NameAlias = customer.NameAlias.Substring(0, 20);

                            #endregion "User first and last name here. Cannot EXCEED 20 CHARACTERS  "

                            copyDirPerson.NameSequence = customer.NameSequence;

                            //check if the PersonName is empty or not - starts here
                            if (copyDirPerson.PersonName != null && copyDirPerson.PersonName.Count() == 1)
                            {
                                copyDirPerson.PersonName[0].RecId = dirPerson.PersonName[0].RecId;
                                copyDirPerson.PersonName[0].RecIdSpecified = true;
                                copyDirPerson.PersonName[0].RecVersion = dirPerson.PersonName[0].RecVersion;
                                copyDirPerson.PersonName[0].RecVersionSpecified = true;

                                //Updating customer name
                                copyDirPerson.PersonName[0].FirstName = customer.FirstName;
                                copyDirPerson.PersonName[0].LastName = customer.LastName;

                                if (string.IsNullOrEmpty(customer.MiddleName))
                                    copyDirPerson.PersonName[0].MiddleName = string.Empty;
                                else
                                    copyDirPerson.PersonName[0].MiddleName = customer.MiddleName;

                                //Setting update action
                                copyDirPerson.PersonName[0].action = AxdEnum_AxdEntityAction.update;
                                copyDirPerson.PersonName[0].actionSpecified = true;
                                copyDirPerson.PersonName[0].updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                                copyDirPerson.PersonName[0].updateModeSpecified = true;
                            }
                            ////check if the PersonName is empty or not - ends here
                        }

                        #endregion "person data "

                        //// Check the address list is empty- if no address 
                        if (customer.AddressList != null)
                        {
                            // Update Address information
                            copyDirPerson.DirPartyPostalAddressView = UpdateAddress(dirPerson, customer).ToArray();
                        }
                        else
                        {
                            // make it null if no address in the list
                            copyDirPerson.DirPartyPostalAddressView = null;
                        }

                        //// Added Anantha -Dec-12-2014 : contact list added
                        if (customer.ContactList != null)
                        {
                            //Update contact information
                            copyDirPerson.DirPartyContactInfoView = UpdateContactInfo(dirPerson, customer).ToArray();
                        }
                        else
                        {
                            // make it null if no address in the list
                            copyDirPerson.DirPartyContactInfoView = null;
                        }

                        #region Update without Copy - NOT ACTIVE

                        #endregion //Commented

                        copyCustomer.DirParty[0] = copyDirPerson;
                    }
            }
            #endregion "Person"
            #region "org"
            else //. if (customer.CustomerType == "Organization")
            {
                dirOrg = custTable.DirParty[0] as AxdEntity_DirParty_DirOrganization;
                //Original DirParty. Retrieved from Read

                //Running the same method as if I were creating a new customer
                //Unless we set the properties to Update, the customer will be duplicated and an error will be thrown
                copyCustomer = SetCustomerTable(AxdEnum_AxdEntityAction.update, customer, false);

                copyDirOrganization = copyCustomer.DirParty[0] as AxdEntity_DirParty_DirOrganization;

                //Now I'm setting the system fields that will allow AX to know which record its going to Update
                copyCustomer._DocumentHash = custTable._DocumentHash;
                copyCustomer.RecId = custTable.RecId;
                copyCustomer.RecIdSpecified = true;
                copyCustomer.RecVersion = custTable.RecVersion;
                copyCustomer.RecVersionSpecified = true;
                string result = string.Empty;
                #region "Create Assessment invoice"
                if ((custTable.CustClassificationId != "Yes"))
                {
                    //if customer is participating VFFD then create intial premium - starts
                    if (customer.IsParticipating)
                    {
                        result = CreateIntialPremium(customer.AccountNumber, customer.PropertyValuation); // Added PropertyValuation in Create initial premium 
                    }
                    // //if customer is participating VFFD then create intial premium - ends
                }
                #endregion "Create Assessment invoice"
                //Setting update action
                copyCustomer.action = AxdEnum_AxdEntityAction.update;
                copyCustomer.actionSpecified = true;

                //For Addresses, Names and Contacts too
                if (copyDirOrganization != null)
                {
                    #region "If person data is not null"

                    if (dirOrg != null)
                    {
                        copyDirOrganization.RecId = dirOrg.RecId;
                        copyDirOrganization.RecIdSpecified = true;
                        copyDirOrganization.RecVersion = dirOrg.RecVersion;
                        copyDirOrganization.RecVersionSpecified = true;

                        //Setting update action
                        copyDirOrganization.action = AxdEnum_AxdEntityAction.update;
                        copyDirOrganization.actionSpecified = true;

                        copyDirOrganization.Name = customer.OrganizationName;
                        copyDirOrganization.OrganizationName[0].RecId = dirOrg.OrganizationName[0].RecId;
                        copyDirOrganization.OrganizationName[0].RecIdSpecified = true;
                        copyDirOrganization.OrganizationName[0].RecVersion = dirOrg.OrganizationName[0].RecVersion;
                        copyDirOrganization.OrganizationName[0].RecVersionSpecified = true;

                        copyDirOrganization.OrganizationName[0].action = AxdEnum_AxdEntityAction.update;
                        copyDirOrganization.OrganizationName[0].actionSpecified = true;
                        copyDirOrganization.OrganizationName[0].updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                        copyDirOrganization.OrganizationName[0].updateModeSpecified = true;
                        copyDirOrganization.LanguageId = language;

                        copyDirOrganization.OrganizationName[0].Name = customer.OrganizationName;

                    }

                    #endregion "person data "

                    //// Check the address list is empty- if no address 
                    if (customer.AddressList != null)
                    {
                        // Update Address information
                        copyDirOrganization.DirPartyPostalAddressView = UpdateAddressOrganization(dirOrg, customer).ToArray();
                    }
                    else
                    {
                        // make it null if no address in the list
                        copyDirOrganization.DirPartyPostalAddressView = null;
                    }

                    //// Added Anantha -Dec-12-2014 : contact list added
                    if (customer.ContactList != null)
                    {
                        //Update contact information
                        copyDirOrganization.DirPartyContactInfoView = UpdateContactInfoOrganization(dirOrg, customer).ToArray();
                    }
                    else
                    {
                        // make it null if no address in the list
                        copyDirOrganization.DirPartyContactInfoView = null;
                    }
                    copyCustomer.DirParty[0] = copyDirOrganization;
                }
            }
            #endregion "org"

            return copyCustomer;
        }

        //private AxdEntity_CustTable PrepareCustomerUpdate1(AxdEntity_CustTable custTable, Customer customer)
        //{
        //    AxdEntity_CustTable copyCustomer = null;
        //    AxdEntity_DirParty_DirPerson copyDirPerson = null;
        //    AxdEntity_DirParty_DirPerson dirPerson = custTable.DirParty[0] as AxdEntity_DirParty_DirPerson; //Original DirParty. Retrieved from Read

        //    //Running the same method as if I were creating a new customer
        //    //Unless we set the properties to Update, the customer will be duplicated and an error will be thrown
        //    copyCustomer = SetCustomerTable(AxdEnum_AxdEntityAction.update, customer);

        //    copyDirPerson = copyCustomer.DirParty[0] as AxdEntity_DirParty_DirPerson;

        //    //Now I'm setting the system fields that will allow AX to know which record its going to Update
        //    copyCustomer._DocumentHash = custTable._DocumentHash;
        //    copyCustomer.RecId = custTable.RecId;
        //    copyCustomer.RecIdSpecified = true;
        //    copyCustomer.RecVersion = custTable.RecVersion;
        //    copyCustomer.RecVersionSpecified = true;

        //    //Setting update action
        //    copyCustomer.action = AxdEnum_AxdEntityAction.update;
        //    copyCustomer.actionSpecified = true;

        //    //For Addresses, Names and Contacts too
        //    if (copyDirPerson != null)
        //    {
        //        #region "If person data is not null"
        //        if (dirPerson != null)
        //        {
        //            copyDirPerson.RecId = dirPerson.RecId;
        //            copyDirPerson.RecIdSpecified = true;
        //            copyDirPerson.RecVersion = dirPerson.RecVersion;
        //            copyDirPerson.RecVersionSpecified = true;

        //            //Setting update action
        //            copyDirPerson.action = AxdEnum_AxdEntityAction.update;
        //            copyDirPerson.actionSpecified = true;


        //            #region "User first and last name here. Cannot EXCEED 20 CHARACTERS  "

        //            copyDirPerson.NameAlias = customer.NameAlias;
        //            // if  name is less >20 then get 20 chars only
        //            if (customer.NameAlias != null)
        //                if (customer.NameAlias.Length > 20)
        //                    copyDirPerson.NameAlias = customer.NameAlias.Substring(0, 20);

        //            #endregion "User first and last name here. Cannot EXCEED 20 CHARACTERS  "

        //            copyDirPerson.NameSequence = customer.NameSequence;

        //            //check if the PersonName is empty or not - starts here
        //            if (copyDirPerson.PersonName != null && copyDirPerson.PersonName.Count() == 1)
        //            {
        //                copyDirPerson.PersonName[0].RecId = dirPerson.PersonName[0].RecId;
        //                copyDirPerson.PersonName[0].RecIdSpecified = true;
        //                copyDirPerson.PersonName[0].RecVersion = dirPerson.PersonName[0].RecVersion;
        //                copyDirPerson.PersonName[0].RecVersionSpecified = true;

        //                //Updating customer name
        //                copyDirPerson.PersonName[0].FirstName = customer.FirstName;
        //                copyDirPerson.PersonName[0].LastName = customer.LastName;
        //                copyDirPerson.PersonName[0].MiddleName = customer.MiddleName;

        //                //Setting update action
        //                copyDirPerson.PersonName[0].action = AxdEnum_AxdEntityAction.update;
        //                copyDirPerson.PersonName[0].actionSpecified = true;
        //                copyDirPerson.PersonName[0].updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
        //                copyDirPerson.PersonName[0].updateModeSpecified = true;
        //            }
        //            ////check if the PersonName is empty or not - ends here
        //        }
        //        #endregion "person data "

        //        if (dirPerson != null)
        //        {
        //            if (dirPerson.DirPartyPostalAddressView != null)
        //                foreach (var eachitem in dirPerson.DirPartyPostalAddressView)
        //                {
        //                    if (Convert.ToBoolean(eachitem.IsPrimary) == false)
        //                    {
        //                        eachitem.action = AxdEnum_AxdEntityAction.delete;
        //                        eachitem.actionSpecified = true;
        //                    }
        //                }
        //        }

        //        //// Check the address list is empty- if no address 
        //        if (customer.AddressList != null)
        //        {
        //            // Update Address information
        //            copyDirPerson.DirPartyPostalAddressView = UpdateAddress(dirPerson, customer).ToArray();
        //        }
        //        else
        //        {
        //            // make it null if no address in the list
        //            copyDirPerson.DirPartyPostalAddressView = null;
        //        }

        //        //// Added Anantha -Dec-12-2014 : contact list added
        //        if (customer.ContactList != null)
        //        {
        //            //Update contact information
        //            copyDirPerson.DirPartyContactInfoView = UpdateContactInfo(dirPerson, customer).ToArray();
        //        }
        //        else
        //        {
        //            // make it null if no address in the list
        //            copyDirPerson.DirPartyContactInfoView = null;
        //        }

        //        #region Update without Copy - NOT ACTIVE

        //        #endregion //Commented

        //        copyCustomer.DirParty[0] = copyDirPerson;
        //    }
        //    return copyCustomer;
        //}
        #endregion "Update Customer"

        // Read Customer 
        public string GetCustomer(string accountNumbers)
        {
            log.Info("Enter GetCustomer");
            QueryServiceClient client = new QueryServiceClient();

            try
            {

                DataSet dataSet;

                Paging paging = null;

                QueryMetadata query;

                QueryDataSourceMetadata customerDataSource;

                query = new QueryMetadata();

                // Set the properties of the query."grantFD

                query.QueryType = QueryType.Join;


                query.DataSources = new QueryDataSourceMetadata[1];


                // Set the properties of the Customers data source.


                customerDataSource = new QueryDataSourceMetadata();

                customerDataSource.Name = "Customers";
                customerDataSource.Enabled = true;
                customerDataSource.Table = "CustTable";

                //Add the data source to the query.
                query.DataSources[0] = customerDataSource;

                // Setting DynamicFieldList property to false so I can specify only a few fields
                customerDataSource.DynamicFieldList = true;

                //query.OrderByFields[0] = sort;

                QueryDataRangeMetadata rangeData = new QueryDataRangeMetadata();

                rangeData.Name = "AccountNum";
                rangeData.FieldName = "AccountNum";
                rangeData.TableName = "CustTable"; ;
                rangeData.Value = accountNumbers;

                rangeData.RangeStatus = RangeStatus.Open;
                rangeData.Enabled = true;

                customerDataSource.Ranges = new QueryDataRangeMetadata[1];
                customerDataSource.Ranges[0] = rangeData;


                // implemented credentials starts
                // implemented credentials starts
                string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

                if (client.ChannelFactory.Credentials != null)
                {
                    client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
                    client.ChannelFactory.Credentials.UserName.Password = servicePassword;
                    log.Info("GetCustomer() entered into credentials");
                }
                // implemented credentials ends

                log.Debug("GetCustomer() Before Client call");
                dataSet = client.ExecuteQuery(query, ref paging); client.Close();
                log.Debug("GetCustomer() After Client call :Success ");

                string xmlData = dataSet.GetXml();
                log.Info("Exit: GetCustomer() Converted Customer data into XML");

                return xmlData;
            }
            catch (TimeoutException exception)
            {
                log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
                return "Error: " + exception.Message;
            }
            catch (CommunicationException exception)
            {
                log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
                return "Error: " + exception.Message;
            }
            catch (Exception exception)
            {
                log.Error("GetCustomer() Error : " + exception.Message);
                client.Abort();
                return "Error: " + exception.Message;
            }
        }

        // Read Customer 
        public Customer GetCustomerObject(string accountNumber)
        {
            log.Info("Enter GetCustomer");
            QueryServiceClient client = new QueryServiceClient();
            Customer Customer = new Customer();
            try
            {

                DataSet dataSet;

                Paging paging = null;

                QueryMetadata query;

                QueryDataSourceMetadata customerDataSource;

                query = new QueryMetadata();

                // Set the properties of the query."grantFD

                query.QueryType = QueryType.Join;


                query.DataSources = new QueryDataSourceMetadata[1];


                // Set the properties of the Customers data source.


                customerDataSource = new QueryDataSourceMetadata();

                customerDataSource.Name = "Customers";
                customerDataSource.Enabled = true;
                customerDataSource.Table = "CustTable";

                //Add the data source to the query.
                query.DataSources[0] = customerDataSource;

                // Setting DynamicFieldList property to false so I can specify only a few fields
                customerDataSource.DynamicFieldList = true;

                //query.OrderByFields[0] = sort;

                QueryDataRangeMetadata rangeData = new QueryDataRangeMetadata();

                rangeData.Name = "AccountNum";
                rangeData.FieldName = "AccountNum";
                rangeData.TableName = "CustTable"; ;
                rangeData.Value = accountNumber;

                rangeData.RangeStatus = RangeStatus.Open;
                rangeData.Enabled = true;

                customerDataSource.Ranges = new QueryDataRangeMetadata[1];
                customerDataSource.Ranges[0] = rangeData;


                // implemented credentials starts
                // implemented credentials starts
                string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

                if (client.ChannelFactory.Credentials != null)
                {
                    client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
                    client.ChannelFactory.Credentials.UserName.Password = servicePassword;
                    log.Info("GetCustomer() entered into credentials");
                }
                // implemented credentials ends

                log.Debug("GetCustomer() Before Client call");
                dataSet = client.ExecuteQuery(query, ref paging);
                client.Close();
                log.Debug("GetCustomer() After Client call :Success ");

                //string xmlData = dataSet.GetXml();
                //log.Info("Exit: GetCustomer() Converted Customer data into XML");


                if (dataSet.Tables.Count > 0)
                {
                    DataTable dtCustomer = dataSet.Tables[0];
                    if (dtCustomer.Rows.Count > 0)
                    {
                        /*  Modified By Ananthakumar - Updated Chris Changes - starts here
           Customer.FirstName = dtCustomer.Rows[0]["FirstName"].ToString();
                           Customer.LastName = dtCustomer.Rows[0]["LastName"].ToString();
                           Customer.MiddleName = dtCustomer.Rows[0]["MiddleName"].ToString();
                           Customer.AccountNumber = dtCustomer.Rows[0]["AccountNumber"].ToString();
                           Customer.VendorID = dtCustomer.Rows[0]["VendAccount"].ToString();
                      
        Modified By Ananthakumar - Updated Chris Changes - ends here 
      */

                        var Party = dtCustomer.Rows[0]["Party"].ToString();
                        var CustName = GetParty(Party);
                        Customer.FirstName = CustName.FirstName;
                        Customer.LastName = CustName.LastName;
                        Customer.MiddleName = CustName.MiddleName;
                        Customer.AccountNumber = dtCustomer.Rows[0]["AccountNum"].ToString();
                        Customer.VendorID = dtCustomer.Rows[0]["VendAccount"].ToString();

                    }
                }

                //  return Customer;
            }
            catch (TimeoutException exception)
            {
                log.Error(string.Format("Error GetCustomerObject(): {0}", exception.Message));
                log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
                // return "Error: " + exception.Message;
            }
            catch (CommunicationException exception)
            {
                log.Error(string.Format("Error GetCustomerObject(): {0}", exception.Message));
                log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
                //   return "Error: " + exception.Message;
            }
            catch (Exception exception)
            {
                log.Error("GetCustomerObject() Error : " + exception.Message);
                client.Abort();
                //return "Error: " + exception.Message;
            }

            return Customer;
        }

        private static Customer GetParty(string recId)
        {
            QueryServiceClient client = new QueryServiceClient();
            Customer c = new Customer();
            try
            {
                DataSet dataSet;
                Paging paging = null;

                QueryMetadata query;
                QueryDataSourceMetadata partyDataSource;

                query = new QueryMetadata();

                // Set the properties of the query.
                query.QueryType = QueryType.Join;
                query.AllowCrossCompany = true;
                query.DataSources = new QueryDataSourceMetadata[1];

                // Set the properties of the Customers data source.
                partyDataSource = new QueryDataSourceMetadata();
                partyDataSource.Name = "DirPartyName";
                partyDataSource.Enabled = true;
                partyDataSource.FetchMode = FetchMode.OneToOne;
                partyDataSource.Table = "DirPersonName";
                // Setting DynamicFieldList property to true returns all fields.
                partyDataSource.DynamicFieldList = true;

                QueryDataRangeMetadata rangeData = new QueryDataRangeMetadata();

                rangeData.Name = "DirParty";
                rangeData.FieldName = "Person";
                rangeData.TableName = "DirPersonName";
                rangeData.Value = recId;

                rangeData.RangeStatus = RangeStatus.Open;
                rangeData.Enabled = true;

                partyDataSource.Ranges = new QueryDataRangeMetadata[1];
                partyDataSource.Ranges[0] = rangeData;
                //Add the data source to the query.
                query.DataSources[0] = partyDataSource;
                string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

                if (client.ChannelFactory.Credentials != null)
                {
                    client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
                    client.ChannelFactory.Credentials.UserName.Password = servicePassword;
                    log.Info("GetParty() entered into credentials");
                }
                // Execute the query you just created.
                dataSet = client.ExecuteQuery(query, ref paging);

                if (dataSet.Tables.Count > 0)
                {
                    DataTable dtCustomer = dataSet.Tables[0];
                    if (dtCustomer.Rows.Count > 0)
                    {
                        c.FirstName = dtCustomer.Rows[0]["FirstName"].ToString();
                        c.LastName = dtCustomer.Rows[0]["LastName"].ToString();
                        c.MiddleName = dtCustomer.Rows[0]["MiddleName"].ToString();
                    }
                }
            }
            catch (TimeoutException exception)
            {
                log.Error(string.Format("Error TimeOutGetGetParty(): {0}", exception.Message));
                log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
                // return "Error: " + exception.Message;
            }
            catch (CommunicationException exception)
            {
                log.Error(string.Format("Error GetPArty(): {0}", exception.Message));
                log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
                //   return "Error: " + exception.Message;
            }
            catch (Exception exception)
            {
                log.Error("GetPArty() Error : " + exception.Message);
                client.Abort();
                //return "Error: " + exception.Message;
            }
            return c;

        }


    }
}