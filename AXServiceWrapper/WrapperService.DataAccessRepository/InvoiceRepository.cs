﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 12/17/2014
'Description: 
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'Ananthakumar Subramani  Jun-02                 getting invoices along with payment status from ax service.
'
'******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using log4net;
using log4net.Config;
using WrapperService.BusinessEntities;
using WrapperService.DataAccessRepository.Interfaces;
using WrapperService.DataAccessRepository.InvoiceQueryServiceReference;
using WrapperService.DataAccessRepository.InvoiceServiceReference;
using PaymentStatusServiceRef = WrapperService.DataAccessRepository.PaymentStatusServiceReference;
using SystemSvcModel = System.ServiceModel;
using InvoiceCancelService = WrapperService.DataAccessRepository.InvoiceCancelService;
using System.ServiceModel;
// Uses to access the AXD tables.

//[assembly: XmlConfigurator(Watch = true)] // uses to track the actions into the logfile / DB based on the config settings

namespace WrapperService.DataAccessRepository
{
    public class InvoiceRepository : IInvoiceRepository
    {
        //Uses to tracking the actions 
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        //private const string ServiceUserid = "dynamic@VentechSolutions.com";
        //private const string ServicePassword = "welcome@123";
        private const string LanguageId = "en-us";

        string Error = string.Empty;
        private IInvoiceRepository _invoiceRepository;
        public InvoiceRepository(IInvoiceRepository iInvoiceRepository)
        {
            XmlConfigurator.Configure();
            this._invoiceRepository = iInvoiceRepository;
        }
        public InvoiceRepository()
        {
            XmlConfigurator.Configure();
        }
        #region "Creating Invoice methods"
        /// <summary>
        /// Creating an invoice
        /// </summary>
        /// <param name="invoice"></param>
        /// <returns></returns>
        public ValidationDetail CreateInvoice(Invoice invoice)
        {
            //Initialize validation detail class to store final oputput
            ValidationDetail validationDetail = new ValidationDetail();
            try
            {

                List<string> errorList = new List<string>();

                List<Invoice> invoiceList = new List<Invoice>();
                invoiceList.Add(invoice);

                log.Info("Enter CreateInvoice()");

                //call AX - starts here
                errorList = AddInvoice(invoiceList);

                //check if any errors 
                if (errorList.Any())
                {
                    validationDetail.ErrorList = errorList.ToList();
                    validationDetail.Result = false;
                }
                else
                {
                    validationDetail.Result = true;
                }
                //call AX - ends here

                log.Info("Exit CreateInvoice()");
            }
            catch (Exception ex)
            {
                log.Error("Exit CreateInvoice()" + ex.Message);
                validationDetail.Result = false;
                validationDetail.ErrorList = new List<string>() { " Error creating while generating invoices.." };
            }
            return validationDetail;
        }

        /// <summary>
        /// Point of Entry to create Free Text Invoices
        /// </summary>
        /// <param name="custInvoiceTableList"> A list of invoices do create</param>
        /// <returns>Returns an EntityKey with all the Natural or Surrogate Keys of the records created</returns>
        List<string> AddInvoice(List<Invoice> custInvoiceTableList)
        {
            log.Info("Entering AddInvoice()");

            //we get the key elements 
            List<string> retVal = new List<string>();

            AxdFreeTextInvoice axdFreeTextInvoice = new AxdFreeTextInvoice();
            FreeTextInvoiceServiceClient client = new FreeTextInvoiceServiceClient();
            try
            {

                log.Info("AddInvoice(): before create call");
                // implemented credentials starts
                // implemented credentials starts
                string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));


                if (client.ChannelFactory.Credentials != null)
                {
                    client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
                    client.ChannelFactory.Credentials.UserName.Password = servicePassword;
                    log.Info("AddInvoice() entered into credentials");
                }
                // implemented credentials ends
                axdFreeTextInvoice.CustInvoiceTable = NewCustInvoiceEntityList(custInvoiceTableList).ToArray();

                //set max value for behaviour
                client.Endpoint.Behaviors.Add(new MaxFaultSizeBehavior(1024000));

                log.Info("AddInvoice(): Before calling client.create");
                //calling AX to create invoice
                var results = client.create(null, axdFreeTextInvoice);
                log.Info("AddInvoice(): Invoice added successfully");
                client.Close();
            }

            catch (SystemSvcModel.FaultException<AifFault> aifFault)
            {
                //retVal.Add(string.Join(Environment.NewLine,
                //    aifFault.Detail.InfologMessageList.Select(i => i.Message)));
                retVal.Add("ERROR from AX: CreateInvoice(): " + string.Join(Environment.NewLine, aifFault.Message));
                log.Error("AddInvoice(): Error: " + aifFault.Message);
            }
            catch (TimeoutException exception)
            {
                log.Error(string.Format("ERROR from AX: CreateInvoice(): {0}", exception.Message));
                retVal.Add("ERROR from AX: " + exception.Message);
                log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }
            catch (CommunicationException exception)
            {
                log.Error(string.Format("ERROR from AX: CreateInvoice(): {0}", exception.Message));
                retVal.Add("ERROR from AX: " + exception.Message);
                log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }

            catch (Exception exception)
            {
                log.Error("AddInvoice(): Error: " + exception.Message);
                retVal.Add(exception.Message);
                client.Abort();
                while (exception.InnerException != null)
                {
                    exception = exception.InnerException;
                    retVal.Add(exception.Message);
                }
            }

            log.Info("Exit AddInvoice()");

            return retVal;
        }

        /// <summary>
        /// Returns a configured List of CustInvoiceTable (Header of the Invoice) Entity
        /// </summary>
        /// <param name="invoiceList"> For each item on the list, one instance of the entity is added to the returning list</param>
        /// <returns>A list with all entities parsed from the input parameter</returns>
        private List<AxdEntity_CustInvoiceTable> NewCustInvoiceEntityList(List<Invoice> invoiceList)
        {
            List<AxdEntity_CustInvoiceTable> custInvoiceEntityList = new List<AxdEntity_CustInvoiceTable>();

            log.Info("Enter NewCustInvoiceEntityList()");
            foreach (Invoice eachInvoice in invoiceList)
            {

                //Set invocie data in temp table
                AxdEntity_CustInvoiceTable tempInvoiceTable = SetCustInvoiceTable(eachInvoice);

                //Get Invoice line item from the current invoice and assigns it
                List<AxdEntity_CustInvoiceLine> custInvoiceLineEntityList = eachInvoice.InvoiceLineList.Select(eachInvoiceLine => SetCustInvoiceLineEntity(eachInvoiceLine)).ToList();

                //Assigning invoice line items in the list
                if (custInvoiceLineEntityList != null && custInvoiceLineEntityList.Count > 0)
                    tempInvoiceTable.CustInvoiceLine = custInvoiceLineEntityList.ToArray();

                //Add the current invoice into invoice list
                custInvoiceEntityList.Add(tempInvoiceTable);
            }
            log.Info("Exit NewCustInvoiceEntityList()");

            return custInvoiceEntityList;
        }

        /// <summary>
        /// Creates a single instance of CustInvoiceTable entity
        /// </summary>
        /// <param name="invoice"></param>
        /// <returns>A single instance of AxdEntity for the Free-Text Invoice Header</returns>
        private AxdEntity_CustInvoiceTable SetCustInvoiceTable(Invoice invoice)
        {
            AxdEntity_CustInvoiceTable ret = null;
            log.Info("Enter SetCustInvoiceTable()");

            // Set data in invoice table from  invoice BE object 

            ret = new AxdEntity_CustInvoiceTable()
            {
                CustomerRef = invoice.CustomerReference,
                InvoiceDate = invoice.InvoiceDate,
                InvoiceDateSpecified = invoice.InvoiceDate.HasValue,
                OrderAccount = invoice.OrderAccount,
                InvoiceAccount = invoice.InvoiceAccount,
                ManualInvoiceId = invoice.InvoiceId,
                PurchOrderFormNum = invoice.PurchaseOrderFormNumber,
                //Default Fields
                CurrencyCode = invoice.CurrencyCode,
                LanguageId = LanguageId,
                VEN_LetterDate = invoice.LetterDate,
                VEN_LetterDateSpecified = invoice.LetterDate.HasValue,

                VEN_AutoPost = (invoice.IsAutoPost) ? AxdExtType_VEN_AutoPost.Yes : AxdExtType_VEN_AutoPost.No,
                VEN_AutoPostSpecified = true,
                //Set Invoice default dimension 
                DefaultDimension = SetDimensionAttributeValue(invoice.InvoiceDefaultDimensionList)
            };    log.Info("Exit SetCustInvoiceTable()");
        

            return ret;
        }

        /// <summary>
        /// Creates a single instance of CustInvoiceLine entity
        /// </summary>
        /// <param name="invoiceLine">A List of Invoice lines PER header. </param>
        /// <returns>A List of CustInvoiceLines entities </returns>
        private AxdEntity_CustInvoiceLine SetCustInvoiceLineEntity(InvoiceLine invoiceLine)
        {
            log.Info("Enter SetCustInvoiceLineEntity()");
            AxdEntity_CustInvoiceLine ret = null;
            // creating and assigning the invoice values  for invoice line
            ret = new AxdEntity_CustInvoiceLine()
            {
                VEN_BillableItemId = invoiceLine.VEN_BillableItemId,
                Quantity = invoiceLine.Quantity,
                QuantitySpecified = true,

                Description = invoiceLine.Description,

                UnitPrice = invoiceLine.UnitPrice,
                UnitPriceSpecified = true,

                AmountCur = invoiceLine.Amount,
                AmountCurSpecified = true,

                VEN_ARCodeId = invoiceLine.ARCode,

                VEN_TransactionId = invoiceLine.ARTrans,
                VEN_TransactionIdSpecified = invoiceLine.ARTrans.HasValue,

                VEN_InvoiceComments = invoiceLine.InvoiceComments,

                //Ledger dimension
                LedgerDimension = SetMainAccount(invoiceLine.MainAccount),

                //Ledger dimension attribute value set
                DefaultDimension = SetDimensionAttributeValue(invoiceLine.InvoiceDefaultDimensionList),
            };

            log.Info("Exit SetCustInvoiceLineEntity()");
            return ret;
        }

        /// <summary>
        /// Handles the financial dimension set-up. Since it requires a Name/Value pair to be translated to a RecId, special treatment is required.
        /// </summary>
        /// <param name="invoiceDefaultDimensionList"> A list of dimensions to include on LINE or HEADER</param>
        /// <returns>DimensionAttributeValueSet is an array of singles instances of dimensions</returns>
        private AxdType_DimensionAttributeValueSet SetDimensionAttributeValue(List<DefaultDimension> invoiceDefaultDimensionList)
        {
            log.Info("Enter SetDimensionAttributeValue()");
            AxdType_DimensionAttributeValueSet ret = null;

            //checking if it is not null and also the count is >0
            if (invoiceDefaultDimensionList != null && invoiceDefaultDimensionList.Count > 0)
            {
                ret = new AxdType_DimensionAttributeValueSet();
                //Set the name and value pair in this.
                var dimensionEntityList = invoiceDefaultDimensionList.Select(invoiceDefaultDimension => new AxdType_DimensionAttributeValue() { Name = invoiceDefaultDimension.Name, Value = invoiceDefaultDimension.Value }).ToList();

                ret.Values = dimensionEntityList.ToArray();
            }
            log.Info("Exit SetDimensionAttributeValue()");

            return ret;
        }

        /// <summary>
        /// Set Main Account for New ledger dimension
        /// </summary>
        /// <param name="mainAccount"></param>
        /// <returns></returns>
        private AxdType_DefaultAccount SetMainAccount(string mainAccount)
        {
            log.Info("Enter SetMainAccount()");
            AxdType_DefaultAccount defaultAccount = null;

            //check if the sting is null or empty
            if (!string.IsNullOrEmpty(mainAccount))
                defaultAccount = new AxdType_DefaultAccount() { MainAccount = mainAccount };

            log.Info("Exit SetMainAccount()");
            return defaultAccount;
        }
        #endregion "Creating Invoice"

        #region "Get Customer Invoices"


        /// <summary>
        /// Read invoice items for each customer
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public List<InvoiceDetails> GetInvoices_withViews(string accountNumber, string status = null)
        {
            log.Info("Enter GetInvoiceDetailss");
            List<InvoiceDetails> InvoiceDetailsList = new List<InvoiceDetails>();
            InvoiceDetails InvoiceDetails = null;

            // DataSet dsFinalList = RunQueryService(accountNumber, status);
            //  DataSet dsFinalList = RunQueryServiceGetInvoices(accountNumber, status);
            DataSet dsFinalList = GetInvoicesfromAXViews(accountNumber, status);

            if (dsFinalList.Tables.Count > 0)
            {
                #region " Looping all invoices"
                foreach (DataRow dr in dsFinalList.Tables[0].Rows)
                {
                    InvoiceDetails = new InvoiceDetails();
                    InvoiceDetails.InvoiceAccount = dr["InvoiceAccount"].ToString();
                    InvoiceDetails.InvoiceDate = dr["InvoiceDate"].ToString();
                    InvoiceDetails.DueDate = dr["DueDate"].ToString();
                    InvoiceDetails.InvoiceAmount = dr["InvoiceAmount"].ToString();
                    InvoiceDetails.InvoiceId = dr["InvoiceId"].ToString();
                    InvoiceDetails.PaymentStatus = dr["InvoiceStatus"].ToString();

                    #region "commented"
                    //Commented By Ananthakumar --Feb-20-2015 Reason: No need to bring all fields from AX - starts here


                    //InvoiceDetails.CustGroup = dr["CustGroup"].ToString();
                    //InvoiceDetails.RefNum = dr["RefNum"].ToString();
                    //InvoiceDetails.SalesId = dr["SalesId"].ToString();
                    //InvoiceDetails.OrderAccount = dr["OrderAccount"].ToString();
                    // InvoiceDetails.CashDisc = dr["CashDisc"].ToString();
                    //InvoiceDetails.CashDiscDate = dr["CashDiscDate"].ToString();
                    //InvoiceDetails.Qty = dr["Qty"].ToString();
                    //InvoiceDetails.Volume = dr["Volume"].ToString();
                    //InvoiceDetails.Weight = dr["Weight"].ToString();
                    //InvoiceDetails.SumLineDisc = dr["SumLineDisc"].ToString();
                    //InvoiceDetails.EndDisc = dr["EndDisc"].ToString();
                    //InvoiceDetails.CurrencyCode = dr["CurrencyCode"].ToString();
                    //InvoiceDetails.ExchRate = dr["ExchRate"].ToString();
                    //InvoiceDetails.LedgerVoucher = dr["LedgerVoucher"].ToString();
                    //InvoiceDetails.Updated = dr["Updated"].ToString();
                    //InvoiceDetails.OnAccountAmount = dr["OnAccountAmount"].ToString();
                    //InvoiceDetails.TaxPrintOnInvoice = dr["TaxPrintOnInvoice"].ToString();
                    //InvoiceDetails.Listcode = dr["Listcode"].ToString();
                    //InvoiceDetails.DocumentNum = dr["DocumentNum"].ToString();
                    //InvoiceDetails.DocumentDate = dr["DocumentDate"].ToString();
                    //InvoiceDetails.CashDiscPercent = dr["CashDiscPercent"].ToString();
                    //InvoiceDetails.IntrastatDispatch = dr["IntrastatDispatch"].ToString();
                    //InvoiceDetails.DeliveryName = dr["DeliveryName"].ToString();
                    //InvoiceDetails.EnterpriseNumber = dr["EnterpriseNumber"].ToString();
                    //InvoiceDetails.PurchaseOrder = dr["PurchaseOrder"].ToString();
                    //InvoiceDetails.DlvTerm = dr["DlvTerm"].ToString();
                    //InvoiceDetails.DlvMode = dr["DlvMode"].ToString();
                    //InvoiceDetails.Payment = dr["Payment"].ToString();
                    //InvoiceDetails.CashDiscCode = dr["CashDiscCode"].ToString();
                    //InvoiceDetails.InvoiceRoundOff = dr["InvoiceRoundOff"].ToString();
                    //InvoiceDetails.SumMarkup = dr["SumMarkup"].ToString();
                    //InvoiceDetails.CovStatus = dr["CovStatus"].ToString();
                    //InvoiceDetails.ReturnItemNum = dr["ReturnItemNum"].ToString();
                    //InvoiceDetails.PostingProfile = dr["PostingProfile"].ToString();
                    //InvoiceDetails.Backorder = dr["Backorder"].ToString();
                    //InvoiceDetails.Prepayment = dr["Prepayment"].ToString();
                    //InvoiceDetails.TaxGroup = dr["TaxGroup"].ToString();
                    //InvoiceDetails.TaxItemGroup = dr["TaxItemGroup"].ToString();
                    //InvoiceDetails.TaxSpecifyByLine = dr["TaxSpecifyByLine"].ToString();
                    //InvoiceDetails.EInvoiceLineSpecific = dr["EInvoiceLineSpecific"].ToString();
                    //InvoiceDetails.OneTimeCustomer = dr["OneTimeCustomer"].ToString();
                    //InvoiceDetails.PaymentSched = dr["PaymentSched"].ToString();
                    //InvoiceDetails.SumTax = dr["SumTax"].ToString();
                    //InvoiceDetails.SalesType = dr["SalesType"].ToString();
                    //InvoiceDetails.EInvoiceAccountCode = dr["EInvoiceAccountCode"].ToString();
                    //InvoiceDetails.InterCompanyPosted = dr["InterCompanyPosted"].ToString();
                    //InvoiceDetails.ParmId = dr["ParmId"].ToString();
                    //InvoiceDetails.ReturnReasonCodeId = dr["ReturnReasonCodeId"].ToString();
                    //InvoiceDetails.EUSalesList = dr["EUSalesList"].ToString();
                    //InvoiceDetails.ExchRateSecondary = dr["ExchRateSecondary"].ToString();
                    //InvoiceDetails.Triangulation = dr["Triangulation"].ToString();
                    //InvoiceDetails.CustomerRef = dr["CustomerRef"].ToString();
                    //InvoiceDetails.VATNum = dr["VATNum"].ToString();
                    //InvoiceDetails.numberSequenceGroup = dr["numberSequenceGroup"].ToString();
                    //InvoiceDetails.InclTax = dr["InclTax"].ToString();
                    //InvoiceDetails.Log = dr["Log"].ToString();
                    //InvoiceDetails.PaymDayId = dr["PaymDayId"].ToString();
                    //InvoiceDetails.InvoicingName = dr["InvoicingName"].ToString();
                    //InvoiceDetails.GiroType = dr["GiroType"].ToString();
                    //InvoiceDetails.ContactPersonId = dr["ContactPersonId"].ToString();
                    //InvoiceDetails.SalesOriginId = dr["SalesOriginId"].ToString();
                    //InvoiceDetails.BillofLadingId = dr["BillofLadingId"].ToString();
                    //InvoiceDetails.inventLocationId = dr["inventLocationId"].ToString();
                    //InvoiceDetails.FixedDueDate = dr["FixedDueDate"].ToString();
                    //InvoiceDetails.InvoiceAmountMST = dr["InvoiceAmountMST"].ToString();
                    //InvoiceDetails.InvoiceRoundOffMST = dr["InvoiceRoundOffMST"].ToString();
                    //InvoiceDetails.SumMarkupMST = dr["SumMarkupMST"].ToString();
                    //InvoiceDetails.SumLineDiscMST = dr["SumLineDiscMST"].ToString();
                    //InvoiceDetails.EndDiscMST = dr["EndDiscMST"].ToString();
                    //InvoiceDetails.SalesBalanceMST = dr["SalesBalanceMST"].ToString();
                    //InvoiceDetails.SUmTaxMST = dr["SUmTaxMST"].ToString();
                    //InvoiceDetails.PrintMgmtSiteId = dr["PrintMgmtSiteId"].ToString();
                    //InvoiceDetails.ReturnStatus = dr["ReturnStatus"].ToString();
                    //InvoiceDetails.InterCompanyCompanyId = dr["InterCompanyCompanyId"].ToString();
                    //InvoiceDetails.InterCompanyPurchId = dr["InterCompanyPurchId"].ToString();
                    //InvoiceDetails.PrintedOriginals = dr["PrintedOriginals"].ToString();
                    //InvoiceDetails.Proforma = dr["Proforma"].ToString();
                    //InvoiceDetails.RCSalesList_UK = dr["RCSalesList_UK"].ToString();
                    //InvoiceDetails.ReverseCharge_UK = dr["ReverseCharge_UK"].ToString();
                    //InvoiceDetails.DeliveryPostalAddress = dr["DeliveryPostalAddress"].ToString();
                    //InvoiceDetails.InvoicePostalAddress = dr["InvoicePostalAddress"].ToString();
                    //InvoiceDetails.SourceDocumentHeader = dr["SourceDocumentHeader"].ToString();
                    //InvoiceDetails.DefaultDimension = dr["DefaultDimension"].ToString();
                    //InvoiceDetails.BankLCExportLine = dr["BankLCExportLine"].ToString();
                    //InvoiceDetails.WorkerSalesTaker = dr["WorkerSalesTaker"].ToString();
                    //InvoiceDetails.ReversedRecId = dr["ReversedRecId"].ToString();
                    //InvoiceDetails.ReceiptDateConfirmed_ES = dr["ReceiptDateConfirmed_ES"].ToString();
                    //InvoiceDetails.PaymId = dr["PaymId"].ToString();
                    //InvoiceDetails.TaxInvoiceSalesId = dr["TaxInvoiceSalesId"].ToString();
                    //InvoiceDetails.IntrastatFulfillmentDate_HU = dr["IntrastatFulfillmentDate_HU"].ToString();
                    //InvoiceDetails.CashDiscBaseDate = dr["CashDiscBaseDate"].ToString();
                    //InvoiceDetails.IsCorrection = dr["IsCorrection"].ToString();
                    //InvoiceDetails.ReasonTableRef = dr["ReasonTableRef"].ToString();
                    //InvoiceDetails.SourceDocumentLine = dr["SourceDocumentLine"].ToString();
                    //InvoiceDetails.TransportationDocument = dr["TransportationDocument"].ToString();
                    //InvoiceDetails.DirectDebitmandate = dr["DirectDebitmandate"].ToString();
                    //InvoiceDetails.modifiedDateTime = dr["modifiedDateTime"].ToString();
                    //InvoiceDetails.createdDateTime = dr["createdDateTime"].ToString();
                    //InvoiceDetails.dEL_createdTime = dr["dEL_createdTime"].ToString();
                    //InvoiceDetails.createdBy = dr["createdBy"].ToString();
                    //InvoiceDetails.dataAreaId = dr["dataAreaId"].ToString();
                    //InvoiceDetails.recVersion = dr["recVersion"].ToString();
                    //InvoiceDetails.Partition = dr["Partition"].ToString();
                    //InvoiceDetails.RecId = dr["RecId"].ToString();

                    //Commented By Ananthakumar --Feb-20-2015 Reason: No need to bring all fields from AX - ends here
                    #endregion "commented"

                    InvoiceDetailsList.Add(InvoiceDetails);
                }
                #endregion " Looping all invoices"
            }
            log.Info("Exit GetInvoiceDetailss");

            return InvoiceDetailsList;
        }

        /// <summary>
        /// Read invoice items for each customer
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public List<InvoiceDetails> GetInvoices(string accountNumber, string status = null)
        {
            List<InvoiceDetails> InvoiceDetailsList = new List<InvoiceDetails>();
            InvoiceDetails InvoiceDetails = null;
            try
            {
                log.Info("Enter GetInvoices");

                // calling AX service in the below method to get invoice details

                List<PaymentStatusServiceRef.InvoiceStatusList> InvoiceStatusList = GetInvoicesfromAXService(accountNumber, status);

                //if (dsFinalList.Tables.Count > 0)
                //{
                #region " Looping all invoices

               // if (InvoiceStatusList.Count > 0)
                    foreach (var eachinvoice in InvoiceStatusList)
                    {
                        InvoiceDetails = new InvoiceDetails();
                        InvoiceDetails.InvoiceAccount = eachinvoice.InvoiceAccount;
                        InvoiceDetails.InvoiceDate = Convert.ToString(eachinvoice.InvoiceDate);
                        InvoiceDetails.DueDate = Convert.ToString(eachinvoice.DueDate);
                        InvoiceDetails.InvoiceAmount = Convert.ToString(eachinvoice.InvoiceAmount);
                        InvoiceDetails.InvoiceId = eachinvoice.InvoiceID;
                        InvoiceDetails.PaymentStatus = eachinvoice.PaymentStatus;
                        InvoiceDetails.Status = true;
                        InvoiceDetails.RemainingAmount = eachinvoice.RemianingAmount.ToString();
                        InvoiceDetailsList.Add(InvoiceDetails);
                    }
                //else
                //{
                //    InvoiceDetails = new InvoiceDetails();
                //    InvoiceDetails.InvoiceAccount = accountNumber;
                //    InvoiceDetails.Status = false;
                //    InvoiceDetails.ErrorMessage = Error;

                //    InvoiceDetailsList.Add(InvoiceDetails);
                //}
                #endregion " Looping all invoices"
                //  }
                log.Info("Exit GetInvoiceDetailss");
            }
            catch (Exception exception)
            {
                log.Error(string.Format("Error GetInvoiceDetailss(): {0}", exception.Message));
                log.Error("-------------------------------------------------------------------------------------------");

               // InvoiceDetailsList.Add(AssignInvoiceErrorMessage(accountNumber, ex.Message));
            }
            return InvoiceDetailsList;
        }

        /// <summary>
        /// Read invoice items for each customer
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public List<CustomerInvoiceListDetails> GetInvoiceDetails(List<string> BillabaleItemIDList)
        {
            var billableId = string.Empty;
            log.Info("Enter GetInvoiceDetails");
            List<CustomerInvoiceListDetails> InvoiceDetailsList = new List<CustomerInvoiceListDetails>();
            PaymentStatusServiceRef.VEN_GetPaymentStatusServiceClient client = new PaymentStatusServiceRef.VEN_GetPaymentStatusServiceClient();
            try
            {

                // implemented credentials starts
                string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));
                if (client.ChannelFactory.Credentials != null)
                {
                    client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
                    client.ChannelFactory.Credentials.UserName.Password = servicePassword;
                    log.Info("GetInvoiceDetails() entered into credentials");
                }
                //  List<string> ids = new List<string> { "10021", "10022", "10026" };
                //calling ax service to get invoice details 
                log.Info("GetInvoiceDetails() before calling GetBillableIDStatus");
                var response = client.GetBillableIDStatus(null, BillabaleItemIDList);
                log.Info("GetInvoiceDetails() after called GetBillableIDStatus");
                foreach (var eachinvoice in response)
                {
                    CustomerInvoiceListDetails InvoiceDetails = new CustomerInvoiceListDetails();
                    InvoiceDetails.BillableItemId = eachinvoice.billableItemId;
                    billableId = eachinvoice.billableItemId;
                    InvoiceDetails.InvoiceAccount = eachinvoice.InvoiceAccount;
                    InvoiceDetails.InvoiceDate = Convert.ToString(eachinvoice.InvoiceDate);
                    InvoiceDetails.DueDate = Convert.ToString(eachinvoice.DueDate);
                    InvoiceDetails.InvoiceAmount = Convert.ToString(eachinvoice.InvoiceAmount);
                    InvoiceDetails.InvoiceId = eachinvoice.InvoiceID;
                    InvoiceDetails.PaymentStatus = eachinvoice.PaymentStatus;
                    InvoiceDetails.InvoiceStatus = eachinvoice.InvoiceStatus;
                    InvoiceDetails.Status = true;
                    InvoiceDetailsList.Add(InvoiceDetails);

                }

                //  dsReturnSet = client.ExecuteQuery(query, ref paging);
                client.Close();

                log.Debug("check unmatch GetInvoiceDetails");

                //Querying unmatched records to send empty data along with billable item id
                var queryUnmatchedRecords = (from eachBillabaleItemID in BillabaleItemIDList
                                             join eachAddressList in InvoiceDetailsList on eachBillabaleItemID equals eachAddressList.BillableItemId
                                                 into combinedResult
                                             from f in combinedResult.DefaultIfEmpty()

                                             select new
                                             {
                                                 eachBillabaleItemID = eachBillabaleItemID,
                                                 MissingProd = (f == null ? string.Empty : f.BillableItemId)

                                             }).Where(a => a.MissingProd == string.Empty);

                var unmatachedBillableIds = queryUnmatchedRecords.ToList();
                foreach (var each in unmatachedBillableIds)
                {
                    CustomerInvoiceListDetails InvoiceDetails = new CustomerInvoiceListDetails();
                    InvoiceDetails.BillableItemId = each.eachBillabaleItemID;
                    billableId = each.eachBillabaleItemID;
                    InvoiceDetails.Status = true;
                    InvoiceDetailsList.Add(InvoiceDetails);
                }

                return InvoiceDetailsList;

            }
            catch (TimeoutException exception)
            {
               // InvoiceDetailsList.AddRange(AssignErrorMessage(BillabaleItemIDList, exception.Message));
                log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message)); 
                log.Error("-------------------------------------------------------------------------------------------");

                client.Abort();
            }
            catch (CommunicationException exception)
            {
               // InvoiceDetailsList.AddRange(AssignErrorMessage(BillabaleItemIDList, exception.Message));
                log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message)); log.Error("-------------------------------------------------------------------------------------------");

                client.Abort();
            }
            catch (Exception exception)
            {
             //   InvoiceDetailsList.AddRange(AssignErrorMessage(BillabaleItemIDList, exception.Message));
                log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                log.Error("-------------------------------------------------------------------------------------------");

                client.Abort();
            }
            log.Info("Exit GetInvoicesfromAXService()");

            return InvoiceDetailsList;
        }
        /// <summary>
        /// assign empty details with status=false with error message if error
        /// </summary>
        /// <param name="BillabaleItemIDList"></param>
        /// <param name="Message"></param>
        /// <returns></returns>
        List<CustomerInvoiceListDetails> AssignErrorMessage(List<string> BillabaleItemIDList, string Message)
        {

            //log.Error(string.Format("Error CreateInvoice(): {0}", Message));
            //log.Error("-------------------------------------------------------------------------------------------");

            List<CustomerInvoiceListDetails> InvoiceDetailsList = new List<CustomerInvoiceListDetails>();
            foreach (var each in BillabaleItemIDList)
            {
                CustomerInvoiceListDetails InvoiceDetails = new CustomerInvoiceListDetails();
                InvoiceDetails.BillableItemId = each;
                InvoiceDetails.Status = false;
                InvoiceDetails.ErrorMessage = Message;
                InvoiceDetailsList.Add(InvoiceDetails);
            }
            return InvoiceDetailsList;
        }

        /// <summary>
        /// assign status= false and error if error
        /// </summary>
        /// <param name="BillabaleItemIDList"></param>
        /// <param name="Message"></param>
        /// <returns></returns>
        InvoiceDetails AssignInvoiceErrorMessage(string AccountNumber, string Message)
        {

            log.Error(string.Format("Error AssignInvoiceErrorMessage(): {0}", Message));
            log.Error("-------------------------------------------------------------------------------------------");

            InvoiceDetails InvoiceDetails = new InvoiceDetails();
            InvoiceDetails.Status = false;
            InvoiceDetails.ErrorMessage = Message;

            return InvoiceDetails;
        }


        //PaymentStatusServiceRef.InvoiceStatusList AssignInvoiceErrorMessageList(string AccountNumber, string Message)
        //{

        //    log.Error(string.Format("Error AssignInvoiceErrorMessage(): {0}", Message));
        //    log.Error("-------------------------------------------------------------------------------------------");
        //     PaymentStatusServiceRef.InvoiceStatusList list = new PaymentStatusServiceRef.InvoiceStatusList();

        //    InvoiceDetails InvoiceDetails = new InvoiceDetails();
        //    InvoiceDetails.Status = false;
        //    InvoiceDetails.ErrorMessage = Message;
        //    InvoiceDetails.InvoiceAccount = AccountNumber;

        //    return InvoiceDetails;
        //}

        /// <summary>
        /// Read all invoices and invoice lines for a customer
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public DataSet RunQueryService(string accountNumber, string status)
        {
            DataSet dsFinalList = null;
            Paging paging = null;
            QueryMetadata queryMetadata;
            QueryDataSourceMetadata dsIDataSourceMetadata, dsInvoiceLineMetadata;
            QueryDataRangeMetadata rangeData = null;
            //  QueryFilterMetadata queryFilter = new QueryFilterMetadata();
            QueryRelationMetadata queryRelationMeta = new QueryRelationMetadata();

            //New Query Entity
            queryMetadata = new QueryMetadata();

            queryMetadata.QueryType = QueryType.Join;
            queryMetadata.AllowCrossCompany = false;
            queryMetadata.DataSources = new List<QueryDataSourceMetadata>();

            dsIDataSourceMetadata = new QueryDataSourceMetadata();

            //declaring the tables
            const string custInvoiceTableName = "CustInvoiceTable";
            const string custInvoiceLineTableName = "CustInvoiceLine";
            const string InvoiceAccountTableName = "InvoiceAccount";

            dsIDataSourceMetadata.Name = custInvoiceTableName;
            dsIDataSourceMetadata.Enabled = true;
            dsIDataSourceMetadata.FetchMode = FetchMode.OneToOne;
            dsIDataSourceMetadata.Table = custInvoiceTableName;
            dsIDataSourceMetadata.DynamicFieldList = true;

            dsInvoiceLineMetadata = new QueryDataSourceMetadata();

            dsInvoiceLineMetadata.Name = custInvoiceLineTableName;
            dsInvoiceLineMetadata.Enabled = true;
            dsInvoiceLineMetadata.FetchMode = FetchMode.OnetoN;
            dsInvoiceLineMetadata.Table = custInvoiceLineTableName;
            dsInvoiceLineMetadata.DynamicFieldList = true;

            dsInvoiceLineMetadata.Relations = new List<QueryRelationMetadata>();

            queryRelationMeta = new QueryRelationMetadata()
            {
                Field = "RecId",
                RelatedField = "ParentRecId",
                JoinDataSource = custInvoiceTableName,
                RelatedTable = custInvoiceLineTableName,
                Table = custInvoiceTableName
            };

            dsInvoiceLineMetadata.Relations.Add(queryRelationMeta);

            dsIDataSourceMetadata.DataSources = new List<QueryDataSourceMetadata>();
            dsIDataSourceMetadata.DataSources.Add(dsInvoiceLineMetadata);

            dsIDataSourceMetadata.Ranges = new List<QueryRangeMetadata>();

            rangeData = new QueryDataRangeMetadata();

            rangeData.Name = InvoiceAccountTableName;
            rangeData.FieldName = InvoiceAccountTableName;
            rangeData.TableName = custInvoiceTableName;
            rangeData.Value = accountNumber;
            // rangeData.RangeStatus = RangeStatus.Open;
            rangeData.Enabled = true;

            RangeStatus rangeStatus = RangeStatus.Open;
            if (string.IsNullOrEmpty(status) || status.ToUpperInvariant() == "ALL")
                rangeStatus = RangeStatus.Open; // to do change relevant Status

            switch (Convert.ToString(status).ToUpper())
            {
                case "OPEN":
                    rangeStatus = RangeStatus.Open;
                    break;
            }
            rangeData.RangeStatus = rangeStatus;
            rangeData.Enabled = true;

            dsIDataSourceMetadata.Ranges.Add(rangeData);

            queryMetadata.DataSources.Add(dsIDataSourceMetadata);

            QueryServiceClient client = new QueryServiceClient();

            dsFinalList = client.ExecuteQuery(queryMetadata, ref paging); client.Close();

            return dsFinalList;
        }

        #endregion "Get Customer Invoices"

        /// <summary>
        /// Get invoice root folder
        /// </summary>
        /// <returns></returns>
        public string GetInvoiceFolder()
        {

            InvoiceServiceReference.FreeTextInvoiceServiceClient client = new InvoiceServiceReference.FreeTextInvoiceServiceClient();
            {

                #region "get Full qualified path"

                // implemented credentials starts
                // implemented credentials starts
                string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

                if (client.ChannelFactory.Credentials != null)
                {
                    client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
                    client.ChannelFactory.Credentials.UserName.Password = servicePassword;
                }
                // implemented credentials ends

                //set max value for behaviour
                client.Endpoint.Behaviors.Add(new MaxFaultSizeBehavior(1024000));

                log.Debug("GetInvoiceFolder():before  Get full path from AX service");
                //calling AX to create invoice
                string folderpath = client.getInvoicesPath(null); client.Close();

                log.Debug("GetInvoiceFolder():After  Get full path from the service" + folderpath);

                #endregion "get Filename with qualified path"

                log.Debug("Exit GetInvoiceFolder(): Folder : " + folderpath);

                return folderpath;
            }
        }
        /// <summary>
        /// Get Invoice Documents for a customer
        /// </summary>
        /// <returns></returns>
        public InvoiceDocumentsList GetInvoiceDocument(string folderpath, string customerId, List<string> invoiceIDs = null)
        {
            log.Info("Enter GetInvoiceDocument()");
            InvoiceDocumentsList invoiceDocumentsList = new InvoiceDocumentsList();
            invoiceDocumentsList.InvoiceDocsList = new List<InvoiceDocument>();
            FreeTextInvoiceServiceClient client = new FreeTextInvoiceServiceClient();

            try
            {

                #region "Implemented credentials starts"
                // implemented credentials starts
                string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

                if (client.ChannelFactory.Credentials != null)
                {
                    client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
                    client.ChannelFactory.Credentials.UserName.Password = servicePassword;
                }
                #endregion "Implemented credentials ends"

                //set max value for behaviour
                client.Endpoint.Behaviors.Add(new MaxFaultSizeBehavior(1024000));

                //get invoice prefix from web config
                string invoiceFileNamePrefix = Convert.ToString(ConfigurationManager.AppSettings["InvoiceFileNamePrefix"]);

                ////If invoice id is empty
                //if (invoiceIDs = null)
                //{
                //}
                //else
                //{

                //for each invoice
                #region "for each invoices"
                foreach (string eachInvoiceId in invoiceIDs)
                {
                    string fname = invoiceFileNamePrefix + eachInvoiceId + ".pdf";
                    string fileName = folderpath + fname;
                    log.Debug("GetInvoiceDocument(): filename for invoice number " + eachInvoiceId + " : " + fileName);

                    string tempFilename = invoiceFileNamePrefix + eachInvoiceId + ".pdf";

                    InvoiceDocument invoiceDocument = new InvoiceDocument();
                    invoiceDocument.FileName = fileName;
                    invoiceDocument.FileServerPath = folderpath;
                    invoiceDocument.InvoiceID = eachInvoiceId;


                    // if (CheckIfFileExists(fileName)) // if file there then read the files
                    if (CheckIfFileExist(folderpath + "\\", fname)) // if file there then read the files
                    {
                        //get the bytes from file 
                        log.Debug(string.Format("GetInvoiceDocument()  file exists {0}", fileName));
                        invoiceDocument.FileContentsByte = ReadAllBytes(fileName);
                        log.Debug(string.Format("GetInvoiceDocument()  invoiceDocument.FileContentsByte {0}", invoiceDocument.FileContentsByte));

                        log.Debug("GetInvoiceDocument() before calling service call getInvoicesPDF");
                        invoiceDocument.Status = true;
                        invoiceDocumentsList.InvoiceDocsList.Add(invoiceDocument);
                        log.Debug("GetInvoiceDocument() after calling service call getInvoicesPDF");

                    }
                    else // if file not there create a new one
                    {
                        var eachInvoiceListtoCreatePdf1 = new string[] { eachInvoiceId };

                        // eachInvoiceListtoCreatePDF1.Add(eachInvoiceID);
                        log.Debug("GetInvoiceDocument() before calling service call getInvoicesPDF() eachInvoiceId :" + eachInvoiceId);

                        //calling AX to create invoice
                        eachInvoiceListtoCreatePdf1 = client.getInvoicesPDF(null, eachInvoiceListtoCreatePdf1);
                        client.Close();

                        log.Debug("GetInvoiceDocument() after calling service call getInvoicesPDF : File ( " + fileName + ").");

                        fileName = folderpath + invoiceFileNamePrefix + eachInvoiceId + ".pdf";
                        invoiceDocument.Status = true;

                        log.Debug("GetInvoiceDocument(): filename path : " + fileName);
                        //}
                        // if (CheckIfFileExists(fileName))
                        if (CheckIfFileExist(folderpath + "\\", fname)) // if file there then read the files
                        {
                            log.Debug("GetInvoiceDocument(): filename path : " + fileName);
                            //get the bytes from file
                            invoiceDocument.FileContentsByte = ReadAllBytes(fileName);
                            log.Debug(string.Format("GetInvoiceDocument():  invoiceDocument.FileContentsByte {0}", invoiceDocument.FileContentsByte));
                            invoiceDocument.FileServerPath = folderpath;

                        }
                        else
                        {
                            log.Debug("GetInvoiceDocument(): filename path : " + fileName);
                            log.Info("GetInvoiceDocument(): " + fileName + " file not exists.");
                            invoiceDocument.Status = false;
                            invoiceDocument.ErrorMessage = Error;
                        }
                        invoiceDocumentsList.InvoiceDocsList.Add(invoiceDocument);
                    }
                }
                #endregion "for each invoices"
                // }
                log.Debug("Exit GetInvoiceDocument()");

            }
            catch (TimeoutException exception)
            {
                invoiceDocumentsList.InvoiceDocsList.Add(new InvoiceDocument() { ErrorMessage=exception.Message, Status=false });

                log.Error(string.Format("Error GetInvoiceDocument(): {0}", exception.Message));
                log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }
            catch (CommunicationException exception)
            {
               invoiceDocumentsList.InvoiceDocsList.Add(new InvoiceDocument() { ErrorMessage=exception.Message, Status=false });
  log.Error(string.Format("Error GetInvoiceDocument(): {0}", exception.Message));
                log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }
            catch (Exception exception)
            {
                invoiceDocumentsList.InvoiceDocsList.Add(new InvoiceDocument() { ErrorMessage=exception.Message, Status=false });

                log.Error("ERROR GetInvoiceDocument(): " + exception.Message);
                client.Abort();
            }
            log.Info("Exit GetInvoiceDocument()");
            return invoiceDocumentsList;
        }

        ///// <summary>
        ///// Get Invoice Documents for a customer
        ///// </summary>
        ///// <returns></returns>
        //public InvoiceDocumentsList GetInvoiceDocument1(string customerId, List<string> invoiceIDs = null)
        //{
        //    Log.Info("Enter GetInvoiceDocument()");
        //    InvoiceDocumentsList invoiceDocumentsList = new InvoiceDocumentsList();
        //    invoiceDocumentsList.InvoiceDocsList = new List<InvoiceDocument>();

        //    List<string> invoicesToExport = new List<string>();
        //    List<string> returnList = null;
        //    using (InvoicesExportHTTPServiceReference.FreeTextInvoiceServiceClient client = new InvoicesExportHTTPServiceReference.FreeTextInvoiceServiceClient())
        //    {
        //        Log.Info("GetInvoiceDocument(): Before calling service for each invoice");
        //        foreach (string eachInvoiceID in invoiceIDs)
        //        {

        //            #region "get Filename with qualified path"

        //            // implemented credentials starts
        //            string serviceUserid = Convert.ToString(ConfigurationManager.AppSettings["ServiceUserid"]);
        //            string servicePassword = Convert.ToString(ConfigurationManager.AppSettings["ServicePassword"]);

        //            if (client.ChannelFactory.Credentials != null)
        //            {
        //                client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
        //                client.ChannelFactory.Credentials.UserName.Password = servicePassword;
        //            }
        //            // implemented credentials ends

        //            //set max value for behaviour
        //            client.Endpoint.Behaviors.Add(new MaxFaultSizeBehavior(1024000));
        //            List<string> eachInvoiceList = new List<string>();
        //            eachInvoiceList.Add(eachInvoiceID);

        //            Log.Debug("GetInvoiceDocument():before  Get full path from the service");
        //            //calling AX to create invoice
        //            returnList = client.getInvoicesPDF(null, eachInvoiceList);
        //            Log.Debug("GetInvoiceDocument():After  Get full path from the service");

        //            #endregion "get Filename with qualified path"

        //            string fileNamefromAx = string.Empty;

        //            if (returnList.Any())
        //            {
        //                fileNamefromAx = returnList[0]; //@"E:\InvoicePDFs\1359476\Invoice21210.pdf";

        //                Log.Debug("GetInvoiceDocument(): get path : " + fileNamefromAx);
        //            }

        //            InvoiceDocument invoiceDocument = new InvoiceDocument();
        //            invoiceDocument.FileName = fileNamefromAx;
        //            invoiceDocument.InvoiceID = eachInvoiceID;

        //            if (CheckIfFileExists(fileNamefromAx))
        //            {
        //                //get the bytes from file
        //                invoiceDocument.FileContentsByte = ReadAllBytes(fileNamefromAx);
        //                invoiceDocumentsList.InvoiceDocsList.Add(invoiceDocument);
        //            }
        //            else
        //            {
        //                // invoicesToExport.Add(eachInvoiceID);

        //                // implemented credentials starts
        //                //    serviceUserid = Convert.ToString(ConfigurationManager.AppSettings["ServiceUserid"]);
        //                //  servicePassword = Convert.ToString(ConfigurationManager.AppSettings["ServicePassword"]);

        //                if (client.ChannelFactory.Credentials != null)
        //                {
        //                    client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
        //                    client.ChannelFactory.Credentials.UserName.Password = servicePassword;
        //                }
        //                // implemented credentials ends

        //                //set max value for behaviour
        //                client.Endpoint.Behaviors.Add(new MaxFaultSizeBehavior(1024000));

        //                List<string> eachInvoiceListtoCreatePDF = new List<string>();
        //                eachInvoiceListtoCreatePDF.Add(eachInvoiceID);
        //                Log.Debug("GetInvoiceDocument() before calling service call getInvoicesPDF");

        //                //calling AX to create invoice
        //                returnList = client.getInvoicesPDF(null, eachInvoiceListtoCreatePDF);
        //                Log.Debug("GetInvoiceDocument() after calling service call getInvoicesPDF");
        //                // }
        //                //}

        //                //if (invoicesToExport.Any())
        //                //{
        //                // implemented credentials starts
        //                //string serviceUserid = Convert.ToString(ConfigurationManager.AppSettings["ServiceUserid"]);
        //                //string servicePassword = Convert.ToString(ConfigurationManager.AppSettings["ServicePassword"]);

        //                //if (client.ChannelFactory.Credentials != null)
        //                //{
        //                //    client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
        //                //    client.ChannelFactory.Credentials.UserName.Password = servicePassword;
        //                //    Log.Info("AddInvoice() entered into credentials");
        //                //}
        //                // implemented credentials ends

        //                fileNamefromAx = "";
        //                if (returnList.Any())
        //                    fileNamefromAx = returnList[0];
        //                if (CheckIfFileExists(fileNamefromAx))
        //                {
        //                    //get the bytes from file
        //                    invoiceDocument.FileContentsByte = ReadAllBytes(fileNamefromAx);
        //                    invoiceDocumentsList.InvoiceDocsList.Add(invoiceDocument);
        //                }
        //            }
        //        }
        //        Log.Info("GetInvoiceDocument(): After calling service");
        //    }
        //    Log.Info("Exit GetInvoiceDocument()");
        //    return invoiceDocumentsList;
        //}

        /// <summary>
        /// Read All bytes from the file
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public byte[] ReadAllBytes(string fileName)
        {
            byte[] buffer = null;
            FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            try
            {
                log.Debug(string.Format("ReadAllBytes(): before read filename: {0} ", fileName));
                buffer = new byte[fs.Length];
                fs.Read(buffer, 0, (int)fs.Length);
                log.Debug(string.Format("ReadAllBytes(): after read filename: {0}", fileName));
            }
            catch (Exception ex)
            {
                log.Error(string.Format("ReadAllBytes(): filename: {0} , Error : {1} ", fileName, ex.Message));
                log.Debug(string.Format("ReadAllBytes(): filename: {0} , Error : {1} ", fileName, ex.Message));

            }
            return buffer;
        }

        /// <summary>
        /// Create a file if missing
        /// </summary>
        /// <param name="filename"></param>
        //private bool CheckIfFileExists(string filename)
        //{
        //    log.Debug("CheckIfFileExists(): filename: " + filename);

        //    FileInfo sFile = new FileInfo(filename);
        //    bool fileExist = Directory.EnumerateFiles(@"\\VSCMHserver04v4\InvoicePDFs", "InvoiceARINV-801105.pdf").Any();
        //    return sFile.Exists;

        //    // return File.Exists(filename);
        //}
        
        /// <summary>
        /// check if file exists 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        private bool CheckIfFileExist(string path, string filename)
        {
            try
            {
                filename = filename.Replace(@"\", "");
                log.Debug("CheckIfFileExists(): filename: " + filename);

                bool fileExist = Directory.EnumerateFiles(path, filename).Any();
                log.Debug("CheckIfFileExist(): after filename: " + filename);
                return fileExist;
            }
            catch (Exception Ex)
            {
                Error = Ex.Message;
                log.Error("CheckIfFileExist(): FileName: " + path + "\\" + filename + " Error: " + Ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Get invoices from AX views - Will get payment status along with invoices
        /// VEN_GetInvoicesAll: Return all invoices, paid or not paid.
        /// VEN_GetInvoicesOpen: Return only invoices not paid or partially paid
        /// VEN_GetInvoicesClosed: Return only invoices PAID in full.

        /// </summary>
        /// <param name="accountNumber"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public DataSet GetInvoicesfromAXViews(string accountNumber, string status)
        {
            log.Info("Enter GetInvoicesfromAXViews");
            DataSet dsReturnSet = null;
            Paging paging = null;
            QueryMetadata query;
            QueryDataSourceMetadata QueryDataSourceMetadata;
            QueryDataRangeMetadata rangeData = null;
            //  QueryFilterMetadata queryFilter = new QueryFilterMetadata();
            QueryRelationMetadata QueryRelationMetadata = new QueryRelationMetadata();

            //New Query Entity
            query = new QueryMetadata();

            query.QueryType = QueryType.Join;
            query.AllowCrossCompany = false;
            query.DataSources = new List<QueryDataSourceMetadata>();

            QueryDataSourceMetadata = new QueryDataSourceMetadata();
            string ViewName = "VEN_GetInvoicesAll";
            switch (Convert.ToString(status).ToUpper())
            {
                case "OPEN":
                    // ViewName = "VEN_GetInvoicesOpen";
                    ViewName = "VEN_GetInvoicesMarkedPayment";
                    break;
                case "CLOSED":
                    ViewName = "VEN_GetInvoicesClosed";
                    break;
                default: //All
                    ViewName = "VEN_GetInvoicesAll";
                    break;
            }
            QueryDataSourceMetadata.Name = ViewName;
            QueryDataSourceMetadata.Enabled = true;
            QueryDataSourceMetadata.FetchMode = FetchMode.OneToOne;
            QueryDataSourceMetadata.Table = ViewName;
            QueryDataSourceMetadata.DynamicFieldList = true;

            QueryDataSourceMetadata.Ranges = new List<QueryRangeMetadata>();

            rangeData = new QueryDataRangeMetadata();
            const string invoiceid = "InvoiceAccount";
            rangeData.Name = invoiceid;
            rangeData.FieldName = invoiceid;
            rangeData.TableName = ViewName;
            //rangeData.Value = "..104573,104684,193206..200000";
            //rangeData.Value = "19880005..19880007";
            rangeData.Value = accountNumber;
            rangeData.RangeStatus = RangeStatus.Open;
            rangeData.Enabled = true;

            QueryDataSourceMetadata.Ranges.Add(rangeData);

            query.DataSources.Add(QueryDataSourceMetadata);
            log.Info("GetInvoicesfromAXViews(): before executing ");

            QueryServiceClient client = new QueryServiceClient();
            try
            {

                // implemented credentials starts
                string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

                if (client.ChannelFactory.Credentials != null)
                {
                    client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
                    client.ChannelFactory.Credentials.UserName.Password = servicePassword;
                    log.Info("GetInvoicesfromAXViews() entered into credentials");
                }
                log.Info("GetInvoicesfromAXViews(): credentials - uid: " + serviceUserid + "; password:" + servicePassword);

                dsReturnSet = client.ExecuteQuery(query, ref paging);
                client.Close();
            }
            catch (TimeoutException exception)
            {
                log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }
            catch (CommunicationException exception)
            {
                log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }
            log.Info("Enter GetInvoicesfromAXViews() after executing");
            log.Info("Exit GetInvoicesfromAXViews()");

            return dsReturnSet;
        }

        /// <summary>
        /// Get invoices from AX Service  - Will get payment status along with invoices
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public List<PaymentStatusServiceRef.InvoiceStatusList> GetInvoicesfromAXService(string accountNumber, string status)
        {
            log.Info("Enter GetInvoicesfromAXService");

            List<PaymentStatusServiceRef.InvoiceStatusList> InvoiceStatusList = new List<PaymentStatusServiceRef.InvoiceStatusList>();

            //try
            //{
            PaymentStatusServiceRef.VEN_GetPaymentStatusServiceClient client = new PaymentStatusServiceRef.VEN_GetPaymentStatusServiceClient();
            try
            {

                // implemented credentials starts
                string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

                if (client.ChannelFactory.Credentials != null)
                {
                    client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
                    client.ChannelFactory.Credentials.UserName.Password = servicePassword;
                }

                //calling ax service to get invoice details 
                InvoiceStatusList = client.GetPaymentStatusList(null, accountNumber, status);
                log.Debug("GetInvoicesfromAXService(): credentials - uid: " + serviceUserid + "; password:" + servicePassword);

                //  dsReturnSet = client.ExecuteQuery(query, ref paging);
                client.Close();
                //sending the detail
            }

            catch (TimeoutException exception)
            {

                log.Error(string.Format("Error GetInvoicesfromAXService(): {0}", exception.Message));
                log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
                Error = exception.Message;
            }
            catch (CommunicationException exception)
            {
                Error = exception.Message;
                log.Error(string.Format("Error GetInvoicesfromAXService(): {0}", exception.Message));
                log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }
            catch (Exception exception)
            {
                Error = exception.Message;
                log.Error(string.Format("Error GetInvoicesfromAXService(): {0}", exception.Message));
                log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }
            log.Info("Exit GetInvoicesfromAXService()");
            return InvoiceStatusList;

        }

        /// <summary>
        /// Get invoices from AX
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <param name="status"></param>
        /// <returns></returns>

        public DataSet RunQueryServiceGetInvoices(string accountNumber, string status)
        {
            log.Info("Enter RunQueryServiceGetInvoices");
            DataSet dsReturnSet = null;
            Paging paging = null;
            QueryMetadata query;
            QueryDataSourceMetadata dsCustInvoiceJour, dsCustTrans, dsCustomerTransOpen, dsSpecTrans;
            QueryDataRangeMetadata rangeData = null;
            //  QueryFilterMetadata queryFilter = new QueryFilterMetadata();
            QueryRelationMetadata QueryRelationMetadata = new QueryRelationMetadata();

            //New Query Entity
            query = new QueryMetadata();

            query.QueryType = QueryType.Join;
            query.AllowCrossCompany = false;
            query.DataSources = new List<QueryDataSourceMetadata>();

            dsCustInvoiceJour = new QueryDataSourceMetadata();

            string CustInvoiceJour = "CustInvoiceJour";
            string custTrans = "CustTrans";
            const string invoiceid = "InvoiceAccount";
            dsCustInvoiceJour.Name = CustInvoiceJour;
            dsCustInvoiceJour.Enabled = true;
            dsCustInvoiceJour.FetchMode = FetchMode.OneToOne;
            dsCustInvoiceJour.Table = CustInvoiceJour;
            dsCustInvoiceJour.DynamicFieldList = true;

            dsCustTrans = new QueryDataSourceMetadata();

            dsCustTrans.Name = custTrans;
            dsCustTrans.Enabled = true;
            dsCustTrans.FetchMode = FetchMode.OnetoN;
            dsCustTrans.Table = custTrans;
            dsCustTrans.DynamicFieldList = true;
            dsCustTrans.JoinMode = JoinMode.ExistJoin; //This means no records from CustTrans will be returned, but if the record doesnt exist on this table, the parent record is discarded as well (EXISTS JOIN)

            dsCustTrans.Relations = new List<QueryRelationMetadata>();

            dsCustTrans.Relations.Add(new QueryRelationMetadata() { Field = "InvoiceId", RelatedField = "Invoice", JoinDataSource = "CustInvoiceJour", RelatedTable = "CustTrans", Table = "CustInvoiceJour" });
            dsCustTrans.Relations.Add(new QueryRelationMetadata() { Field = "InvoiceAccount", RelatedField = "AccountNum", JoinDataSource = "CustInvoiceJour", RelatedTable = "CustTrans", Table = "CustInvoiceJour" });
            dsCustTrans.Relations.Add(new QueryRelationMetadata() { Field = "InvoiceDate", RelatedField = "TransDate", JoinDataSource = "CustInvoiceJour", RelatedTable = "CustTrans", Table = "CustInvoiceJour" });
            dsCustTrans.Relations.Add(new QueryRelationMetadata() { Field = "LedgerVoucher", RelatedField = "Voucher", JoinDataSource = "CustInvoiceJour", RelatedTable = "CustTrans", Table = "CustInvoiceJour" });


            const string custtransopen = "CustTransOpen";

            if (Convert.ToString(status).ToUpper() != "ALL")
            {
                dsCustomerTransOpen = new QueryDataSourceMetadata();

                dsCustomerTransOpen.Name = custtransopen;
                dsCustomerTransOpen.Enabled = true;
                dsCustomerTransOpen.FetchMode = FetchMode.OnetoN;
                dsCustomerTransOpen.Table = custtransopen;
                dsCustomerTransOpen.DynamicFieldList = true;

                dsCustomerTransOpen.Relations = new List<QueryRelationMetadata>();
                dsCustomerTransOpen.Relations.Add(new QueryRelationMetadata() { Field = "AccountNum", RelatedField = "AccountNum", JoinDataSource = "CustTrans", RelatedTable = "CustTransOpen", Table = "CustTrans" });
                dsCustomerTransOpen.Relations.Add(new QueryRelationMetadata() { Field = "RecId", RelatedField = "RefRecId", JoinDataSource = "CustTrans", RelatedTable = "CustTransOpen", Table = "CustTrans" });

                if (Convert.ToString(status).ToUpper() == "OPEN")
                {
                    dsCustomerTransOpen.JoinMode = JoinMode.ExistJoin; //This means no records from CustTransOpen will be returned, but if the record exists on this table, the parent record is discarded as well (NOTEXISTS JOIN)    
                    dsSpecTrans = new QueryDataSourceMetadata();

                    const string spectrans = "SpecTrans";

                    dsSpecTrans.Name = spectrans;
                    dsSpecTrans.Enabled = true;
                    dsSpecTrans.FetchMode = FetchMode.OnetoN;
                    dsSpecTrans.Table = spectrans;
                    dsSpecTrans.DynamicFieldList = true;
                    dsSpecTrans.JoinMode = JoinMode.NotExistJoin;

                    dsSpecTrans.Relations = new List<QueryRelationMetadata>();
                    dsSpecTrans.Relations.Add(new QueryRelationMetadata() { Field = "dataAreaId", RelatedField = "RefCompany", JoinDataSource = custtransopen, RelatedTable = spectrans, Table = custtransopen });
                    dsSpecTrans.Relations.Add(new QueryRelationMetadata() { Field = "TableId", RelatedField = "RefTableId", JoinDataSource = custtransopen, RelatedTable = spectrans, Table = custtransopen });
                    dsSpecTrans.Relations.Add(new QueryRelationMetadata() { Field = "RecId", RelatedField = "RefRecId", JoinDataSource = custtransopen, RelatedTable = spectrans, Table = custtransopen });

                    dsCustomerTransOpen.DataSources = new List<QueryDataSourceMetadata>();
                    dsCustomerTransOpen.DataSources.Add(dsSpecTrans);
                }
                else if (Convert.ToString(status).ToUpper() == "CLOSED")
                {
                    dsCustomerTransOpen.JoinMode = JoinMode.NotExistJoin; //This means no records from CustTransOpen will be returned, but if the record exists on this table, the parent record is discarded as well (NOTEXISTS JOIN)              
                }
                dsCustTrans.DataSources = new List<QueryDataSourceMetadata>();
                dsCustTrans.DataSources.Add(dsCustomerTransOpen);
            }

            dsCustInvoiceJour.DataSources = new List<QueryDataSourceMetadata>();
            dsCustInvoiceJour.DataSources.Add(dsCustTrans);

            dsCustInvoiceJour.Ranges = new List<QueryRangeMetadata>();

            rangeData = new QueryDataRangeMetadata();

            rangeData.Name = invoiceid;
            rangeData.FieldName = invoiceid;
            rangeData.TableName = CustInvoiceJour;
            //rangeData.Value = "..104573,104684,193206..200000";
            //rangeData.Value = "19880005..19880007";
            rangeData.Value = accountNumber;
            rangeData.RangeStatus = RangeStatus.Open;
            rangeData.Enabled = true;

            dsCustInvoiceJour.Ranges.Add(rangeData);

            /*
            rangeData = new QueryDataRangeMetadata();
            rangeData.Name = "InvoiceAccount";
            rangeData.FieldName = "InvoiceAccount";
            rangeData.TableName = "CustInvoiceTable";
            rangeData.Value = "104586..104590";
            rangeData.RangeStatus = RangeStatus.Open;
            rangeData.Enabled = true;             
            */

            query.DataSources.Add(dsCustInvoiceJour);
            log.Info("RunQueryServiceGetInvoices(): before executing ");

            QueryServiceClient client = new QueryServiceClient();
            try
            {

                // implemented credentials starts
                string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

                if (client.ChannelFactory.Credentials != null)
                {
                    client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
                    client.ChannelFactory.Credentials.UserName.Password = servicePassword;
                    log.Info("RunQueryServiceGetInvoices() entered into credentials");
                }
                log.Info("RunQueryServiceGetInvoices(): credentials - uid: " + serviceUserid + "; password:" + servicePassword);

                dsReturnSet = client.ExecuteQuery(query, ref paging);
                client.Close();
            }
            catch (Exception exception)
            {
                log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }
            log.Info("Enter RunQueryServiceGetInvoices() after executing");
            log.Info("Exit RunQueryServiceGetInvoices()");

            return dsReturnSet;
        }


        #region "CancelInvoices"
        /// <summary>
        /// Cancel an invoice
        /// </summary>
        /// <param name="invoice"></param>
        /// <returns></returns>
        public ValidationDetail CancelInvoice(InvoiceCancellation InvoiceCancellation)
        {
            //Initialize validation detail class to store final oputput
            ValidationDetail validationDetail = new ValidationDetail();

            List<string> errorList = new List<string>();

            log.Info("Enter CancelInvoice()");

            //call AX - starts here
            errorList = CancelInvoiceinAX(InvoiceCancellation);

            //check if any errors 
            if (errorList.Any())
            {
                validationDetail.ErrorList = errorList.ToList();
                validationDetail.Result = false;
            }
            else
            {
                validationDetail.Result = true;
            }
            //call AX - ends here

            log.Info("Exit CreateInvoice()");
            return validationDetail;
        }

        /// <summary>
        /// Cancel invoices in AX
        /// </summary>
        List<string> CancelInvoiceinAX(InvoiceCancellation InvoiceCancellation)
        {
            log.Info("Enter CancelInvoiceinAX()");

            //we get the key elements 
            List<string> retVal = new List<string>();

            InvoiceCancelService.CustInvoiceManagerServiceClient client = new InvoiceCancelService.CustInvoiceManagerServiceClient();

            try
            {
                log.Info("CancelInvoiceinAX(): before create call");
                // implemented credentials starts
                string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

                //check client Credentials is not null
                if (client.ChannelFactory.Credentials != null)
                {
                    client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
                    client.ChannelFactory.Credentials.UserName.Password = servicePassword;
                    log.Info("CancelInvoiceinAX() entered into credentials");
                }
                // implemented credentials ends

                //set max value for behaviour
                client.Endpoint.Behaviors.Add(new MaxFaultSizeBehavior(1024000));

                List<InvoiceCancelService.VEN_InvoiceCancellationContract> cancellationList = new List<InvoiceCancelService.VEN_InvoiceCancellationContract>();

                cancellationList.Add(SetCancelInvoiceTable(InvoiceCancellation));

                log.Info("CancelInvoiceinAX(): Before calling client.cancelInvoices");

                //calling AX to cancel invoice
                var results = client.cancelInvoices(null, cancellationList);
                client.Close();
                string str = results[0].ToString();
                if (str.StartsWith("0;Success"))
                {
                    log.Info("CancelInvoiceinAX(): Success: " + str);
                }
                else
                {
                    retVal.Add(str);
                    log.Info("CancelInvoiceinAX(): Error: " + str);
                }
            }
            catch (SystemSvcModel.FaultException<AifFault> aifFault)
            {
                //retVal.Add(string.Join(Environment.NewLine,
                //    aifFault.Detail.InfologMessageList.Select(i => i.Message)));
                retVal.Add("ERROR from AX: " + string.Join(Environment.NewLine, aifFault.Message));
                log.Error("ERROR from AX: CancelInvoiceinAX(): Error: " + aifFault.Message);
                client.Abort();
            }
            catch (TimeoutException exception)
            {
                log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                retVal.Add("ERROR from AX: " + exception.Message);
                log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }
            //catch (CommunicationException exception)
            //{
            //    log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
            //    retVal.Add("ERROR from AX: " + exception.Message);
            //    log.Error("-------------------------------------------------------------------------------------------");
            //    client.Abort();
            //}
            catch (Exception exception)
            {
                log.Error("ERROR from AX: CancelInvoiceinAX(): Error: " + exception.Message);
                retVal.Add(exception.Message);
                client.Abort();
                //while (exception.InnerException != null)
                //{
                //    exception = exception.InnerException;
                //    retVal.Add(exception.Message);
                //}
            }
            log.Info("Exit CancelInvoiceinAX()");

            return retVal;
        }

        /// <summary>

        // set AX invoice cancellation object from Wrapper object
        private InvoiceCancelService.VEN_InvoiceCancellationContract SetCancelInvoiceTable(InvoiceCancellation invoiceCancellation)
        {

            log.Info("Enter SetCancelInvoiceTable()");

            //create AX invoice cancellation contract object 
            var ret = new InvoiceCancelService.VEN_InvoiceCancellationContract()
             {
                 parmBillableItemId = invoiceCancellation.BillableItemID,
                 parmCancellationId = invoiceCancellation.CancellationID,
                 // parmCancellationReason = invoiceCancellation.Reason,
                 parmCancellationReason = "ERROR",
                 parmCancellationTimestamp = invoiceCancellation.CancellationDate
             };
            log.Info("Exit SetCancelInvoiceTable()");

            return ret;
        }
        #endregion "CancelInvoices"
    }
}