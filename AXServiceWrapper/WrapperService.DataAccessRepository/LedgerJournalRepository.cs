﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 12/17/2014
'Description: 
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using log4net;
using log4net.Config;
using WrapperService.BusinessEntities;
using WrapperService.DataAccessRepository.Interfaces;
using WrapperService.DataAccessRepository.LedgerServiceReference;
using SystemSvcModel = System.ServiceModel;
// Uses to access the AXD tables.

//[assembly: XmlConfigurator(Watch = true)] // uses to track the actions into the logfile / DB based on the config settings

namespace WrapperService.DataAccessRepository
{

    public class LedgerJournalRepository : ILedgerJournalRepository
    {
        //Uses to tracking the actions 
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        //private string ServiceUserid = "dynamic@VentechSolutions.com";
        //private const string ServicePassword = "welcome@123";

        private ILedgerJournalRepository _ledgerJournalRepository;

        public LedgerJournalRepository(ILedgerJournalRepository iLedgerJournalRepository)
        {
            XmlConfigurator.Configure();
            this._ledgerJournalRepository = iLedgerJournalRepository;

        }

        public LedgerJournalRepository()
        {
            XmlConfigurator.Configure();
        }

        #region "Creating LedgerJournal methods"

        /// <summary>
        /// Creating an LedgerJournal
        /// </summary>
        /// <param name="ledgerJournal"></param>
        /// <returns></returns>
        public ValidationDetail CreatePayment(LedgerJournal ledgerJournal)
        {
            //Initialize validation detail class to store final oputput
            ValidationDetail validationDetail = new ValidationDetail();

            List<string> errorList = new List<string>();

            List<LedgerJournal> ledgerJournalList = new List<LedgerJournal>();
            ledgerJournalList.Add(ledgerJournal);

            Log.Info("Enter CreatePayment()");

            //call AX - starts here
            errorList = CreatePaymentJournal(ledgerJournalList);

            //check if any errors 
            if (errorList.Any())
            {
                validationDetail.ErrorList = errorList.ToList();
                validationDetail.Result = false;
            }
            else
            {
                validationDetail.Result = true;
            }
            //call AX - ends here

            Log.Info("Exit CreatePayment()");
            return validationDetail;
        }
        #endregion "Creating LedgerJournal methods"

        #region "AX Methods"
        /// <summary>
        /// Create Payment Journal
        /// </summary>
        /// <param name="ledgerJournalList"></param>
        /// <returns></returns>
        // public EntityKey[] CreatePaymentJournal(List<LedgerJournal> ledgerJournalList)
        List<string> CreatePaymentJournal(List<LedgerJournal> ledgerJournalList)
        {

            Log.Info("Enter CreatePaymentJournal()");
            //we get the key elements 
            List<string> retVal = new List<string>();

            AxdCustPaymJournal axdCustPaymJournal = null;
            EntityKey[] returnKey = null;

            if (ledgerJournalList != null && ledgerJournalList.Count > 0)
            {
                axdCustPaymJournal = new AxdCustPaymJournal();
                axdCustPaymJournal.LedgerJournalTable = SetLedgerJournalList(ledgerJournalList).ToArray();

                CustPaymJournalServiceClient client = new CustPaymJournalServiceClient();
                 try
                    {
                        // implemented credentials starts
                        string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                        string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));


                        if (client.ChannelFactory.Credentials != null)
                        {
                            client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
                            client.ChannelFactory.Credentials.UserName.Password = servicePassword;
                            Log.Info("AddInvoice() entered into credentials");
                        }
                        // implemented credentials ends
                        client.Endpoint.Behaviors.Add(new MaxFaultSizeBehavior(1024000));
                        Log.Debug("CreatePaymentJournal() Before client call");

                        returnKey = client.create(null, axdCustPaymJournal);
                        Log.Debug("CreatePaymentJournal() Before client call");

                        Log.Info("CreatePaymentJournal(): Payment created successfully");

                    }
                    catch (SystemSvcModel.FaultException<AifFault> aifExc)
                    {
                       //retVal.Add(string.Join(Environment.NewLine, aifFault.Detail.InfologMessageList.Select(i => i.Message)));
                        string error = string.Join(Environment.NewLine, aifExc.Message);
                        retVal.Add(error);
                        Log.Error("ERROR from AX:  CreatePaymentJournal()" + error);
                    }
                    catch (Exception exception)
                    {
                        Log.Error("ERROR from AX: CreatePaymentJournal(): Error: " + exception.Message);
                        retVal.Add(exception.Message);

                        while (exception.InnerException != null)
                        {
                            exception = exception.InnerException;
                            retVal.Add(exception.Message);
                        }
                    }
                    finally
                    {
                        if (client.State == SystemSvcModel.CommunicationState.Faulted)
                            client.Abort();
                        else
                            client.Close();
                    }
                }

            
            Log.Info("CreatePaymentJournal(): Exit");

            return retVal;
        }
        /// <summary>
        /// Set New Ledger Journal Entities from the list
        /// </summary>
        /// <param name="ledgerJournalList"></param>
        /// <returns></returns>
        private List<AxdEntity_LedgerJournalTable> SetLedgerJournalList(List<LedgerJournal> ledgerJournalList)
        {
            Log.Info("Enter SetLedgerJournalList()");
            List<AxdEntity_LedgerJournalTable> ret = null;
            AxdEntity_LedgerJournalTable journalTableEntity = null;

            if (ledgerJournalList != null && ledgerJournalList.Count > 0)
            {

                ret = new List<AxdEntity_LedgerJournalTable>();
                //Looping each from the list
                foreach (LedgerJournal eachLedgerJournal in ledgerJournalList)
                {
                    journalTableEntity = SetLedgerJournal(eachLedgerJournal);
                    ret.Add(journalTableEntity);
                }
            }
            Log.Info("Exit SetLedgerJournalList()");

            return ret;
        }
        /// <summary>
        /// Set Ledger Journal
        /// </summary>
        /// <param name="ledgerJournal"></param>
        /// <returns></returns>
        private AxdEntity_LedgerJournalTable SetLedgerJournal(LedgerJournal ledgerJournal)
        {
            Log.Info("Enter SetLedgerJournal()");
            List<AxdEntity_LedgerJournalTrans> journalTransEntityList = null;


            //Assign Config settings
            string JournalName = Convert.ToString(ConfigurationManager.AppSettings["JournalName"]);
            string company = Convert.ToString(ConfigurationManager.AppSettings["Company"]);
            Log.Debug("Assigned JournalName=" + JournalName + " and CompanyName=" + company + " SetLedgerJournal(): ");

            AxdEntity_LedgerJournalTable retEntity = new AxdEntity_LedgerJournalTable()
            {
                JournalName = JournalName,
                Name = ledgerJournal.Description
            };

            if (ledgerJournal.LedgerJournalTransactionList != null)
            {
                journalTransEntityList = new List<AxdEntity_LedgerJournalTrans>();
                foreach (LedgerJournalTransaction journalTrans in ledgerJournal.LedgerJournalTransactionList)
                {
                    journalTransEntityList.Add(CreateLedgerJournalTransaction(journalTrans, company));
                }
                retEntity.LedgerJournalTrans = journalTransEntityList.ToArray();
            }
            Log.Info("Exit SetLedgerJournal()");

            return retEntity;
        }
        /// <summary>
        /// Create ledger joural transaction entity
        /// </summary>
        /// <param name="ledgerJournalTransaction"></param>
        /// <returns></returns>
        private AxdEntity_LedgerJournalTrans CreateLedgerJournalTransaction(LedgerJournalTransaction ledgerJournalTransaction, string company)
        {
            Log.Info("Enter CreateLedgerJournalTransaction()");

            AxdEntity_LedgerJournalTrans retEntity = null;
            if (ledgerJournalTransaction != null)
            {
                AxdEnum_LedgerJournalACType? offsetAccountType =
                    GetAccountType(ledgerJournalTransaction.OffsetAccountType);

                retEntity = new AxdEntity_LedgerJournalTrans()
                {
                    AccountType = GetAccountType(ledgerJournalTransaction.AccountType),
                    AccountTypeSpecified = true,

                    AmountCurCredit = ledgerJournalTransaction.Credit,
                    AmountCurCreditSpecified = ledgerJournalTransaction.Credit.HasValue,
                    AmountCurDebit = ledgerJournalTransaction.Debit,
                    AmountCurDebitSpecified = ledgerJournalTransaction.Debit.HasValue,
                    Txt = ledgerJournalTransaction.Description,
                    TransDate = ledgerJournalTransaction.TransactionDate,
                  //  ref
                    PaymReference = ledgerJournalTransaction.PaymentConfirmationNumber, //added PaymentConfirmationNumber July-20-2015

                    TransDateSpecified = ledgerJournalTransaction.TransactionDate.HasValue,
                    PaymentStatus=AxdEnum_CustVendPaymStatus.Recieved, //Added Paymetn Status Received as default
                    PaymentStatusSpecified=true,
                    Invoice = ledgerJournalTransaction.InvoiceId,
                    MarkedInvoice = ledgerJournalTransaction.InvoiceId,
                    //Not needed - starts here
                    //   Due = journalTrans_DO.DueDate,
                    //  DueSpecified = journalTrans_DO.DueDate.HasValue,
                    //Not needed - ends here
                    LedgerDimension = SetMultiTypeAccount(ledgerJournalTransaction.LedgerDimension),
                    OffsetLedgerDimension = SetMultiTypeAccount(ledgerJournalTransaction.OffsetLedgerDimension),

                    OffsetAccountType = offsetAccountType,
                    OffsetAccountTypeSpecified = offsetAccountType.HasValue,

                    //   Company = ledgerJournalTransaction.Company,
                    Company = company,
                  //  OffsetCompany = ledgerJournalTransaction.OffsetCompany,
                    OffsetCompany = company,
                    PaymMode = (ledgerJournalTransaction.PaymentMode.ToLower()=="credit")?"CreditCard":ledgerJournalTransaction.PaymentMode,
                    PaymId = ledgerJournalTransaction.PaymentId,
                    PaymentNotes = ledgerJournalTransaction.PaymentNotes,
                    //VEN_ARCodeId = ledgerJournalTransaction.ARCode //Not needed

                };

                //if (ledgerJournalTransaction.AccountType != AxdEnum_LedgerJournalACType.Ledger && ledgerJournalTransaction.DefaultDimension != null)
                //if (retEntity.AccountType != AxdEnum_LedgerJournalACType.Ledger)
                //{
                //    retEntity.DefaultDimension = SetDefaultDimensionList(ledgerJournalTransaction.DefaultDimension);
                //}

                //if (ledgerJournalTransaction.OffsetAccountType != AxdEnum_LedgerJournalACType.Ledger
                //    && ledgerJournalTransaction.OffsetDefaultDimension != null)
                //{
                //    retEntity.OffsetDefaultDimension = SetDefaultDimensionList(ledgerJournalTransaction.OffsetDefaultDimension);
                //}

                //if (retEntity.OffsetAccountType != AxdEnum_LedgerJournalACType.Ledger && retEntity.OffsetDefaultDimension != null)
                //{
                //    retEntity.OffsetDefaultDimension = SetDefaultDimensionList(new List<DefaultDimension>().Add(ledgerJournalTransaction.OffsetLedgerDimension))));
                //}
                //if (retEntity.OffsetAccountType != AxdEnum_LedgerJournalACType.Ledger &&
                //    retEntity.OffsetDefaultDimension != null)
                //{
                //    retEntity.OffsetDefaultDimension =
                //        SetDefaultDimensionList(ledgerJournalTransaction.OffsetLedgerDimension);
                //}
            }
            Log.Info("Exit CreateLedgerJournalTransaction()");
            return retEntity;
        }
        ///Getting address type
        AxdEnum_LedgerJournalACType? GetAccountType(string type)
        {

            AxdEnum_LedgerJournalACType? accountTypeField;
            if (type == null)
                type = string.Empty;

            switch (Convert.ToString(type).ToUpper())
            {
                case "BANK":
                    accountTypeField = AxdEnum_LedgerJournalACType.Bank;
                    break;

                case "LEDGER":
                    accountTypeField = AxdEnum_LedgerJournalACType.Ledger;
                    break;
                case "CUST":
                    accountTypeField = AxdEnum_LedgerJournalACType.Cust;
                    break;
                case "EMPLOYEE_RU":
                    accountTypeField = AxdEnum_LedgerJournalACType.Employee_RU;
                    break;
                case "FIXEDASSETS_RU":
                    accountTypeField = AxdEnum_LedgerJournalACType.FixedAssets_RU;
                    break;
                case "FIXEDASSETS":
                    accountTypeField = AxdEnum_LedgerJournalACType.FixedAssets;
                    break;
                case "PROJECT":
                    accountTypeField = AxdEnum_LedgerJournalACType.Project;
                    break;
                case "RCASH":
                    accountTypeField = AxdEnum_LedgerJournalACType.RCash;
                    break;
                case "RDEFERRALS":
                    accountTypeField = AxdEnum_LedgerJournalACType.RDeferrals;
                    break;
                case "VEND":
                    accountTypeField = AxdEnum_LedgerJournalACType.Vend;
                    break;
                default:
                    accountTypeField = null;
                    break;
            }
            return accountTypeField;
        }

        /// <summary>
        /// Handles the financial dimension set-up. Since it requires a Name/Value pair to be translated to a RecId, special treatment is required.
        /// </summary>
        /// <param name="defaultDimensionList"> A list of dimensions to include on LINE or HEADER</param>
        /// <returns>DimensionAttributeValueSet is an array of singles instances of dimensions</returns>
        private AxdType_DimensionAttributeValueSet SetDefaultDimensionList(List<DefaultDimension> defaultDimensionList)
        {
            Log.Info("Enter SetDefaultDimensionList()");
            AxdType_DimensionAttributeValueSet ret = null;

            if (defaultDimensionList != null && defaultDimensionList.Count > 0)
            {
                ret = new AxdType_DimensionAttributeValueSet();

                //var axdTypeDimensionAttributeValueList = new List<AxdType_DimensionAttributeValue>();

                //foreach (DefaultDimension eachDefaultDimension in defaultDimensionList)
                //{
                //    axdTypeDimensionAttributeValueList.Add(SetDefaultDimension(eachDefaultDimension));
                //}
                //ret.Values = axdTypeDimensionAttributeValueList.ToArray();

                ret.Values = defaultDimensionList.Select(eachDefaultDimension => SetDefaultDimension(eachDefaultDimension)).ToArray();
            }
            Log.Info("Exit SetDefaultDimensionList()");
            return ret;
        }

        /// <summary>
        /// Set Ledger Dimension MultiType Account
        /// </summary>
        /// <param name="ledgerDimension"></param>
        /// <returns></returns>
        private AxdType_MultiTypeAccount SetMultiTypeAccount(LedgerDimension ledgerDimension)
        {
            Log.Info("Enter SetMultiTypeAccount()");
            List<AxdType_DimensionAttributeValue> axdTypeDimensionAttributeValue = null;
            AxdType_MultiTypeAccount retEntity = null;

            if (ledgerDimension != null)
            {
                retEntity = new AxdType_MultiTypeAccount()
                {
                    Account = ledgerDimension.Account, 
                    DisplayValue = ledgerDimension.DisplayValue,
                    Values = null
                };

                if (ledgerDimension.DefaultDimensionList != null)
                {
                    if (ledgerDimension.DefaultDimensionList.Any())
                    {
                        Log.Info("Entered SetMultiTypeAccount()");

                        axdTypeDimensionAttributeValue = new List<AxdType_DimensionAttributeValue>();

                        //Looping each element and set default dimension
                        foreach (DefaultDimension defaultDim in ledgerDimension.DefaultDimensionList)
                        {
                            axdTypeDimensionAttributeValue.Add(SetDefaultDimension(defaultDim));
                        }
                        //  axdTypeDimensionAttributeValue = ledgerDimension.Values.Select(defaultDim => NewDimensionAttributeValue(defaultDim)).ToList();
                        retEntity.Values = axdTypeDimensionAttributeValue.ToArray();
                    }
                }
            }
            Log.Info("Exit SetMultiTypeAccount()");
            return retEntity;
        }

        /// <summary>
        /// Set Default dimension values here
        /// </summary>
        /// <param name="defaultDimension"></param>
        /// <returns></returns>
        private AxdType_DimensionAttributeValue SetDefaultDimension(DefaultDimension defaultDimension)
        {
            Log.Info("Enter SetDefaultDimension()");
            AxdType_DimensionAttributeValue retEntity = new AxdType_DimensionAttributeValue()
            {
                Name = defaultDimension.Name,
                Value = defaultDimension.Value
            };
            Log.Info("Exit SetDefaultDimension()");
            return retEntity;
        }

        #endregion "AX Methods"

    }
}