﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/03/2014
'Description: 
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using log4net;
using log4net.Config;
using WrapperService.BusinessEntities;
using WrapperService.DataAccessRepository.Interfaces;
using WrapperService.DataAccessRepository.VendorServiceReference;

// Uses to access the AXD tables.
//using WrapperService.DataAccessRepository.InvoiceQueryServiceReference;

[assembly: XmlConfigurator(Watch = true)] // uses to track the actions into the logfile / DB based on the config settings

namespace WrapperService.DataAccessRepository
{
    public class VendorRepository : IVendorRepository
    {
        //Uses to tracking the actions 
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        const string Business = "Business";
        const string language = "en-us";
        //const string language = "";

        const string USA = "USA";

        private IVendorRepository _VendorRepository;
        public VendorRepository(IVendorRepository iVendorRepository)
        {
            XmlConfigurator.Configure();
            _VendorRepository = iVendorRepository;
        }
        public VendorRepository()
        {
            XmlConfigurator.Configure();
        }
        #region "General insert/update/delete methods"

        /// <summary>
        /// Check new Vendor
        /// </summary>
        /// <param name="accountNumner"></param>
        /// <returns></returns>
        public ValidationDetail IsExistingVendor(string accountNumner)
        {
            log.Info("Enter IsExistingVendor(" + accountNumner + ")");
            ValidationDetail validationDetail = new ValidationDetail();
            try
            {
                // just pass the "AccountNum" field and value to rad Vendor
                AxdVendTable AxdVendTable = ReadVendor("AccountNum", accountNumner);

                if (AxdVendTable != null)
                {
                    //Check whether if the Vendor is exists or not.
                    if (AxdVendTable.VendTable != null) // New Vendor
                    {
                        log.Info("Enter IsExistingVendor(" + accountNumner + ") :" + " Yes");
                        validationDetail.Result = true;
                        return validationDetail; // If the Vendor is exists then return it
                    }
                }
                log.Info("Exit IsExistingVendor(" + accountNumner + ") :" + "No, New Vendor");
                validationDetail.Result = false;
            }
            catch (Exception ex)
            {
                log.Error("Exit IsExistingVendor(" + accountNumner + ") :" + ex.Message.ToString());
                validationDetail.Result = false;
                validationDetail.ErrorList = new List<string> { ex.Message.ToString() };
            }
            return validationDetail;
            //call AX - ends here
        }

        // Read Vendor 
        private AxdVendTable ReadVendor(string fieldName, string value)
        {
            log.Info("Enter ReadVendor");
            AxdVendTable retAxdVendTable = null;
            //try
            {
                EntityKey[] VendorKeys = null;
                //You can look-up N Vendors in AX. In this case I'm fetching one Vendor, but you can fill the array with as many Vendor's keys as you want.
                VendorKeys = new EntityKey[1];
                VendorKeys[0] = new EntityKey();

                //Definig the Key field (Primary Key on Vendor table) and it's value to be searched inside AX
                VendorKeys[0].KeyData = new KeyField[1] { CreateKeyField(fieldName, value) };
                log.Info("ReadVendor before client object intiates starts");

                //Commented by Ananthakumar - implemented Call context
                VendTableServiceClient client = new VendTableServiceClient();
                try
                {

                    // implemented credentials starts
                    string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                    string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));


                    if (client.ChannelFactory.Credentials != null)
                    {
                        client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
                        client.ChannelFactory.Credentials.UserName.Password = servicePassword;
                        log.Info("ReadVendor() entered into credentials");
                    }
                    // implemented credentials ends

                    log.Debug("ReadVendor: Before read from client object");
                    log.Debug("ReadVendor: client.InnerChannel.State :" + client.InnerChannel.State.ToString());

                    if (client.InnerChannel.State != CommunicationState.Faulted)
                    {
                        // retAxdVendTable = client.read(new CallContext { Company = "SFM", LogonAsUser = "vsusa\dynamic" }, VendorKeys);
                        retAxdVendTable = client.read(null, VendorKeys);
                        client.Close();
                    }
                    else
                    {
                        // channel faulted - re-create your client and then try again
                        log.Debug("ReadVendor: else client.InnerChannel.State : " + client.InnerChannel.State.ToString());
                    }
                    log.Debug("ReadVendor: after read from client object");
                }
                catch (TimeoutException exception)
                {
                    log.Error(string.Format("Error ReadVendor(): {0}", exception.Message));
                    log.Error("-------------------------------------------------------------------------------------------");
                    client.Abort();
                }
                catch (CommunicationException exception)
                {
                    log.Error(string.Format("Error ReadVendor(): {0}", exception.Message));
                    log.Error("-------------------------------------------------------------------------------------------");
                    client.Abort();
                }
                catch (Exception ex)
                {
                    log.Error(string.Format("Error: ReadVendor() : {0}", ex.Message));
                    log.Error("-------------------------------------------------------------------------------------------");
                    client.Abort();
                }
                // log.Info("ReadVendor before client object intiates ends");

                log.Info("Exit ReadVendor");

            }
            //catch (Exception ex)
            //{

            //}
            return retAxdVendTable;
        }
        
        /// <summary>
        /// This creates the Keys used to fetch records from AX. For the Vendors, you pass the field "AccountNum" and the Vendor ID
        /// </summary>
        private KeyField CreateKeyField(string fieldName, string value)
        {
            KeyField KeyField = new KeyField();

            KeyField.Field = fieldName;
            KeyField.Value = value;
            return KeyField;
        }

        /// <summary>
        /// Create the EntityKeys used to indicate which Vendors will be updated on the update call. You can add N Vendors in the key
        /// </summary>
        private EntityKey[] CreateEntityKeys(string fieldName, string accountNum)
        {
            EntityKey[] keys = new EntityKey[1];
            keys[0] = new EntityKey();
            keys[0].KeyData = new KeyField[1];

            keys[0].KeyData = new KeyField[1] { CreateKeyField(fieldName, accountNum) };

            return keys;
        }

        /// <summary>
        /// Insert a Vendor , call AX server to fetch
        /// </summary>
        /// <param name="Vendor"></param>
        /// <returns></returns>
        public ValidationDetail InsertVendor(Vendor Vendor)
        {
            // Validationdetail object is used to return final output from this method, 
            ValidationDetail validationDetail = new ValidationDetail();
            List<string> errorList = new List<string>();
            try
            {
                log.Info("Entering InsertVendor()");

                //call AX - starts here
                errorList = CreateVendor(Vendor);

                if (errorList.Any())
                {
                    validationDetail.ErrorList = errorList.ToList();
                    validationDetail.Result = false;
                }
                else
                {
                    validationDetail.Result = true;
                }
                //call AX - ends here

                log.Info("Exit InsertVendor()");
            }
            catch (Exception ex)
            {
                log.Error("Exit InsertVendor:" + ex.Message.ToString());
                validationDetail.Result = false;
                validationDetail.ErrorList = new List<string> { ex.Message.ToString() };
            }
            return validationDetail;
        }

        /// <summary>
        /// Update Vendor
        /// </summary>
        /// <param name="Vendor"></param>
        /// <returns></returns>
        public ValidationDetail UpdateVendor(Vendor Vendor)
        {
            log.Info("Entering into UpdateVendor().");
            ValidationDetail validationDetail = new ValidationDetail();
            List<string> errorList = new List<string>();
            try
            {
                // To-Do Update Vendor logic is here
                errorList = UpdateVendorinAx(Vendor);

                if (errorList.Any())
                {
                    validationDetail.ErrorList = errorList.ToList();
                    validationDetail.Result = false;
                }
                else
                {
                    validationDetail.Result = true;
                }
                //call AX - ends here

                log.Info("Exit UpdateVendor().");
            }
            catch (Exception ex)
            {
                log.Error("Exit UpdateVendor:" + ex.Message.ToString());
                validationDetail.Result = false;
                validationDetail.ErrorList = new List<string> { ex.Message.ToString() };
            }
            return validationDetail;
        }
         
         #endregion "General insert/update methods"

        #region "Creating Vendor"

        /// <summary>
        /// Vendor creation
        /// </summary>
        /// <returns></returns>
        List<string> CreateVendor(Vendor Vendor)
        {
            log.Info("CreateVendor()");

            //we get the key elements 
            List<string> retVal = new List<string>();

            #region Objects for Creation
            AxdVendTable AxdVendTable = new AxdVendTable(); //Only one per call. You can use the same for successive calls.
            List<AxdEntity_VendTable> axdEntityVendTable = new List<AxdEntity_VendTable>(); //List of Vendors to create and/or update
            #endregion

            axdEntityVendTable.Add(SetVendorTable(AxdEnum_AxdEntityAction.create, Vendor, true));
            AxdVendTable.VendTable = axdEntityVendTable.ToArray();

            // AxdVendTable.VendTable[0].AS400_ID = Vendor.AS400ID;

            //AxdVendTable.VendTable[0].Blocked = (Vendor.IsDisable == true) ? AxdExtType_CustBlocked.All : AxdExtType_CustBlocked.No;
            //AxdVendTable.VendTable[0].BlockedSpecified = true;
            //Setting System Document properties
            AxdVendTable.ValidTimeStateType = AxdEnum_AxdValidTimeStateType.AsOf;
            AxdVendTable.ValidTimeStateTypeSpecified = true;

            VendTableServiceClient client = new VendTableServiceClient();
              try
                {
                    log.Info("CreateVendor(): before create call");
                    // implemented credentials starts
                    // implemented credentials starts
                    string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                    string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

                    if (client.ChannelFactory.Credentials != null)
                    {
                        client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
                        client.ChannelFactory.Credentials.UserName.Password = servicePassword;
                        log.Info("ReadVendor() entered into credentials");
                    }
                    // implemented credentials ends

                    client.create(null, AxdVendTable); client.Close();
                    log.Info("CreateVendor(): after create call");

                }
                catch (FaultException<AifFault> aifFault)
                {
                    //1retVal.Add(string.Join(Environment.NewLine, aifFault.Detail.InfologMessageList.Select(i => i.Message)));
                    retVal.Add(string.Join(Environment.NewLine, aifFault.Message));
                    log.Error("ERROR from AX: CreateVendor(): " + aifFault.Message);
                    client.Abort();
                }
                catch (TimeoutException exception)
                {
                    log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                    retVal.Add("ERROR from AX: " + exception.Message);
                    log.Error("-------------------------------------------------------------------------------------------");
                    retVal.Add(exception.Message);
                    client.Abort();
                }
                catch (CommunicationException exception)
                {
                    log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                    retVal.Add("ERROR from AX: " + exception.Message);
                    log.Error("-------------------------------------------------------------------------------------------");
                    retVal.Add(exception.Message);
                    client.Abort();
                }
                catch (Exception exception)
                {
                    log.Error("ERROR from AX: CreateVendor(): Error: " + exception.Message);
                    retVal.Add("ERROR from AX: " + exception.Message);
                    client.Abort();
                    //while (exception.InnerException != null)
                    //{
                    //    exception = exception.InnerException;
                    //    retVal.Add(exception.Message);
                    //}
                }
            
            return retVal;
        }

        /// <summary>
        /// Set neccessary AX fields and wrapper service fields to AX temop Vendor object.
        /// </summary>
        /// <returns></returns>
        private List<string> UpdateVendorinAx(Vendor Vendor)
        {
            List<string> retList = new List<string>();
            AxdVendTable AxdVendTable = new AxdVendTable();
            log.Info("Enter UpdateVendorinAx()");
            VendTableServiceClient client = new VendTableServiceClient();
            try
            {
                // get Vendor data from AX by passing Vendor account number 
                AxdVendTable = ReadVendor("AccountNum", Vendor.AccountNumber);

                // if Vendor table is not empty
                if (AxdVendTable.VendTable != null)
                {
                    // if Vendor table has data
                    if (AxdVendTable.VendTable.Length > 0)
                    {
                        AxdVendTable.VendTable[0] = PrepareVendorUpdate(AxdVendTable.VendTable[0], Vendor);
                        AxdVendTable.VendTable[0].action = AxdEnum_AxdEntityAction.update;
                        AxdVendTable.VendTable[0].actionSpecified = true;

                        //AxdVendTable.VendTable[0].Blocked = Vendor.IsDisable ? AxdExtType_CustBlocked.All : AxdExtType_CustBlocked.No;
                        //AxdVendTable.VendTable[0].BlockedSpecified = true;
                            // implemented credentials starts
                            string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                            string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

                            if (client.ChannelFactory.Credentials != null)
                            {
                                client.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
                                client.ChannelFactory.Credentials.UserName.Password = servicePassword;
                                log.Info("ReadVendor() entered into credentials");
                            }
                            // implemented credentials ends
                            client.update(null, CreateEntityKeys("AccountNum", Vendor.AccountNumber), AxdVendTable);
                            client.Close();
                    }
                    else
                    {
                        retList.Add("Vendor table is null for this account number");
                    }
                    //retList = AxdVendTables.VendTable.Select(c => c.AccountNum).ToList();
                }
            }
            catch (FaultException<AifFault> aifExc)
            {
                //1 string error = string.Join(Environment.NewLine, aifExc.Detail.InfologMessageList.Select(i => i.Message));
                string error = string.Join(Environment.NewLine, aifExc.Message);
                retList.Add(error);
                log.Error("ERROR from AX: UpdateVendorinAX()" + error);
                client.Abort();
            }
            catch (TimeoutException exception)
            {
                log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                retList.Add("ERROR from AX: " + exception.Message);
                log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }
            catch (CommunicationException exception)
            {
                log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                retList.Add("ERROR from AX: " + exception.Message);
                log.Error("-------------------------------------------------------------------------------------------");
                retList.Add(exception.Message);
                client.Abort();
            }
             
            catch (Exception e)
            {
                string error = e.Message.ToString();

                retList.Add(error);
                // Looping inner exception
                while (e.InnerException != null)
                {
                    string innerException = e.InnerException.Message.ToString();
                    e = e.InnerException;
                    retList.Add(innerException);

                    log.Debug("Error UpdateVendorinAX()" + innerException);

                }
                log.Error("ERROR from AX: UpdateVendorinAX()  " + e.Message);
            }
            client.Abort();
            log.Info("Exit UpdateVendorinAX()");
            return retList;
        }

        /// <summary>
        /// Create Vendor Header information
        /// </summary>
        private AxdEntity_VendTable SetVendorTable(AxdEnum_AxdEntityAction action, Vendor Vendor, bool _isVendorCreate)
        {
            log.Info("Enter SetVendorTable() ");

            AxdEntity_VendTable retVendTable = new AxdEntity_VendTable();
            //Check either create or update or other actions 
            if (action != AxdEnum_AxdEntityAction.create)
            {
                retVendTable.action = action;
                retVendTable.actionSpecified = true;
            }
            //  retVendTable.actionSpecified = true;

            retVendTable.AccountNum = Vendor.AccountNumber; //Assign Vendor ID
            retVendTable.Currency = Vendor.Currency; //Assign Currency here
            retVendTable.VendGroup = Vendor.Group; //"Assign group name here


            retVendTable.DirPartyTable = new AxdEntity_DirPartyTable_DirPartyTable[] { CreateDirPerson(AxdEnum_AxdEntityAction.create, Vendor, _isVendorCreate) };
            
            log.Info("Exit SetVendorTable() ");

            return retVendTable;
        }
         
        /// <summary>
        /// DirPerson represents Personal information of VendorS OF TYPE PERSON ONLY.
        /// </summary>
        private AxdEntity_DirPartyTable_DirPerson CreateDirPerson(AxdEnum_AxdEntityAction action, Vendor Vendor, bool _isVendorCreate)
        {
            log.Info("Enter CreateDirPerson() ");

            AxdEntity_DirPartyTable_DirPerson  retPerson = new AxdEntity_DirPartyTable_DirPerson ();
            // Will review when update heppens - once it's ready - 
            if (action != AxdEnum_AxdEntityAction.create)
            {
                //This property says what to do with this information. If you use it as update, you need reference data from the original record INSIDE AX.
                retPerson.action = action; //This is the default value. Dont need to set it if you are going to create records.

            }

            if (!_isVendorCreate)
                retPerson.actionSpecified = true; //Same rule applies to all Data Objects in terms of the Action property.
            //retPerson.NameAlias = Vendor.NameAlias;

            //retPerson.PersonSuffix = Vendor.Suffix; // added by ananthakumar - Feb-23-2015
            //// if  name is less >20 then get 20 chars only
            //if (Vendor.NameAlias != null)
            //    if (Vendor.NameAlias.Length > 20)
            //        retPerson.NameAlias = Vendor.NameAlias.Substring(0, 20);
            //Comment - No need
            //retPerson.NameSequence = Vendor.NameSequence;
            retPerson.PersonName = new AxdEntity_PersonName[1] { SetPersonName(action, Vendor, _isVendorCreate) }; //There can be only one instance of Person Name

            retPerson.LanguageId = language;

            // retPerson.DirPartyPostalAddressView = new AxdEntity_DirPartyPostalAddressView[] { CreateAddress(_action, _Vendor).ToArray() };
            retPerson.DirPartyPostalAddressView = CreateAddress(Vendor).ToArray();


            //Adding contact info
            List<AxdEntity_DirPartyContactInfoView> axdEntityDirPartyContactInfoViewList = new List<AxdEntity_DirPartyContactInfoView>();

            foreach (var eachContact in Vendor.ContactList)
            {
                AxdEntity_DirPartyContactInfoView contactInfoView = new AxdEntity_DirPartyContactInfoView();

                contactInfoView = CreateContactInfo(AxdEnum_AxdEntityAction.create, eachContact);
                axdEntityDirPartyContactInfoViewList.Add(contactInfoView);
            }
            retPerson.DirPartyContactInfoView = axdEntityDirPartyContactInfoViewList.ToArray();
            log.Info("Exit CreateDirPerson() ");

            return retPerson;
        }

         /// <summary>
        /// Create Any Postal address through this instance
        /// </summary>
        /// <param name="Vendor"></param>
        /// <param name="_isPrimaryAddress"></param>
        private List<AxdEntity_DirPartyPostalAddressView> CreateAddress(Vendor Vendor, AxdExtType_LogisticsIsPrimaryAddress _isPrimaryAddress = AxdExtType_LogisticsIsPrimaryAddress.Yes)
        {

            log.Info("Enter CreateAddress()  Account Number : " + Vendor.AccountNumber);

            //Create a postal address
            List<AxdEntity_DirPartyPostalAddressView> AddressList = new List<AxdEntity_DirPartyPostalAddressView>();
            //Check each address
            foreach (var eachAddress in Vendor.AddressList)
            {
                log.Info("CreateAddress() : Foreach  in  Account Number : " + Vendor.AccountNumber);

                AxdEntity_DirPartyPostalAddressView address = new AxdEntity_DirPartyPostalAddressView();

                //if (_action != AxdEnum_AxdEntityAction.create)
                //{
                //    address.action = _action;
                //    address.actionSpecified = true; : 
                //}

                address.IsPrimary = _isPrimaryAddress;
                address.IsPrimarySpecified = eachAddress.IsPrimaryAddress;
                address.LocationName = eachAddress.LocationName;
                address.Street = eachAddress.Street;
                //address.StreetNumber = eachAddress.Street2;
                address.City = eachAddress.City;
                address.State = eachAddress.State;
                address.CountryRegionId = eachAddress.CountryRegionID;
                address.ZipCode = FormatZipCode(eachAddress.ZipCode);
                address.Roles = eachAddress.Roles;
                //Add each address into address list
                AddressList.Add(address);
            }
            //return address list 
            log.Info("Exit CreateAddress() Account Number : " + Vendor.AccountNumber);

            return AddressList;
        }

        private string FormatZipCode(string ZipCode)
        {
            if (ZipCode.Length == 9)
                ZipCode = ZipCode.Substring(0, 5) + "-"+  ZipCode.Substring(5);
            return (ZipCode);
        }
        
        /// <summary>
        /// Update address with rec id
        /// </summary>
        /// <param name="axdEntityDirPartyDirPerson"></param>
        /// <param name="Vendor"></param>
        /// <returns></returns>
        private List<AxdEntity_DirPartyPostalAddressView> UpdateAddress(AxdEntity_DirPartyTable_DirPerson  axdEntityDirPartyDirPerson, Vendor Vendor)
        {
            log.Info("Enter UpdateAddress()  Account Number : " + Vendor.AccountNumber);

            //local variable uses to store the Vendor address details - once it is identified as create or update then put the action and store it in this object.
            List<AxdEntity_DirPartyPostalAddressView> addressList = new List<AxdEntity_DirPartyPostalAddressView>();

            // Local object uses to loop Vendor addresses
            List<AxdEntity_DirPartyPostalAddressView> addressListFromAxService = new List<AxdEntity_DirPartyPostalAddressView>();
            if (axdEntityDirPartyDirPerson != null)
            {
                //Checking if existing address there are not, if there just all addresses to our local variable
                if (axdEntityDirPartyDirPerson.DirPartyPostalAddressView != null)
                {
                    addressListFromAxService.AddRange(axdEntityDirPartyDirPerson.DirPartyPostalAddressView);

                    #region "Update Address"

                    if (Vendor.AddressList.Count() == 0)
                    {
                        AxdEntity_DirPartyPostalAddressView addressTemp = new AxdEntity_DirPartyPostalAddressView();
                        log.Info("Enter UpdateAddress(): No address from ACADIS and AX has " + addressListFromAxService.Count() + " address. So updating empty details in the address since we cannot delete shared address in AX.");

                        var query = from VendorAddress in addressListFromAxService
                                    select VendorAddress;

                        var enumerable = query.ToList();
                        int count = enumerable.Count;
                        #region "if only one address iin ax "
                        if (count == 1) // if count is 1 then update the record
                        {

                            addressTemp.action = AxdEnum_AxdEntityAction.update;
                            addressTemp.actionSpecified = true;

                            addressTemp.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                            addressTemp.updateModeSpecified = true;

                            addressTemp.RecId = enumerable[0].RecId;
                            addressTemp.RecIdSpecified = true;
                            addressTemp.RecVersion = enumerable[0].RecVersion;
                            addressTemp.RecVersionSpecified = true;

                            addressTemp.LocationName = string.Empty;
                            addressTemp.Street = string.Empty;
                            addressTemp.City = string.Empty;
                            addressTemp.State = string.Empty;
                            string coutry = (enumerable[0].CountryRegionId != null) ? enumerable[0].CountryRegionId : USA;

                            addressTemp.CountryRegionId = coutry;
                            addressTemp.ZipCode = string.Empty; ;

                            string roles = (enumerable[0].Roles != null) ? enumerable[0].Roles : Business;
                            addressTemp.Roles = roles;

                            //Add each address into address list
                            addressList.Add(addressTemp);
                            log.Info("Exit UpdateAddress()");
                            return addressList;
                        }
                        #endregion "if only one address in ax "
                        else
                        {
                            #region "if only one address in ax "

                            var querySelecttemp = from VendorAddress in addressListFromAxService
                                                  where VendorAddress.IsPrimary == AxdExtType_LogisticsIsPrimaryAddress.Yes
                                                  select VendorAddress;

                            var enumerable1 = querySelecttemp.ToList();

                            #region "if No primary address select first and update with empty record"
                            if (enumerable1.Count == 0)
                            {
                                #region "take first address to update "

                                query = (from VendorAddress in addressListFromAxService
                                         select VendorAddress).Take(1);

                                if (query.Count() == 1) // if count is 1 then update the record
                                {

                                    addressTemp.action = AxdEnum_AxdEntityAction.update;
                                    addressTemp.actionSpecified = true;

                                    addressTemp.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                                    addressTemp.updateModeSpecified = true;

                                    addressTemp.RecId = enumerable[0].RecId;
                                    addressTemp.RecIdSpecified = true;
                                    addressTemp.RecVersion = enumerable[0].RecVersion;
                                    addressTemp.RecVersionSpecified = true;

                                    addressTemp.LocationName = string.Empty;
                                    addressTemp.Street = string.Empty;
                                    addressTemp.City = string.Empty;
                                    addressTemp.State = string.Empty;
                                    string coutry = (enumerable[0].CountryRegionId != null) ? enumerable[0].CountryRegionId : USA;

                                    addressTemp.CountryRegionId = coutry;
                                    addressTemp.ZipCode = string.Empty; ;

                                    string roles = (enumerable[0].Roles != null) ? enumerable[0].Roles : Business;
                                    addressTemp.Roles = roles;

                                    //Add each address into address list
                                    addressList.Add(addressTemp);

                                }
                                #endregion "take first address to update "

                                #region "skip first address and delete other address "
                                foreach (var eachPostalAddress in addressListFromAxService.Skip(1))
                                {
                                    AxdEntity_DirPartyPostalAddressView address = new AxdEntity_DirPartyPostalAddressView();

                                    log.Info("Enter UpdateAddress(): For each address");
                                    address.action = AxdEnum_AxdEntityAction.delete;
                                    address.actionSpecified = true;
                                    address.RecId = eachPostalAddress.RecId;
                                    address.RecIdSpecified = true;
                                    address.RecVersion = eachPostalAddress.RecVersion;
                                    address.RecVersionSpecified = true;

                                    //Added the address in the address list
                                    addressList.Add(address);
                                    log.Info("Exit UpdateAddress()");
                                    return addressList;
                                }
                                #endregion "skip first address and delete other address "
                            }
                            #endregion "if No primary address select first and update with empty record"
                            #region "if primary address found"
                            else
                            {
                                #region "if  primary address select - update address"

                                addressTemp.action = AxdEnum_AxdEntityAction.update;
                                addressTemp.actionSpecified = true;

                                addressTemp.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                                addressTemp.updateModeSpecified = true;

                                addressTemp.RecId = enumerable1[0].RecId;
                                addressTemp.RecIdSpecified = true;
                                addressTemp.RecVersion = enumerable1[0].RecVersion;
                                addressTemp.RecVersionSpecified = true;

                                addressTemp.LocationName = string.Empty;
                                addressTemp.Street = string.Empty;
                                addressTemp.City = string.Empty;
                                addressTemp.State = string.Empty;
                                string coutry = (enumerable1[0].CountryRegionId != null) ? enumerable1[0].CountryRegionId : USA;

                                addressTemp.CountryRegionId = coutry;
                                addressTemp.ZipCode = string.Empty; ;

                                string roles = (enumerable1[0].Roles != null) ? enumerable1[0].Roles : Business;
                                addressTemp.Roles = roles;

                                //Add each address into address list
                                addressList.Add(addressTemp);
                                #endregion "if  primary address select - update address"

                                #region "if Update other records"

                                var queryUnmatchedRecords = (from eachaddressListFromAxService in addressListFromAxService
                                                             join eachAddressList in querySelecttemp.ToList() on eachaddressListFromAxService.LocationName equals
                                                                 eachAddressList.LocationName
                                                                 into combinedResult
                                                             from f in combinedResult.DefaultIfEmpty()

                                                             select new
                                                             {
                                                                 IsPrimary = eachaddressListFromAxService.IsPrimary,
                                                                 LocationName = eachaddressListFromAxService.LocationName,
                                                                 RecId = eachaddressListFromAxService.RecId,
                                                                 RecVersion = eachaddressListFromAxService.RecVersion,
                                                                 MissingProd = (f == null ? string.Empty : f.LocationName)

                                                             }).Where(a => a.MissingProd == string.Empty);

                                var unmatachedAddress = queryUnmatchedRecords.ToList();


                                foreach (var eachPostalAddress in unmatachedAddress.ToList())
                                {
                                    AxdEntity_DirPartyPostalAddressView address = new AxdEntity_DirPartyPostalAddressView();

                                    log.Info("Enter UpdateAddress(): For each address");
                                    address.action = AxdEnum_AxdEntityAction.delete;
                                    address.actionSpecified = true;
                                    address.RecId = eachPostalAddress.RecId;
                                    address.RecIdSpecified = true;
                                    address.RecVersion = eachPostalAddress.RecVersion;
                                    address.RecVersionSpecified = true;

                                    //Added the address in the address list
                                    addressList.Add(address);
                                }
                                log.Info("Exit UpdateAddress()");
                                return addressList;
                                #endregion "if Update other records"
                            }
                            #endregion "if primary address found"

                            #endregion "if only one address in ax "
                        }

                    }
                    #endregion "Update address"
                }
                

                #region " Looping Vendor addresses"
                // Looping all addresses

                ////To-Do Use ACADIS-ID to update the info.
                var querySelect = from VendorAddress in addressListFromAxService
                                  orderby VendorAddress.IsPrimary descending
                                  select VendorAddress;

                var enumerableQuery = querySelect.ToList();
                int i = 0;
                int addresscount = enumerableQuery.Count();

                foreach (var eachPostalAddress in Vendor.AddressList)
                {
                    AxdEntity_DirPartyPostalAddressView address = new AxdEntity_DirPartyPostalAddressView();


                    if (i < addresscount) // if count is 1 then update the record
                    {

                        if (enumerableQuery[i].RecId == null)
                        {
                            #region "Create address"

                            log.Info("Enter Create addres: For each address");

                            address.action = AxdEnum_AxdEntityAction.create;
                            address.actionSpecified = true;

                            //address.RecId = enumerable[0].RecId;
                            //address.RecIdSpecified = true;
                            //address.RecVersion = enumerable[0].RecVersion;
                            //address.RecVersionSpecified = true;

                            #endregion "Create address"
                        }
                        else
                        {
                            #region "Update Address"

                            log.Info("Enter UpdateAddress(): For each address");
                            address.action = AxdEnum_AxdEntityAction.update;
                            address.actionSpecified = true;

                            address.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                            address.updateModeSpecified = true;

                            address.RecId = enumerableQuery[i].RecId;
                            address.RecIdSpecified = true;
                            address.RecVersion = enumerableQuery[i].RecVersion;
                            address.RecVersionSpecified = true;

                            #endregion "Update address"
                        }
                    }
                    else
                    {
                        #region "Create address"

                        log.Info("Enter Create addres: For each address");

                        address.action = AxdEnum_AxdEntityAction.create;
                        address.actionSpecified = true;

                        #endregion "Create address"
                    }
                    i++;
                    #region "Address data"

                    // Checking is it is logistics primary address or not
                    AxdExtType_LogisticsIsPrimaryAddress isPrimary = (eachPostalAddress.IsPrimaryAddress)
                        ? AxdExtType_LogisticsIsPrimaryAddress.Yes
                        : AxdExtType_LogisticsIsPrimaryAddress.No;
                    address.IsPrimary = isPrimary;
                    address.IsPrimarySpecified = true;

                    address.LocationName = eachPostalAddress.LocationName;
                    address.Street = eachPostalAddress.Street;
                    address.City = eachPostalAddress.City;
                    address.State = eachPostalAddress.State;
                    address.CountryRegionId = eachPostalAddress.CountryRegionID;
                    address.ZipCode = FormatZipCode(eachPostalAddress.ZipCode); 
                    address.Roles = eachPostalAddress.Roles;

                    //Add each address into address list
                    addressList.Add(address);

                    #endregion " address"
                    log.Info("Exit UpdateAddress()");
                }
                #endregion " Loo[ping Vendor addresses"

                if (i <= addresscount)
                {
                    #region "unmatached Address"

                    var queryUnmatchedRecords = (from eachaddressListFromAxService in addressListFromAxService
                                                 orderby eachaddressListFromAxService.IsPrimary descending
                                                 select eachaddressListFromAxService).Skip(i);

                    var unmatachedAddress = queryUnmatchedRecords.ToList();


                    foreach (var eachPostalAddress in unmatachedAddress.ToList())
                    {
                        AxdEntity_DirPartyPostalAddressView address = new AxdEntity_DirPartyPostalAddressView();

                        log.Info("Enter UpdateAddress(): For each address");
                        address.action = AxdEnum_AxdEntityAction.delete;
                        address.actionSpecified = true;

                        //address.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                        //address.updateModeSpecified = true;

                        //
                        address.RecId = eachPostalAddress.RecId;
                        address.RecIdSpecified = true;
                        address.RecVersion = eachPostalAddress.RecVersion;
                        address.RecVersionSpecified = true;

                        //Added the address in the address list
                        addressList.Add(address);
                    }

                    #endregion "unmatached Address"
                }
            }
            log.Info("Exit UpdateAddress()  Account Number : " + Vendor.AccountNumber);
            return addressList;

        }
       /// <summary>
        /// Update address with rec id
        /// </summary>
        /// <param name="axdEntityDirPartyDirPerson"></param>
        /// <param name="Vendor"></param>
        /// <returns></returns>
        private List<AxdEntity_DirPartyContactInfoView> UpdateContactInfo(AxdEntity_DirPartyTable_DirPerson  axdEntityDirPartyDirPerson, Vendor Vendor)
        {
            log.Info("Enter UpdateContact()  Account Number : " + Vendor.AccountNumber);

            //local variable uses to store the Vendor Contact details - once it is identified as create or update then put the action and store it in this object.
            List<AxdEntity_DirPartyContactInfoView> ContactList = new List<AxdEntity_DirPartyContactInfoView>();

            // Local object uses to loop Vendor Contactes
            List<AxdEntity_DirPartyContactInfoView> ContactListFromAxService = new List<AxdEntity_DirPartyContactInfoView>();
            if (axdEntityDirPartyDirPerson != null)
            {
                //Checking if existing Contact there are not, if there just all Contactes to our local variable
                if (axdEntityDirPartyDirPerson.DirPartyContactInfoView != null)
                {
                    //ContactListFromAxService.AddRange(axdEntityDirPartyDirPerson.DirPartyContactInfoView);

                    #region "Delete existing Contact"
                    foreach (var eachPostalContact in axdEntityDirPartyDirPerson.DirPartyContactInfoView.ToList())
                    {
                        AxdEntity_DirPartyContactInfoView Contact = new AxdEntity_DirPartyContactInfoView();

                        log.Info("Enter UpdateContact(): For each Contact");
                        Contact.action = AxdEnum_AxdEntityAction.delete;
                        Contact.actionSpecified = true;

                        //Contact.updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                        //Contact.updateModeSpecified = true;

                        //
                        Contact.RecId = eachPostalContact.RecId;
                        Contact.RecIdSpecified = true;
                        Contact.RecVersion = eachPostalContact.RecVersion;
                        Contact.RecVersionSpecified = true;

                        //Added the Contact in the Contact list
                        ContactList.Add(Contact);
                    }

                    #endregion "Delete existing Contact"
                }

                #region "Create Contact"

                if (Vendor.ContactList.Count() > 0)
                {

                    foreach (var eachPostalContact in Vendor.ContactList)
                    {
                        AxdEntity_DirPartyContactInfoView ContactTemp = new AxdEntity_DirPartyContactInfoView();

                        // Checking is it is logistics primary Contact or not
                        AxdEnum_NoYes isPrimary = (eachPostalContact.IsPrimary)
                            ? AxdEnum_NoYes.Yes
                            : AxdEnum_NoYes.No;

                        ContactTemp.action = AxdEnum_AxdEntityAction.create;
                        ContactTemp.actionSpecified = true;
                        ContactTemp.IsPrimary = isPrimary;
                        ContactTemp.IsPrimarySpecified = true;

                        ContactTemp.Type = GetAddressType(eachPostalContact.Type);
                        ContactTemp.TypeSpecified = true;

                        ContactTemp.LocationName = eachPostalContact.ContactName;
                        ContactTemp.Locator = eachPostalContact.Locator;
                        ContactTemp.LocatorExtension = eachPostalContact.Extension; //Added for phone extension - Feb-23-2015
                        ContactTemp.Roles = eachPostalContact.Roles;
                        //Add each Contact into Contact list
                        ContactList.Add(ContactTemp);
                    }
                }

                #endregion "Create Contact"
            }
            log.Info("Exit UpdateContact()  Account Number : " + Vendor.AccountNumber);
            return ContactList;

        }
    
        /// <summary>
        /// Create any electronic contact (phone, mobile, fax, email, etc.)
        /// </summary>
        private AxdEntity_DirPartyContactInfoView CreateContactInfo(AxdEnum_AxdEntityAction action, Contact contact)
        {
            //Create an electronic address
            AxdEntity_DirPartyContactInfoView electronicAddress = new AxdEntity_DirPartyContactInfoView();

            if (action != AxdEnum_AxdEntityAction.create)
            {
                electronicAddress.action = action;
                electronicAddress.actionSpecified = true;
            }

            // check if primary address
            AxdEnum_NoYes isPrimaryContact = (contact.IsPrimary) ? AxdEnum_NoYes.Yes : AxdEnum_NoYes.No;
            electronicAddress.IsPrimary = isPrimaryContact;
            electronicAddress.IsPrimarySpecified = true;

            //get address type enum value 
            electronicAddress.Type = GetAddressType(contact.Type);
            electronicAddress.TypeSpecified = true;

            electronicAddress.LocationName = contact.ContactName;
            electronicAddress.Locator = contact.Locator;
            electronicAddress.LocatorExtension = contact.Extension; //Added for phone extension - Feb-23-2015
            electronicAddress.Roles = contact.Roles;
            //electronicAddress.LocatorExtension = contact.Roles;

            return electronicAddress;
        }
        ///Getting address type
        AxdEnum_LogisticsElectronicAddressMethodType GetAddressType(string type)
        {

            AxdEnum_LogisticsElectronicAddressMethodType addressMethodType;
            if (type == null)
                type = string.Empty;

            switch (Convert.ToString(type).ToUpper())
            {
                case "EMAIL":
                    addressMethodType = AxdEnum_LogisticsElectronicAddressMethodType.Email;
                    break;

                case "PHONE":
                    addressMethodType = AxdEnum_LogisticsElectronicAddressMethodType.Phone;
                    break;
                case "FAX":
                    addressMethodType = AxdEnum_LogisticsElectronicAddressMethodType.Fax;
                    break;
                case "TELEX":
                    addressMethodType = AxdEnum_LogisticsElectronicAddressMethodType.Telex;
                    break;
                case "URL":
                    addressMethodType = AxdEnum_LogisticsElectronicAddressMethodType.URL;
                    break;
                default:
                    addressMethodType = AxdEnum_LogisticsElectronicAddressMethodType.None;
                    break;
            }
            return addressMethodType;
        }
        /// <summary>
        /// PersonName is the Data Object that holds the Name of VendorS OF TYPE PERSON ONLY
        /// </summary>        
        private AxdEntity_PersonName SetPersonName(AxdEnum_AxdEntityAction action, Vendor Vendor, bool _isVendorCreate)
        {
            AxdEntity_PersonName retPersonName = new AxdEntity_PersonName();

            //for update we have to set the action  
            if (action != AxdEnum_AxdEntityAction.create)
            {
                retPersonName.action = action;
            }
            if (!_isVendorCreate)
                retPersonName.actionSpecified = true;
            //Set First name,middle name and last name
            retPersonName.FirstName = Vendor.FirstName;
            if (string.IsNullOrEmpty(Vendor.MiddleName))
                retPersonName.MiddleName = string.Empty;
            else
                retPersonName.MiddleName = Vendor.MiddleName;

            retPersonName.LastName = Vendor.LastName;
            return retPersonName;
        }
        #endregion "Creating Vendor"

        #region "Update Vendor"
        private AxdEntity_VendTable PrepareVendorUpdate(AxdEntity_VendTable VendTable, Vendor Vendor)
        {
            AxdEntity_VendTable copyVendor = null;
            AxdEntity_DirPartyTable_DirPerson  copyDirPerson = null;
            AxdEntity_DirPartyTable_DirPerson  dirPerson = null;
            
            #region "Person"
            {

                dirPerson = VendTable.DirPartyTable[0] as AxdEntity_DirPartyTable_DirPerson ;
                //Original DirParty. Retrieved from Read

                //Running the same method as if I were creating a new Vendor
                //Unless we set the properties to Update, the Vendor will be duplicated and an error will be thrown
                copyVendor = SetVendorTable(AxdEnum_AxdEntityAction.update, Vendor, false);

                copyDirPerson = copyVendor.DirPartyTable[0] as AxdEntity_DirPartyTable_DirPerson ;

                //Now I'm setting the system fields that will allow AX to know which record its going to Update
                copyVendor._DocumentHash = VendTable._DocumentHash;
                //copyVendor.RecId = VendTable.RecId;
                //copyVendor.RecIdSpecified = true;
                //copyVendor.RecVersion = VendTable.RecVersion;
                //copyVendor.RecVersionSpecified = true;

                //Setting update action
                copyVendor.action = AxdEnum_AxdEntityAction.update;
                copyVendor.actionSpecified = true;

                //For Addresses, Names and Contacts too
                if (copyDirPerson != null)
                {
                    #region "If person data is not null"

                    if (dirPerson != null)
                    {
                        copyDirPerson.RecId = dirPerson.RecId;
                        copyDirPerson.RecIdSpecified = true;
                        copyDirPerson.RecVersion = dirPerson.RecVersion;
                        copyDirPerson.RecVersionSpecified = true;


                        dirPerson.LanguageId = language;

                        //Setting update action
                        copyDirPerson.action = AxdEnum_AxdEntityAction.update;
                        copyDirPerson.actionSpecified = true;


                        //#region "User first and last name here. Cannot EXCEED 20 CHARACTERS  "

                        //copyDirPerson.NameAlias = Vendor.NameAlias;
                        //// if  name is less >20 then get 20 chars only
                        //if (Vendor.NameAlias != null)
                        //    if (Vendor.NameAlias.Length > 20)
                        //        copyDirPerson.NameAlias = Vendor.NameAlias.Substring(0, 20);

                        //#endregion "User first and last name here. Cannot EXCEED 20 CHARACTERS  "


                        //check if the PersonName is empty or not - starts here
                        if (copyDirPerson.PersonName != null && copyDirPerson.PersonName.Count() == 1)
                        {
                            copyDirPerson.PersonName[0].RecId = dirPerson.PersonName[0].RecId;
                            copyDirPerson.PersonName[0].RecIdSpecified = true;
                            copyDirPerson.PersonName[0].RecVersion = dirPerson.PersonName[0].RecVersion;
                            copyDirPerson.PersonName[0].RecVersionSpecified = true;

                            //Updating Vendor name
                            copyDirPerson.PersonName[0].FirstName = Vendor.FirstName;
                            copyDirPerson.PersonName[0].LastName = Vendor.LastName;

                            if (string.IsNullOrEmpty(Vendor.MiddleName))
                                copyDirPerson.PersonName[0].MiddleName = string.Empty;
                            else
                                copyDirPerson.PersonName[0].MiddleName = Vendor.MiddleName;

                            //Setting update action
                            copyDirPerson.PersonName[0].action = AxdEnum_AxdEntityAction.update;
                            copyDirPerson.PersonName[0].actionSpecified = true;
                            copyDirPerson.PersonName[0].updateMode = AxdEnum_ValidTimeStateUpdate.Correction;
                            copyDirPerson.PersonName[0].updateModeSpecified = true;
                        }
                        ////check if the PersonName is empty or not - ends here
                    }

                    #endregion "person data "

                    //// Check the address list is empty- if no address 
                    if (Vendor.AddressList != null)
                    {
                        // Update Address information
                        copyDirPerson.DirPartyPostalAddressView = UpdateAddress(dirPerson, Vendor).ToArray();
                    }
                    else
                    {
                        // make it null if no address in the list
                        copyDirPerson.DirPartyPostalAddressView = null;
                    }

                    //// Added Anantha -Dec-12-2014 : contact list added
                    if (Vendor.ContactList != null)
                    {
                        //Update contact information
                        copyDirPerson.DirPartyContactInfoView = UpdateContactInfo(dirPerson, Vendor).ToArray();
                    }
                    else
                    {
                        // make it null if no address in the list
                        copyDirPerson.DirPartyContactInfoView = null;
                    }

                    #region Update without Copy - NOT ACTIVE

                    #endregion //Commented

                    copyVendor.DirPartyTable[0] = copyDirPerson;
                }
            }
            #endregion "Person"
            return copyVendor;
        }

           #endregion "Update Vendor"

       }
}