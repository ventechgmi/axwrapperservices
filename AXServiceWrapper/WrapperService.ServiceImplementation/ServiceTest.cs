﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WrapperService.ServiceImplementation
{
    
     [ServiceContract]
    public interface IServiceTest
    {
        [OperationContract]
        string GetCurrentDate();
    }
     
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class ServiceTest : IServiceTest
    {
        public string GetCurrentDate()
        {
            return DateTime.Now.ToString();
        }
    }
}
