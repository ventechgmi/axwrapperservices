﻿/******************************************************************************************
'Created By     : ANANTHAKUMAR S
'Created Date   : 12/12/2014
'Description    : This is for translating the DefaultDimension business entities to data contracts and viceversa.
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DC = WrapperService.DataContracts;
using BE = WrapperService.BusinessEntities;

namespace WrapperService.ServiceImplementation.Translators
{

    #region "DefaultDimension Translator"
    /// <summary>
    /// To translate DefaultDimension datacontract to business entities and viceversa
    /// </summary>
    public class DefaultDimensionTranslator
    {
        /// <summary>
        /// Translate the data contract into business entity
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static BE.DefaultDimension TranslateToEntity(DC.DefaultDimension inputModel)
        {
            if (inputModel == null)
                return null;
           
            BE.DefaultDimension model = new BE.DefaultDimension();
            Mapper.Reset();
         
            Mapper.CreateMap<DC.DefaultDimension, BE.DefaultDimension>();

             Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<DC.DefaultDimension, BE.DefaultDimension>(inputModel);
            return model;
        }
        /// <summary>
        /// Translate the business entity to data contract
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static DC.DefaultDimension TranslateToDataContract(BE.DefaultDimension inputModel)
        {
            if (inputModel == null)
                return null;
           
            DC.DefaultDimension model = new DC.DefaultDimension();
            Mapper.Reset();
            Mapper.CreateMap<BE.DefaultDimension, DC.DefaultDimension>();
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<BE.DefaultDimension, DC.DefaultDimension>(inputModel);

            return model;
        }

        /// <summary>
        /// Translate the data contracts to business entities
        /// </summary>
        /// <returns></returns>
        public static List<BE.DefaultDimension> TranslateToEntities(List<DC.DefaultDimension> input)
        {
            return input.ConvertAll(c => TranslateToEntity(c)).ToList();
        }

        /// <summary>
        /// Translate the business entities to data contracts
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static List<DC.DefaultDimension> TranslateToDataContracts(List<BE.DefaultDimension> input)
        {
            return input.ConvertAll(c => TranslateToDataContract(c)).ToList();
        }
    }
    #endregion "DefaultDimension Translator"
}
