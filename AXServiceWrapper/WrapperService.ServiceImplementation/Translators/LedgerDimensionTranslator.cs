﻿/**********************************************************************************************************************************
Created By     : ANANTHAKUMAR S
Created Date   : 12/19/2014
Description    : This is for translating the LedgerDimension business entities to data contracts and viceversa.

-----------------------------------------------------------------------------------------------------------------------------------
Modified By     :  
Modified Date   : 
Description     : 

***********************************************************************************************************************************/

using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DC = WrapperService.DataContracts;
using BE = WrapperService.BusinessEntities;

namespace WrapperService.ServiceImplementation.Translators
{

    #region "LedgerDimension Translator"
    /// <summary>
    /// To translate LedgerDimension datacontract to business entities and viceversa
    /// </summary>
    public class LedgerDimensionTranslator
    {
        /// <summary>
        /// Translate the data contract into business entity
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static BE.LedgerDimension TranslateToEntity(DC.LedgerDimension inputModel)
        {
            if (inputModel == null)
                return null;
           
            BE.LedgerDimension model = new BE.LedgerDimension();
            Mapper.Reset();
            //First mapping child objects

            Mapper.CreateMap<DC.LedgerDimension, BE.LedgerDimension>()
                .ForMember(s => s.DefaultDimensionList, c => c.MapFrom(m => m.DefaultDimensionList)); //every object in the class should map like this
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<DC.LedgerDimension, BE.LedgerDimension>(inputModel);
            return model;
        }
        /// <summary>
        /// Translate the business entity to data contract
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static DC.LedgerDimension TranslateToDataContract(BE.LedgerDimension inputModel)
        {
            if (inputModel == null)
                return null;
           
            DC.LedgerDimension model = new DC.LedgerDimension();
           
            Mapper.Reset();
            Mapper.CreateMap<BE.LedgerDimension, DC.LedgerDimension>()
                .ForMember(s => s.DefaultDimensionList, c => c.MapFrom(m => m.DefaultDimensionList)); //every object in the class should map like this
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<BE.LedgerDimension, DC.LedgerDimension>(inputModel);

            return model;
        }

        /// <summary>
        /// Translate the data contracts to business entities
        /// </summary>
        /// <returns></returns>
        public static List<BE.LedgerDimension> TranslateToEntities(List<DC.LedgerDimension> input)
        {
            return input.ConvertAll(c => TranslateToEntity(c)).ToList();
        }

        /// <summary>
        /// Translate the business entities to data contracts
        /// </summary>
        /// <returns></returns>
        public static List<DC.LedgerDimension> TranslateToDataContracts(List<BE.LedgerDimension> input)
        {
            return input.ConvertAll(c => TranslateToDataContract(c)).ToList();
        }
    }
    #endregion "LedgerDimension Translator"
}
