﻿/**********************************************************************************************************************************
Created By     : ANANTHAKUMAR SUBRAMANI
Created Date   : 09/17/2015
Description    : This is for translating the Vendor business entities to data contracts and viceversa.
***********************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using WrapperService.BusinessEntities;
using DC = WrapperService.DataContracts;
using BE = WrapperService.BusinessEntities;

namespace WrapperService.ServiceImplementation.Translators
{

    #region "Vendor Translator"
    /// <summary>
    /// To translate Vendor datacontract to business entities and viceversa
    /// </summary>
    public class VendorTranslator
    {
        /// <summary>
        /// Translate the data contract into business entity
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static BE.Vendor TranslateToEntity(DC.Vendor inputModel)
        {
            if (inputModel == null)
                return null;
           
          
            BE.Vendor model = new BE.Vendor();
            Mapper.Reset();
            //First mapping child objects

            Mapper.CreateMap<DC.Vendor, BE.Vendor>()
                .ForMember(s => s.AddressList, c => c.MapFrom(m => m.AddressList))
                .ForMember(s => s.ContactList, c => c.MapFrom(m => m.ContactList)); // Added Ananthakumar - Dec-12-2014 : For contact details
            Mapper.CreateMap<DC.Address, BE.Address>();
            Mapper.CreateMap<DC.Contact, BE.Contact>(); // Added Ananthakumar - Dec-12-2014 : For contact details
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<DC.Vendor, BE.Vendor>(inputModel);
            return model;
        }
        /// <summary>
        /// Translate the business entity to data contract
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static DC.Vendor TranslateToDataContract(BE.Vendor inputModel)
        {
            if (inputModel == null)
                return null;
           
            DC.Vendor model = new DC.Vendor();
            Mapper.Reset();
            Mapper.CreateMap<BE.Vendor, DC.Vendor>()
                .ForMember(s => s.AddressList, c => c.MapFrom(m => m.AddressList))
                .ForMember(s => s.ContactList, c => c.MapFrom(m => m.ContactList)); // Added Ananthakumar - Dec-12-2014 : For contact details
            Mapper.CreateMap<BE.Address, DC.Address>();
            Mapper.CreateMap<BE.Contact, DC.Contact>(); // Added Ananthakumar - Dec-12-2014 : For contact details
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<BE.Vendor, DC.Vendor>(inputModel);

            return model;
        }

        /// <summary>
        /// Translate the data contracts to business entities
        /// </summary>
        /// <returns></returns>
        public static List<BE.Vendor> TranslateToEntities(List<DC.Vendor> input)
        {
            return input.ConvertAll(c => TranslateToEntity(c)).ToList();
        }

        /// <summary>
        /// Translate the business entities to data contracts
        /// </summary>
        /// <returns></returns>
        public static List<DC.Vendor> TranslateToDataContracts(List<BE.Vendor> input)
        {
            return input.ConvertAll(c => TranslateToDataContract(c)).ToList();
        }
    }
    #endregion "Vendor Translator"
}
