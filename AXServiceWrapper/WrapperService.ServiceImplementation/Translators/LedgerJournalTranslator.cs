﻿/**********************************************************************************************************************************
Created By     : ANANTHAKUMAR S
Created Date   : 12/19/2014
Description    : This is for translating the LedgerJournal business entities to data contracts and viceversa.

-----------------------------------------------------------------------------------------------------------------------------------
Modified By     :  
Modified Date   : 
Description     : 

***********************************************************************************************************************************/

using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DC = WrapperService.DataContracts;
using BE = WrapperService.BusinessEntities;

namespace WrapperService.ServiceImplementation.Translators
{

    #region "LedgerJournal Translator"
    /// <summary>
    /// To translate LedgerJournal datacontract to business entities and viceversa
    /// </summary>
    public class LedgerJournalTranslator
    {
        /// <summary>
        /// Translate the data contract into business entity
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static BE.LedgerJournal TranslateToEntity(DC.LedgerJournal inputModel)
        {
            if (inputModel == null)
                return null;
           
            BE.LedgerJournal model = new BE.LedgerJournal();
            Mapper.Reset();
            //First mapping child objects
            
            Mapper.CreateMap<DC.LedgerJournal, BE.LedgerJournal>()
                .ForMember(s => s.LedgerJournalTransactionList, c => c.MapFrom(m => m.LedgerJournalTransactionList)); //every object in the class should map like this
            Mapper.CreateMap<DC.LedgerJournalTransaction, BE.LedgerJournalTransaction>();
            Mapper.CreateMap<DC.LedgerDimension, BE.LedgerDimension>();
              Mapper.CreateMap<DC.DefaultDimension, BE.DefaultDimension>();  
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<DC.LedgerJournal, BE.LedgerJournal>(inputModel);
            return model;
        }
        /// <summary>
        /// Translate the business entity to data contract
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static DC.LedgerJournal TranslateToDataContract(BE.LedgerJournal inputModel)
        {
            if (inputModel == null)
                return null;
           
            DC.LedgerJournal model = new DC.LedgerJournal();
           
            Mapper.Reset();
            Mapper.CreateMap<BE.LedgerJournal, DC.LedgerJournal>()
                .ForMember(s => s.LedgerJournalTransactionList, c => c.MapFrom(m => m.LedgerJournalTransactionList)); //every object in the class should map like this
            Mapper.CreateMap<BE.LedgerDimension, DC.LedgerDimension>();
            Mapper.CreateMap<BE.LedgerJournalTransaction, DC.LedgerJournalTransaction>();
            Mapper.CreateMap<BE.DefaultDimension, DC.DefaultDimension>();
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<BE.LedgerJournal, DC.LedgerJournal>(inputModel);

            return model;
        }

        /// <summary>
        /// Translate the data contracts to business entities
        /// </summary>
        /// <returns></returns>
        public static List<BE.LedgerJournal> TranslateToEntities(List<DC.LedgerJournal> input)
        {
            return input.ConvertAll(c => TranslateToEntity(c)).ToList();
        }

        /// <summary>
        /// Translate the business entities to data contracts
        /// </summary>
        /// <returns></returns>
        public static List<DC.LedgerJournal> TranslateToDataContracts(List<BE.LedgerJournal> input)
        {
            return input.ConvertAll(c => TranslateToDataContract(c)).ToList();
        }
    }
    #endregion "LedgerJournal Translator"
}
