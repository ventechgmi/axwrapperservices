﻿/******************************************************************************************
'Created By     : ANANTHAKUMAR S
'Created Date   : 12/04/2014
'Description    : This is for translating the InvoiceLine business entities to data contracts and viceversa.
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DC = WrapperService.DataContracts;
using BE = WrapperService.BusinessEntities;

namespace WrapperService.ServiceImplementation.Translators.Invoice
{

    #region "InvoiceLine Translator"
    /// <summary>
    /// To translate InvoiceLine datacontract to business entities and viceversa
    /// </summary>
    public class InvoiceLineTranslator
    {
        /// <summary>
        /// Translate the data contract into business entity
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static BE.InvoiceLine TranslateToEntity(DC.InvoiceLine inputModel)
        {
            if (inputModel == null)
                return null;
           
            BE.InvoiceLine model = new BE.InvoiceLine();
            Mapper.Reset();

            Mapper.CreateMap<DC.InvoiceLine, BE.InvoiceLine>()
                .ForMember(s => s.InvoiceDefaultDimensionList, c => c.MapFrom(m => m.InvoiceDefaultDimensionList));

            Mapper.CreateMap<DC.DefaultDimension, BE.DefaultDimension>();

            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<DC.InvoiceLine, BE.InvoiceLine>(inputModel);
            return model;
        }

        /// <summary>
        /// Translate the business entity to data contract
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static DC.InvoiceLine TranslateToDataContract(BE.InvoiceLine inputModel)
        {
            if (inputModel == null)
                return null;
           
            DC.InvoiceLine model = new DC.InvoiceLine();
            Mapper.Reset();
            Mapper.CreateMap<BE.InvoiceLine, DC.InvoiceLine>()
                   .ForMember(s => s.InvoiceDefaultDimensionList, c => c.MapFrom(m => m.InvoiceDefaultDimensionList));
           
            Mapper.CreateMap<BE.DefaultDimension, DC.DefaultDimension>();  
          
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<BE.InvoiceLine, DC.InvoiceLine>(inputModel);

            return model;
        }

        /// <summary>
        /// Translate the data contracts to business entities
        /// </summary>
        /// <param></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public static List<BE.InvoiceLine> TranslateToEntities(List<DC.InvoiceLine> input)
        {
            return input.ConvertAll(c => TranslateToEntity(c)).ToList();
        }
        /// <summary>
        /// Translate the business entities to data contracts
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static List<DC.InvoiceLine> TranslateToDataContracts(List<BE.InvoiceLine> input)
        {
            return input.ConvertAll(c => TranslateToDataContract(c)).ToList();
        }
    }
    #endregion "InvoiceLine Translator"
}
