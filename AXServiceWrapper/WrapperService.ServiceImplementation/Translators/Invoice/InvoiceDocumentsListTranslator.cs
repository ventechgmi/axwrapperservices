﻿/**********************************************************************************************************************************
Created By     : ANANTHAKUMAR S
Created Date   : 01/06/2015
Description    : This is for translating the Invoice Documents List business entities to data contracts and viceversa.

-----------------------------------------------------------------------------------------------------------------------------------
Modified By     :  
Modified Date   : 
Description     : 

***********************************************************************************************************************************/

using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DC = WrapperService.DataContracts;
using BE = WrapperService.BusinessEntities;

namespace WrapperService.ServiceImplementation.Translators.Invoice
{

    #region "Invoice Translator"
    /// <summary>
    /// To translate Invoice datacontract to business entities and viceversa
    /// </summary>
    public class InvoiceDocumentsListTranslator
    {
        /// <summary>
        /// Translate the data contract into business entity
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static BE.InvoiceDocumentsList TranslateToEntity(DC.InvoiceDocumentsList inputModel)
        {
            if (inputModel == null)
                return null;
           
            BE.InvoiceDocumentsList model = new BE.InvoiceDocumentsList();
            Mapper.Reset();
            //First mapping child objects

            Mapper.CreateMap<DC.InvoiceDocumentsList, BE.InvoiceDocumentsList>()
                .ForMember(s => s.InvoiceDocsList, c => c.MapFrom(m => m.InvoiceDocumentList));
                //every object in the class should map like this
            Mapper.CreateMap<DC.InvoiceDocument, BE.InvoiceDocument>();
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<DC.InvoiceDocumentsList, BE.InvoiceDocumentsList>(inputModel);
            return model;
        }

        /// <summary>
        /// Translate the business entity to data contract
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static DC.InvoiceDocumentsList TranslateToDataContract(BE.InvoiceDocumentsList inputModel)
        {
            if (inputModel == null)
                return null;
           
            DC.InvoiceDocumentsList model = new DC.InvoiceDocumentsList();
            Mapper.Reset();
            Mapper.CreateMap<BE.InvoiceDocumentsList, DC.InvoiceDocumentsList>()
                .ForMember(s => s.InvoiceDocumentList, c => c.MapFrom(m => m.InvoiceDocsList));
            //every object in the class should map like this
            Mapper.CreateMap<BE.InvoiceDocument, DC.InvoiceDocument>();
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<BE.InvoiceDocumentsList, DC.InvoiceDocumentsList>(inputModel);

            return model;
        }

        /// <summary>
        /// Translate the data contracts to business entities
        /// </summary>
        /// <returns></returns>
        public static List<BE.InvoiceDocumentsList> TranslateToEntities(List<DC.InvoiceDocumentsList> input)
        {
            return input.ConvertAll(c => TranslateToEntity(c)).ToList();
        }

        /// <summary>
        /// Translate the business entities to data contracts
        /// </summary>
        /// <returns></returns>
        public static List<DC.InvoiceDocumentsList> TranslateToDataContracts(List<BE.InvoiceDocumentsList> input)
        {
            return input.ConvertAll(c => TranslateToDataContract(c)).ToList();
        }
    }
    #endregion "Invoice Translator"
}
