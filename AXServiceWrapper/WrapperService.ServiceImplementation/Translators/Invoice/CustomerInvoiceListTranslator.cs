﻿/**********************************************************************************************************************************
Created By     : ANANTHAKUMAR S
Created Date   : 12/31/2014
Description    : This is for translating the Invoice business entities to data contracts and viceversa.

-----------------------------------------------------------------------------------------------------------------------------------
Modified By     :  
Modified Date   : 
Description     : 

***********************************************************************************************************************************/

using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DC = WrapperService.DataContracts;
using BE = WrapperService.BusinessEntities;

namespace WrapperService.ServiceImplementation.Translators.Invoice
{

    #region "Invoice Translator"
    /// <summary>
    /// To translate Invoice datacontract to business entities and viceversa
    /// </summary>
    public class CustomerInvoiceListTranslator
    {
        /// <summary>
        /// Translate the data contract into business entity
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static BE.CustomerInvoiceList TranslateToEntity(DC.CustomerInvoiceList inputModel)
        {
            if (inputModel == null)
                return null;
           
            BE.CustomerInvoiceList model = new BE.CustomerInvoiceList();
            Mapper.Reset();
            //First mapping child objects

            Mapper.CreateMap<DC.CustomerInvoiceList, BE.CustomerInvoiceList>()
                .ForMember(s => s.InvoiceDetailsList, c => c.MapFrom(m => m.InvoiceDetailsList));
                //every object in the class should map like this
            //Mapper.CreateMap<DC.Invoice, BE.Invoice>();
            //Mapper.CreateMap<DC.InvoiceLine, BE.InvoiceLine>();
            //Mapper.CreateMap<DC.DefaultDimension, BE.DefaultDimension>();
            Mapper.CreateMap<DC.InvoiceDetails, BE.InvoiceDetails>();
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<DC.CustomerInvoiceList, BE.CustomerInvoiceList>(inputModel);
            return model;
        }

        /// <summary>
        /// Translate the business entity to data contract
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static DC.CustomerInvoiceList TranslateToDataContract(BE.CustomerInvoiceList inputModel)
        {
            if (inputModel == null)
                return null;
           
            DC.CustomerInvoiceList model = new DC.CustomerInvoiceList();
            Mapper.Reset();
             Mapper.CreateMap<BE.CustomerInvoiceList, DC.CustomerInvoiceList>();
            //Mapper.CreateMap<BE.CustomerInvoiceList, DC.CustomerInvoiceList>()
            //    .ForMember(s => s.InvoiceDetailsList, c => c.MapFrom(m => m.InvoiceDetailsList));
                //every object in the class should map like this
            //Mapper.CreateMap<BE.Invoice, DC.Invoice>();
            //Mapper.CreateMap<BE.InvoiceLine, DC.InvoiceLine>();
            //Mapper.CreateMap<BE.DefaultDimension, DC.DefaultDimension>(); // Added Ananthakumar - Dec-12-2014 : For contact details
             Mapper.CreateMap<BE.InvoiceDetails, DC.InvoiceDetails>();
           
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<BE.CustomerInvoiceList, DC.CustomerInvoiceList>(inputModel);

            return model;
        }

        /// <summary>
        /// Translate the data contracts to business entities
        /// </summary>
        /// <returns></returns>
        public static List<BE.CustomerInvoiceList> TranslateToEntities(List<DC.CustomerInvoiceList> input)
        {
            return input.ConvertAll(c => TranslateToEntity(c)).ToList();
        }

        /// <summary>
        /// Translate the business entities to data contracts
        /// </summary>
        /// <returns></returns>
        public static List<DC.CustomerInvoiceList> TranslateToDataContracts(List<BE.CustomerInvoiceList> input)
        {
            return input.ConvertAll(c => TranslateToDataContract(c)).ToList();
        }
    }
    #endregion "Invoice Translator"
}
