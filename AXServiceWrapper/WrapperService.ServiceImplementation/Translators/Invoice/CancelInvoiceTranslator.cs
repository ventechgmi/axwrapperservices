﻿/**********************************************************************************************************************************
Created By     : ANANTHAKUMAR S
Created Date   : 04/07/2015
Description    : translating the Invoice cancallation data contracts to business entities

-----------------------------------------------------------------------------------------------------------------------------------
Modified By     :  
Modified Date   : 
Description     : 

***********************************************************************************************************************************/

using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DC = WrapperService.DataContracts;
using BE = WrapperService.BusinessEntities;

namespace WrapperService.ServiceImplementation.Translators.Invoice
{

    #region "Invoice Translator"
    /// <summary>
    /// To translate Invoice datacontract to business entities
    /// </summary>
    public class CancelInvoiceTranslator
    {

        /// <summary>
        /// Translate the data contract into business entity
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static BE.InvoiceCancellation TranslateToEntity(DC.InvoiceCancellation inputModel)
        {
            if (inputModel == null)
                return null;
           
          
            BE.InvoiceCancellation model = new BE.InvoiceCancellation();
            Mapper.Reset();
            //First mapping child objects

            Mapper.CreateMap<DC.InvoiceCancellation, BE.InvoiceCancellation>();
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<DC.InvoiceCancellation, BE.InvoiceCancellation>(inputModel);
            return model;
        }
      
 
        /// <summary>
        /// Translate the business entities to data contracts
        /// </summary>
        /// <returns></returns>
        public static List<BE.InvoiceCancellation> TranslateToEntities(List<DC.InvoiceCancellation> input)
        {
            return input.ConvertAll(c => TranslateToEntity(c)).ToList();
        }
    }
    #endregion "Invoice Translator"
}
