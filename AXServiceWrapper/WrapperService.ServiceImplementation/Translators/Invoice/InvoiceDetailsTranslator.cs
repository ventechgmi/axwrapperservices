﻿/******************************************************************************************
'Created By     : ANANTHAKUMAR S
'Created Date   : 14/01/2015
'Description    : This is for translating the InvoiceDetails business entities to data contracts and viceversa.
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DC = WrapperService.DataContracts;
using BE = WrapperService.BusinessEntities;

namespace WrapperService.ServiceImplementation.Translators.Invoice
{

    #region "InvoiceDetails Translator"
    /// <summary>
    /// To translate InvoiceDetails datacontract to business entities and viceversa
    /// </summary>
    public class InvoiceDetailsTranslator
    {
        /// <summary>
        /// Translate the data contract into business entity
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static BE.InvoiceDetails TranslateToEntity(DC.InvoiceDetails inputModel)
        {
            if (inputModel == null)
                return null;
           
            BE.InvoiceDetails model = new BE.InvoiceDetails();
            Mapper.Reset();

            Mapper.CreateMap<DC.InvoiceDetails, BE.InvoiceDetails>();

            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<DC.InvoiceDetails, BE.InvoiceDetails>(inputModel);
            return model;
        }

        /// <summary>
        /// Translate the business entity to data contract
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static DC.InvoiceDetails TranslateToDataContract(BE.InvoiceDetails inputModel)
        {
            if (inputModel == null)
                return null;
           
            DC.InvoiceDetails model = new DC.InvoiceDetails();
            Mapper.Reset();
            Mapper.CreateMap<BE.InvoiceDetails, DC.InvoiceDetails>();
           
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<BE.InvoiceDetails, DC.InvoiceDetails>(inputModel);

            return model;
        }

        /// <summary>
        /// Translate the data contracts to business entities
        /// </summary>
        /// <param></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public static List<BE.InvoiceDetails> TranslateToEntities(List<DC.InvoiceDetails> input)
        {
            return input.ConvertAll(c => TranslateToEntity(c)).ToList();
        }
        /// <summary>
        /// Translate the business entities to data contracts
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static List<DC.InvoiceDetails> TranslateToDataContracts(List<BE.InvoiceDetails> input)
        {
            return input.ConvertAll(c => TranslateToDataContract(c)).ToList();
        }
    }
    #endregion "InvoiceDetails Translator"
}
