﻿/**********************************************************************************************************************************
Created By     : ANANTHAKUMAR S
Created Date   : 12/31/2014
Description    : This is for translating the Invoice business entities to data contracts and viceversa.

-----------------------------------------------------------------------------------------------------------------------------------
Modified By     :  
Modified Date   : 
Description     : 

***********************************************************************************************************************************/

using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DC = WrapperService.DataContracts;
using BE = WrapperService.BusinessEntities;

namespace WrapperService.ServiceImplementation.Translators.Invoice
{

    #region "Invoice Translator"
    /// <summary>
    /// To translate Invoice datacontract to business entities and viceversa
    /// </summary>
    public class CustomerInvoiceListDetailsTranslator
    {
        /// <summary>
        /// Translate the data contract into business entity
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static BE.CustomerInvoiceListDetails TranslateToEntity(DC.CustomerInvoiceListDetails inputModel)
        {
            if (inputModel == null)
                return null;
           
            BE.CustomerInvoiceListDetails model = new BE.CustomerInvoiceListDetails();
            Mapper.Reset();
            //First mapping child objects

            Mapper.CreateMap<DC.CustomerInvoiceListDetails, BE.CustomerInvoiceListDetails>();
                //every object in the class should map like this
            //Mapper.CreateMap<DC.Invoice, BE.Invoice>();
            //Mapper.CreateMap<DC.InvoiceLine, BE.InvoiceLine>();
            //Mapper.CreateMap<DC.DefaultDimension, BE.DefaultDimension>();
            Mapper.CreateMap<DC.InvoiceDetails, BE.InvoiceDetails>();
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<DC.CustomerInvoiceListDetails, BE.CustomerInvoiceListDetails>(inputModel);
            return model;
        }

        /// <summary>
        /// Translate the business entity to data contract
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static DC.CustomerInvoiceListDetails TranslateToDataContract(BE.CustomerInvoiceListDetails inputModel)
        {
            if (inputModel == null)
                return null;
           
            DC.CustomerInvoiceListDetails model = new DC.CustomerInvoiceListDetails();
            Mapper.Reset();
             Mapper.CreateMap<BE.CustomerInvoiceListDetails, DC.CustomerInvoiceListDetails>();
            //Mapper.CreateMap<BE.CustomerInvoiceListDetails, DC.CustomerInvoiceListDetails>()
            //    .ForMember(s => s.InvoiceDetailsList, c => c.MapFrom(m => m.InvoiceDetailsList));
                //every object in the class should map like this
            //Mapper.CreateMap<BE.Invoice, DC.Invoice>();
            //Mapper.CreateMap<BE.InvoiceLine, DC.InvoiceLine>();
            //Mapper.CreateMap<BE.DefaultDimension, DC.DefaultDimension>(); // Added Ananthakumar - Dec-12-2014 : For contact details
             Mapper.CreateMap<BE.InvoiceDetails, DC.InvoiceDetails>();
           
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<BE.CustomerInvoiceListDetails, DC.CustomerInvoiceListDetails>(inputModel);

            return model;
        }

        /// <summary>
        /// Translate the data contracts to business entities
        /// </summary>
        /// <returns></returns>
        public static List<BE.CustomerInvoiceListDetails> TranslateToEntities(List<DC.CustomerInvoiceListDetails> input)
        {
            return input.ConvertAll(c => TranslateToEntity(c)).ToList();
        }

        /// <summary>
        /// Translate the business entities to data contracts
        /// </summary>
        /// <returns></returns>
        public static List<DC.CustomerInvoiceListDetails> TranslateToDataContracts(List<BE.CustomerInvoiceListDetails> input)
        {
            return input.ConvertAll(c => TranslateToDataContract(c)).ToList();
        }
    }
    #endregion "Invoice Translator"
}
