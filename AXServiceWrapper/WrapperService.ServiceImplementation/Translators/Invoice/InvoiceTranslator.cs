﻿/**********************************************************************************************************************************
Created By     : ANANTHAKUMAR S
Created Date   : 12/17/2014
Description    : This is for translating the Invoice business entities to data contracts and viceversa.

-----------------------------------------------------------------------------------------------------------------------------------
Modified By     :  
Modified Date   : 
Description     : 

***********************************************************************************************************************************/

using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DC = WrapperService.DataContracts;
using BE = WrapperService.BusinessEntities;

namespace WrapperService.ServiceImplementation.Translators.Invoice
{

    #region "Invoice Translator"
    /// <summary>
    /// To translate Invoice datacontract to business entities and viceversa
    /// </summary>
    public class InvoiceTranslator
    {
        /// <summary>
        /// Translate the data contract into business entity
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static BE.Invoice TranslateToEntity(DC.Invoice inputModel)
        {
            if (inputModel == null)
                return null;
           
            BE.Invoice model = new BE.Invoice();
            Mapper.Reset();
            //First mapping child objects

            Mapper.CreateMap<DC.Invoice, BE.Invoice>()
                .ForMember(s => s.InvoiceLineList, c => c.MapFrom(m => m.InvoiceLineList)) //every object in the class should map like this
                .ForMember(s => s.InvoiceDefaultDimensionList, c => c.MapFrom(m => m.InvoiceDefaultDimensionList)); //every object in the class should map like this
            Mapper.CreateMap<DC.InvoiceLine, BE.InvoiceLine>();
            Mapper.CreateMap<DC.DefaultDimension, BE.DefaultDimension>();  
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<DC.Invoice, BE.Invoice>(inputModel);
            return model;
        }
        /// <summary>
        /// Translate the business entity to data contract
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static DC.Invoice TranslateToDataContract(BE.Invoice inputModel)
        {
            if (inputModel == null)
                return null;
           
            DC.Invoice model = new DC.Invoice();
            Mapper.Reset();
            Mapper.CreateMap<BE.Invoice, DC.Invoice>()
                .ForMember(s => s.InvoiceLineList, c => c.MapFrom(m => m.InvoiceLineList))
                .ForMember(s => s.InvoiceDefaultDimensionList, c => c.MapFrom(m => m.InvoiceDefaultDimensionList)); // Added Ananthakumar - Dec-12-2014 : For contact details
            Mapper.CreateMap<BE.InvoiceLine, DC.InvoiceLine>();
            Mapper.CreateMap<BE.DefaultDimension, DC.DefaultDimension>(); // Added Ananthakumar - Dec-12-2014 : For contact details
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<BE.Invoice, DC.Invoice>(inputModel);

            return model;
        }

        /// <summary>
        /// Translate the data contracts to business entities
        /// </summary>
        /// <returns></returns>
        public static List<BE.Invoice> TranslateToEntities(List<DC.Invoice> input)
        {
            return input.ConvertAll(c => TranslateToEntity(c)).ToList();
        }

        /// <summary>
        /// Translate the business entities to data contracts
        /// </summary>
        /// <returns></returns>
        public static List<DC.Invoice> TranslateToDataContracts(List<BE.Invoice> input)
        {
            return input.ConvertAll(c => TranslateToDataContract(c)).ToList();
        }
    }
    #endregion "Invoice Translator"
}
