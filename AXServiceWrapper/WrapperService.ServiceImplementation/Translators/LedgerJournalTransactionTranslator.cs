﻿/**********************************************************************************************************************************
Created By     : ANANTHAKUMAR SUBRAMANI
Created Date   : 12/19/2014
Description    : This is for translating the LedgerJournalTransactionTransaction business entities to data contracts and viceversa.

-----------------------------------------------------------------------------------------------------------------------------------
Modified By     :  
Modified Date   : 
Description     : 

***********************************************************************************************************************************/

using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.ServiceModel.Dispatcher;
using AutoMapper;
using DC = WrapperService.DataContracts;
using BE = WrapperService.BusinessEntities;

namespace WrapperService.ServiceImplementation.Translators
{

    #region "LedgerJournalTransaction Translator"
    /// <summary>
    /// To translate LedgerJournalTransaction datacontract to business entities and viceversa
    /// </summary>
    public class LedgerJournalTransactionTranslator
    {
        /// <summary>
        /// Translate the data contract into business entity
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static BE.LedgerJournalTransaction TranslateToEntity(DC.LedgerJournalTransaction inputModel)
        {
            if (inputModel == null)
                return null;
           
            BE.LedgerJournalTransaction model = new BE.LedgerJournalTransaction();
            Mapper.Reset();
            //First mapping child objects

            Mapper.CreateMap<DC.LedgerJournalTransaction, BE.LedgerJournalTransaction>()
                .ForMember(s => s.LedgerDimension, c => c.MapFrom(m => m.LedgerDimension)); //every object in the class should map like this
          
            Mapper.CreateMap<DC.LedgerDimension, BE.LedgerDimension>();
       
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<DC.LedgerJournalTransaction, BE.LedgerJournalTransaction>(inputModel);
            return model;
        }
        /// <summary>
        /// Translate the business entity to data contract
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static DC.LedgerJournalTransaction TranslateToDataContract(BE.LedgerJournalTransaction inputModel)
        {
            if (inputModel == null)
                return null;
           
            DC.LedgerJournalTransaction model = new DC.LedgerJournalTransaction();
           
            Mapper.Reset();
            Mapper.CreateMap<BE.LedgerJournalTransaction, DC.LedgerJournalTransaction>()
                .ForMember(s => s.LedgerDimension, c => c.MapFrom(m => m.LedgerDimension)); //every object in the class should map like this
            Mapper.CreateMap<BE.LedgerDimension, DC.LedgerDimension>();
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<BE.LedgerJournalTransaction, DC.LedgerJournalTransaction>(inputModel);

            return model;
        }

        /// <summary>
        /// Translate the data contracts to business entities
        /// </summary>
        /// <returns></returns>
        public static List<BE.LedgerJournalTransaction> TranslateToEntities(List<DC.LedgerJournalTransaction> input)
        {
            return input.ConvertAll(c => TranslateToEntity(c)).ToList();
        }

        /// <summary>
        /// Translate the business entities to data contracts
        /// </summary>
        /// <returns></returns>
        public static List<DC.LedgerJournalTransaction> TranslateToDataContracts(List<BE.LedgerJournalTransaction> input)
        {
            return input.ConvertAll(c => TranslateToDataContract(c)).ToList();
        }
    }
    #endregion "LedgerJournalTransaction Translator"
}
