﻿/******************************************************************************************
'Created By     : ANANTHAKUMAR S
'Created Date   : 12/03/2014
'Description    :
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DC = WrapperService.DataContracts;
using BE = WrapperService.BusinessEntities;

namespace WrapperService.ServiceImplementation.Translators
{
    #region "Validation Translator"
    public class ValidationDetailTranslator
    {
        //translates the validation detail object into 
         public static DC.ValidationDetail TranslateToDataContract(BE.ValidationDetail inputModel)
         {
             if (inputModel == null)
                 return null;
           
             DC.ValidationDetail model = new DC.ValidationDetail();

             //Resetting mapper
             Mapper.Reset();

             //mapping business entity to data contact to translate
             Mapper.CreateMap<BE.ValidationDetail, DC.ValidationDetail>();
             Mapper.AssertConfigurationIsValid();

             model = Mapper.Map<BE.ValidationDetail, DC.ValidationDetail>(inputModel);
             return model;
         }

         public static List<DC.ValidationDetail> TranslateToDataContracts(List<BE.ValidationDetail> input)
         {
             return input.ConvertAll(c => TranslateToDataContract(c)).ToList();
         }
    }
    #endregion "Validation Translator"
}
