﻿/******************************************************************************************
'Created By     : ANANTHAKUMAR S
'Created Date   : 09/15/2015
'Description    : This is for translating the Secretary business entities to data contracts and viceversa.
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using WrapperService.BusinessEntities;
using DC = WrapperService.DataContracts;
using BE = WrapperService.BusinessEntities;

namespace WrapperService.ServiceImplementation.Translators
{

    #region "Secretary Translator"
    /// <summary>
    /// To translate Secretary datacontract to business entities and viceversa
    /// </summary>
    public class SecretaryTranslator
    {
        /// <summary>
        /// Translate the data contract into business entity
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static BE.Secretary TranslateToEntity(DC.Secretary inputModel)
        {
            if (inputModel == null)
                return null;
           
            BE.Secretary model = new BE.Secretary();
            Mapper.Reset();
            //Mapper.CreateMap<DC.Secretary, DC.Secretary>()
            //  .ForMember(dest => inputModel.SecretaryList, opt => opt.MapFrom(src => src.SecretaryList));
         
            Mapper.CreateMap<DC.Secretary, BE.Secretary>();

             Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<DC.Secretary, BE.Secretary>(inputModel);
            return model;
        }
        /// <summary>
        /// Translate the business entity to data contract
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static DC.Secretary TranslateToDataContract(BE.Secretary inputModel)
        {
            if (inputModel == null)
                return null;
         
            DC.Secretary model = new DC.Secretary();
            Mapper.Reset();
            Mapper.CreateMap<BE.Secretary, DC.Secretary>();
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<BE.Secretary, DC.Secretary>(inputModel);

            return model;
        }
        /// <summary>
        /// Translate the data contracts to business entities
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static List<BE.Secretary> TranslateToEntities(List<DC.Secretary> input)
        {
            return input.ConvertAll(c => TranslateToEntity(c)).ToList();
        }
        /// <summary>
        /// Translate the business entities to data contracts
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static List<DC.Secretary> TranslateToDataContracts(List<BE.Secretary> input)
        {
            return input.ConvertAll(c => TranslateToDataContract(c)).ToList();
        }
    }
    #endregion "Secretary Translator"
}
