﻿/******************************************************************************************
'Created By     : ANANTHAKUMAR S
'Created Date   : 12/04/2014
'Description    : This is for translating the FiscalOfficer business entities to data contracts and viceversa.
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using WrapperService.BusinessEntities;
using DC = WrapperService.DataContracts;
using BE = WrapperService.BusinessEntities;

namespace WrapperService.ServiceImplementation.Translators
{

    #region "FiscalOfficer Translator"
    /// <summary>
    /// To translate FiscalOfficer datacontract to business entities and viceversa
    /// </summary>
    public class FiscalOfficerTranslator
    {
        /// <summary>
        /// Translate the data contract into business entity
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static BE.FiscalOfficer TranslateToEntity(DC.FiscalOfficer inputModel)
        {
            if (inputModel == null)
                return null;
           
            BE.FiscalOfficer model = new BE.FiscalOfficer();
            Mapper.Reset();
            //Mapper.CreateMap<DC.FiscalOfficer, DC.FiscalOfficer>()
            //  .ForMember(dest => inputModel.FiscalOfficerList, opt => opt.MapFrom(src => src.FiscalOfficerList));
         
            Mapper.CreateMap<DC.FiscalOfficer, BE.FiscalOfficer>();

             Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<DC.FiscalOfficer, BE.FiscalOfficer>(inputModel);
            return model;
        }
        /// <summary>
        /// Translate the business entity to data contract
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static DC.FiscalOfficer TranslateToDataContract(BE.FiscalOfficer inputModel)
        {
            if (inputModel == null)
                return null;
         
            DC.FiscalOfficer model = new DC.FiscalOfficer();
            Mapper.Reset();
            Mapper.CreateMap<BE.FiscalOfficer, DC.FiscalOfficer>();
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<BE.FiscalOfficer, DC.FiscalOfficer>(inputModel);

            return model;
        }
        /// <summary>
        /// Translate the data contracts to business entities
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static List<BE.FiscalOfficer> TranslateToEntities(List<DC.FiscalOfficer> input)
        {
            return input.ConvertAll(c => TranslateToEntity(c)).ToList();
        }
        /// <summary>
        /// Translate the business entities to data contracts
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static List<DC.FiscalOfficer> TranslateToDataContracts(List<BE.FiscalOfficer> input)
        {
            return input.ConvertAll(c => TranslateToDataContract(c)).ToList();
        }
    }
    #endregion "FiscalOfficer Translator"
}
