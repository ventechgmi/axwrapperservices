﻿/**********************************************************************************************************************************
Created By     : ANANTHAKUMAR S
Created Date   : 12/03/2014
Description    : This is for translating the customer business entities to data contracts and viceversa.

-----------------------------------------------------------------------------------------------------------------------------------
Modified By     : ANANTHAKUMAR S
Modified Date   : 12/12/2014
Description    : Added contact list for a customer

***********************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using WrapperService.BusinessEntities;
using DC = WrapperService.DataContracts;
using BE = WrapperService.BusinessEntities;

namespace WrapperService.ServiceImplementation.Translators
{

    #region "Customer Translator"
    /// <summary>
    /// To translate customer datacontract to business entities and viceversa
    /// </summary>
    public class CustomerTranslator
    {
        /// <summary>
        /// Translate the data contract into business entity
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static BE.Customer TranslateToEntity(DC.Customer inputModel)
        {
            if (inputModel == null)
                return null;
           
          
            BE.Customer model = new BE.Customer();
            Mapper.Reset();
            //First mapping child objects

            Mapper.CreateMap<DC.Customer, BE.Customer>()
                .ForMember(s => s.AddressList, c => c.MapFrom(m => m.AddressList))
                .ForMember(s => s.ContactList, c => c.MapFrom(m => m.ContactList)); // Added Ananthakumar - Dec-12-2014 : For contact details
            Mapper.CreateMap<DC.Address, BE.Address>();
            Mapper.CreateMap<DC.Contact, BE.Contact>(); // Added Ananthakumar - Dec-12-2014 : For contact details
            Mapper.CreateMap<DC.FiscalOfficer, BE.FiscalOfficer>(); // Added Ananthakumar - 09-15-2015 - VFFD changes
            Mapper.CreateMap<DC.Secretary, BE.Secretary>(); //   Added Ananthakumar - 09-15-2015 - VFFD changes
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<DC.Customer, BE.Customer>(inputModel);
            return model;
        }
        /// <summary>
        /// Translate the business entity to data contract
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static DC.Customer TranslateToDataContract(BE.Customer inputModel)
        {
            if (inputModel == null)
                return null;
           
            DC.Customer model = new DC.Customer();
            Mapper.Reset();
            Mapper.CreateMap<BE.Customer, DC.Customer>()
                .ForMember(s => s.AddressList, c => c.MapFrom(m => m.AddressList))
                .ForMember(s => s.ContactList, c => c.MapFrom(m => m.ContactList)); // Added Ananthakumar - Dec-12-2014 : For contact details
            Mapper.CreateMap<BE.Address, DC.Address>();
            Mapper.CreateMap<BE.Contact, DC.Contact>(); // Added Ananthakumar - Dec-12-2014 : For contact details
            Mapper.CreateMap<BE.FiscalOfficer, DC.FiscalOfficer>(); // Added Ananthakumar - 09-15-2015 - VFFD changes
            Mapper.CreateMap<BE.Secretary, DC.Secretary>(); //   Added Ananthakumar - 09-15-2015 - VFFD changes
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<BE.Customer, DC.Customer>(inputModel);

            return model;
        }

        /// <summary>
        /// Translate the data contracts to business entities
        /// </summary>
        /// <returns></returns>
        public static List<BE.Customer> TranslateToEntities(List<DC.Customer> input)
        {
            return input.ConvertAll(c => TranslateToEntity(c)).ToList();
        }

        /// <summary>
        /// Translate the business entities to data contracts
        /// </summary>
        /// <returns></returns>
        public static List<DC.Customer> TranslateToDataContracts(List<BE.Customer> input)
        {
            return input.ConvertAll(c => TranslateToDataContract(c)).ToList();
        }
    }
    #endregion "Customer Translator"
}
