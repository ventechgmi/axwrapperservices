﻿/******************************************************************************************
'Created By     : ANANTHAKUMAR S
'Created Date   : 12/04/2014
'Description    : This is for translating the Address business entities to data contracts and viceversa.
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using WrapperService.BusinessEntities;
using DC = WrapperService.DataContracts;
using BE = WrapperService.BusinessEntities;

namespace WrapperService.ServiceImplementation.Translators
{

    #region "Address Translator"
    /// <summary>
    /// To translate Address datacontract to business entities and viceversa
    /// </summary>
    public class AddressTranslator
    {
        /// <summary>
        /// Translate the data contract into business entity
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static BE.Address TranslateToEntity(DC.Address inputModel)
        {
            if (inputModel == null)
                return null;
           
            BE.Address model = new BE.Address();
            Mapper.Reset();
            //Mapper.CreateMap<DC.Address, DC.Address>()
            //  .ForMember(dest => inputModel.AddressList, opt => opt.MapFrom(src => src.AddressList));
         
            Mapper.CreateMap<DC.Address, BE.Address>();

             Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<DC.Address, BE.Address>(inputModel);
            return model;
        }
        /// <summary>
        /// Translate the business entity to data contract
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static DC.Address TranslateToDataContract(BE.Address inputModel)
        {
            if (inputModel == null)
                return null;
         
            DC.Address model = new DC.Address();
            Mapper.Reset();
            Mapper.CreateMap<BE.Address, DC.Address>();
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<BE.Address, DC.Address>(inputModel);

            return model;
        }
        /// <summary>
        /// Translate the data contracts to business entities
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static List<BE.Address> TranslateToEntities(List<DC.Address> input)
        {
            return input.ConvertAll(c => TranslateToEntity(c)).ToList();
        }
        /// <summary>
        /// Translate the business entities to data contracts
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static List<DC.Address> TranslateToDataContracts(List<BE.Address> input)
        {
            return input.ConvertAll(c => TranslateToDataContract(c)).ToList();
        }
    }
    #endregion "Address Translator"
}
