﻿/******************************************************************************************
'Created By     : ANANTHAKUMAR S
'Created Date   : 12/12/2014
'Description    : This is for translating the contact business entities to data contracts and viceversa.
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DC = WrapperService.DataContracts;
using BE = WrapperService.BusinessEntities;

namespace WrapperService.ServiceImplementation.Translators
{

    #region "Contact Translator"
    /// <summary>
    /// To translate Contact datacontract to business entities and viceversa
    /// </summary>
    public class ContactTranslator
    {
        /// <summary>
        /// Translate the data contract into business entity
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static BE.Contact TranslateToEntity(DC.Contact inputModel)
        {
            if (inputModel == null)
                return null;
           
            BE.Contact model = new BE.Contact();
            Mapper.Reset();
            //Mapper.CreateMap<DC.Contact, DC.Contact>()
            //  .ForMember(dest => inputModel.ContactList, opt => opt.MapFrom(src => src.ContactList));
         
            Mapper.CreateMap<DC.Contact, BE.Contact>();

             Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<DC.Contact, BE.Contact>(inputModel);
            return model;
        }
        /// <summary>
        /// Translate the business entity to data contract
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static DC.Contact TranslateToDataContract(BE.Contact inputModel)
        {
            if (inputModel == null)
                return null;
         
            DC.Contact model = new DC.Contact();
            Mapper.Reset();
            Mapper.CreateMap<BE.Contact, DC.Contact>();
            Mapper.AssertConfigurationIsValid();
            model = Mapper.Map<BE.Contact, DC.Contact>(inputModel);

            return model;
        }
        /// <summary>
        /// Translate the data contracts to business entities
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static List<BE.Contact> TranslateToEntities(List<DC.Contact> input)
        {
            return input.ConvertAll(c => TranslateToEntity(c)).ToList();
        }
        /// <summary>
        /// Translate the business entities to data contracts
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static List<DC.Contact> TranslateToDataContracts(List<BE.Contact> input)
        {
            return input.ConvertAll(c => TranslateToDataContract(c)).ToList();
        }
    }
    #endregion "Contact Translator"
}
