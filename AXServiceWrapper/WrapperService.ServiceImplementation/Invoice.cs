﻿/**********************************************************************************************************************************
Created By     : ANANTHAKUMAR SUBRAMANI
Created Date   : 12/17/2014
Description    : Service implementations for invoice activities ( Now, Implemented create alone )

-----------------------------------------------------------------------------------------------------------------------------------
Modified By     :  
Modified Date   : 
Description     : 

***********************************************************************************************************************************/

using System;
using System.Configuration;
using System.Net.Security;
using System.Security;
using System.ServiceModel;
using System.ServiceModel.Activation;
using WrapperService.BusinessLogic;
using WrapperService.DataAccessRepository.Interfaces;
using WrapperService.MessageContracts;
using WrapperService.ServiceContracts;
using WrapperService.ServiceImplementation.Translators;
using WrapperService.ServiceImplementation.Translators.Invoice;

namespace WrapperService.ServiceImplementation
{
    
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    //[ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    //[ServiceContract(ProtectionLevel = ProtectionLevel.Sign)]
     public class Invoice : IInvoice
    {
        // public class Invoice : IInvoice
        public Invoice()
        {
        }
        private readonly IInvoiceRepository _invoiceRepository;

        public Invoice(IInvoiceRepository invoiceRepository)
        {
            _invoiceRepository = invoiceRepository;
        }

        /// <summary>
        /// Create Invoice request and save Invoice 
        /// </summary>
        /// <returns></returns>
        public CreateInvoiceResponse CreateInvoice(CreateInvoiceRequest createInvoiceRequest)
        {
          //  InvoiceFactory InvoiceFactory = new InvoiceFactory(IInvoiceRepository);
            InvoiceFactory invoiceFactory = new InvoiceFactory(_invoiceRepository);
            
            CreateInvoiceResponse createInvoiceResponse = new CreateInvoiceResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
           // dynamic validationDetail = ValidationDetailTranslator.TranslateToDataContract(invoiceFactory.CreateInvoice(InvoiceTranslator.TranslateToEntities(createInvoiceRequest.Invoice, createInvoiceRequest.IsAutoPost)));
            dynamic validationDetail = ValidationDetailTranslator.TranslateToDataContract(invoiceFactory.CreateInvoice(InvoiceTranslator.TranslateToEntities(createInvoiceRequest.Invoice)));
            createInvoiceResponse.ValidationDetail = validationDetail;
            return createInvoiceResponse;
        }

        /// <summary>
        /// Get Customers invoices
        /// </summary>
        /// <returns></returns>
        public GetInvoicesResponse GetInvoices(GetInvoicesRequest getInvoicesRequest)
        {
            //  InvoiceFactory InvoiceFactory = new InvoiceFactory(IInvoiceRepository);
            InvoiceFactory invoiceFactory = new InvoiceFactory(_invoiceRepository);

            GetInvoicesResponse getInvoicesResponse = new GetInvoicesResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            getInvoicesResponse.CustomerInvoiceList = CustomerInvoiceListTranslator.TranslateToDataContracts(invoiceFactory.GetInvoices(getInvoicesRequest.AccountNumbers, getInvoicesRequest.Status));
                    // GetInvoicesResponse.ValidationDetail = validationDetail;
            return getInvoicesResponse;
        }

        /// <summary>
        /// Get Customers invoices
        /// </summary>
        /// <returns></returns>
        public GetInvoiceDetailsResponse GetInvoiceDetails(GetInvoiceDetailsRequest GetInvoiceDetailsRequest)
        {
            //  InvoiceFactory InvoiceFactory = new InvoiceFactory(IInvoiceRepository);
            InvoiceFactory invoiceFactory = new InvoiceFactory(_invoiceRepository);

            GetInvoiceDetailsResponse GetInvoiceDetailsResponse = new GetInvoiceDetailsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            GetInvoiceDetailsResponse.InvoiceDetailsList = CustomerInvoiceListDetailsTranslator.TranslateToDataContracts(invoiceFactory.GetInvoiceDetails(GetInvoiceDetailsRequest.BillabaleItemIDList));
            // GetInvoiceDetailsResponse.GetInvoiceDetailsRequest = GetInvoiceDetailsRequest;
            return GetInvoiceDetailsResponse;
        }

        /// <summary>
        /// Get Customers invoice PDF
        /// </summary>
        /// <returns></returns>
        public GetInvoiceDocumentsResponse GetInvoiceDocuments(GetInvoiceDocumentRequest getInvoiceDocumentRequest)
        {
            //
            InvoiceFactory invoiceFactory = new InvoiceFactory(_invoiceRepository);

            GetInvoiceDocumentsResponse getInvoiceDocumentsResponse = new GetInvoiceDocumentsResponse();
            //Translates the data contracts to business entities, pass it to repository and
            getInvoiceDocumentsResponse.InvoiceDocumentsList = InvoiceDocumentsListTranslator.TranslateToDataContracts(invoiceFactory.GetInvoiceDocuments(getInvoiceDocumentRequest.CustomerID, getInvoiceDocumentRequest.InvoiceIDsList));
            return getInvoiceDocumentsResponse;
        }

        /// <summary>
        /// Cancel Invoices
        /// </summary>
        /// <returns></returns>
        public CancelInvoiceResponse CancelInvoice(CancelInvoiceRequest CancelInvoiceRequest)
        {
            InvoiceFactory invoiceFactory = new InvoiceFactory(_invoiceRepository);

            CancelInvoiceResponse CancelInvoiceResponse = new CancelInvoiceResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            // dynamic validationDetail = ValidationDetailTranslator.TranslateToDataContract(invoiceFactory.CancelInvoice(InvoiceTranslator.TranslateToEntities(CancelInvoiceRequest.Invoice, CancelInvoiceRequest.IsAutoPost)));
            dynamic validationDetail = ValidationDetailTranslator.TranslateToDataContract(invoiceFactory.CancellInvoices(CancelInvoiceTranslator.TranslateToEntities(CancelInvoiceRequest.Invoice)));
            CancelInvoiceResponse.ValidationDetail = validationDetail;
            return CancelInvoiceResponse;
        }
       
    }
   
}

