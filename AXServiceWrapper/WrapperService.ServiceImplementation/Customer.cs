﻿/******************************************************************************************
'Created By     : ANANTHAKUMAR SUBRAMANI
'Created Date   : 12/04/2014
'Description    : Service implementations for Customer activities (Add, Update and Delete)
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System.ServiceModel.Activation;
using WrapperService.BusinessLogic;
using WrapperService.DataAccessRepository.Interfaces;
using WrapperService.MessageContracts;
using WrapperService.ServiceContracts;
using WrapperService.ServiceImplementation.Translators;

namespace WrapperService.ServiceImplementation
{
  //  [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.w3.org/2001/XMLSchema")]
 //   [System.Xml.Serialization.XmlRootAttribute("StringHeader", Namespace = "http://xwsinterop/soapwsdl/headers", IsNullable = true)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Customer : ICustomer
    {
      
        public Customer()
        {
        }
        private readonly ICustomerRepository _customerRepository;
        public Customer(ICustomerRepository customerRepository)
        {
            this._customerRepository = customerRepository;
        }
        /// <summary>
        /// Check the customer exists or not
        /// </summary>
        /// <param name="isCustomerExistsRequest"></param>
        /// <returns></returns>
        public IsCustomerExistsResponse IsCustomerExists(IsCustomerExistsRequest isCustomerExistsRequest)
        {
            var customerFactory = new CustomerFactory(_customerRepository);

            var isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            dynamic validationDetail = ValidationDetailTranslator.TranslateToDataContract(customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetail;
            return isCustomerExistsResponse;
        }

        /// <summary>
        /// Save customer request and save customer 
        /// </summary>
        /// <param name="saveCustomerRequest"></param>
        /// <returns></returns>
        public SaveCustomerResponse SaveCustomer(SaveCustomerRequest saveCustomerRequest)
        {
          //  CustomerFactory CustomerFactory = new CustomerFactory(ICustomerRepository);
            var customerFactory = new CustomerFactory(_customerRepository);
            
            var saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            dynamic validationDetail = ValidationDetailTranslator.TranslateToDataContract(customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;
            return saveCustomerResponse;
        }

        /// <summary>
        /// Disable Customer request and Disable Customer 
        /// </summary>
        /// <param name="disableCustomerRequest"></param>
        /// <returns></returns>
        public DisableCustomerResponse DisableCustomer(DisableCustomerRequest disableCustomerRequest)
        {
            //Creating an object with constructor injection
            var customerFactory = new CustomerFactory(_customerRepository);

            var disableCustomerResponse = new DisableCustomerResponse();

            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            dynamic validationDetail = ValidationDetailTranslator.TranslateToDataContract(customerFactory.DisableCustomer(disableCustomerRequest.AccountNumbers));

            disableCustomerResponse.ValidationDetail = validationDetail;
            return disableCustomerResponse;
        }

        /// <summary>
        /// Get customer(s) data based on Accoount numbers and returns a final XML string
        /// </summary>
        /// <param name="getCustomerRequest"></param>
        /// <returns></returns>
        public GetCustomerResponse GetCustomer(GetCustomerRequest getCustomerRequest)
        {
            //Creating an object with constructor injection
            var customerFactory = new CustomerFactory(_customerRepository);

            var getCustomerResponse = new GetCustomerResponse();

            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            getCustomerResponse.XMLString = customerFactory.GetCustomer(getCustomerRequest.AccountNumbersList);

            const string dataStartsWith = "<ClientDefinedQuery";
            getCustomerResponse.Result = getCustomerResponse.XMLString.StartsWith(dataStartsWith);

           // getCustomerResponse.XMLString = validationDetail;
            return getCustomerResponse;
        }

        /// <summary>
        /// Get customer(s) data based on Accoount numbers and returns a final XML string
        /// </summary>
        /// <param name="getCustomerRequest"></param>
        /// <returns></returns>
        public GetCustomerObjectResponse GetCustomerObject(GetCustomerObjectRequest GetCustomerObjectRequest)
        {
            //Creating an object with constructor injection
            var customerFactory = new CustomerFactory(_customerRepository);

            var GetCustomerObjectResponse = new GetCustomerObjectResponse();

            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            GetCustomerObjectResponse.Customer = CustomerTranslator.TranslateToDataContract(customerFactory.GetCustomerObject(GetCustomerObjectRequest.AccountNumbersList));

           // const string dataStartsWith = "<ClientDefinedQuery";
            GetCustomerObjectResponse.Result = GetCustomerObjectResponse.Customer.AccountNumber!= null;

            // GetCustomerObjectResponse.XMLString = validationDetail;
            return GetCustomerObjectResponse;
        }
    }
}

