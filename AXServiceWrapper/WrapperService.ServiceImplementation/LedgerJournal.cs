﻿/******************************************************************************************
'Created By     : ANANTHAKUMAR SUBRAMANI
'Created Date   : 12/19/2014
'Description    : Service implementations for Payment/Ledger Journal activities
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System.ServiceModel.Activation;
using WrapperService.BusinessLogic;
using WrapperService.DataAccessRepository.Interfaces;
using WrapperService.MessageContracts;
using WrapperService.ServiceContracts;
using WrapperService.ServiceImplementation.Translators;

namespace WrapperService.ServiceImplementation
{

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class LedgerJournal : ILedgerJournal
    {
        public LedgerJournal()
        {
        }
        private readonly ILedgerJournalRepository _ledgerJournalRepository;

        public LedgerJournal(ILedgerJournalRepository ledgerJournalRepository)
        {
            _ledgerJournalRepository = ledgerJournalRepository;
        }

        /// <summary>
        /// Create LedgerJournal request and save LedgerJournal 
        /// </summary>
        /// <returns></returns>
        public CreatePaymentResponse CreatePayment(CreatePaymentRequest createPaymentRequest)
        {
          //  LedgerJournalFactory LedgerJournalFactory = new LedgerJournalFactory(ILedgerJournalRepository);
            LedgerJournalFactory ledgerJournalFactory = new LedgerJournalFactory(_ledgerJournalRepository);
            CreatePaymentResponse createPaymentResponse = new CreatePaymentResponse();
          
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            dynamic validationDetail = ValidationDetailTranslator.TranslateToDataContract(ledgerJournalFactory.CreatePayment(LedgerJournalTranslator.TranslateToEntities(createPaymentRequest.LedgerJournalList)));
            createPaymentResponse.ValidationDetail = validationDetail;
            return createPaymentResponse;
        }
    }
}

