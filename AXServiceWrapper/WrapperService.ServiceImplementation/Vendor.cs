﻿/******************************************************************************************
Created By     : ANANTHAKUMAR SUBRAMANI
Created Date   : 09/17/2015
'Description    : Service implementations for Vendor activities (Add, Update and Delete)
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System.ServiceModel.Activation;
using WrapperService.BusinessLogic;
using WrapperService.DataAccessRepository.Interfaces;
using WrapperService.MessageContracts;
using WrapperService.ServiceContracts;
using WrapperService.ServiceImplementation.Translators;

namespace WrapperService.ServiceImplementation
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Vendor : IVendor
    {
      
        public Vendor()
        {
        }
        
        /// <summary>
        /// Check the Vendor exists or not
        /// </summary>
        /// <param name="isVendorExistsRequest"></param>
        /// <returns></returns>
        public IsVendorExistsResponse IsVendorExists(IsVendorExistsRequest isVendorExistsRequest)
        {
            var VendorFactory = new VendorFactory();

            var isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            dynamic validationDetail = ValidationDetailTranslator.TranslateToDataContract(VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetail;
            return isVendorExistsResponse;
        }

        /// <summary>
        /// Save Vendor request and save Vendor 
        /// </summary>
        /// <param name="saveVendorRequest"></param>
        /// <returns></returns>
        public SaveVendorResponse SaveVendor(SaveVendorRequest saveVendorRequest)
        {
          //  VendorFactory VendorFactory = new VendorFactory(IVendorRepository);
            var VendorFactory = new VendorFactory();
            
            var saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            dynamic validationDetail = ValidationDetailTranslator.TranslateToDataContract(VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;
            return saveVendorResponse;
        } 
    }
}

