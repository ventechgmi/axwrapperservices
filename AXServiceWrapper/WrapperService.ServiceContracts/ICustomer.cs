﻿/******************************************************************************************
'Created By: ANANTHAKUMAR S
'Created Date: 12/03/2014
'Description:
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.ServiceModel;
using WrapperService.DataContracts;
using WCF = System.ServiceModel;

using WrapperService.MessageContracts;

namespace WrapperService.ServiceContracts
{
    
    [ServiceContract(SessionMode = WCF.SessionMode.Allowed, ProtectionLevel = ProtectionLevel.None)]
    [ServiceKnownType(typeof(List<Customer>))]  
    public interface ICustomer
    {

        #region "Customer Methods"
        [OperationContract()]
        SaveCustomerResponse SaveCustomer(SaveCustomerRequest saveCustomerRequest);

        // Rename disableCustomer to DisableCustomer - starts here

        [OperationContract()]
        DisableCustomerResponse DisableCustomer(DisableCustomerRequest disableCustomerRequest);
       
        // Rename disableCustomer to DisableCustomer - ends here

        /// <summary>
        /// Get customer data based on acocunt Numbers 
        /// </summary>
        /// <param name="getCustomerRequest"></param>
        /// <returns></returns>
       [OperationContract()]
       GetCustomerResponse GetCustomer(GetCustomerRequest getCustomerRequest);
      
        /// <summary>
        /// Check customer is exists or not
        /// </summary>
        /// <param name="isCustomerExistsRequest"></param>
        /// <returns></returns>
       [OperationContract()]
       IsCustomerExistsResponse IsCustomerExists(IsCustomerExistsRequest isCustomerExistsRequest);

       /// <summary>
       /// Get customer Object based on acocunt Numbers 
       /// </summary>
       /// <param name="getCustomerRequest"></param>
       /// <returns></returns>
       [OperationContract()]
       GetCustomerObjectResponse GetCustomerObject(GetCustomerObjectRequest GetCustomerObjectRequest);
    
       #endregion

    }
}