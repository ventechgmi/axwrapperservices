﻿/******************************************************************************************
'Created By: ANANTHAKUMAR S
'Created Date: 12/17/2014
'Description:
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using System.Net.Security;
using WrapperService.DataContracts;
using WrapperService.MessageContracts;
using WCF = System.ServiceModel;

namespace WrapperService.ServiceContracts
{
    
    [WCF.ServiceContractAttribute(SessionMode = WCF.SessionMode.Allowed, ProtectionLevel = ProtectionLevel.None)]
    [WCF.ServiceKnownTypeAttribute(typeof(List<Invoice>))]
    public interface IInvoice
    {

        #region "Invoice Methods"
        /// <summary>
        /// Create invoice 
        /// </summary>
        /// <param name="createInvoiceRequest"></param>
        /// <returns></returns>
        [WCF.OperationContractAttribute()]
        CreateInvoiceResponse CreateInvoice(CreateInvoiceRequest createInvoiceRequest);


        /// <summary>
        /// Get Customer Invoices
        /// </summary>
        /// <returns></returns>
        [WCF.OperationContractAttribute()]
        GetInvoicesResponse GetInvoices(GetInvoicesRequest getInvoicesRequest);

        /// <summary>
        /// Get Invoice PDF
        /// </summary>
        /// <returns></returns>
        [WCF.OperationContractAttribute()]
        GetInvoiceDocumentsResponse GetInvoiceDocuments(GetInvoiceDocumentRequest getInvoiceDocumentRequest);


        /// <summary>
        /// Cancel invoices
        /// </summary>
        /// <param name="createInvoiceRequest"></param>
        /// <returns></returns>
        [WCF.OperationContractAttribute()]
        CancelInvoiceResponse CancelInvoice(CancelInvoiceRequest cancelInvoiceRequest);


        /// <summary>
        /// Get Invoice details based on billable item ids
        /// </summary>
        /// <returns></returns>
        [WCF.OperationContractAttribute()]
        GetInvoiceDetailsResponse GetInvoiceDetails(GetInvoiceDetailsRequest getInvoicesRequest);

        #endregion

    }
}
