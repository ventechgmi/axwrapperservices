﻿/******************************************************************************************
'Created By: ANANTHAKUMAR S
'Created Date: 12/03/2014
'Description:
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.ServiceModel;
using WrapperService.DataContracts;
using WCF = System.ServiceModel;

using WrapperService.MessageContracts;

namespace WrapperService.ServiceContracts
{
    
    [ServiceContract(SessionMode = WCF.SessionMode.Allowed, ProtectionLevel = ProtectionLevel.None)]
    [ServiceKnownType(typeof(List<Vendor>))]  
    public interface IVendor
    {

        #region "Vendor Methods"

        /// <summary>
        /// Save Vendor 
        /// </summary>
        /// <param name="saveVendorRequest"></param>
        /// <returns></returns>
        [OperationContract()]
        SaveVendorResponse SaveVendor(SaveVendorRequest saveVendorRequest);

        /// <summary>
        /// Check Vendor is exists or not
        /// </summary>
        /// <param name="isVendorExistsRequest"></param>
        /// <returns></returns>
       [OperationContract()]
       IsVendorExistsResponse IsVendorExists(IsVendorExistsRequest isVendorExistsRequest);
       #endregion

    }
}
