﻿/******************************************************************************************
'Created By: ANANTHAKUMAR S
'Created Date: 12/17/2014
'Description:
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using System.Net.Security;
using WrapperService.DataContracts;
using WrapperService.MessageContracts;
using WCF = System.ServiceModel;

namespace WrapperService.ServiceContracts
{
    
    [WCF.ServiceContractAttribute(SessionMode = WCF.SessionMode.Allowed, ProtectionLevel = ProtectionLevel.None)]
    [WCF.ServiceKnownTypeAttribute(typeof(List<Invoice>))]
    public interface ILedgerJournal
    {

        #region "Ledger Journal payment Methods"

        /// <summary>
        /// Create Payments 
        /// </summary>
        /// <param name="createPaymentRequest"></param>
        /// <returns></returns>
        [WCF.OperationContractAttribute()]
        CreatePaymentResponse CreatePayment(CreatePaymentRequest createPaymentRequest);
        #endregion "Ledger Journal payment Methods"
    }
}
