﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 12/17/2014
'Description: Fiscal Officer
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
namespace WrapperService.DataContracts.Interfaces
{
    public interface IFiscalOfficer
    {
        /// <summary>
        /// Account ID
        /// </summary>
        string AccountID { get; set; }
        /// <summary>
        /// First Name
        /// </summary>
        string FirstName { get; set; }
        /// <summary>
        /// Last name
        /// </summary>
        string LastName { get; set; }
        /// <summary>
        /// Middle name
        /// </summary>
        string MiddleName { get; set; }
        /// <summary>
        ///  Name
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// Suffix
        /// </summary>
        string Suffix { get; set; }
    }
}