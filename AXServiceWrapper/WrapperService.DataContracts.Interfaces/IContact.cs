﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/04/2014
'Description: Customer Address interface
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrapperService.DataContracts.Interfaces
{
     public interface IContact
    {
        /// <summary>
        /// It is an ACADIS unique id
        /// </summary>
        
         string ACADISID { get; set; }

        /// <summary>
        /// This is the location name in DIRParty contact view
        /// </summary>
        
         string ContactName { get; set; }

        /// <summary>
        /// Customer delivery location - Home,Delivery -value will be like Home  or  Home:Delivery
        /// </summary>
        
         string Roles { get; set; }

        /// <summary>
        /// If it is 0 or empty ,the type is Email
        /// </summary>
        
         string Type { get; set; }

        /// <summary>
        /// Type Specified
        /// </summary>
        
         bool TypeSpecified { get; set; }

        /// <summary>
        /// Phone Number or email or mail etc.
        /// </summary>
        
         string Locator { get; set; }
        /// <summary>
        /// Is Primary Address
        /// </summary>
        
         bool IsPrimary { get; set; }

        
         string Extension { get; set; } //Added By Ananthakumar - Feb-23-2015 Reason: Added for phone Extension
      
    }
}
