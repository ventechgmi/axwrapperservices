﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 12/17/2014
'Description: Invoice related properties
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
namespace WrapperService.DataContracts.Interfaces
{
    public interface IInvoice
    {
        /// <summary>
        /// Customer Reference
        /// </summary>
        string CustomerReference { get; set; }
        /// <summary>
        /// Invoice date
        /// </summary>
        DateTime? InvoiceDate { get; set; }
        /// <summary>
        /// Order Account
        /// </summary>
        string OrderAccount { get; set; }
        /// <summary>
        /// Invoice Account
        /// </summary>
        string InvoiceAccount { get; set; }
        /// <summary>
        /// Invoice Id
        /// </summary>
        string InvoiceId { get; set; }
        /// <summary>
        /// Currency Code
        /// </summary>
        string CurrencyCode { get; set; }
        /// <summary>
        /// Purchase order form number
        /// </summary>
        string PurchaseOrderFormNumber { get; set; }
        /// <summary>
        /// Letter Date
        /// </summary>
        DateTime? LetterDate { get; set; }

        /// <summary>
        /// Added AutoPost
        /// </summary>
        bool IsAutoPost { get; set; }
    }
}