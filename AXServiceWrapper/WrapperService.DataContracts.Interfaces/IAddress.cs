﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/04/2014
'Description: Customer Address interface
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrapperService.DataContracts.Interfaces
{
   public  interface IAddress
    {

        /// <summary>
        /// It is an ACADIS unique id
        /// </summary>
          string ACADISID { get; set; }
        
       /// <summary>
        /// Customer Street Name
        /// </summary>
         string Street { get; set; }

        /// <summary>
        /// Customer City Name
        /// </summary>
         string City { get; set; }


        /// <summary>
        /// Customer State
        /// </summary>
         string State { get; set; }


        /// <summary>
        /// Country Region ID
        /// </summary>
         string CountryRegionID { get; set; }


        /// <summary>
        /// Country Zipcode
        /// </summary>
         string ZipCode { get; set; }

        /// <summary>
        /// Customer delivery location - Home,Delivery -value will be like Home  or  Home:Delivery
        /// </summary>
         string Roles { get; set; }

        /// <summary>
        /// Location Name - Email
        /// </summary>
         string LocationName { get; set; }

         
        /// <summary>
        /// If it is 0 or empty ,the type is Email
        /// </summary>
         string Type { get; set; }

        /// <summary>
        /// Type Specified
        /// </summary>
         bool TypeSpecified { get; set; }

        /// <summary>
        /// Phone Number or email or mail etc.
        /// </summary>
         string Locator { get; set; }

         /// <summary>
         /// Is Primary Address
         /// </summary>
         bool IsPrimaryAddress { get; set; }
         //Added By Ananthakumar - Feb-23-2015 Reason: Line2 - starts here
         /// <summary>
         /// Customer Street Name2 (Line2)
         /// </summary>
           string Street2 { get; set; }
        //Added By Ananthakumar - Feb-23-2015 Reason: Line2 - ends here
     
    }
}
