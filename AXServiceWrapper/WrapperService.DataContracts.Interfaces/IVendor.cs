﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/03/2014
'Description: Vendor interface
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrapperService.DataContracts.Interfaces
{
   public  interface IVendor
    {
        /// <summary>
        /// Handles running unique account number
        /// </summary>
         string AccountNumber { get; set; }
        /// <summary>
        /// Vendor first name
        /// </summary>
         string FirstName { get; set; }
        /// <summary>
        /// Vendor middle Name
        /// </summary>
         string MiddleName { get; set; }
        /// Vendor Last Name
         string LastName { get; set; }
       
        /// <summary>
        /// Curreny Type 
        /// </summary>
         string Currency { get; set; }

        /// <summary>
        /// Vendor Group
        /// </summary>
         string Group { get; set; }

         
         
    }
}
