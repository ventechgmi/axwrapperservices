﻿/******************************************************************************************
'Created By     : ANANTHAKUMAR SUBRAMANI
'Created Date   : 12/17/2014
'Description    :  Unit Test project for Invoice management
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using WrapperService.BusinessEntities;
using WrapperService.DataAccessRepository.Interfaces;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using TestContext = Microsoft.VisualStudio.TestTools.UnitTesting.TestContext;
using System;

// used for mocking for objects
//using Invoice = WrapperService.BusinessEntities.Invoice;

namespace WrapperTestProject
{
    /// <summary>
    ///This is a test class for InvoiceTest and is intended
    ///to contain all InvoiceTest Unit Tests
    ///</summary>
    [TestFixture()]
    public class InvoiceTest
    {
        private TestContext _testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return _testContextInstance;
            }
            set
            {
                _testContextInstance = value;
            }
        }

        /// <summary>
        /// Create a new  invoice with out address
        /// </summary>
        [Test]
        public void CreateInvoiceAllData()
        {
            //declaring invoice line list
            List<InvoiceLine> invoiceLineList = new List<InvoiceLine>();

            //adding a description in invoice line list
            invoiceLineList.Add(new InvoiceLine() { Amount = 250, Description = "Test", ARCode = "C" , Quantity =1, UnitPrice=10, MainAccount="86100" , ARTrans=1 });

            //declaring invoice default dimension list by using collection initializer
            List<DefaultDimension> invoiceDefaultDimensionList = new List<DefaultDimension>
            {
                new DefaultDimension() {Name = "ACCOUNT", Value = "Admin"},
                new DefaultDimension() {Name = "AGENCYUSE", Value = "Admin"},
                new DefaultDimension() {Name = "ALI", Value = "800610"},
                new DefaultDimension() {Name = "DEPARTMENT", Value = "COM651600"},
                new DefaultDimension() {Name = "FUND", Value = "5460"},
                new DefaultDimension() {Name = "PROGRAM", Value = "3320C"}
            };

            //set up invoice data
            Invoice newInvoice = new Invoice()
            {
                CustomerReference = "12",
                InvoiceDate = DateTime.Today.AddDays(-30),
                OrderAccount = "1359476",
                InvoiceAccount = "1359476",
                InvoiceId = "INV123",
                PurchaseOrderFormNumber = "321",
                CurrencyCode = "USD",
                InvoiceLineList = invoiceLineList,
                InvoiceDefaultDimensionList = invoiceDefaultDimensionList
            };

            //// Assign the addnewwInvoice in expected,
            Invoice expectedResult = newInvoice;

            //Invoice Repository 
            Mock<IInvoiceRepository> mockInvoiceRepository = new Mock<IInvoiceRepository>();
            ValidationDetail actualValidationResult = new ValidationDetail
            {
                Result = true
            };

            
            //Setup the repository insert method
            mockInvoiceRepository.Setup(method => method.CreateInvoice(newInvoice)).Returns(actualValidationResult);

            //
            IInvoiceRepository value = mockInvoiceRepository.Object; // pass mock instance to interface
            // call mock to get data that is in database
            actualValidationResult = value.CreateInvoice(newInvoice); 
            
            //expected result
            ValidationDetail expectedValidationResult = new ValidationDetail
            {
                Result = true
            };

            Assert.IsNotNull(expectedResult); // test everything is OK that this stage
            Assert.AreEqual(actualValidationResult.Result, expectedValidationResult.Result); // test everything is OK that this stage

            Assert.IsNotNull(expectedResult.InvoiceId); // check if null
        }

        ///unit test for create a successful Invoice.
        [Test]
        public void CreateInvoiceWithoutInvDimension()
        {
            //declaring invoice line list
            List<InvoiceLine> invoiceLineList = new List<InvoiceLine>();

            //adding a description in invoice line list
            invoiceLineList.Add(new InvoiceLine() { Amount = 250, Description = "Test", ARCode = "C", Quantity = 1, UnitPrice = 10, MainAccount = "86100", ARTrans = 1 });
  
            //set up invoice data
            Invoice newInvoice = new Invoice()
            {
                CustomerReference = "Customer Ref",
                InvoiceDate = DateTime.Today.AddDays(-30),
                OrderAccount = "1359476",
                InvoiceAccount = "1359476",
                InvoiceId = "INV123",
                CurrencyCode = "USD",
                PurchaseOrderFormNumber = "321",
                InvoiceLineList = invoiceLineList,
            };
            //// Assign the addnewwInvoice in expected,
            Invoice expectedResult = newInvoice;

            //Invoice actualResult = new  Invoice();

            //Invoice Repository 
            Mock<IInvoiceRepository> mockInvoiceRepository = new Mock<IInvoiceRepository>();
            ValidationDetail actualValidationResult = new ValidationDetail
            {
                Result = true
            };
            
            //Setup the repository insert method
            mockInvoiceRepository.Setup(method => method.CreateInvoice(newInvoice)).Returns(actualValidationResult);

            //
            IInvoiceRepository value = mockInvoiceRepository.Object; // pass mock instance to interface

            actualValidationResult = value.CreateInvoice(newInvoice); // call mock to get data that is in database

            ValidationDetail expectedValidationResult = new ValidationDetail
            {
                Result = true
            };
            Assert.IsNotNull(expectedResult); // test everything is OK that this stage
            Assert.AreEqual(actualValidationResult.Result, expectedValidationResult.Result); // test everything is OK that this stage

            Assert.IsNotNull(expectedResult.InvoiceId); // check if null
        }

      /// <summary>
      /// Create an invoice without customer number
      /// </summary>
        [Test]
        public void CreateInvoiceWithoutCustomerNumber()
        {
            //declaring invoice line list
            List<InvoiceLine> invoiceLineList = new List<InvoiceLine>();

            //adding a description in invoice line list
            invoiceLineList.Add(new InvoiceLine() { Amount = 250, Description = "Adding invoice without invoice dimension", ARCode = "C" });

            //adding a description in invoice line list
            invoiceLineList.Add(new InvoiceLine() { Amount = 11, Description = "Testing by Ananth", ARCode = "C" });

            //set up invoice data
            Invoice newInvoice = new Invoice()
            {
                CustomerReference = "Customer Ref",
                InvoiceDate = DateTime.Today.AddDays(-30),
                OrderAccount = "",
                InvoiceAccount = "",
                InvoiceId = "INV123",
                PurchaseOrderFormNumber = "321",
                CurrencyCode = "USD",
                InvoiceLineList = invoiceLineList,
            };
            //// Assign the addnewwInvoice in expected,
            Invoice expectedResult = newInvoice;

            //Invoice actualResult = new  Invoice();

            //Invoice Repository 
            Mock<IInvoiceRepository> mockInvoiceRepository = new Mock<IInvoiceRepository>();
            ValidationDetail actualValidationResult = new ValidationDetail
            {
                Result = true
            };

            
            //Setup the repository insert method
            mockInvoiceRepository.Setup(method => method.CreateInvoice(newInvoice)).Returns(actualValidationResult);

            // pass mock instance to interface
            IInvoiceRepository value = mockInvoiceRepository.Object;

            //get actual result
            actualValidationResult = value.CreateInvoice(newInvoice); // call mock to get data that is in database
            
            //expected validation result
            ValidationDetail expectedValidationResult = new ValidationDetail
            {
                Result = true
            };

            Assert.IsNotNull(expectedResult); // test everything is OK that this stage
            Assert.AreEqual(actualValidationResult.Result, expectedValidationResult.Result); // test everything is OK that this stage

            Assert.IsNotNull(expectedResult.InvoiceId); // check if null
        }

        #region "get Customer Invoices"
        /// <summary>
        /// get Customer Invoices
        /// </summary>
        [Test]
        public void GetInvoices()
        {
            //declaring invoice line list
            List<InvoiceDetails> customerInvoiceList = new List<InvoiceDetails>();
            customerInvoiceList.Add(new InvoiceDetails() { InvoiceAccount = "201041" });
            const string accountNumber = "201041";
            const string status = "OPEN";

            //Invoice Repository 
            Mock<IInvoiceRepository> mockInvoiceRepository = new Mock<IInvoiceRepository>();

            //Setup the repository insert method
            mockInvoiceRepository.Setup(method => method.GetInvoices(accountNumber, status)).Returns(customerInvoiceList);

            // pass mock instance to interface
            IInvoiceRepository value = mockInvoiceRepository.Object;

            //get actual result
            var customerInvoiceListActual = value.GetInvoices(accountNumber, status); // call mock to get data that is in database

            Assert.AreEqual(customerInvoiceListActual[0].InvoiceAccount, accountNumber); // test everything is OK that this stage
        }
        #endregion " get Customer invoices"

        #region "Get Invoice Documents"
        /// <summary>
        /// Get Invoice Documents for customers
        /// </summary>
        [Test]
        public void GetInvoiceDocuemts()
        {
            //declaring invoice line list
            List<InvoiceDocumentsList> invoiceDocumentsListCollection = new List<InvoiceDocumentsList>();

            string CustomerID = "1359476";
            List<string> InvoiceIDs = new List<string>() {"1359476"};

            InvoiceDocumentsList invoiceDocumentsList = new InvoiceDocumentsList();
            invoiceDocumentsList.CustomerID = "1359476";

            invoiceDocumentsList.InvoiceDocsList = new List<InvoiceDocument>() { new InvoiceDocument() { FileName = "1359476.pdf", FileContentsByte = new byte[10], FileServerPath= string.Empty, InvoiceID = "1359476" } };
            invoiceDocumentsListCollection.Add(invoiceDocumentsList);

            //Invoice Repository 
            Mock<IInvoiceRepository> mockInvoiceRepository = new Mock<IInvoiceRepository>();

            string folderPath = string.Empty;
            //Setup the repository insert method
            mockInvoiceRepository.Setup(method => method.GetInvoiceDocument(folderPath,CustomerID, InvoiceIDs)).Returns(invoiceDocumentsList);

            // pass mock instance to interface
            IInvoiceRepository value = mockInvoiceRepository.Object;

           
            //get actual result
            var invoiceDocumentListActual = value.GetInvoiceDocument(folderPath,CustomerID, InvoiceIDs); // call mock to get data that is in database

            Assert.AreEqual(invoiceDocumentListActual.InvoiceDocsList[0].InvoiceID, InvoiceIDs[0]); // test everything is OK that this stage
        }
        #endregion " get Customer invoices"

        #region "Invoice Cancellation"

        /// <summary>
        /// Cancel invoice
        /// </summary>
        [Test]
        public void CancelInvoiceAllData()
        {

            //declaring InvoiceCancellation object
            InvoiceCancellation InvoiceCancellation = new InvoiceCancellation() { BillableItemID = "2", CancellationID = "1", CancellationDate = DateTime.Now, Reason = "Test" };
           
               //// Assign the InvoiceCancellation in expected,
            InvoiceCancellation expectedResult = InvoiceCancellation;

            //Invoice Repository 
            Mock<IInvoiceRepository> mockInvoiceRepository = new Mock<IInvoiceRepository>();
            ValidationDetail actualValidationResult = new ValidationDetail
            {
                Result = true
            };


            //Setup the repository insert method
            mockInvoiceRepository.Setup(method => method.CancelInvoice(InvoiceCancellation)).Returns(actualValidationResult);

            //
            IInvoiceRepository value = mockInvoiceRepository.Object; // pass mock instance to interface
            // call mock to get data that is in database
            actualValidationResult = value.CancelInvoice(InvoiceCancellation);

            //expected result
            ValidationDetail expectedValidationResult = new ValidationDetail
            {
                Result = true
            };

            Assert.IsNotNull(expectedResult); // test everything is OK that this stage
            Assert.AreEqual(actualValidationResult.Result, expectedValidationResult.Result); // test everything is OK that this stage

            Assert.IsNotNull(expectedResult.BillableItemID); // check if null
        }
        /// <summary>
        /// Cancel invoice
        /// </summary>
        [Test]
        public void CancelInvoicewithoutBillableItemID()
        {

            //declaring InvoiceCancellation object
            InvoiceCancellation InvoiceCancellation = new InvoiceCancellation() { CancellationID = "1", CancellationDate = DateTime.Now, Reason = "Test" };
           
               //// Assign the InvoiceCancellation in expected,
            InvoiceCancellation expectedResult = InvoiceCancellation;

            //Invoice Repository 
            Mock<IInvoiceRepository> mockInvoiceRepository = new Mock<IInvoiceRepository>();
            ValidationDetail actualValidationResult = new ValidationDetail
            {
                Result = false
            };


            //Setup the repository insert method
            mockInvoiceRepository.Setup(method => method.CancelInvoice(InvoiceCancellation)).Returns(actualValidationResult);

            //
            IInvoiceRepository value = mockInvoiceRepository.Object; // pass mock instance to interface
            // call mock to get data that is in database
            actualValidationResult = value.CancelInvoice(InvoiceCancellation);

            //expected result
            ValidationDetail expectedValidationResult = new ValidationDetail
            {
                Result = false
            };

            Assert.IsNotNull(expectedResult); // test everything is OK that this stage
            Assert.AreEqual(actualValidationResult.Result, expectedValidationResult.Result); // test everything is OK that this stage

            Assert.IsNotNull(expectedResult.BillableItemID); // check if null
        }
    }
    #endregion "Invoice Cancellation"
}
