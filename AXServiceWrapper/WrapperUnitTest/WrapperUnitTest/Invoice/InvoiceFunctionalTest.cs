﻿/******************************************************************************************
'Created By     : ANANTHAKUMAR SUBRAMANI
'Created Date   : 12/17/2014
'Description    :  Functional test for Invoice management ( create operation alone now)
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using WrapperService.BusinessLogic;
using WrapperService.DataContracts;
using WrapperService.MessageContracts;
using WrapperService.ServiceImplementation.Translators;
using WrapperService.ServiceImplementation.Translators.Invoice;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using TestContext = Microsoft.VisualStudio.TestTools.UnitTesting.TestContext;

// used for mocking for objects
//using Invoice = WrapperService.BusinessEntities.Invoice;

namespace WrapperTestProject
{


    /// <summary>
    ///This is a test class for InvoiceTest and is intended
    ///to contain all InvoiceTest Unit Tests
    ///</summary>
    [TestFixture()]
    public class InvoiceFunctionalTest
    {
        //Set the prefix for each invoice number
        private const string InvoiceNumberPrefix = "INV";

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        [TestInitialize()]
        public void Initialize()
        {

        }

        /// <summary>
        /// Generates a random string with the given length
        /// </summary>
        /// <param name="size">Size of the string</param>
        /// <returns>Random string</returns>
        private string RandomInvoiceNumber(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();

            for (int i = 0; i < size; i++)
            {
                builder.Append(Convert.ToInt32(random.Next(9)));
            }
            return builder.ToString();
        }

        // Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void InvoiceTestCleanup()
        {

        }

        #region "Positive Test cases"

        /// <summary>
        /// Create invoice with all data - Invoice, invoicve line and invoice dimension.
        /// </summary>
        [Test()]
        public void CreateInvoiceAllData()
        {
            //Create a 3 digit random invoice number 
            string invoiceNumber = RandomInvoiceNumber(3);
            //declaring invoice list
            List<Invoice> invoiceList = new List<Invoice>();

            //declaring invoice line list
            List<InvoiceLine> invoiceLineList = new List<InvoiceLine>();

            //declaring invoice default dimension list
            List<DefaultDimension> invoiceDefaultDimensionList = new List<DefaultDimension>
            {
                new DefaultDimension() {Name = "ACCOUNT", Value = "Admin"},
                new DefaultDimension() {Name = "AGENCYUSE", Value = "Admin"},
                new DefaultDimension() {Name = "ALI", Value = "800610"},
                new DefaultDimension() {Name = "DEPARTMENT", Value = "COM651600"},
                new DefaultDimension() {Name = "FUND", Value = "5460"},
                new DefaultDimension() {Name = "PROGRAM", Value = "3320C"}
            };

            //adding a description in invoice line list
            invoiceLineList.Add(new InvoiceLine()
            {
                Description = "Testing by Ananth",
                ARCode = "C",
                Quantity = 2,
                UnitPrice = 10,
                Amount = 20,
                VEN_BillableItemId = "1359476"
            });

            //adding a some default dimensions in invoice def dimension list
            //defaultDimensionList.Add(new InvoiceDefaultDimension() { Name = "GRANT", Value = "" }); //This Dimension has no values inside AX. Hence, commented.

            //set up invoice data
            Invoice newInvoice = new Invoice()
            {
                CustomerReference = "1",
                InvoiceDate = DateTime.Today.AddDays(-30),
                OrderAccount = "1359476",
                InvoiceAccount = "1359476",
                //InvoiceId = InvoiceNumberPrefix + invoiceNumber,
                PurchaseOrderFormNumber = "123",
                CurrencyCode = "USD",
                InvoiceLineList = invoiceLineList,
                InvoiceDefaultDimensionList = invoiceDefaultDimensionList
            };

            //adding an invoice in invoice lilst
            invoiceList.Add(newInvoice);

            CreateInvoiceRequest createInvoiceRequest = new CreateInvoiceRequest {Invoice = invoiceList};
            //Create Invoice factory object
            InvoiceFactory invoiceFactory = new InvoiceFactory();
            
            CreateInvoiceResponse createInvoiceResponse = new CreateInvoiceResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail actualValidationDetail =
                ValidationDetailTranslator.TranslateToDataContract(invoiceFactory.CreateInvoice(InvoiceTranslator.TranslateToEntities(createInvoiceRequest.Invoice)));
            createInvoiceResponse.ValidationDetail = actualValidationDetail;

            //Expected result is true
            const bool expectedResult = true;

            //Expected should be true - there are no end point
            Assert.AreEqual(expectedResult, actualValidationDetail.Result);

        }

        /// <summary>
        /// Create invoice with all data - Invoice, invoicve line and invoice dimension.
        /// </summary>
        [Test()]
        public void CreateInvoiceMultipleInvoiceLine()
        {
            //Create a 3 digit random invoice number 
            string invoiceNumber = RandomInvoiceNumber(3);
            //declaring invoice list
            List<Invoice> invoiceList = new List<Invoice>();

            #region  "Invoice line items"

            //declaring invoice line list
            List<InvoiceLine> invoiceLineList = new List<InvoiceLine>
            {
                new InvoiceLine()
                {
                    Description = "Testing Invoice Line 1Testing Invoice Line 1Testing Invoice Line 1Testing Invoice Line 1Testing Invoice Line 1",
                    ARCode = "C",
                    Quantity = 2,
                    UnitPrice = 10,
                    Amount = 20,
                    ARTrans = 1
                },
                new InvoiceLine()
                {
                    Description = "Testing Invoice Line 2",
                    ARCode = "C",
                    Quantity = 3,
                    UnitPrice = 10,
                    Amount = 30,
                    ARTrans = 2
                },
                new InvoiceLine()
                {
                    Description = "Testing Invoice Line 3",
                    ARCode = "C",
                    Quantity = 3,
                    UnitPrice = 30,
                    Amount = 90,
                    ARTrans = 3
                },
                new InvoiceLine()
                {
                    Description = "Testing Invoice Line 4",
                    ARCode = "C",
                    Quantity = 100,
                    UnitPrice = 4,
                    Amount = 400,
                    ARTrans = 4
                }
            };

            #endregion "Invoice line items"

            //declaring invoice default dimension list by using collection initializer
            List<DefaultDimension> invoiceDefaultDimensionList = new List<DefaultDimension>
            {
                new DefaultDimension() {Name = "ACCOUNT", Value = "Admin"},
                new DefaultDimension() {Name = "AGENCYUSE", Value = "Admin"},
                new DefaultDimension() {Name = "ALI", Value = "800610"},
                new DefaultDimension() {Name = "DEPARTMENT", Value = "COM651600"},
                new DefaultDimension() {Name = "FUND", Value = "5460"},
                new DefaultDimension() {Name = "PROGRAM", Value = "3320C"}
            };

            //adding a some default dimensions in invoice def dimension list
            //defaultDimensionList.Add(new InvoiceDefaultDimension() { Name = "GRANT", Value = "" }); //This Dimension has no values inside AX. Hence, commented.

            //set up invoice data
            Invoice newInvoice = new Invoice()
            {
                CustomerReference = "123",
                InvoiceDate = DateTime.Today.AddDays(-30),
                OrderAccount = "1359476",
                InvoiceAccount = "1359476",
                //InvoiceId = InvoiceNumberPrefix + invoiceNumber,
                PurchaseOrderFormNumber = "123",
                CurrencyCode = "USD",
                InvoiceLineList = invoiceLineList,
                InvoiceDefaultDimensionList = invoiceDefaultDimensionList
            };

            //adding an invoice in invoice lilst
            invoiceList.Add(newInvoice);

            CreateInvoiceRequest createInvoiceRequest = new CreateInvoiceRequest {Invoice = invoiceList};
            //Create Invoice factory object
            InvoiceFactory invoiceFactory = new InvoiceFactory();
            
            CreateInvoiceResponse createInvoiceResponse = new CreateInvoiceResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail actualValidationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    invoiceFactory.CreateInvoice(InvoiceTranslator.TranslateToEntities(createInvoiceRequest.Invoice)));
            createInvoiceResponse.ValidationDetail = actualValidationDetail;

            //Expected result is true
            const bool expectedResult = true;

            //Expected should be true - there are no end point
            Assert.AreEqual(expectedResult, actualValidationDetail.Result);

        }

        /// <summary>
        /// Create an invoice without invoice dimensions, Now pass Invoice and Invoice line
        /// </summary>
        [Test()]
        public void CreateInvoiceWithoutInvDimensionNoAutoPost()
        {
            //Create a 3 digit random invoice number 
            string invoiceNumber = RandomInvoiceNumber(3);

            //declaring invoice list
            List<Invoice> invoiceList = new List<Invoice>();

            //declaring invoice line list
            List<InvoiceLine> invoiceLineList = new List<InvoiceLine>();

            //adding a description in invoice line list
            invoiceLineList.Add(new InvoiceLine()
            {
                Amount = 250,
                Description = "Adding invoice without invoice dimension",
                ARCode = "C",
                Quantity = 1,
                UnitPrice = 250
            });

            //set up invoice data
            Invoice newInvoice = new Invoice()
            {
                CustomerReference = "Customer Ref",
                InvoiceDate = DateTime.Today.AddDays(-30),
                OrderAccount = "1359476",
                InvoiceAccount = "1359476",
                InvoiceId = InvoiceNumberPrefix + invoiceNumber,
                CurrencyCode = "USD",
                PurchaseOrderFormNumber = "321",
                InvoiceLineList = invoiceLineList,
                IsAutoPost = false,
            };

            //adding an invoice in invoice lilst
            invoiceList.Add(newInvoice);

            CreateInvoiceRequest createInvoiceRequest = new CreateInvoiceRequest {Invoice = invoiceList};
            //Create Invoice factory object
            InvoiceFactory invoiceFactory = new InvoiceFactory();
            
          
            CreateInvoiceResponse createInvoiceResponse = new CreateInvoiceResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail actualValidationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    invoiceFactory.CreateInvoice(InvoiceTranslator.TranslateToEntities(createInvoiceRequest.Invoice)));
            createInvoiceResponse.ValidationDetail = actualValidationDetail;

            //Expected result is true
            const bool expectedResult = true;

            //Expected should be true - there are no end point
            Assert.AreEqual(expectedResult, actualValidationDetail.Result);

        }

        /// <summary>
        /// Create an invoice without invoice dimensions, Now pass Invoice and Invoice line
        /// </summary>
        [Test()]
        public void CreateInvoiceWithoutInvDimensionAutoPost()
        {
            //Create a 3 digit random invoice number 
            //string invoiceNumber = RandomInvoiceNumber(3);

            //declaring invoice list
            List<Invoice> invoiceList = new List<Invoice>();

            //declaring invoice line list
            List<InvoiceLine> invoiceLineList = new List<InvoiceLine>();

            //adding a description in invoice line list
            invoiceLineList.Add(new InvoiceLine()
            {
                Amount = 250,
                Description = "Adding invoice without invoice dimension",
                ARCode = "C",
                Quantity = 1,
                UnitPrice = 250
            });

            //set up invoice data
            Invoice newInvoice = new Invoice()
            {
                CustomerReference = "Customer Ref",
                InvoiceDate = DateTime.Today.AddDays(-30),
                OrderAccount = "1359476",
                InvoiceAccount = "1359476",
              //  InvoiceId = InvoiceNumberPrefix + invoiceNumber,
                CurrencyCode = "USD",
                PurchaseOrderFormNumber = "321",
                InvoiceLineList = invoiceLineList,
                IsAutoPost = true,
            };

            //adding an invoice in invoice lilst
            invoiceList.Add(newInvoice);

            CreateInvoiceRequest createInvoiceRequest = new CreateInvoiceRequest { Invoice = invoiceList };
            //Create Invoice factory object
            InvoiceFactory invoiceFactory = new InvoiceFactory();


            CreateInvoiceResponse createInvoiceResponse = new CreateInvoiceResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail actualValidationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    invoiceFactory.CreateInvoice(InvoiceTranslator.TranslateToEntities(createInvoiceRequest.Invoice)));
            createInvoiceResponse.ValidationDetail = actualValidationDetail;

            //Expected result is true
            const bool expectedResult = true;

            //Expected should be true - there are no end point
            Assert.AreEqual(expectedResult, actualValidationDetail.Result);

        }

        /// <summary>
        /// get Invoices for a customer - Open invocie
        /// </summary>
        [Test()]
        public void GetInvoiceForSingleCustomer()
        {
            GetInvoicesRequest getInvoicesRequest = new GetInvoicesRequest
            {
                AccountNumbers = new List<string>() {"008"},
                Status="Open"
            };
            //Create Invoice factory object
            InvoiceFactory invoiceFactory = new InvoiceFactory();

            GetInvoicesResponse getInvoicesResponse = new GetInvoicesResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            getInvoicesResponse.CustomerInvoiceList =
                CustomerInvoiceListTranslator.TranslateToDataContracts(
                    invoiceFactory.GetInvoices(getInvoicesRequest.AccountNumbers, getInvoicesRequest.Status));

            //Expected result is true
            const bool expectedResult = true;
            bool actualResult = getInvoicesResponse.CustomerInvoiceList[0].InvoiceDetailsList.Any();

            //Expected should be true - there are no end point
            Assert.AreEqual(expectedResult, actualResult);

        }
        /// <summary>
        /// get all invoices  for a customer
        /// </summary>
        public void GetInvoicesForSingleCustomerwithALL()
        {
            GetInvoicesRequest getInvoicesRequest = new GetInvoicesRequest
            {
                AccountNumbers = new List<string>() { "008" },
                Status = "ALL"
            };
            //Create Invoice factory object
            InvoiceFactory invoiceFactory = new InvoiceFactory();

            GetInvoicesResponse getInvoicesResponse = new GetInvoicesResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            getInvoicesResponse.CustomerInvoiceList =
                CustomerInvoiceListTranslator.TranslateToDataContracts(
                    invoiceFactory.GetInvoices(getInvoicesRequest.AccountNumbers, getInvoicesRequest.Status));

            //Expected result is true
            const bool expectedResult = true;
            bool actualResult = getInvoicesResponse.CustomerInvoiceList[0].InvoiceDetailsList.Any();

            //Expected should be true - there are no end point
            Assert.AreEqual(expectedResult, actualResult);

        }

        /// <summary>
        /// get Closed invoices  for a customer
        /// </summary>
        public void GetInvoicesForSingleCustomerwithClosed()
        {
            GetInvoicesRequest getInvoicesRequest = new GetInvoicesRequest
            {
                AccountNumbers = new List<string>() { "008" },
                Status = "CLOSED"
            };
            //Create Invoice factory object
            InvoiceFactory invoiceFactory = new InvoiceFactory();

            GetInvoicesResponse getInvoicesResponse = new GetInvoicesResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            getInvoicesResponse.CustomerInvoiceList =
                CustomerInvoiceListTranslator.TranslateToDataContracts(
                    invoiceFactory.GetInvoices(getInvoicesRequest.AccountNumbers, getInvoicesRequest.Status));

            //Expected result is true
            const bool expectedResult = true;
            bool actualResult = getInvoicesResponse.CustomerInvoiceList[0].InvoiceDetailsList.Any();

            //Expected should be true - there are no end point
            Assert.AreEqual(expectedResult, actualResult);

        }
        /// <summary>
        /// get Invoices for multipl customers
        /// </summary>
        [Test()]
        public void GetInvoiceForMultipleCustomers()
        {
            //declaring invoice list
            // List<CustomerInvoiceList> customerInvoiceList = new List<CustomerInvoiceList>();


            GetInvoicesRequest GetInvoicesRequest = new GetInvoicesRequest
            {
                AccountNumbers = new List<string>() {"201041", "1359476", "201041"},
                Status="OPEN"
            };
            //Create Invoice factory object
            InvoiceFactory invoiceFactory = new InvoiceFactory();

            GetInvoicesResponse GetInvoicesResponse = new GetInvoicesResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            GetInvoicesResponse.CustomerInvoiceList =
                CustomerInvoiceListTranslator.TranslateToDataContracts(
                    invoiceFactory.GetInvoices(GetInvoicesRequest.AccountNumbers, GetInvoicesRequest.Status));

            //Expected result is true
            const bool expectedResult = true;
            bool actualResult1 = GetInvoicesResponse.CustomerInvoiceList[0].InvoiceDetailsList.Any();
            bool actualResult2 = GetInvoicesResponse.CustomerInvoiceList[1].InvoiceDetailsList.Any();
            bool actualResult3 = GetInvoicesResponse.CustomerInvoiceList[2].InvoiceDetailsList.Any();

            bool actualResult = actualResult1 && actualResult2 && actualResult3;

            //Expected should be true - there are no end point
            Assert.AreEqual(expectedResult, actualResult);

        }

        #endregion "Positive Test cases"

        #region "Positive Test cases - getInvoiceDocuments"

        /// <summary>
        /// get Invoices for multipl customers
        /// </summary>
        [Test()]
        public void GetInvoiceDocumentsAll()
        {

            List<string> invoiceIDs = new List<string>();
            invoiceIDs.Add("21210");
             //invoiceIDs.Add("INV017");
             //invoiceIDs.Add("INV076");

            var getInvoiceDocumentRequest = new GetInvoiceDocumentRequest
            {
                CustomerID = "1359476",
                InvoiceIDsList = invoiceIDs
            };
            //Create Invoice factory object
            InvoiceFactory invoiceFactory = new InvoiceFactory();

            GetInvoiceDocumentsResponse getInvoiceDocumentsResponse = new GetInvoiceDocumentsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            getInvoiceDocumentsResponse.InvoiceDocumentsList =
                InvoiceDocumentsListTranslator.TranslateToDataContracts(
                    invoiceFactory.GetInvoiceDocuments(getInvoiceDocumentRequest.CustomerID,
                        getInvoiceDocumentRequest.InvoiceIDsList));

            //Expected result is true
            const bool expectedResult = true;
            bool actualResult = getInvoiceDocumentsResponse.InvoiceDocumentsList.Any();

            //Expected should be true - there are no end point
            Assert.AreEqual(expectedResult, actualResult);

        }

        /// <summary>
        /// get Invoices for multipl customers
        /// </summary>
        [Test()]
        public void GetInvoiceDocumentsMultipleInvoices()
        {

            List<string> invoiceIDs = new List<string>();
            invoiceIDs.Add("INV001");
            invoiceIDs.Add("INV017");
            invoiceIDs.Add("INV076");

            var getInvoiceDocumentRequest = new GetInvoiceDocumentRequest
            {
                CustomerID="1359476",
                InvoiceIDsList = invoiceIDs
            };
            //Create Invoice factory object
            InvoiceFactory invoiceFactory = new InvoiceFactory();

            GetInvoiceDocumentsResponse getInvoiceDocumentsResponse = new GetInvoiceDocumentsResponse();

            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            getInvoiceDocumentsResponse.InvoiceDocumentsList =
                InvoiceDocumentsListTranslator.TranslateToDataContracts(invoiceFactory.GetInvoiceDocuments(getInvoiceDocumentRequest.CustomerID, getInvoiceDocumentRequest.InvoiceIDsList));

            //Expected result is true
            const bool expectedResult = true;

            bool actualResult = getInvoiceDocumentsResponse.InvoiceDocumentsList.Any();

            //Expected should be true - there are no end point
            Assert.AreEqual(expectedResult, actualResult);

        }

        #endregion "Positive Test cases - getInvoiceDocuments"

        #region " Negative Test cases"

        /// <summary>
        /// check if invoice server is down
        /// </summary>
        [Test()]
        public void CreateInvoiceAxServiceUp()
        {

            //Create a 3 digit random invoice number 
             string invoiceNumber = RandomInvoiceNumber(3);

            //declaring invoice list
            List<Invoice> invoiceList = new List<Invoice>();

            //declaring invoice line list
            List<InvoiceLine> invoiceLineList = new List<InvoiceLine>();

            //adding a description in invoice line list
            invoiceLineList.Add(new InvoiceLine()
            {
                Amount = 250,
                Description = "Adding invoice without invoice dimension",
                ARCode = "C",
                Quantity = 1,
                UnitPrice = 250
            });

            //set up invoice data
            Invoice newInvoice = new Invoice()
            {
                CustomerReference = "Customer Ref",
                InvoiceDate = DateTime.Today.AddDays(-30),
                OrderAccount = "1359476",
                InvoiceAccount = "1359476",
                InvoiceId = InvoiceNumberPrefix + invoiceNumber,
                CurrencyCode = "USD",
                PurchaseOrderFormNumber = "321",
                InvoiceLineList = invoiceLineList
            };

            //adding an invoice in invoice lilst
            invoiceList.Add(newInvoice);

            CreateInvoiceRequest createInvoiceRequest = new CreateInvoiceRequest {Invoice = invoiceList};
            //Create Invoice factory object
            InvoiceFactory invoiceFactory = new InvoiceFactory();
            
          
            CreateInvoiceResponse createInvoiceResponse = new CreateInvoiceResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail actualValidationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    invoiceFactory.CreateInvoice(InvoiceTranslator.TranslateToEntities(createInvoiceRequest.Invoice)));
            createInvoiceResponse.ValidationDetail = actualValidationDetail;

            //If any error while connecting to the invoice service we receive the error message as "There was no endpoint listening at..."
            const string errorMessage = "There was no endpoint listening at ";
            bool isError = false;
            if (actualValidationDetail.ErrorList.Any())
                isError = (actualValidationDetail.ErrorList[0].Contains(errorMessage));
            ////Expected should be true - there are no end point
            //Assert.AreEqual(true, isError);

            //Expected should be false - if it is up and running
            Assert.AreEqual(false, isError);
        }

        /// <summary>
        /// Create an invoice without invoice dimensions and Invoice line - negative test
        /// </summary>
        [Test()]
        public void CreateNewInvoiceWithoutInvoiceLine()
        {
            //Create a 3 digit random invoice number 
            string invoiceNumber = RandomInvoiceNumber(3);

            //declaring invoice list
            List<Invoice> invoiceList = new List<Invoice>();

            ////adding a description in invoice line list
            //invoiceLineList.Add(new InvoiceLine() { Amount = 11, Description = "Create an invoice without invoice dimensions and Invoice line", ARCode = "C" });

            //set up invoice data
            Invoice newInvoice = new Invoice()
            {
                CustomerReference = "Customer Ref",
                InvoiceDate = DateTime.Today.AddDays(-30),
                OrderAccount = "1359476",
                InvoiceAccount = "1359476",
                InvoiceId = InvoiceNumberPrefix + invoiceNumber,
                CurrencyCode = "USD",
                PurchaseOrderFormNumber = "123",

            };

            //adding an invoice in invoice lilst
            invoiceList.Add(newInvoice);

            CreateInvoiceRequest createInvoiceRequest = new CreateInvoiceRequest {Invoice = invoiceList};
            //Create Invoice factory object
            InvoiceFactory invoiceFactory = new InvoiceFactory();
            
          
            CreateInvoiceResponse createInvoiceResponse = new CreateInvoiceResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail actualValidationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
              invoiceFactory.CreateInvoice(InvoiceTranslator.TranslateToEntities(createInvoiceRequest.Invoice))); 
            createInvoiceResponse.ValidationDetail = actualValidationDetail;

            //Expected result is true
            const string expectedErrorMsg = "Object reference not set to an instance of an object.";
            bool isError = false;
            if (actualValidationDetail.ErrorList.Any())
                isError = (actualValidationDetail.ErrorList[0].Contains(expectedErrorMsg));

            //Expected should be true - Invoice line is expected from AX.
            Assert.AreEqual(true, isError);
        }

        /// <summary>
        /// Create an invoice  - without of InvoiceAccount or OrderAccount - InvoiceAccount is Customer Account number
        /// </summary>
        [Test()]
        public void CreateInvoiceWithoutCustomerNumber()
        {
            //Create a 3 digit random invoice number 
            string invoiceNumber = RandomInvoiceNumber(3);

            //declaring invoice list
            List<Invoice> invoiceList = new List<Invoice>();

            //declaring invoice line list
            List<InvoiceLine> invoiceLineList = new List<InvoiceLine>
            {
                new InvoiceLine()
                {
                    Amount = 250,
                    Description = "Adding invoice without invoice dimension",
                    ARCode = "C"
                }
            };

            //adding a description in invoice line list

            //set up invoice data
            Invoice newInvoice = new Invoice()
            {
                CustomerReference = "Customer Ref",
                InvoiceDate = DateTime.Today.AddDays(-30),
                OrderAccount = "",
                InvoiceAccount = "1359476",
                InvoiceId = InvoiceNumberPrefix + invoiceNumber,
                CurrencyCode = "USD",
                PurchaseOrderFormNumber = "321",
                InvoiceLineList = invoiceLineList,
            };

            //adding an invoice in invoice lilst
            invoiceList.Add(newInvoice);

            CreateInvoiceRequest createInvoiceRequest = new CreateInvoiceRequest {Invoice = invoiceList};
            //Create Invoice factory object
            InvoiceFactory invoiceFactory = new InvoiceFactory();
            
          
            CreateInvoiceResponse createInvoiceResponse = new CreateInvoiceResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail actualValidationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
             invoiceFactory.CreateInvoice(InvoiceTranslator.TranslateToEntities(createInvoiceRequest.Invoice))); 
            createInvoiceResponse.ValidationDetail = actualValidationDetail;

            //If any error while connecting to the invoice service we receive the error message as "There was no endpoint listening at..."
            //  const string errorMessage = "Invoice Account should not be empty.";
            const string errorMessage = "Invoice Account or Order Account should not be empty.";
            bool isError = false;
            if (actualValidationDetail.ErrorList.Any())
                isError = (actualValidationDetail.ErrorList[0].Contains(errorMessage));
            //Expected should be true - there are no end point
            Assert.AreEqual(true, isError);
        }
        #endregion " Negative Test cases"

        #region " Negative Test cases - GetInvoices"

        /// <summary>
        /// get Invoices for multipl customers
        /// </summary>
        [Test()]
        public void GetInvoicesWithoutAccountNumber()
        {
            GetInvoicesRequest GetInvoicesRequest = new GetInvoicesRequest {AccountNumbers = new List<string>() {""}, Status="OPEN"};
            //Create Invoice factory object
            InvoiceFactory invoiceFactory = new InvoiceFactory();

            GetInvoicesResponse GetInvoicesResponse = new GetInvoicesResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            GetInvoicesResponse.CustomerInvoiceList =
                CustomerInvoiceListTranslator.TranslateToDataContracts(
                    invoiceFactory.GetInvoices(GetInvoicesRequest.AccountNumbers, GetInvoicesRequest.Status));

            //Expected result is true
            const bool expectedResult = true;
            bool actualResult = !GetInvoicesResponse.CustomerInvoiceList.Any();

            //Expected should be true - there are no end point
            Assert.AreEqual(expectedResult, actualResult);
        }
        /// <summary>
        /// get Invoices for  GetInvoiceDocumentsWithOutFileFolder - if CustomerID is null then folder path is empty from the service
        /// </summary>
        [Test()]
        public void GetInvoiceDocumentsWithOutFileFolder()
        {

            List<string> invoiceIDs = new List<string>();
            invoiceIDs.Add("21210");
            //invoiceIDs.Add("INV017");
            //invoiceIDs.Add("INV076");

            var getInvoiceDocumentRequest = new GetInvoiceDocumentRequest
            {
                CustomerID= "1359476", 
                InvoiceIDsList = invoiceIDs
            };
            //Create Invoice factory object
            InvoiceFactory invoiceFactory = new InvoiceFactory();

            GetInvoiceDocumentsResponse getInvoiceDocumentsResponse = new GetInvoiceDocumentsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            getInvoiceDocumentsResponse.InvoiceDocumentsList = InvoiceDocumentsListTranslator.TranslateToDataContracts(invoiceFactory.GetInvoiceDocuments(getInvoiceDocumentRequest.CustomerID, getInvoiceDocumentRequest.InvoiceIDsList));

            //Expected result is true
            const bool expectedResult = true;
            // if CustomerID is null then folder path is empty from the service
            bool actualResult = getInvoiceDocumentsResponse.InvoiceDocumentsList[0].CustomerID==null;

            //Expected should be true - there are no end point
            Assert.AreEqual(expectedResult, actualResult);

        }

        /// <summary>
        /// get Invoices for multipl customers
        /// </summary>
        [Test()]
        public void GetInvoiceswithInvalidAccountNumber()
        {

            GetInvoicesRequest getInvoicesRequest = new GetInvoicesRequest
            {
                AccountNumbers = new List<string>() {"as123"},
                Status="OPEN"
            };
            //Create Invoice factory object
            InvoiceFactory invoiceFactory = new InvoiceFactory();

            GetInvoicesResponse getInvoicesResponse = new GetInvoicesResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            getInvoicesResponse.CustomerInvoiceList =
                CustomerInvoiceListTranslator.TranslateToDataContracts(
                    invoiceFactory.GetInvoices(getInvoicesRequest.AccountNumbers, getInvoicesRequest.Status));

            //Expected result is true
            const bool expectedResult = true;
            bool actualResult = !getInvoicesResponse.CustomerInvoiceList[0].InvoiceDetailsList.Any();

            //Expected should be true - there are no end point
            Assert.AreEqual(expectedResult, actualResult);

        }

        #endregion " Negative Test cases - GetInvoices"

        #region "Positive Test cases - CancelInvoices"

        /// <summary>
        /// Cancel invoice with all data - single
        /// </summary>
        [Test()]
        public void CancelInvoiceSingleData()
        {
            //declaring invoice list
            List<InvoiceCancellation> InvoiceCancellationList = new List<InvoiceCancellation>();

            ////adding a new invoice to cancel
            //InvoiceCancellationList.Add(new InvoiceCancellation()
            //{
            //    CancellationDate=DateTime.Now,
            //    BillableItemID = "103",
            //    CancellationID="1",
            //    Reason = "ERROR"
            //});


            //adding a new invoice to cancel
            InvoiceCancellationList.Add(new InvoiceCancellation()
            {
                CancellationDate = DateTime.Now,
                BillableItemID = "10021",
                CancellationID = "1",
                Reason = "ERROR"
            });
            CancelInvoiceRequest CancelInvoiceRequest = new CancelInvoiceRequest { Invoice = InvoiceCancellationList };
            //Cancel Invoice factory object
            InvoiceFactory invoiceFactory = new InvoiceFactory();

            CancelInvoiceResponse CancelInvoiceResponse = new CancelInvoiceResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail actualValidationDetail =  ValidationDetailTranslator.TranslateToDataContract(invoiceFactory.CancellInvoices(CancelInvoiceTranslator.TranslateToEntities(CancelInvoiceRequest.Invoice)));
            CancelInvoiceResponse.ValidationDetail = actualValidationDetail;

            //Expected result is true
            const bool expectedResult = true;

            //Expected should be true - there are no end point
            Assert.AreEqual(expectedResult, actualValidationDetail.Result);

        }
      
        /// <summary>
        /// Cancel invoice with all data - multiple
        /// </summary>
        [Test()]
        public void CancelInvoiceMultiple()
        {
            //declaring invoice list
            List<InvoiceCancellation> InvoiceCancellationList = new List<InvoiceCancellation>();

            //adding a new invoice to cancel
            InvoiceCancellationList.Add(new InvoiceCancellation()
            {
                CancellationDate=DateTime.Now,
                BillableItemID="1",
                CancellationID="1",
                Reason="Test"
            });

             //adding a new invoice to cancel
            InvoiceCancellationList.Add(new InvoiceCancellation()
            {
                CancellationDate=DateTime.Now,
                BillableItemID="2",
                CancellationID="2",
                Reason="Test"
            });

            CancelInvoiceRequest CancelInvoiceRequest = new CancelInvoiceRequest { Invoice = InvoiceCancellationList };
            //Cancel Invoice factory object
            InvoiceFactory invoiceFactory = new InvoiceFactory();

            CancelInvoiceResponse CancelInvoiceResponse = new CancelInvoiceResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail actualValidationDetail =  ValidationDetailTranslator.TranslateToDataContract(invoiceFactory.CancellInvoices(CancelInvoiceTranslator.TranslateToEntities(CancelInvoiceRequest.Invoice)));
            CancelInvoiceResponse.ValidationDetail = actualValidationDetail;

            //Expected result is true
            const bool expectedResult = true;

            //Expected should be true - there are no end point
            Assert.AreEqual(expectedResult, actualValidationDetail.Result);

        }
        #endregion "Positive Test cases - CancelInvoices"

       #region "Negative Test cases - CancelInvoices"
        /// <summary>
        /// Cancel invoice with all data - single - - no billableid
        /// </summary>
        [Test()]
        public void CancelInvoicewithoutBillableItemID()
        {
            //declaring invoice list
            List<InvoiceCancellation> InvoiceCancellationList = new List<InvoiceCancellation>();

            //adding a new invoice to cancel
            InvoiceCancellationList.Add(new InvoiceCancellation()
            {
                CancellationDate=DateTime.Now,
                BillableItemID="",
                CancellationID="1",
                Reason="Test"
            });

            CancelInvoiceRequest CancelInvoiceRequest = new CancelInvoiceRequest { Invoice = InvoiceCancellationList };
            //Cancel Invoice factory object
            InvoiceFactory invoiceFactory = new InvoiceFactory();

            CancelInvoiceResponse CancelInvoiceResponse = new CancelInvoiceResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail actualValidationDetail = ValidationDetailTranslator.TranslateToDataContract(invoiceFactory.CancellInvoices(CancelInvoiceTranslator.TranslateToEntities(CancelInvoiceRequest.Invoice)));
            CancelInvoiceResponse.ValidationDetail = actualValidationDetail;

            //Expected result is true
            const bool expectedResult = true;

            //Expected should be true - there are no end point
            Assert.AreEqual(expectedResult, actualValidationDetail.Result);

        }

        /// <summary>
        /// Cancel invoice with all data - multiple - no Billable Item ID
        /// </summary>
        [Test()]
        public void CancelInvoiceMultipleNoBillableItemID()
        {
            //declaring invoice list
            List<InvoiceCancellation> InvoiceCancellationList = new List<InvoiceCancellation>();

            //adding a new invoice to cancel
            InvoiceCancellationList.Add(new InvoiceCancellation()
            {
                CancellationDate=DateTime.Now,
                BillableItemID="",
                CancellationID="1",
                Reason="Test"
            });

             //adding a new invoice to cancel
            InvoiceCancellationList.Add(new InvoiceCancellation()
            {
                CancellationDate=DateTime.Now,
                BillableItemID="",
                CancellationID="2",
                Reason="Test"
            });

            CancelInvoiceRequest CancelInvoiceRequest = new CancelInvoiceRequest { Invoice = InvoiceCancellationList };
            //Cancel Invoice factory object
            InvoiceFactory invoiceFactory = new InvoiceFactory();

            CancelInvoiceResponse CancelInvoiceResponse = new CancelInvoiceResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail actualValidationDetail = ValidationDetailTranslator.TranslateToDataContract(invoiceFactory.CancellInvoices(CancelInvoiceTranslator.TranslateToEntities(CancelInvoiceRequest.Invoice)));
            CancelInvoiceResponse.ValidationDetail = actualValidationDetail;

            //Expected result is true
            const bool expectedResult = true;

            //Expected should be true - there are no end point
            Assert.AreEqual(expectedResult, actualValidationDetail.Result);

        }
        #endregion "Negative Test cases - CancelInvoices"
    
    }
}