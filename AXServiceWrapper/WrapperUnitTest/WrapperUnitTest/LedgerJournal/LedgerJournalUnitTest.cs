﻿/******************************************************************************************
'Created By     : ANANTHAKUMAR SUBRAMANI
'Created Date   : 12/22/2014
'Description    :  Unit Test project for Ledger Journal /payment management
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using WrapperService.BusinessEntities;
using WrapperService.DataAccessRepository.Interfaces;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using TestContext = Microsoft.VisualStudio.TestTools.UnitTesting.TestContext;
using System;
using System.Configuration;


namespace WrapperTestProject
{
    /// <summary>
    ///This is a test class for LedgerJournalTest and is intended
    ///to contain all LedgerJournalTest Unit Tests
    ///</summary>
    [TestFixture()]
    public class LedgerJournalTest
    {
        private TestContext _testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return _testContextInstance;
            }
            set
            {
                _testContextInstance = value;
            }
        }

        /// <summary>
        /// Create a new  LedgerJournal with out address
        /// </summary>
        [Test]
        public void CreateLedgerJournalAllData()
        {
            //List<LedgerJournal> LedgerJournalList = new List<LedgerJournal>();
            List<LedgerJournalTransaction> LedgerJournalTransactionList = new List<LedgerJournalTransaction>();
 

            ////Company name and JournalName from config files
            //string companyName = ConfigurationManager.AppSettings["Company"];
            //string JournalName = ConfigurationManager.AppSettings["JournalName"];

            LedgerJournalTransactionList.Add(new LedgerJournalTransaction()
            {
                AccountType = "Cust",
                TransactionDate = DateTime.Today,
                Description = "Line 1 Text",
                Debit = 0,
                Credit = 10,
                PaymentId = "19900119",
                //DueDate = DateTime.Today,
                OffsetAccountType = "Ledger",
                // Company = "",
                // OffsetCompany = "",
                LedgerDimension = new LedgerDimension()
                {
                    Account = "287630344", //Payment Customer Account
                    DisplayValue = "287630344" //Same as Account
                },
               //  DefaultDimension = ledgerDefaultDimension,
                OffsetLedgerDimension = new LedgerDimension()
                {
                    Account = "200102",
                    DisplayValue = "200102"
                    //Values = offsetDefaultDimension
                }
            });
            LedgerJournal LedgerJournal = new LedgerJournal()
            {
                // JournalName = JournalName,
                Description = "Test Journal AIF",
                LedgerJournalTransactionList = LedgerJournalTransactionList
            };

            //// Assign the addnewwLedgerJournal in expected,
            LedgerJournal expectedResult = LedgerJournal;

            //LedgerJournal Repository 
            Mock<ILedgerJournalRepository> mockLedgerJournalRepository = new Mock<ILedgerJournalRepository>();
            ValidationDetail actualValidationResult = new ValidationDetail
            {
                Result = true
            };

            //Setup the repository insert method
            mockLedgerJournalRepository.Setup(method => method.CreatePayment(LedgerJournal)).Returns(actualValidationResult);

            //
            ILedgerJournalRepository value = mockLedgerJournalRepository.Object; // pass mock instance to interface
            // call mock to get data that is in database
            actualValidationResult = value.CreatePayment(LedgerJournal);

            //expected result
            ValidationDetail expectedValidationResult = new ValidationDetail
            {
                Result = true
            };

            Assert.IsNotNull(expectedResult); // test everything is OK that this stage
            Assert.AreEqual(actualValidationResult.Result, expectedValidationResult.Result); // test everything is OK that this stage
        }
    }
}
