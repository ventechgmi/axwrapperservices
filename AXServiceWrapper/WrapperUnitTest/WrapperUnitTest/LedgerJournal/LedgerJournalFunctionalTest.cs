﻿/******************************************************************************************
'Created By     : ANANTHAKUMAR SUBRAMANI
'Created Date   : 12/22/2014
'Description    :  Functional test for Ledger Journal or Customer Payment management ( create operation alone now)
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using WrapperService.BusinessLogic;
using WrapperService.DataContracts;
using WrapperService.MessageContracts;
using WrapperService.ServiceImplementation.Translators;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using TestContext = Microsoft.VisualStudio.TestTools.UnitTesting.TestContext;

// used for mocking for objects
//using Invoice = WrapperService.BusinessEntities.Invoice;

namespace WrapperTestProject
{


    /// <summary>
    ///This is a test class for InvoiceTest and is intended
    ///to contain all InvoiceTest Unit Tests
    ///</summary>
    [TestFixture()]
    public class LedgerJournalFunctionalTest
    {
        //Set the prefix for each invoice number
        private const string InvoiceNumberPrefix = "INV";

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        [TestInitialize()]
        public void Initialize()
        {
        }
        
        // Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void LedgerJournalFunctionalTestCleanup()
        {
        }
        #region "Positive Test cases"
        /// <summary>
        /// Create payment with all data
        /// </summary>
        [Test()]
        public void CreatePaymentAllData()
        {
            List<DefaultDimension> ledgerDefaultDimension = new List<DefaultDimension>();
            List<DefaultDimension> offsetDefaultDimension = new List<DefaultDimension>();
            List<LedgerJournal> journalTableList = new List<LedgerJournal>();
            List<LedgerJournalTransaction> journalLineList = new List<LedgerJournalTransaction>();

            ledgerDefaultDimension.Add(new DefaultDimension() { Name = "FUND", Value = "5460" });
            ledgerDefaultDimension.Add(new DefaultDimension() { Name = "ALI", Value = "800610" });
            ledgerDefaultDimension.Add(new DefaultDimension() { Name = "DEPARTMENT", Value = "COM651300" });
            ledgerDefaultDimension.Add(new DefaultDimension() { Name = "PROGRAM", Value = "3320C" });
            ledgerDefaultDimension.Add(new DefaultDimension() { Name = "ACCOUNT", Value = "Admin" });
            ledgerDefaultDimension.Add(new DefaultDimension() { Name = "AGENCYUSE", Value = "Admin" });

            offsetDefaultDimension.Add(new DefaultDimension() { Name = "FUND", Value = "5F10" });
            offsetDefaultDimension.Add(new DefaultDimension() { Name = "ALI", Value = "800610" });
            offsetDefaultDimension.Add(new DefaultDimension() { Name = "DEPARTMENT", Value = "COM651300" });
            offsetDefaultDimension.Add(new DefaultDimension() { Name = "PROGRAM", Value = "3320C" });
            offsetDefaultDimension.Add(new DefaultDimension() { Name = "ACCOUNT", Value = "Admin" });
            offsetDefaultDimension.Add(new DefaultDimension() { Name = "AGENCYUSE", Value = "Admin" });


            //Company name and JournalName from config files
            string JournalName = ConfigurationManager.AppSettings["JournalName"];

            journalLineList.Add(new LedgerJournalTransaction()
            {
                AccountType = "Cust",
                TransactionDate = DateTime.Today,
                Description = "CreatePaymentAll Data",
                Debit = 0,
                Credit = 70,
                InvoiceId = "19900870",
                PaymentId = "19900119",
                PaymentMode = "CREDITCARD",
                //DueDate = DateTime.Today,
                OffsetAccountType = "Ledger",
                //Company = companyName, // Company Name will be set in the service itself from configuartion
                //OffsetCompany = companyName, // Company Name will be set in the service itself from configuartion
                LedgerDimension = new LedgerDimension()
                {
                    Account = "104298", //Payment Customer Account
                    DisplayValue = "104298" //Same as Account 287630344
                },
                //DefaultDimension = ledgerDefaultDimension,
                OffsetLedgerDimension = new LedgerDimension()
                {
                    //Account = "1359476",
                    ////DisplayValue = "5F10-200102-800610-COM651300-3320C-Admin-Admin-",
                    //DisplayValue = "1359476"
                    //Values = offsetDefaultDimension 200102
                    Account = "200102", //PaymentCustomerAccount,//"1359476",
                    DisplayValue = "200102",
                }
            });
            journalTableList.Add(new LedgerJournal()
            {
                JournalName = JournalName,
                Description = "Test Journal AIF",
                LedgerJournalTransactionList = journalLineList
            });

            CreatePaymentRequest createPaymentRequest = new CreatePaymentRequest { LedgerJournalList = journalTableList };
            //Create Payment factory object
            LedgerJournalFactory ledgerJournalFactory = new LedgerJournalFactory();

            CreatePaymentResponse createPaymentResponse = new CreatePaymentResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail actualValidationDetail = ValidationDetailTranslator.TranslateToDataContract(ledgerJournalFactory.CreatePayment(LedgerJournalTranslator.TranslateToEntities(createPaymentRequest.LedgerJournalList)));
            createPaymentResponse.ValidationDetail = actualValidationDetail;

            //Expected result is true
            const bool expectedResult = true;

            //Expected should be true - there are no end point
            Assert.AreEqual(expectedResult, actualValidationDetail.Result);

       
        }
        [Test()]
        public void CreatePayment()
        {
            List<LedgerJournal> journalTableList = new List<LedgerJournal>();
            List<LedgerJournalTransaction> journalLineList = new List<LedgerJournalTransaction>();

            ////Company name and JournalName from config files
            //string companyName = ConfigurationManager.AppSettings["Company"];
            //string JournalName = ConfigurationManager.AppSettings["JournalName"];

            journalLineList.Add(new LedgerJournalTransaction()
            {
                AccountType = "Cust",
                TransactionDate = DateTime.Today,
                Description = "Create Payment",
                Debit = 0,
                Credit = 70,
                InvoiceId = "19900870",
                PaymentId = "19900119",
                //DueDate = DateTime.Today,
                OffsetAccountType = "Ledger",
                //Company = companyName, // Company Name will be set in the service itself from configuartion
                //OffsetCompany = companyName, // Company Name will be set in the service itself from configuartion
                LedgerDimension = new LedgerDimension()
                {
                    Account = "104298", //Payment Customer Account
                    DisplayValue = "104298" //Same as Account 287630344
                },
                //DefaultDimension = ledgerDefaultDimension,
                OffsetLedgerDimension = new LedgerDimension()
                {
                    //Account = "1359476",
                    ////DisplayValue = "5F10-200102-800610-COM651300-3320C-Admin-Admin-",
                    //DisplayValue = "1359476"
                    //Values = offsetDefaultDimension 200102
                    Account = "200102", //PaymentCustomerAccount,//"1359476",
                    DisplayValue = "200102",
                }
                
               
            });
            journalTableList.Add(new LedgerJournal()
            {
                JournalName = string.Empty, // journal Name will be set in the service itself from configuartion
                Description = "Test Journal AIF",
                LedgerJournalTransactionList = journalLineList
            });

            CreatePaymentRequest createPaymentRequest = new CreatePaymentRequest { LedgerJournalList = journalTableList };
            //Create Payment factory object
            LedgerJournalFactory ledgerJournalFactory = new LedgerJournalFactory();

            CreatePaymentResponse createPaymentResponse = new CreatePaymentResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail actualValidationDetail = ValidationDetailTranslator.TranslateToDataContract(ledgerJournalFactory.CreatePayment(LedgerJournalTranslator.TranslateToEntities(createPaymentRequest.LedgerJournalList)));
            createPaymentResponse.ValidationDetail = actualValidationDetail;

            //Expected result is true
            const bool expectedResult = true;

            //Expected should be true - there are no end point
            Assert.AreEqual(expectedResult, actualValidationDetail.Result);
        }
         #endregion "Ledger Items"

        #region "Negative Test cases"
        [Test()]
        public void CreatePaymentwithOutCustomerAccount()
        {
            List<LedgerJournal> journalTableList = new List<LedgerJournal>();
            List<LedgerJournalTransaction> journalLineList = new List<LedgerJournalTransaction>();

            ////Company name and JournalName from config files
            //string companyName = ConfigurationManager.AppSettings["Company"];
            //string JournalName = ConfigurationManager.AppSettings["JournalName"];

            journalLineList.Add(new LedgerJournalTransaction()
            {
                AccountType = "Cust",
                TransactionDate = DateTime.Today,
                Description = "Line 1 Text",
                Debit = 0,
                Credit = 10,
                PaymentId = "",
                //DueDate = DateTime.Today,
                OffsetAccountType = "Ledger",
                //Company = companyName, // Company Name will be set in the service itself from configuartion
                //OffsetCompany = companyName, // Company Name will be set in the service itself from configuartion
                LedgerDimension = new LedgerDimension()
                {
                    Account = "", //Payment Customer Account
                    DisplayValue = "1359476" //Same as Account 287630344
                },
                //DefaultDimension = ledgerDefaultDimension,
                OffsetLedgerDimension = new LedgerDimension()
                {
                    Account = "1359476",
                    //DisplayValue = "5F10-200102-800610-COM651300-3320C-Admin-Admin-",
                    DisplayValue = "1359476"
                    //Values = offsetDefaultDimension 200102
                }
            });
            journalTableList.Add(new LedgerJournal()
            {
                JournalName = string.Empty, // journal Name will be set in the service itself from configuartion
                Description = "Test Journal AIF",
                LedgerJournalTransactionList = journalLineList
            });

            CreatePaymentRequest createPaymentRequest = new CreatePaymentRequest { LedgerJournalList = journalTableList };
            //Create Payment factory object
            LedgerJournalFactory ledgerJournalFactory = new LedgerJournalFactory();

            CreatePaymentResponse createPaymentResponse = new CreatePaymentResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail actualValidationDetail = ValidationDetailTranslator.TranslateToDataContract(ledgerJournalFactory.CreatePayment(LedgerJournalTranslator.TranslateToEntities(createPaymentRequest.LedgerJournalList)));
            createPaymentResponse.ValidationDetail = actualValidationDetail;

            //Expected result is true
            const bool expectedResult = true;

            //Expected should be true - there are no end point
            Assert.AreEqual(expectedResult, actualValidationDetail.Result);
        }
        #endregion "Negative Items"
    }
}