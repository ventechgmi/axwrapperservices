﻿/******************************************************************************************
'Created By     : ANANTHAKUMAR Subramani
'Created Date   : 12/06/2014
'Description    :  Functional test for Vendor management ( create/update/Disable Vendor operations)
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using WrapperService.BusinessLogic;
using WrapperService.DataContracts;
using WrapperService.MessageContracts;
using WrapperService.ServiceImplementation.Translators;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using TestContext = Microsoft.VisualStudio.TestTools.UnitTesting.TestContext;

namespace WrapperTestProject
{


    /// <summary>
    ///This is a test class for VendorTest and is intended
    ///to contain all VendorTest Unit Tests
    ///</summary>
    [TestFixture()]
    public class VendorFunctionalTest
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        [TestInitialize()]
        public void Initialize()
        {

        }

        // Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void VendorTestCleanup()
        {
            //Call Disable Vendor 

        }

        #region " Positive Test cases"


        /// <summary>
        /// Generates a random string with the given length
        /// </summary>
        /// <param name="size">Size of the string</param>
        /// <returns>Random string</returns>
        private string RandomAccountNumber(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();

            for (int i = 0; i < size; i++)
            {
                builder.Append(Convert.ToInt32(random.Next(9)));
            }
            return builder.ToString();
        }



        #region " Create and Delete Positive tests"
        /// <summary>
        /// Create a cutomer with single address
        /// </summary>
        [Test()]
        public void CreateVendorNoAddressNoContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);
            //  DeletePermanantly();
            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "NoAddressNoContact",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;

            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }
        [Test()]
        public void CreateVendorNoAddressNoContactOrg()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);
            //  DeletePermanantly();
            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Organization",
                //="ventech Corp."
            };

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;

            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }


        /// <summary>
        ///     Create Vendor without Currency 
        /// </summary>
        [Test()]
        public void CreateVendorWithoutCurrency()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "SingleAddress",
                MiddleName = "",
                // Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;

            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }
        /// <summary>
        /// Create Vendor Single address no contact
        /// </summary>
        [Test()]
        public void CreateVendorSingleAddressNoContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "SingleAddressnoContact",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Address objFirstAddress = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Location Name",
                Locator = "",
                Roles = "DELIVERY",
                State = "OH",
                Street = "1st street",
                //Type = "EMAIL",
                //TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        /// Create Vendor Single address no contact with account is Disable when create
        /// </summary>
        [Test()]
        public void CreateVendorSingleAddressNoContactwithDisableMode()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "SingleAddressnoContact",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person",

                //IsDisable=true
            };

            //Address data
            Address objFirstAddress = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Location Name",
                Locator = "",
                Roles = "DELIVERY",
                State = "OH",
                Street = "1st street",
                //Type = "EMAIL",
                //TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }


        /// <summary>
        /// Create Vendor Single address no contact with account is Disable when create
        /// </summary>
        [Test()]
        public void CreateVendorSingleAddrNoContactwithEnableAccount()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "SingleAddressnoContact",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //IsDisable = false,
                //VendorType = "Person"
            };

            //Address data
            Address objFirstAddress = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Location Name",
                Locator = "",
                Roles = "DELIVERY",
                State = "OH",
                Street = "1st street",
                //Type = "EMAIL",
                //TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Vendor multiple address no contact
        /// </summary>
        [Test()]
        public void CreateVendorMultipleAddressNoContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "MultipleAd",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "OfficeAddress",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress1);
            addNewVendor.AddressList.Add(objFirstAddress2);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Vendor no address single contact
        /// </summary>
        [Test()]
        public void CreateVendorNoAddressSingleContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "SingleAddress",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "Phone",
                Locator = "Home",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            addNewVendor.ContactList = new List<Contact>();
            //Add the address in address list of Vendor object.
            addNewVendor.ContactList.Add(objContact1);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Vendor no address multiple contact
        /// </summary>
        [Test()]
        public void CreateVendorNoAddressMultipleContacts()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "NoAddressMultipleContacts",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            //Address data
            Contact objContact2 = new Contact
            {
                ACADISID = "124",
                IsPrimary = false,
                ContactName = "6141231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            addNewVendor.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor.ContactList.Add(objContact1);
            addNewVendor.ContactList.Add(objContact2);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Vendor single address single contact
        /// </summary>
        [Test()]
        public void CreateVendorSingleAddressSingleContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "SingleAddress",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objAddress1);

            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };


            addNewVendor.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor.ContactList.Add(objContact1);


            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }


        /// <summary>
        ///Create Vendor  Single Address Multiple Contacts
        /// </summary>
        [Test()]
        public void CreateVendorSingleAddressMultipleContacts()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "multi contacts",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objAddress1);

            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            //contact data
            Contact objContact2 = new Contact
            {
                ACADISID = "124",
                IsPrimary = false,
                ContactName = "6141231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            addNewVendor.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor.ContactList.Add(objContact1);
            addNewVendor.ContactList.Add(objContact2);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }


        /// <summary>
        /// Create Vendor Multiple Addresses with Multiple Contacts
        /// </summary>
        [Test()]
        public void CreateVendorMultipleAddressMultipleContacts()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "Multiple",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objAddress1);
            addNewVendor.AddressList.Add(objAddress2);

            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            //contact data
            Contact objContact2 = new Contact
            {
                ACADISID = "124",
                IsPrimary = false,
                ContactName = "6141231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            addNewVendor.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor.ContactList.Add(objContact1);
            addNewVendor.ContactList.Add(objContact2);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Vendor Single address Single contact
        /// </summary>
        [Test()]
        public void CreateVendorMultipleAddressSingleContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "SingleAddressnoContact",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "OfficeAddress",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress1);
            addNewVendor.AddressList.Add(objFirstAddress2);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }



        /// <summary>
        ///     Create Vendor Multiple Addresses with Multiple Contacts
        /// </summary>
        [Test()]
        public void CreateMultipleVendorMultipleAddressSingleContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "Multiple",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };
            //Creating Account number
            string accountNumber2 = RandomAccountNumber(9);

            Vendor addNewVendor2 = new Vendor
            {
                AccountNumber = accountNumber2,
                FirstName = "CreateVendor",
                LastName = "Multiple",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            string accountNumber3 = RandomAccountNumber(9);

            Vendor addNewVendor3 = new Vendor
            {
                AccountNumber = accountNumber3,
                FirstName = "CreateVendor",
                LastName = "Multiple",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };
            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objAddress1);
            addNewVendor.AddressList.Add(objAddress2);


            addNewVendor2.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor2.AddressList.Add(objAddress1);
            addNewVendor2.AddressList.Add(objAddress2);


            addNewVendor3.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor3.AddressList.Add(objAddress1);
            addNewVendor3.AddressList.Add(objAddress2);
            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            ////contact data
            //Contact objContact2 = new Contact
            //{
            //    ACADISID = "124",
            //    IsPrimary = false,
            //    ContactName = "6141231234",
            //    Locator = "Phone",
            //    Roles = "HOME",
            //    Type = "EMAIL",
            //    TypeSpecified = true,
            //};
            addNewVendor.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor.ContactList.Add(objContact1);

            addNewVendor2.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor2.ContactList.Add(objContact1);


            addNewVendor3.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor3.ContactList.Add(objContact1);


            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Vendor multiple address no contact
        /// </summary>
        [Test()]
        public void CreateMultipleVendorsSingleAddressNoContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "MultipleVendor",
                LastName = "Single",
                MiddleName = "1",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            string accountNumber2 = RandomAccountNumber(9);
            //   Create a new Vendor, 
            Vendor addNewVendor2 = new Vendor
            {
                AccountNumber = accountNumber2,
                FirstName = "CreateVendor",
                LastName = "single",
                MiddleName = "2",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "OfficeAddress",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };

            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress1);
            addNewVendor.AddressList.Add(objFirstAddress2);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);
            VendorList.Add(addNewVendor2);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }
        #region "Updated for Vendor type - organization - Feb-23-2015"
        /// <summary>
        ///     Create Vendor without Currency 
        /// </summary>
        [Test()]
        public void CreateVendorWithoutCurrency_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,

                Group = "FIRE",
                //NameAlias = "",
                //NameSequence = "FirstLast",
                //VendorType = "Organization",
                // = "Ventech Solutions INC"
            };

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;

            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }
        /// <summary>
        /// Create Vendor Single address no contact
        /// </summary>
        [Test()]
        public void CreateVendorSingleAddressNoContact_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "",
                //NameSequence = "FirstLast",
                //VendorType = "Organization",
                // = "Ventech Solutions INC"
            };

            //Address data
            Address objFirstAddress = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Location Name",
                Locator = "",
                Roles = "DELIVERY",
                State = "OH",
                Street = "1st street",
                //Type = "EMAIL",
                //TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        /// Create Vendor Single address no contact with account is Disable when create
        /// </summary>
        [Test()]
        public void CreateVendorSingleAddressNoContactwithDisableMode_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "",
                //NameSequence = "FirstLast",
                //VendorType = "Organization",
                //="Ventech Solutions INC",

                //IsDisable = true
            };

            //Address data
            Address objFirstAddress = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Location Name",
                Locator = "",
                Roles = "DELIVERY",
                State = "OH",
                Street = "1st street",
                //Type = "EMAIL",
                //TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }


        /// <summary>
        /// Create Vendor Single address no contact with account is Disable when create
        /// </summary>
        [Test()]
        public void CreateVendorSingleAddrNoContactwithEnableAccount_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "",
                //NameSequence = "FirstLast",
                //VendorType = "Organization",
                //="Ventech Solutions INC",
                //IsDisable = false 
            };

            //Address data
            Address objFirstAddress = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Location Name",
                Locator = "",
                Roles = "DELIVERY",
                State = "OH",
                Street = "1st street",
                //Type = "EMAIL",
                //TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Vendor multiple address no contact
        /// </summary>
        [Test()]
        public void CreateVendorMultipleAddressNoContact_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "",
                //NameSequence = "FirstLast",
                //VendorType = "Organization",
                // = "Ventech Solutions INC"
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "OfficeAddress",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress1);
            addNewVendor.AddressList.Add(objFirstAddress2);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Vendor no address single contact
        /// </summary>
        [Test()]
        public void CreateVendorNoAddressSingleContact_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "",
                //NameSequence = "FirstLast",
                //VendorType = "Organization",
                // = "Ventech Solutions INC"
            };

            //Address data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "Phone",
                Locator = "Home",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            addNewVendor.ContactList = new List<Contact>();
            //Add the address in address list of Vendor object.
            addNewVendor.ContactList.Add(objContact1);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Vendor no address multiple contact
        /// </summary>
        [Test()]
        public void CreateVendorNoAddressMultipleContacts_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "",
                //NameSequence = "FirstLast",
                //VendorType = "Organization",
                // = "Ventech Solutions INC"
            };

            //Address data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            //Address data
            Contact objContact2 = new Contact
            {
                ACADISID = "124",
                IsPrimary = false,
                ContactName = "6141231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            addNewVendor.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor.ContactList.Add(objContact1);
            addNewVendor.ContactList.Add(objContact2);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Vendor single address single contact
        /// </summary>
        [Test()]
        public void CreateVendorSingleAddressSingleContact_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "FIRE",
                FirstName = "FirstName",
                LastName = "LastName"
                //NameAlias = "",
                //NameSequence = "FirstLast",
                //VendorType = "Organization",
                // = "Ventech Solutions INC"
            };

            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objAddress1);

            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };


            addNewVendor.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor.ContactList.Add(objContact1);


            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }


        /// <summary>
        ///Create Vendor  Single Address Multiple Contacts
        /// </summary>
        [Test()]
        public void CreateVendorSingleAddressMultipleContacts_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "FIRE",
                FirstName = "FirstName",
                LastName = "LastName"
            };

            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objAddress1);

            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            //contact data
            Contact objContact2 = new Contact
            {
                ACADISID = "124",
                IsPrimary = false,
                ContactName = "6141231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            addNewVendor.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor.ContactList.Add(objContact1);
            addNewVendor.ContactList.Add(objContact2);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }


        /// <summary>
        /// Create Vendor Multiple Addresses with Multiple Contacts
        /// </summary>
        [Test()]
        public void CreateVendorMultipleAddressMultipleContacts_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "FIRE",
                FirstName = "FirstName",
                LastName = "LastName"
            };

            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objAddress1);
            addNewVendor.AddressList.Add(objAddress2);

            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            //contact data
            Contact objContact2 = new Contact
            {
                ACADISID = "124",
                IsPrimary = false,
                ContactName = "6141231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            addNewVendor.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor.ContactList.Add(objContact1);
            addNewVendor.ContactList.Add(objContact2);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Vendor Single address Single contact
        /// </summary>
        [Test()]
        public void CreateVendorMultipleAddressSingleContact_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "FIRE",
                FirstName = "FirstName",
                LastName = "LastName"
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "OfficeAddress",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress1);
            addNewVendor.AddressList.Add(objFirstAddress2);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }



        /// <summary>
        ///     Create Vendor Multiple Addresses with Multiple Contacts
        /// </summary>
        [Test()]
        public void CreateMultipleVendorMultipleAddressSingleContact_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "FIRE",
                FirstName = "FirstName",
                LastName = "LastName"
            };
            //Creating Account number
            string accountNumber2 = RandomAccountNumber(9);

            Vendor addNewVendor2 = new Vendor
            {
                AccountNumber = accountNumber2,
                Currency = "USD",
                Group = "FIRE",
                FirstName = "FirstName",
                LastName = "LastName"
            };

            string accountNumber3 = RandomAccountNumber(9);

            Vendor addNewVendor3 = new Vendor
            {
                AccountNumber = accountNumber3,
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "",
                //NameSequence = "FirstLast",
                //VendorType = "Organization",
                // = "Ventech Solutions INC3"
            };
            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objAddress1);
            addNewVendor.AddressList.Add(objAddress2);


            addNewVendor2.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor2.AddressList.Add(objAddress1);
            addNewVendor2.AddressList.Add(objAddress2);


            addNewVendor3.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor3.AddressList.Add(objAddress1);
            addNewVendor3.AddressList.Add(objAddress2);
            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            addNewVendor.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor.ContactList.Add(objContact1);

            addNewVendor2.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor2.ContactList.Add(objContact1);


            addNewVendor3.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor3.ContactList.Add(objContact1);


            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Vendor multiple address no contact
        /// </summary>
        [Test()]
        public void CreateMultipleVendorsSingleAddressNoContact_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "FIRE",
                FirstName = "FirstName",
                LastName = "LastName"
            };

            string accountNumber2 = RandomAccountNumber(9);
            //   Create a new Vendor, 
            Vendor addNewVendor2 = new Vendor
            {
                AccountNumber = accountNumber2,
                Currency = "USD",
                Group = "FIRE",
                FirstName = "FirstName",
                LastName = "LastName"
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "OfficeAddress",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };

            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress1);
            addNewVendor.AddressList.Add(objFirstAddress2);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);
            VendorList.Add(addNewVendor2);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }
        #endregion ""Updated for Vendor type - organization - Feb-23-2015"
        #endregion " Create Positive tests"

        #region " Update Positive tests"
        /// <summary>
        /// Create a cutomer with single address
        /// </summary>
        [Test()]
        public void UpdateVendorNoAddressNoContact()
        {
            //Create 
            string accountNumber = "010556607";

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "NoAddressNoContact",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE"
                    //NameAlias = "AKS",
                    //NameSequence = "FirstLast"
                ,
                //IsDisable = true,
                //VendorType = "Person"
            };


            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            #region " Create Vendor"
            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;
            #endregion " Create Vendor"

            #region " Update"
            addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "Updated FName",
                LastName = "Updated LName",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Assigning current object
            VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory = new VendorFactory();

            saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            validationDetail =
              ValidationDetailTranslator.TranslateToDataContract(
                  VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;
            #endregion " Update"
            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        /// Create a cutomer with single address
        /// </summary>
        [Test()]
        public void UpdateVendorNoAddressNoContactwithDisable()
        {
            //Create 
            string accountNumber = "010556607";

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "NoAddressNoContact",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",

            };


            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            #region " Create Vendor"
            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;
            #endregion " Create Vendor"

            #region " Update"
            addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "Updated FName",
                LastName = "Updated LName",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Assigning current object
            VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory = new VendorFactory();

            saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            validationDetail =
              ValidationDetailTranslator.TranslateToDataContract(
                  VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;
            #endregion " Update"
            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }


        /// <summary>
        /// Create Vendor Single address no contact
        /// </summary>
        [Test()]
        public void UpdateVendorSingleAddressNoContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "SingleAddressnoContact",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Address objFirstAddress = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;


            #region " Update"
            addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "Updated FName",
                LastName = "Updated LName",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Assigning current object
            VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.

            objFirstAddress.City = "Lewis Center";
            addNewVendor.AddressList.Add(objFirstAddress);

            saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory = new VendorFactory();

            saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            validationDetail =
              ValidationDetailTranslator.TranslateToDataContract(
                  VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;
            #endregion " Update"


            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///Create Vendor multiple address no contact
        /// </summary>
        [Test()]
        public void UpdateVendorMultipleAddressNoContact()
        {
            //Create 
            string accountNumber = "533262100";//RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "MultipleAd",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "OfficeAddress",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress1);
            addNewVendor.AddressList.Add(objFirstAddress2);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            #region " Update"
            addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "Updated FName",
                LastName = "Updated LName",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Assigning current object
            VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            //Address data
            objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis Center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };

            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress1);
            addNewVendor.AddressList.Add(objFirstAddress2);

            VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);


            saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory = new VendorFactory();

            saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            validationDetail = ValidationDetailTranslator.TranslateToDataContract(VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;
            #endregion " Update"
            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }


        /// <summary>
        ///     Create Vendor no address single contact
        /// </summary>
        [Test()]
        public void UpdateVendorNoAddressSingleContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "SingleAddress",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "Phone",
                Locator = "Home",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            addNewVendor.ContactList = new List<Contact>();
            //Add the address in address list of Vendor object.
            addNewVendor.ContactList.Add(objContact1);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;


            #region " Update"
            addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "Updated FName",
                LastName = "Updated LName",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Assigning current object
            VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "Phone",
                Locator = "1231231234",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis Center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress1);
            addNewVendor.AddressList.Add(objFirstAddress2);

            addNewVendor.ContactList = new List<Contact>();
            addNewVendor.ContactList.Add(objContact1);

            VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);


            saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory = new VendorFactory();

            saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            validationDetail =
              ValidationDetailTranslator.TranslateToDataContract(
                  VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;
            #endregion " Update"

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }


        /// <summary>
        ///     Create Vendor no address multiple contact
        /// </summary>
        [Test()]
        public void UpdateVendorNoAddressMultipleContacts()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "Lname",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            //Address data
            Contact objContact2 = new Contact
            {
                ACADISID = "124",
                IsPrimary = false,
                ContactName = "6141231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            addNewVendor.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor.ContactList.Add(objContact1);
            addNewVendor.ContactList.Add(objContact2);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            #region " Update"
            addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "Updated FName",
                LastName = "Updated LName",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Assigning current object
            VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "Phone",
                Locator = "1231231234",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis Center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress1);
            addNewVendor.AddressList.Add(objFirstAddress2);

            addNewVendor.ContactList = new List<Contact>();
            addNewVendor.ContactList.Add(objContact1);

            VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);


            saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory = new VendorFactory();

            saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            validationDetail =
              ValidationDetailTranslator.TranslateToDataContract(
                  VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;
            #endregion " Update"
            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Vendor single address single contact
        /// </summary>
        [Test()]
        public void UpdateVendorSingleAddressSingleContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "SingleAddress",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objAddress1);

            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };


            addNewVendor.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor.ContactList.Add(objContact1);


            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            #region " Update"
            addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "Updated FName",
                LastName = "Updated LName",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Assigning current object
            VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "Phone",
                Locator = "1231231234",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis Center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress1);

            addNewVendor.ContactList = new List<Contact>();
            addNewVendor.ContactList.Add(objContact1);

            VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);


            saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory = new VendorFactory();

            saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            validationDetail =
              ValidationDetailTranslator.TranslateToDataContract(
                  VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;
            #endregion " Update"
            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }


        /// <summary>
        ///Update Vendor  Single Address Multiple Contacts
        /// </summary>
        [Test()]
        public void UpdateVendorSingleAddressMultipleContacts()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "multi contacts",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objAddress1);

            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            //contact data
            Contact objContact2 = new Contact
            {
                ACADISID = "124",
                IsPrimary = false,
                ContactName = "6141231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            addNewVendor.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor.ContactList.Add(objContact1);
            addNewVendor.ContactList.Add(objContact2);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }


        /// <summary>
        /// Update Vendor Multiple Addresses with Multiple Contacts
        /// </summary>
        [Test()]
        public void UpdateVendorMultipleAddressMultipleContacts()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "Multiple",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objAddress1);
            addNewVendor.AddressList.Add(objAddress2);

            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            //contact data
            Contact objContact2 = new Contact
            {
                ACADISID = "124",
                IsPrimary = false,
                ContactName = "6141231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            addNewVendor.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor.ContactList.Add(objContact1);
            addNewVendor.ContactList.Add(objContact2);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Update Vendor Single address Single contact
        /// </summary>
        [Test()]
        public void UpdateVendorMultipleAddressSingleContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "SingleAddressnoContact",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "OfficeAddress",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress1);
            addNewVendor.AddressList.Add(objFirstAddress2);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }



        /// <summary>
        ///     Update Vendor Multiple Addresses with Multiple Contacts
        /// </summary>
        [Test()]
        public void UpdateMultipleVendorMultipleAddressSingleContact()
        {
            //Update 
            string accountNumber = RandomAccountNumber(9);

            //   Update a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "Multiple",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };
            //Creating Account number
            string accountNumber2 = RandomAccountNumber(9);

            Vendor addNewVendor2 = new Vendor
            {
                AccountNumber = accountNumber2,
                FirstName = "CreateVendor",
                LastName = "Multiple",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            string accountNumber3 = RandomAccountNumber(9);

            Vendor addNewVendor3 = new Vendor
            {
                AccountNumber = accountNumber3,
                FirstName = "CreateVendor",
                LastName = "Multiple",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };
            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objAddress1);
            addNewVendor.AddressList.Add(objAddress2);


            addNewVendor2.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor2.AddressList.Add(objAddress1);
            addNewVendor2.AddressList.Add(objAddress2);


            addNewVendor3.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor3.AddressList.Add(objAddress1);
            addNewVendor3.AddressList.Add(objAddress2);
            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            ////contact data
            //Contact objContact2 = new Contact
            //{
            //    ACADISID = "124",
            //    IsPrimary = false,
            //    ContactName = "6141231234",
            //    Locator = "Phone",
            //    Roles = "HOME",
            //    Type = "EMAIL",
            //    TypeSpecified = true,
            //};
            addNewVendor.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor.ContactList.Add(objContact1);

            addNewVendor2.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor2.ContactList.Add(objContact1);


            addNewVendor3.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor3.ContactList.Add(objContact1);


            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);
            #region " Update"
            addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "Updated FName",
                LastName = "Updated LName",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Assigning current object
            VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "Phone",
                Locator = "1231231234",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis Center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress1);
            addNewVendor.AddressList.Add(objFirstAddress1);

            addNewVendor.ContactList = new List<Contact>();
            //Add the address in address list of Vendor object.

            addNewVendor.ContactList.Add(objContact1);

            VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);


            //Add the address in address list of Vendor object.
            addNewVendor2.AddressList.Add(objFirstAddress1);
            addNewVendor2.AddressList.Add(objFirstAddress1);
            addNewVendor2.ContactList.Add(objContact1);

            VendorList.Add(addNewVendor2);



            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress1);
            addNewVendor.ContactList.Add(objContact1);

            VendorList.Add(addNewVendor);


            saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory = new VendorFactory();

            saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            validationDetail =
              ValidationDetailTranslator.TranslateToDataContract(
                  VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;
            #endregion " Update"
            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Update Vendor multiple address no contact
        /// </summary>
        [Test()]
        public void UpdateMultipleVendorsSingleAddressNoContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "MultipleVendor",
                LastName = "Single",
                MiddleName = "1",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            string accountNumber2 = RandomAccountNumber(9);
            //   Create a new Vendor, 
            Vendor addNewVendor2 = new Vendor
            {
                AccountNumber = accountNumber2,
                FirstName = "CreateVendor",
                LastName = "single",
                MiddleName = "2",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "OfficeAddress",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };

            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress1);
            addNewVendor.AddressList.Add(objFirstAddress2);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);
            VendorList.Add(addNewVendor2);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            //Check the Vendor added in there or not
            IsVendorExistsRequest isVendorExistsRequest = new IsVendorExistsRequest();
            //adding account number into accounlt number list
            isVendorExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsVendorExistsResponse isVendorExistsResponse = new IsVendorExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.IsVendorExisting(isVendorExistsRequest.AccountNumbers));
            isVendorExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        #endregion "Update"
        /// <summary>
        /// Update Xml file with account number tag to delete.
        /// </summary>
        /// <param name="accountNumber"></param>
        public void UpdateXml(string accountNumber)
        {
            XmlDocument doc = new XmlDocument();
            const string path =
                @"E:\VSUSA\Projects\AXWrapper\AXServiceWrapper\WrapperUnitTest\WrapperUnitTest\Files\AccountNumberstoDelete.xml";
            doc.Load(path);
            // Create a new element node.
            XmlNode newElem = doc.CreateNode("element", "accountNumber", "");
            newElem.InnerText = accountNumber;

            XmlElement root = doc.DocumentElement;
            root.AppendChild(newElem);

            //doc.("/AccountNumbers/accountNumber").Attributes["value"].Value = AccountNumber;

            //doc.SelectSingleNode("/AccountNumbers/accountNumber").Attributes["value"].Value = AccountNumber;

            //foreach (XmlNode node1 in node.ChildNodes)
            //    foreach (XmlNode node2 in node1.ChildNodes)
            //        if (node2.Name == "price")
            //        {
            //            Decimal price = Decimal.Parse(node2.InnerText);
            //            // Increase all the book prices by 20%
            //            String newprice = ((Decimal)price * (new Decimal(1.20))).ToString("#.00");
            //            Console.WriteLine("Old Price = " + node2.InnerText + "\tNew price = " + newprice);
            //            node2.InnerText = newprice;
            //        }
            try
            {
                doc.Save(path);

            }
            catch (Exception)
            {

            }

        }


        #endregion " Positive Test cases"


        #region " Negative Test cases"

        #region "Create"
        /// <summary>
        /// Create Vendor Single address no contact  here Group name is empty
        /// </summary>
        [Test()]
        public void CreateVendortWithoutAccountNumber()
        {
            //Account nunmber is empty 
            string accountNumber = string.Empty;

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "SingleAddressnoContact",
                MiddleName = "",
                Currency = "USD",
                Group = "",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Address objFirstAddress = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;

            string expectedError = "Account Number should not be empty.";

            Assert.AreEqual(expectedError, validationDetail.ErrorList[0].ToString());    //Expected should be true - if Vendor added

        }
        /// <summary>
        /// Trying to crerate a Vendor without group
        /// </summary>
        [Test()]
        public void CreateVendortwithoutGroup()
        {
            //Create account number
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "SingleAddressnoContact",
                MiddleName = "",
                Currency = "USD",
                Group = "",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Address objFirstAddress = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;

            const string expectedError = "Field 'Vendor group' must be filled in.";
            bool isError = false;
            if (validationDetail.ErrorList != null)
                isError = (validationDetail.ErrorList[0].StartsWith(expectedError));

            //Expected should be true - if Vendor added
            Assert.AreEqual(true, isError);

        }

        /// <summary>
        ///     Create Vendor Adding Vendor without Country region
        /// </summary>
        [Test()]
        public void CreateVendorAddAddressWithoutCountryRegion()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "MultipleAd",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                //   CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "OfficeAddress",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };

            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress1);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;

            const string errorCountryRegion = "Check country/region";
            bool isErrorOnCountryRegion = false;
            if (validationDetail.ErrorList.Any())
                isErrorOnCountryRegion = (validationDetail.ErrorList[0].Contains(errorCountryRegion));

            //Expected should be true - country region is missing
            Assert.AreEqual(true, isErrorOnCountryRegion);

            const string errorState = "State not found";
            bool isErrorState = false;
            if (validationDetail.ErrorList.Any())
                isErrorState = (validationDetail.ErrorList[0].Contains(errorState));

            //Expected should be true - state is missing
            Assert.AreEqual(true, isErrorState);
        }

        /// <summary>
        ///     Create Vendor single address single contact
        /// </summary>
        [Test()]
        public void CreateVendorSingleAddressWithOutRole()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "SingleAddress",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Adding address data without adding Roles
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                // Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objAddress1);

            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };


            addNewVendor.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor.ContactList.Add(objContact1);


            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;


            const string expectedErrorMsg = "The field Type is blank or the relation record is missing";
            bool isErrorState = false;
            if (validationDetail.ErrorList.Any())
                isErrorState = (validationDetail.ErrorList[0].Contains(expectedErrorMsg));

            //Expected should be true - state is missing
            Assert.AreEqual(true, isErrorState);
        }


        /// <summary>
        ///Create Vendor- adding contacts withoiout roles
        /// </summary>
        [Test()]
        public void CreateVendorContactsWithoutRoles()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "multi contacts",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objAddress1);

            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                //Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            //contact data
            Contact objContact2 = new Contact
            {
                ACADISID = "124",
                IsPrimary = false,
                ContactName = "6141231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            addNewVendor.ContactList = new List<Contact>();
            //Add the address in contact list of Vendor object.
            addNewVendor.ContactList.Add(objContact1);
            addNewVendor.ContactList.Add(objContact2);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;

            const string expectedErrorMsg = "The field Type is blank or the relation record is missing";
            bool isErrorState = false;
            if (validationDetail.ErrorList.Any())
                isErrorState = (validationDetail.ErrorList[0].Contains(expectedErrorMsg));

            //Expected should be true - Role type is missing
            Assert.AreEqual(true, isErrorState);
        }


        /// <summary>
        /// Create Vendor Add address with Invalid state - state should be 2 digits. Im tryinig to give "OHIO" as value
        /// </summary>
        [Test()]
        public void CreateVendorAddAddressWithInvalidState()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "Multiple",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OHIO",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };



            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objAddress1);

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;

            const string expectedErrorMsg = "State not found.";
            bool isErrorState = false;
            if (validationDetail.ErrorList.Any())
                isErrorState = (validationDetail.ErrorList[0].Contains(expectedErrorMsg));

            //Expected should be true - state is missing
            Assert.AreEqual(true, isErrorState);
        }

        #endregion "Create"

        /// <summary>
        ///Trying to create Vendor but no Ax service is down
        /// </summary>
        [Test()]
        public void CreateVendorAxServiceDown()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = accountNumber,
                FirstName = "CreateVendor",
                LastName = "MultipleAd",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast",
                //VendorType = "Person"
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                ////AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address 2",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress1);
            addNewVendor.AddressList.Add(objFirstAddress2);


            //Contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "Phone 1",
                Locator = "Home-Hall",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            Contact objContact2 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "Phone",
                Locator = "Home-Dining",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            addNewVendor.ContactList = new List<Contact>();
            //Add the address in address list of Vendor object.
            addNewVendor.ContactList.Add(objContact1);
            addNewVendor.ContactList.Add(objContact2);
            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
            //Create Vendor factory object
            VendorFactory VendorFactory = new VendorFactory();

            SaveVendorResponse saveVendorResponse = new SaveVendorResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    VendorFactory.SaveVendor(VendorTranslator.TranslateToEntities(saveVendorRequest.Vendor)));
            saveVendorResponse.ValidationDetail = validationDetail;

            const string errorMessage = "There was no endpoint listening at ";
            bool isError = false;
            if (validationDetail.ErrorList.Any())
                isError = (validationDetail.ErrorList[0].Contains(errorMessage));

            //Expected should be true - there are no end point
            Assert.AreEqual(true, isError);

        }

        #endregion " Negative Test cases"

    }
}