﻿/******************************************************************************************
'Created By     : ANANTHAKUMAR Subramani
'Created Date   : 09/17/2015
'Description    :  Unit Test project for Vendor management
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using WrapperService.BusinessEntities;
using WrapperService.DataAccessRepository.Interfaces;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using TestContext = Microsoft.VisualStudio.TestTools.UnitTesting.TestContext;

// used for mocking for objects
//using Vendor = WrapperService.BusinessEntities.Vendor;

namespace WrapperTestProject
{
    /// <summary>
    ///This is a test class for VendorTest and is intended
    ///to contain all VendorTest Unit Tests
    ///</summary>
    [TestFixture()]
    public class VendorTest
    {
        private TestContext _testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return _testContextInstance;
            }
            set
            {
                _testContextInstance = value;
            }
        }

        /// <summary>
        /// Create a new  Vendor with out address
        /// </summary>
        [Test]
        public void CreateNewVendorwithoutAddress()
        {
            // Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = "123",
                FirstName = "FirstName",
                LastName = "LastName",
                MiddleName = "",
                Currency = "USD",
                Group = "FIRE",
            };

            //// Assign the addnewwVendor in expected,
            Vendor expectedResult = addNewVendor;

            //Vendor actualResult = new  Vendor();

            //Vendor Repository 
            Mock<IVendorRepository> mockVendorRepository = new Mock<IVendorRepository>();
            ValidationDetail actualValidationResult = new ValidationDetail
            {
                Result = true
            };

            //Setup the repository insert method
            mockVendorRepository.Setup(method => method.InsertVendor(addNewVendor)).Returns(actualValidationResult);

            //
            IVendorRepository value = mockVendorRepository.Object; // pass mock instance to interface

            actualValidationResult = value.InsertVendor(addNewVendor); // call mock to get data that is in database

            ValidationDetail expectedValidationResult = new ValidationDetail
            {
                Result = true
            };
            Assert.IsNotNull(expectedResult); // test everything is OK that this stage
            Assert.AreEqual(actualValidationResult.Result, expectedValidationResult.Result); // test everything is OK that this stage

            Assert.IsNotNull(expectedResult.AccountNumber); // check if null
        }

        ///unit test for create a successful Vendor.
        [Test]
        public void CreateVendorWithAddress()
        {
            // Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = "111111111",
                FirstName = "Anantha",
                LastName = "Subramani",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                //NameAlias = "AKS",
                //NameSequence = "FirstLast"
            };


            //Address data
            Address objFirstAddress = new Address
            {
               // AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "",
                IsPrimaryAddress = true,
                LocationName = "EMAIL",
                Locator = "HOME",
                Roles = "",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress);

            // Assign the addnewwVendor in expected,
            Vendor expectedResult = addNewVendor;

            //Vendor actualResult = new  Vendor();

            //Vendor Repository 
            Mock<IVendorRepository> mockVendorRepository = new Mock<IVendorRepository>();
            ValidationDetail actualValidationResult = new ValidationDetail
            {
                Result = true
            };
           

            //Setup the repository insert method
            mockVendorRepository.Setup(method => method.InsertVendor(addNewVendor)).Returns(actualValidationResult);

            //
            IVendorRepository value = mockVendorRepository.Object; // pass mock instance to interface

            actualValidationResult = value.InsertVendor(addNewVendor); // call mock to get data that is in database

            ValidationDetail expectedValidationResult = new ValidationDetail
            {
                Result = true
            };

            Assert.IsNotNull(expectedResult); // test everything is OK that this stage
            Assert.AreEqual(actualValidationResult.Result, expectedValidationResult.Result); // test everything is OK that this stage

            Assert.IsNotNull(expectedResult.AccountNumber); // check if null
        }


        /// <summary>
        /// Create a Update  Vendor with out address
        /// </summary>
        [Test]
        public void UpdateVendorwithoutAddress()
        {
            // Create a new Vendor, 
            Vendor updateVendor = new Vendor
            {
                AccountNumber = "777777778",
                FirstName = "Ananathakumar",
                LastName = "Subramani",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                //NameAlias = "Anantha",
                //NameSequence = "FirstLast"
            };

            //// Assign the addnewwVendor in expected,
            Vendor expectedResult = updateVendor;

            //Vendor actualResult = new  Vendor();

            //mocking Vendor Repository 
            Mock<IVendorRepository> mockVendorRepository = new Mock<IVendorRepository>();
           
            // creating Expected and Actual objects
             ValidationDetail actualValidationResult = new ValidationDetail
            {
                Result = true
            };
          

            //Setup the repository insert method
            mockVendorRepository.Setup(method => method.UpdateVendor(updateVendor)).Returns(actualValidationResult);

            //
            IVendorRepository value = mockVendorRepository.Object; // pass mock instance to interface

            //Calling update Vendor
            actualValidationResult = value.UpdateVendor(updateVendor); // call mock to get data that is in database

            ValidationDetail expectedValidationResult = new ValidationDetail
            {
                Result = true
            };

            Assert.IsNotNull(expectedResult); // test everything is OK that this stage
            Assert.AreEqual(actualValidationResult.Result, expectedValidationResult.Result); // test everything is OK that this stage

            Assert.IsNotNull(expectedResult.AccountNumber); // check if null
        }
       

        /// <summary>
        /// unit test for Update a Vendor.
        /// </summary>
        [Test]
        public void UpdateVendorWithAddress()
        {
            // Create a new Vendor, 
            Vendor addNewVendor = new Vendor
            {
                AccountNumber = "777777777",
                FirstName = "ANAND",
                LastName = "S",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                //NameAlias = "KSA",
                //NameSequence = "LastFirst"
            };


            //Address data
            Address objFirstAddress = new Address
            {
                //AddressMethodType = "Fax",
                City = "Columbus",
                CountryRegionID = "",
                IsPrimaryAddress = true,
                LocationName = "Fax",
                Locator = "HOME",
                Roles = "",
                State = "OH",
                Street = "11st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewVendor.AddressList = new List<Address>();
            //Add the address in address list of Vendor object.
            addNewVendor.AddressList.Add(objFirstAddress);

            // Assign the addnewwVendor in expected,
            Vendor expectedResult = addNewVendor;

            //Vendor actualResult = new  Vendor();

            //Vendor Repository 
            Mock<IVendorRepository> mockVendorRepository = new Mock<IVendorRepository>();
            ValidationDetail actualValidationResult = new ValidationDetail
            {
                Result = true
            };


            //Setup the repository insert method
            mockVendorRepository.Setup(method => method.UpdateVendor(addNewVendor)).Returns(actualValidationResult);

            //
            IVendorRepository value = mockVendorRepository.Object; // pass mock instance to interface

            actualValidationResult = value.UpdateVendor(addNewVendor); // call mock to get data that is in database

            ValidationDetail expectedValidationResult = new ValidationDetail
            {
                Result = true
            };

            Assert.IsNotNull(expectedResult); // test everything is OK that this stage
            Assert.AreEqual(actualValidationResult.Result, expectedValidationResult.Result); // test everything is OK that this stage

            Assert.IsNotNull(expectedResult.AccountNumber); // check if null
        }

         
       

    }
}
