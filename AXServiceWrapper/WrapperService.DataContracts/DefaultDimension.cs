﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/17/2014
'Description: Invoice Default Dimension data contracts
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Runtime.Serialization;
using WrapperService.DataContracts.Interfaces;

namespace WrapperService.DataContracts
{
    [DataContract]
    public class DefaultDimension : IDefaultDimension
    {
        /// <summary>
        /// Dimension name
        /// </summary>
        [DataMember]
        public string Name { get; set; }
        /// <summary>
        /// value
        /// </summary>
        [DataMember]
        public string Value { get; set; }

    }
}
