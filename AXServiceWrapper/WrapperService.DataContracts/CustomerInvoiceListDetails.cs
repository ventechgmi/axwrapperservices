﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: Jun/03/2015
'Description: Invoice details data Contracts
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using WrapperService.DataContracts.Interfaces;

namespace WrapperService.DataContracts
{
    [DataContract]
    public class CustomerInvoiceListDetails 
    {
       /// <summary>
        /// BillableItemId
        /// </summary>
        [DataMember]
        public string BillableItemId { get; set; }

        [DataMember]
        public string InvoiceAccount { get; set; }
        [DataMember]
        public string InvoiceAmount { get; set; }
        [DataMember]
        public string InvoiceId { get; set; }
        [DataMember]
        public string InvoiceDate { get; set; }
        [DataMember]
        public string DueDate { get; set; }
        /// <summary>
        /// Paymnet Status
        /// </summary>
         [DataMember]
       public string PaymentStatus { get; set; }
        [DataMember]
        public string InvoiceStatus { get; set; }


        [DataMember]
        public bool Status { get; set; }
        [DataMember]
        public string ErrorMessage { get; set; }
    }
}
