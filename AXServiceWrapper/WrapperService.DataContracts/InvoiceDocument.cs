﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/03/2014
'Description: Address Class
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using System.Runtime.Serialization;

namespace WrapperService.DataContracts
{
    [DataContract]
    public class InvoiceDocument
    {
        /// <summary>
        /// Invoice ID
        /// </summary>
        [DataMember]
        public string InvoiceID { get; set; }
        /// <summary>
        /// FileName
        /// </summary>
        [DataMember]
        public string FileName { get; set; }
        ///// <summary>
        ///// File Extension
        ///// </summary>
        //[DataMember]
        //public string FileExtension { get; set; }

        /// <summary>
        /// Server Path
        /// </summary>
        [DataMember]
        public string FileServerPath { get; set; }
        
        ///// <summary>
        ///// Size of the file
        ///// </summary>
        //[DataMember]
        //public Int32 FileSize { get; set; }

        ///// <summary>
        ///// File Contents Base 64
        ///// </summary>
        //[DataMember]
        //public string FileContentsBase64 { get; set; }
        /// <summary>
        /// Total Bytes
        /// </summary>
        [DataMember]
        public byte[] FileContentsByte { get; set; }

        [DataMember]
        public bool Status { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }
    }
}
