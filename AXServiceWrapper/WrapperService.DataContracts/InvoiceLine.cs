﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/17/2014
'Description: Invoice line data contracts
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using System.Runtime.Serialization;
using WrapperService.DataContracts.Interfaces;

namespace WrapperService.DataContracts
{
    [DataContract]
    public class InvoiceLine : IInvoiceLine
    {
        /// <summary>
        /// invoice line description
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Invoice amount
        /// </summary>
        [DataMember]
        public decimal? Amount { get; set; }

        /// <summary>
        /// Invoice main account
        /// </summary>
        [DataMember]
        public string MainAccount { get; set; }
        /// <summary>
        /// Invoice ARCode
        /// </summary>
        [DataMember]
        public string ARCode { get; set; }

        /// <summary>
        /// Transaction
        /// </summary>
        [DataMember]
        public int? ARTrans { get; set; }

        /// <summary>
        /// INvoice comments
        /// </summary>
        [DataMember]
        public string InvoiceComments { get; set; }
        /// <summary>
        /// Invoice default dimension list
        /// </summary>
        [DataMember]
        public List<DefaultDimension> InvoiceDefaultDimensionList;

        // Adding two fields for unit price,quantity - Starts here

        /// <summary>
        /// Total Amount
        /// </summary>
        [DataMember]
        public decimal? UnitPrice { get; set; }

        /// <summary>
        /// Quantity
        /// </summary>
        [DataMember]
        public int Quantity { get; set; }
        // Adding two fields for unit price,quantity - ends here

        //Added billable id for cancellation
        /// <summary>
        /// billable id for cancellation
        /// </summary>
        [DataMember]
        public string VEN_BillableItemId { get; set; }

    }
}
