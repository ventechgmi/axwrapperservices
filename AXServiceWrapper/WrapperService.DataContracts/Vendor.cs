﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 09/17/2015
'Description: Vendor Contract
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using System.Runtime.Serialization;
using WrapperService.DataContracts.Interfaces;

namespace WrapperService.DataContracts
{
    [DataContract]
    public class Vendor : IVendor
    {
        /// <summary>
        /// Handles running unique account number
        /// </summary>
        [DataMember]
        public string AccountNumber { get; set; }

        /// <summary>
        /// Vendor first name
        /// </summary>
        [DataMember]
        public string FirstName { get; set; }

        /// <summary>
        /// Vendor middle Name
        /// </summary>
        [DataMember]
        public string MiddleName { get; set; }

        /// Vendor Last Name
        [DataMember]
        public string LastName { get; set; }

        
        /// <summary>
        /// Curreny Type 
        /// </summary>
        [DataMember]
        public string Currency { get; set; }

        /// <summary>
        /// Vendor Group
        /// </summary>
        [DataMember]
        public string Group { get; set; }
         
        /// <summary>
        /// Vendor Group
        /// </summary>
        [DataMember]
        public List<Address> AddressList { get; set; }

        /// <summary>
        /// Contact info view for a Vendor - added Dec-12-2014
        /// </summary>
        [DataMember]
        public List<Contact> ContactList { get; set; }

        
    }
}
