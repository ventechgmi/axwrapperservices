﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/19/2014
'Description: Ledger Journal Transaction Class
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using System.Runtime.Serialization;
using WrapperService.DataContracts.Interfaces;

namespace WrapperService.DataContracts
{
    [DataContract]
    public class LedgerJournalTransaction : ILedgerJournalTransaction
    {
        /// <summary>
        /// Transaction date
        /// </summary>
        [DataMember]

        public DateTime? TransactionDate { get; set; }

        // public CustPaymJournalWCF.AxdEnum_LedgerJournalACType? AccountType { get; set; }
        /// <summary>
        /// Account Type 
        /// </summary>
        [DataMember]
        public string AccountType { get; set; }

        /// <summary>
        /// Ledger dimension
        /// </summary>
        [DataMember]
        public LedgerDimension LedgerDimension { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Debit 
        /// </summary>
        [DataMember]
        public decimal? Debit { get; set; }

        /// <summary>
        /// Credit
        /// </summary>
        [DataMember]
        public decimal? Credit { get; set; }

        //  public CustPaymJournalWCF.AxdEnum_LedgerJournalACType? OffsetAccountType { get; set; }
        /// <summary>
        /// Offset Account Type
        /// </summary>
        [DataMember]
        public string OffsetAccountType { get; set; }

        /// <summary>
        /// Offset Ledger Dimension
        /// </summary>
        [DataMember]
        public LedgerDimension OffsetLedgerDimension { get; set; }

        /// <summary>
        /// Invoice Id
        /// </summary>
        [DataMember]
        public string InvoiceId { get; set; }

        //public DateTime? DueDate { get; set; } //Not needed
        // public List<DefaultDimension> DefaultDimension { get; set; } //Not needed
        // public List<DefaultDimension> OffsetDefaultDimension { get; set; } //Not needed

        /// <summary>
        /// Company Name
        /// </summary>
        [DataMember]
        public string Company { get; set; } //SFM

        /// <summary>
        /// Offset Company Name
        /// </summary>
        [DataMember]
        public string OffsetCompany { get; set; } //SMF

        /// <summary>
        /// Payment Mode
        /// </summary>
        [DataMember]
        public string PaymentMode { get; set; } //CreditCard

        /// <summary>
        /// Payment Id
        /// </summary>
        [DataMember]
        public string PaymentId { get; set; }

        /// <summary>
        /// Payment Notes
        /// </summary>
        [DataMember]
        public string PaymentNotes { get; set; } //Random note

        //Added ARCode in ledger creation - Jan-22-2015 - starts here 
        /// <summary>
        /// ARCode
        /// </summary>
        [DataMember]
        public string ARCode { get; set; }
        //Added ARCode in ledger creation - Jan-22-2015 - ends here 

        /// <summary>
        /// Paymen tConfirmation Number
        /// </summary>
        [DataMember]
        public string PaymentConfirmationNumber { get; set; }
    }
}
