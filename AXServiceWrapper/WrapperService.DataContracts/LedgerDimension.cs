﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/19/2014
'Description: Ledger Dimension Class
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using System.Runtime.Serialization;
using WrapperService.DataContracts.Interfaces;

namespace WrapperService.DataContracts
{
      [DataContract]
    public class LedgerDimension : ILedgerDimension
    {
        /// <summary>
        /// Account Name
        /// </summary>
          [DataMember]
          public string Account { get; set; }

        /// <summary>
        /// Value
        /// </summary>
          [DataMember]
          public string DisplayValue { get; set; }
        
          [DataMember]
          public List<DefaultDimension> DefaultDimensionList { get; set; }
    }
}
