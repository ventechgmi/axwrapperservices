﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/03/2014
'Description: Secretary Contract
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using System.Runtime.Serialization;
using WrapperService.DataContracts.Interfaces;

namespace WrapperService.DataContracts
{
    [DataContract]
    public class Secretary : ISecretary
    {
        /// <summary>
        /// Account ID
        /// </summary>
        [DataMember]
        public string AccountID { get; set; }
        /// <summary>
        /// First Name
        /// </summary>
        [DataMember]
        public string FirstName { get; set; }
        /// <summary>
        /// Last name
        /// </summary>
        [DataMember]
        public string LastName { get; set; }
        /// <summary>
        /// Middle name
        /// </summary>
        [DataMember]
        public string MiddleName { get; set; }

        /// <summary>
        ///  Name
        /// </summary>
        [DataMember]
        public string Name { get; set; }
      
        /// <summary>
        /// Suffix
        /// </summary>
        [DataMember]
        public string Suffix { get; set; }


        /// <summary>
        /// Type is either Secretary or Fiscal
        /// </summary>
        [DataMember]
        public string Type { get; set; }
    }
}
