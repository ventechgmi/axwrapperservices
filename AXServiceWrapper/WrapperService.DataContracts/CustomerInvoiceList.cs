﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/17/2014
'Description: Invoice table data Contracts
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using WrapperService.DataContracts.Interfaces;

namespace WrapperService.DataContracts
{
    [DataContract]
    public class CustomerInvoiceList 
    {
        /// <summary>
        /// Customer Reference
        /// </summary>
        [DataMember]
        public string AccountNumber { get; set; }

        /// <summary>
        /// Invoice List
        /// </summary>
        [DataMember]
        public List<InvoiceDetails> InvoiceDetailsList { get; set; }

    }
}
