﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/03/2014
'Description: Address Class
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WrapperService.DataContracts
{
    [DataContract]
    public class InvoiceDocumentsList
    {
        
    
        /// <summary>
        /// Customer ID
        /// </summary>
        [DataMember]
        public string CustomerID { get; set; }

        /// <summary>
        /// Invoice Line list
        /// </summary>
        [DataMember]
        public List<InvoiceDocument> InvoiceDocumentList { get; set; }

       

    }
}
