﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/17/2014
'Description: Invoice table data Contracts
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using WrapperService.DataContracts.Interfaces;

namespace WrapperService.DataContracts
{
    [DataContract]
    public class Invoice:IInvoice
    {
        /// <summary>
        /// Customer Reference
        /// </summary>
        [DataMember]
        public string CustomerReference { get; set; }
        /// <summary>
        /// Invoice date
        /// </summary>
        [DataMember]
        public DateTime? InvoiceDate { get; set; }
        /// <summary>
        /// Order Account
        /// </summary>
        [DataMember]
        public string OrderAccount { get; set; }
        /// <summary>
        /// Invoice Account
        /// </summary>
        [DataMember]
        public string InvoiceAccount { get; set; }
        /// <summary>
        /// Invoice Id
        /// </summary>
        [DataMember]
        public string InvoiceId { get; set; }

        /// <summary>
        /// Currency code
        /// </summary>
        [DataMember]
        public string CurrencyCode { get; set; }
        /// <summary>
        /// Purchase order form number
        /// </summary>
        [DataMember]
        public string PurchaseOrderFormNumber { get; set; }
        /// <summary>
        /// Letter Date
        /// </summary>
        [DataMember]
        public DateTime? LetterDate { get; set; }
        /// <summary>
        /// Invoice Line list
        /// </summary>
        [DataMember]
        public List<InvoiceLine> InvoiceLineList { get; set; }
        /// <summary>
        /// Invoice default dimension list
        /// </summary>
        [DataMember]
        public List<DefaultDimension> InvoiceDefaultDimensionList;

        /// <summary>
        /// Added AutoPost
        /// </summary>
        [DataMember]
        public bool IsAutoPost { get; set; }
    }
}
