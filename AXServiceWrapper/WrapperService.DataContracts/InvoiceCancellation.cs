﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 04/07/2015
'Description: To cancell invoices
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using WrapperService.DataContracts.Interfaces;

namespace WrapperService.DataContracts
{
    [DataContract]
    public class InvoiceCancellation : IInvoiceCancellation
    {
          /// <summary>
        /// Billable Item ID
        /// </summary>
        [DataMember]
        public string BillableItemID { get; set; }

        /// <summary>
        /// Cancellation Date
        /// </summary>
        [DataMember]
        public DateTime CancellationDate { get; set; }

        /// <summary>
        /// Cancellation Id
        /// </summary>
        [DataMember]
        public string CancellationID { get; set; }

        /// <summary>
        /// Reason for cancellation
        /// </summary>
        [DataMember]
        public string Reason { get; set; }

    }
}