﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/03/2014
'Description: Address Class
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Runtime.Serialization;
using WrapperService.DataContracts.Interfaces;

namespace WrapperService.DataContracts
{
   [DataContract]
    public class Contact  :IContact 
    {
        /// <summary>
        /// It is an ACADIS unique id
        /// </summary>
        [DataMember]
        public string ACADISID { get; set; }

        /// <summary>
        /// This is the location name in DIRParty contact view
        /// </summary>
        [DataMember]
        public string ContactName { get; set; }

        /// <summary>
        /// Customer delivery location - Home,Delivery -value will be like Home  or  Home:Delivery
        /// </summary>
        [DataMember]
        public string Roles { get; set; }

        /// <summary>
        /// If it is 0 or empty ,the type is Email
        /// </summary>
        [DataMember]
        public string Type { get; set; }

        /// <summary>
        /// Type Specified
        /// </summary>
        [DataMember]
        public bool TypeSpecified { get; set; }

        /// <summary>
        /// Phone Number or email or mail etc.
        /// </summary>
        [DataMember]
        public string Locator { get; set; }
        /// <summary>
        /// Is Primary Address
        /// </summary>
        [DataMember]
        public bool IsPrimary { get; set; }

        [DataMember]
        public string Extension { get; set; } //Added By Ananthakumar - Feb-23-2015 Reason: Added for phone Extension
      
    }
}
