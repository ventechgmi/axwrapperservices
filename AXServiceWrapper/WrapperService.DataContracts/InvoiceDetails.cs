﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 01/07/2015
'Description: Invoice Default Dimension data contracts
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Runtime.Serialization;
using WrapperService.DataContracts.Interfaces;

namespace WrapperService.DataContracts
{
    [DataContract]
    public class InvoiceDetails : IInvoiceDetails
    {

        [DataMember]
        public string InvoiceAccount { get; set; }
        [DataMember]
        public string InvoiceAmount { get; set; }
      
        [DataMember]
        public string RemainingAmount { get; set; }
       
        [DataMember]
        public string InvoiceId { get; set; }
        [DataMember]
        public string InvoiceDate { get; set; }
        [DataMember]
        public string DueDate { get; set; }

        /// <summary>
        /// Paymnet Status
        /// </summary>
        [DataMember]
        public string PaymentStatus { get; set; }

        [DataMember]
        public string InvoiceStatus { get; set; }
        //Commented By Ananthakumar --Feb-20-2015 Reason: No need to bring all fields from AX - ends here


        [DataMember]
        public bool Status { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }

        //[DataMember]
        //public string CustGroup { get; set; }
        //[DataMember]
        //public string RefNum { get; set; }
        //[DataMember]
        //public string SalesId { get; set; }
        //[DataMember]
        //public string OrderAccount { get; set; }
        //[DataMember]
        //public string InvoiceAccount { get; set; }
        //[DataMember]
        //public string InvoiceDate { get; set; }
        //[DataMember]
        //public string DueDate { get; set; }
        //[DataMember]
        //public string CashDisc { get; set; }
        //[DataMember]
        //public string CashDiscDate { get; set; }
        //[DataMember]
        //public string Qty { get; set; }
        //[DataMember]
        //public string Volume { get; set; }
        //[DataMember]
        //public string Weight { get; set; }
        //[DataMember]
        //public string SumLineDisc { get; set; }
        //[DataMember]
        //public string SalesBalance { get; set; }
        //[DataMember]
        //public string EndDisc { get; set; }
        //[DataMember]
        //public string InvoiceAmount { get; set; }
        //[DataMember]
        //public string CurrencyCode { get; set; }
        //[DataMember]
        //public string ExchRate { get; set; }
        //[DataMember]
        //public string InvoiceId { get; set; }
        //[DataMember]
        //public string LedgerVoucher { get; set; }
        //[DataMember]
        //public string Updated { get; set; }
        //[DataMember]
        //public string OnAccountAmount { get; set; }
        //[DataMember]
        //public string TaxPrintOnInvoice { get; set; }
        //[DataMember]
        //public string Listcode { get; set; }
        //[DataMember]
        //public string DocumentNum { get; set; }
        //[DataMember]
        //public string DocumentDate { get; set; }
        //[DataMember]
        //public string CashDiscPercent { get; set; }
        //[DataMember]
        //public string IntrastatDispatch { get; set; }
        //[DataMember]
        //public string DeliveryName { get; set; }
        //[DataMember]
        //public string EnterpriseNumber { get; set; }
        //[DataMember]
        //public string PurchaseOrder { get; set; }
        //[DataMember]
        //public string DlvTerm { get; set; }
        //[DataMember]
        //public string DlvMode { get; set; }
        //[DataMember]
        //public string Payment { get; set; }
        //[DataMember]
        //public string CashDiscCode { get; set; }
        //[DataMember]
        //public string InvoiceRoundOff { get; set; }
        //[DataMember]
        //public string SumMarkup { get; set; }
        //[DataMember]
        //public string CovStatus { get; set; }
        //[DataMember]
        //public string ReturnItemNum { get; set; }
        //[DataMember]
        //public string PostingProfile { get; set; }
        //[DataMember]
        //public string Backorder { get; set; }
        //[DataMember]
        //public string Prepayment { get; set; }
        //[DataMember]
        //public string TaxGroup { get; set; }
        //[DataMember]
        //public string TaxItemGroup { get; set; }
        //[DataMember]
        //public string TaxSpecifyByLine { get; set; }
        //[DataMember]
        //public string EInvoiceLineSpecific { get; set; }
        //[DataMember]
        //public string OneTimeCustomer { get; set; }
        //[DataMember]
        //public string PaymentSched { get; set; }
        //[DataMember]
        //public string SumTax { get; set; }
        //[DataMember]
        //public string SalesType { get; set; }
        //[DataMember]
        //public string EInvoiceAccountCode { get; set; }
        //[DataMember]
        //public string InterCompanyPosted { get; set; }
        //[DataMember]
        //public string ParmId { get; set; }
        //[DataMember]
        //public string ReturnReasonCodeId { get; set; }
        //[DataMember]
        //public string EUSalesList { get; set; }
        //[DataMember]
        //public string ExchRateSecondary { get; set; }
        //[DataMember]
        //public string Triangulation { get; set; }
        //[DataMember]
        //public string CustomerRef { get; set; }
        //[DataMember]
        //public string VATNum { get; set; }
        //[DataMember]
        //public string numberSequenceGroup { get; set; }
        //[DataMember]
        //public string InclTax { get; set; }
        //[DataMember]
        //public string Log { get; set; }
        //[DataMember]
        //public string PaymDayId { get; set; }
        //[DataMember]
        //public string InvoicingName { get; set; }
        //[DataMember]
        //public string GiroType { get; set; }
        //[DataMember]
        //public string ContactPersonId { get; set; }
        //[DataMember]
        //public string SalesOriginId { get; set; }
        //[DataMember]
        //public string BillofLadingId { get; set; }
        //[DataMember]
        //public string inventLocationId { get; set; }
        //[DataMember]
        //public string FixedDueDate { get; set; }
        //[DataMember]
        //public string InvoiceAmountMST { get; set; }
        //[DataMember]
        //public string InvoiceRoundOffMST { get; set; }
        //[DataMember]
        //public string SumMarkupMST { get; set; }
        //[DataMember]
        //public string SumLineDiscMST { get; set; }
        //[DataMember]
        //public string EndDiscMST { get; set; }
        //[DataMember]
        //public string SalesBalanceMST { get; set; }
        //[DataMember]
        //public string SUmTaxMST { get; set; }
        //[DataMember]
        //public string PrintMgmtSiteId { get; set; }
        //[DataMember]
        //public string ReturnStatus { get; set; }
        //[DataMember]
        //public string InterCompanyCompanyId { get; set; }
        //[DataMember]
        //public string InterCompanyPurchId { get; set; }
        //[DataMember]
        //public string PrintedOriginals { get; set; }
        //[DataMember]
        //public string Proforma { get; set; }
        //[DataMember]
        //public string RCSalesList_UK { get; set; }
        //[DataMember]
        //public string ReverseCharge_UK { get; set; }
        //[DataMember]
        //public string DeliveryPostalAddress { get; set; }
        //[DataMember]
        //public string InvoicePostalAddress { get; set; }
        //[DataMember]
        //public string SourceDocumentHeader { get; set; }
        //[DataMember]
        //public string DefaultDimension { get; set; }
        //[DataMember]
        //public string BankLCExportLine { get; set; }
        //[DataMember]
        //public string WorkerSalesTaker { get; set; }
        //[DataMember]
        //public string ReversedRecId { get; set; }
        //[DataMember]
        //public string ReceiptDateConfirmed_ES { get; set; }
        //[DataMember]
        //public string PaymId { get; set; }
        //[DataMember]
        //public string TaxInvoiceSalesId { get; set; }
        //[DataMember]
        //public string IntrastatFulfillmentDate_HU { get; set; }
        //[DataMember]
        //public string CashDiscBaseDate { get; set; }
        //[DataMember]
        //public string IsCorrection { get; set; }
        //[DataMember]
        //public string ReasonTableRef { get; set; }
        //[DataMember]
        //public string SourceDocumentLine { get; set; }
        //[DataMember]
        //public string TransportationDocument { get; set; }
        //[DataMember]
        //public string DirectDebitmandate { get; set; }
        //[DataMember]
        //public string modifiedDateTime { get; set; }
        //[DataMember]
        //public string createdDateTime { get; set; }
        //[DataMember]
        //public string dEL_createdTime { get; set; }
        //[DataMember]
        //public string createdBy { get; set; }
        //[DataMember]
        //public string dataAreaId { get; set; }
        //[DataMember]
        //public string recVersion { get; set; }
        //[DataMember]
        //public string Partition { get; set; }
        //[DataMember]
        //public string RecId { get; set; }

        //Commented By Ananthakumar --Feb-20-2015 Reason: No need to bring all fields from AX - ends here
    }
}