﻿/******************************************************************************************
'Created By: ANANTHAKUMAR S
'Created Date: 12/03/2014
'Description: 
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace WrapperService.DataContracts
{
    [DataContract]
    public class ValidationDetail  
    {
        [DataMember]
        public Int32 Id { get; set; }
        [DataMember]
        public bool Result { get; set; }
        [DataMember]
        public List<string> ErrorList { get; set; }
        //This would contain business validations
        [DataMember]
        public Int32 ErrorType { get; set; }
        [DataMember]
        public string Number { get; set; }
    }
}