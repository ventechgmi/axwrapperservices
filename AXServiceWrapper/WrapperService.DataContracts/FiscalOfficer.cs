﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/03/2014
'Description: Customer Contract
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using System.Runtime.Serialization;
using WrapperService.DataContracts.Interfaces;

namespace WrapperService.DataContracts
{
    [DataContract]
    public class FiscalOfficer : IFiscalOfficer
    {
        /// <summary>
        /// Fiscal Officer Account ID
        /// </summary>
        [DataMember]
        public string AccountID { get; set; }
        /// <summary>
        /// Fiscal officer First Name
        /// </summary>
        [DataMember]
        public string FirstName { get; set; }
        /// <summary>
        /// Fiscal officer Last name
        /// </summary>
        [DataMember]
        public string LastName { get; set; }
        /// <summary>
        /// Fiscal officer Middle name
        /// </summary>
        [DataMember]
        public string MiddleName { get; set; }
        /// <summary>
        /// Fiscal officer  name
        /// </summary>
        [DataMember]
        public string Name { get; set; }
        /// <summary>
        /// Fiscal officer Suffix
        /// </summary>
        [DataMember]
        public string Suffix { get; set; }

        /// <summary>
        /// Type is either Secretary or Fiscal
        /// </summary>
        [DataMember]
        public string Type { get; set; }
    }
}
