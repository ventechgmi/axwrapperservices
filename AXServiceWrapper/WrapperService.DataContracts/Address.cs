﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/03/2014
'Description: Address Class
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Runtime.Serialization;
using WrapperService.DataContracts.Interfaces;

namespace WrapperService.DataContracts
{
   [DataContract]
    public class Address  :IAddress 
    {

        /// <summary>
        /// It is an ACADIS unique id
        /// </summary>
        [DataMember]
        public string ACADISID { get; set; }
        
       /// <summary>
        /// Customer Street Name
        /// </summary>
       [DataMember]
       public string Street { get; set; }

        /// <summary>
        /// Customer City Name
        /// </summary>
       [DataMember]
       public string City { get; set; }


        /// <summary>
        /// Customer State
        /// </summary>
       [DataMember]
       public string State { get; set; }


        /// <summary>
        /// Country Region ID
        /// </summary>
       [DataMember]
       public string CountryRegionID { get; set; }


        /// <summary>
        /// Country Zipcode
        /// </summary>
       [DataMember]
       public string ZipCode { get; set; }

        /// <summary>
        /// Customer delivery location - Home,Delivery -value will be like Home  or  Home:Delivery
        /// </summary>
       [DataMember]
       public string Roles { get; set; }

        /// <summary>
        /// Location Name - Email
        /// </summary>
       [DataMember]
       public string LocationName { get; set; }

        /// <summary>
        /// Address Type Method - Email/Phone/Mail
        /// </summary>
     //  [DataMember]
      // public string AddressMethodType { get; set; }

        /// <summary>
        /// If it is 0 or empty ,the type is Email
        /// </summary>
       [DataMember]
       public string Type { get; set; }

        /// <summary>
        /// Type Specified
        /// </summary>
       [DataMember]
       public bool TypeSpecified { get; set; }

        /// <summary>
        /// Phone Number or email or mail etc.
        /// </summary>
       [DataMember]
       public string Locator { get; set; }
    
       /// <summary>
       /// Is Primary Address
       /// </summary>
       [DataMember]
       public bool IsPrimaryAddress { get; set; }

       //Added By Ananthakumar - Feb-23-2015 Reason: Line2 - starts here
       /// <summary>
       /// Customer Street Name2 (Line2)
       /// </summary>
       [DataMember]
       public string Street2 { get; set; }
        //Added By Ananthakumar - Feb-23-2015 Reason: Line2 - ends here
     
    }
}
