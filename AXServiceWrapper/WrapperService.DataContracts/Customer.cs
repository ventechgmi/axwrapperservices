﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/03/2014
'Description: Customer Contract
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using System.Runtime.Serialization;
using WrapperService.DataContracts.Interfaces;

namespace WrapperService.DataContracts
{
    [DataContract]
    public class Customer : ICustomer
    {
        /// <summary>
        /// Handles running unique account number
        /// </summary>
        [DataMember]
        public string AccountNumber { get; set; }

        /// <summary>
        /// Customer first name
        /// </summary>
        [DataMember]
        public string FirstName { get; set; }

        /// <summary>
        /// Customer middle Name
        /// </summary>
        [DataMember]
        public string MiddleName { get; set; }

        /// Customer Last Name
        [DataMember]
        public string LastName { get; set; }

        /// <summary>
        /// Customer Name Sequence which means to show the first name last name combination  like  - FirstLast, LastFirst - Its a configurable item in the web.config
        /// </summary>
        [DataMember]
        public string NameSequence { get; set; }

        /// <summary>
        /// Customer Alais Name
        /// </summary>
        [DataMember]
        public string NameAlias { get; set; }

        /// <summary>
        /// Curreny Type 
        /// </summary>
        [DataMember]
        public string Currency { get; set; }

        /// <summary>
        /// Customer Group
        /// </summary>
        [DataMember]
        public string Group { get; set; }

        /// <summary>
        /// Customer Group
        /// </summary>
        [DataMember]
        public List<Address> AddressList { get; set; }

        /// <summary>
        /// Contact info view for a customer - added Dec-12-2014
        /// </summary>
        [DataMember]
        public List<Contact> ContactList { get; set; }

        //Added for enable or disable the customer - Jan-23-2015
        /// <summary>
        /// IsDisable
        /// </summary>
        [DataMember]
        public bool IsDisable { get; set; }

        //Added By Ananthakumar - Feb-23-2015 Reason: Customertype , Suffix - starts here
        /// <summary>
        /// Customertype is like Person , Organization etc
        /// </summary>
        [DataMember]
        public string CustomerType { get; set; }
        /// <summary>
        /// Suffix
        /// </summary>
        [DataMember]
        public string Suffix { get; set; }
        /// <summary>
        /// OrganizationName
        /// </summary>
        [DataMember]
        public string OrganizationName { get; set; }

        //Added By Ananthakumar - Feb-23-2015 Reason: Customertype , Suffix - ends here       
        /// <summary>
        /// AS 400 ID
        /// </summary>
        [DataMember]
        public string AS400ID { get; set; }


        /// for VFFD  changes - Starts here
        /// is participating 
        [DataMember]
        public bool IsParticipating { get; set; }
        /// <summary>
        /// Property valuation
        /// </summary>
        [DataMember]
        public decimal? PropertyValuation { get; set; }

        /// <summary>
        /// Fiscal Officer 
        /// </summary>
        [DataMember]
        public FiscalOfficer FiscalOfficer { get; set; }

        /// <summary>
        /// Secretary 
        /// </summary>
        [DataMember]
        public Secretary Secretary { get; set; }

        /// <summary>
        /// Fund Billing Address 
        /// </summary>
        [DataMember]
        public Address FundBillingAddress { get; set; }
        /// for VFFD  changes - ends here
        /// 
        /// <summary>
        /// VendorID
        /// </summary>
        [DataMember]
        public string VendorID { get; set; }
    }
}
