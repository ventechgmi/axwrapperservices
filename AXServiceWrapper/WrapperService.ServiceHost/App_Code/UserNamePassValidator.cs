﻿using System;
using System.Configuration;
using System.IdentityModel.Tokens;
using System.ServiceModel;
using System.Text;
using System.IO;

 
namespace WrapperService.ServiceImplementation
{
   public class UserNamePassValidator : System.IdentityModel.Selectors.UserNamePasswordValidator
    {
        public override void Validate(string userName, string password)
        {
            if(  userName==null ||  password==null)
            {
                throw new ArgumentNullException();
            }
            // implemented credentials starts
            string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
            string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

            if (!(userName == serviceUserid && password == servicePassword))
            {
                throw new SecurityTokenException("Incorrect Username or Password");
            }
        }

      

    }

   
   }
    
