﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/03/2014
'Description: 
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using WrapperService.BusinessEntities;
using WrapperService.DataAccessRepository;
using WrapperService.DataAccessRepository.Interfaces;

namespace WrapperService.BusinessLogic
{

    public class VendorFactory
    {
        private readonly IVendorRepository _IVendorRepository;

        public VendorFactory()
        {
        }

        //public VendorFactory(IVendorRepository IVendorRepository)
        //{
        //    this._IVendorRepository = IVendorRepository;
        //}

        /// <summary>
        /// Create or Update Vendors list and returns the validation details
        /// </summary>
        /// <param name="VendorList"></param>
        /// <returns></returns>
        public ValidationDetail SaveVendor(List<Vendor> VendorList)
        {
            //getting error list if any errors
            List<string> errorList = new List<string>();
            // talks about the overall action result
            ValidationDetail validationDetail = new ValidationDetail();
            //uses for each and every Vendor insert or update
            ValidationDetail validation = new ValidationDetail();
            // Creating a Vendor repository object to call either insert or update calls
            VendorRepository VendorRepository = new VendorRepository();

            //Looping each Vendor 
            foreach (Vendor eachVendor in VendorList)
            {
                //check the account number is already there are not - true for update, false for create

                //check if account number is emty
                if (!string.IsNullOrEmpty(eachVendor.AccountNumber)) // test account # 777777777
                {
                    // bool IsNewVendor = VendorRepository.IsVendorExists(eachVendor.Accountnumber);
                    validation = VendorRepository.IsExistingVendor(eachVendor.AccountNumber);

                    //if true update the Vendor
                    if (validation.Result)
                        validation = VendorRepository.UpdateVendor(eachVendor);
                    else
                    {
                        // if any error - since the readVendor will return "No document Key" error for new Vendor. error handles here , if the error list is null then we update
                        if (validation.ErrorList != null)
                        {
                            // if "No valid document Key error means thats a new Vendor, since the acocunt number will not be in the ax.
                            if (validation.ErrorList[0].Contains("No valid document"))
                                validation = VendorRepository.InsertVendor(eachVendor);
                        }
                        else // means no error list then it is also a new Vendor
                            validation = VendorRepository.InsertVendor(eachVendor);
                    }

                    //true for success & false for failure
                    if (validation.Result == false && validation.ErrorList.Any())
                    {
                        //Addding the error in the error list
                        // errorList.Add("There was some error while saving this item . A notification is sent to the administrator.");
                        errorList.Add(validation.ErrorList[0].ToString());
                    }
                }
                else
                {
                    errorList.Add("Account Number should not be empty.");
                }
            }
            //finally if any errors then the result will be false
            if (errorList.Count != 0)
            {
                //if any errors then the result is false and assigning the error list
                validationDetail.Result = false;
                validationDetail.ErrorList = errorList;
            }
            else
            {
                //if any errors then the result is true and returns the validation detail object.
                validationDetail.Result = true;
            }
            return validationDetail;
        }

        /// <summary>
        /// Create or Update Vendors list and returns the validation details
        /// </summary>
        /// <param name="VendorList"></param>
        /// <returns></returns>
        public ValidationDetail IsVendorExisting(List<string> accountNumbers)
        {
            //getting error list if any errors
            List<string> errorList = new List<string>();
            // talks about the overall action result
            ValidationDetail validationDetail = new ValidationDetail();
            //uses for each and every Vendor insert or update
            ValidationDetail validation = new ValidationDetail();
            // Creating a Vendor repository object to call either insert or update calls
            VendorRepository VendorRepository = new VendorRepository();

            //Looping each Vendor 
            foreach (string eachAccountNumber in accountNumbers)
            {
                //check the account number is already there are not - true for update, false for create

                //check if account number is emty
                if (!string.IsNullOrEmpty(eachAccountNumber)) // test account # 777777777
                {
                    // bool IsNewVendor = VendorRepository.IsVendorExists(eachVendor.Accountnumber);
                    validation = VendorRepository.IsExistingVendor(eachAccountNumber);

                    //true for success & false for failure
                    if (validation.Result == false && validation.ErrorList.Any())
                    {
                        //Addding the error in the error list
                        // errorList.Add("There was some error while saving this item . A notification is sent to the administrator.");
                        errorList.Add(validation.ErrorList[0].ToString());
                    }
                }
                else
                {
                    errorList.Add("Account Number should not be empty.");
                }
            }
            //finally if any errors then the result will be false
            if (errorList.Count != 0)
            {
                //if any errors then the result is false and assigning the error list
                validationDetail.Result = false;
                validationDetail.ErrorList = errorList;
            }
            else
            {
                //if any errors then the result is true and returns the validation detail object.
                validationDetail.Result = true;
            }
            return validationDetail;
        }
    }
}
