﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/03/2014
'Description: 
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using WrapperService.BusinessEntities;
using WrapperService.DataAccessRepository;
using WrapperService.DataAccessRepository.Interfaces;

namespace WrapperService.BusinessLogic
{

    public class CustomerFactory
    {
        private readonly ICustomerRepository _ICustomerRepository;

        public CustomerFactory()
        {
        }

        public CustomerFactory(ICustomerRepository ICustomerRepository)
        {
            _ICustomerRepository = ICustomerRepository;
        }

        /// <summary>
        /// Create or Update Customers list and returns the validation details
        /// </summary>
        /// <param name="customerList"></param>
        /// <returns></returns>
        public ValidationDetail SaveCustomer(List<Customer> customerList)
        {
            //getting error list if any errors
            List<string> errorList = new List<string>();
            // talks about the overall action result
            ValidationDetail validationDetail = new ValidationDetail();
            //uses for each and every customer insert or update
            ValidationDetail validation = new ValidationDetail();
            // Creating a customer repository object to call either insert or update calls
            CustomerRepository CustomerRepository = new CustomerRepository(_ICustomerRepository);

            //Looping each customer 
            foreach (Customer eachCustomer in customerList)
            {
                //check the account number is already there are not - true for update, false for create

                //check if account number is emty
                if (!string.IsNullOrEmpty(eachCustomer.AccountNumber)) // test account # 777777777
                {
                    // bool IsNewCustomer = CustomerRepository.IsCustomerExists(eachCustomer.Accountnumber);
                    validation = CustomerRepository.IsExistingCustomer(eachCustomer.AccountNumber);

                    //if true update the customer
                    if (validation.Result)
                        validation = CustomerRepository.UpdateCustomer(eachCustomer);
                    else
                    {
                        // if any error - since the readcustomer will return "No document Key" error for new customer. error handles here , if the error list is null then we update
                        if (validation.ErrorList != null)
                        {
                            // if "No valid document Key error means thats a new customer, since the acocunt number will not be in the ax.
                            if (validation.ErrorList[0].Contains("No valid document"))
                                validation = CustomerRepository.InsertCustomer(eachCustomer);
                        }
                        else // means no error list then it is also a new customer
                            validation = CustomerRepository.InsertCustomer(eachCustomer);
                    }

                    //true for success & false for failure
                    if (validation.Result == false && validation.ErrorList.Any())
                    {
                        //Addding the error in the error list
                        // errorList.Add("There was some error while saving this item . A notification is sent to the administrator.");
                        errorList.Add(validation.ErrorList[0].ToString());
                    }
                }
                else
                {
                    errorList.Add("Account Number should not be empty.");
                }
            }
            //finally if any errors then the result will be false
            if (errorList.Count != 0)
            {
                //if any errors then the result is false and assigning the error list
                validationDetail.Result = false;
                validationDetail.ErrorList = errorList;
            }
            else
            {
                //if any errors then the result is true and returns the validation detail object.
                validationDetail.Result = true;
            }
            return validationDetail;
        }


        /// <summary>
        /// Create or Update Customers list and returns the validation details
        /// </summary>
        /// <param name="customerList"></param>
        /// <returns></returns>
        public ValidationDetail IsCustomerExisting(List<string> accountNumbers)
        {
            //getting error list if any errors
            List<string> errorList = new List<string>();
            // talks about the overall action result
            ValidationDetail validationDetail = new ValidationDetail();
            //uses for each and every customer insert or update
            ValidationDetail validation = new ValidationDetail();
            // Creating a customer repository object to call either insert or update calls
            CustomerRepository customerRepository = new CustomerRepository(_ICustomerRepository);

            //Looping each customer 
            foreach (string eachAccountNumber in accountNumbers)
            {
                //check the account number is already there are not - true for update, false for create

                //check if account number is emty
                if (!string.IsNullOrEmpty(eachAccountNumber)) // test account # 777777777
                {
                    // bool IsNewCustomer = CustomerRepository.IsCustomerExists(eachCustomer.Accountnumber);
                    validation = customerRepository.IsExistingCustomer(eachAccountNumber);

                    //true for success & false for failure
                    if (validation.Result == false && validation.ErrorList.Any())
                    {
                        //Addding the error in the error list
                        // errorList.Add("There was some error while saving this item . A notification is sent to the administrator.");
                        errorList.Add(validation.ErrorList[0].ToString());
                    }
                }
                else
                {
                    errorList.Add("Account Number should not be empty.");
                }
            }
            //finally if any errors then the result will be false
            if (errorList.Count != 0)
            {
                //if any errors then the result is false and assigning the error list
                validationDetail.Result = false;
                validationDetail.ErrorList = errorList;
            }
            else
            {
                //if any errors then the result is true and returns the validation detail object.
                validationDetail.Result = true;
            }
            return validationDetail;
        }

        /// <summary>
        /// Disable a customer
        /// </summary>
        /// <param name="accountNumbers"></param>
        /// <returns></returns>
        public ValidationDetail DisableCustomer(List<string> accountNumbers)
        {
            //getting error list if any errors
            List<string> errorList = new List<string>();
            // talks about the overall action result
            ValidationDetail validationDetail = new ValidationDetail();
            //uses for each and every customer insert or update
            ValidationDetail validation = new ValidationDetail();
            // Creating a customer repository object to call either insert or update calls
            CustomerRepository CustomerRepository = new CustomerRepository(_ICustomerRepository);

            //Looping each customer 
            foreach (string eachAccountNumber in accountNumbers)
            {
                //check the account number is already there are not - true for update, false for create

                //check if account number is empty
                if (!string.IsNullOrEmpty(eachAccountNumber)) // test account # 777777777
                {

                    //Call delete method  the result into validation detail
                    validation = CustomerRepository.DisableCustomer(eachAccountNumber);

                    //true for success & false for failure
                    if (validation.Result == false && validation.ErrorList.Any())
                    {
                        //Addding the error in the error list
                        // errorList.Add("There was some error while saving this item . A notification is sent to the administrator.");
                        errorList.Add(validation.ErrorList[0].ToString());
                    }
                }
                else
                {
                    errorList.Add("Account Number should not be empty.");
                }
            }
            //finally if any errors then the result will be false
            if (errorList.Count != 0)
            {
                //if any errors then the result is false and assigning the error list
                validationDetail.Result = false;
                validationDetail.ErrorList = errorList;
            }
            else
            {
                //if any errors then the result is true and returns the validation detail object.
                validationDetail.Result = true;
            }
            return validationDetail;
        }

        /// <summary>
        /// Get Customer data
        /// </summary>
        /// <param name="accountNumbers"></param>
        /// <returns></returns>
        public string GetCustomer(List<string> accountNumbers)
        {
            // Creating a customer repository object to call either insert or update calls
            CustomerRepository customerRepository = new CustomerRepository(_ICustomerRepository);

            //Looping each customer 
            //foreach (var eachAccountNumber in accountNumbers)
            //{
            //    //check the account number is already there are not - true for update, false for create
            string accountnumber = String.Join(",", accountNumbers).Trim();
            //    //check if account number is emty
            if (!string.IsNullOrEmpty(accountnumber)) // test account # 777777777
                {
                    return customerRepository.GetCustomer(accountnumber);
                }
                return "Account Number should not be empty.";
        }

        /// <summary>
        /// Get Customer data
        /// </summary>
        /// <param name="accountNumbers"></param>
        /// <returns></returns>
        public Customer GetCustomerObject(List<string> accountNumbers)
        {
            // Creating a customer repository object to call either insert or update calls
            CustomerRepository customerRepository = new CustomerRepository(_ICustomerRepository);
        
            string accountnumber = String.Join(",", accountNumbers).Trim();
            //    //check if account number is emty
            if (!string.IsNullOrEmpty(accountnumber)) // test account # 777777777
            {
                return customerRepository.GetCustomerObject(accountnumber);
            }
          //  return "Account Number should not be empty.";
            return new Customer();
        }
    }
}
