﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/19/2014
'Description: 
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using System.Linq;
using WrapperService.BusinessEntities;
using WrapperService.DataAccessRepository;
using WrapperService.DataAccessRepository.Interfaces;

namespace WrapperService.BusinessLogic
{

    public class InvoiceFactory
    {
        private readonly IInvoiceRepository _iInvoiceRepository;

        public InvoiceFactory()
        {
        }

        public InvoiceFactory(IInvoiceRepository invoiceRepository)
        {
            _iInvoiceRepository = invoiceRepository;
        }

        /// <summary>
        /// Create or Update Invoices list and returns the validation details
        /// </summary>
        /// <param name="invoiceList"></param>
        /// <returns></returns>
        public ValidationDetail CreateInvoice(List<Invoice> invoiceList)
        {
            //getting error list if any errors
            List<string> errorList = new List<string>();
            // talks about the overall action result
            ValidationDetail validationDetail = new ValidationDetail();
            //uses for each and every Invoice insert or update
            ValidationDetail validation = new ValidationDetail();
            // Creating a Invoice repository object to call create invoice
            InvoiceRepository invoiceRepository = new InvoiceRepository(_iInvoiceRepository);

            //Looping each Invoice 
            foreach (Invoice eachInvoice in invoiceList)
            {
                //check if InvoiceAccount or OrderAccount  are empty  // InvoiceAccount is Customer account number.
                if (!(string.IsNullOrEmpty(eachInvoice.InvoiceAccount) || string.IsNullOrEmpty(eachInvoice.OrderAccount)))
                {
                    //Actual calling to the repository
                    validation = invoiceRepository.CreateInvoice(eachInvoice);

                    //true for success & false for failure
                    if (validation.Result == false && validation.ErrorList.Any())
                    {
                        //Addding the error in the error list
                        // errorList.Add("There was some error while saving this item . A notification is sent to the administrator.");
                        errorList.Add(validation.ErrorList[0].ToString());
                    }
                }
                else
                    errorList.Add("Invoice Account or Order Account should not be empty.");
            }
            //finally if any errors then the result will be false
            if (errorList.Count != 0)
            {
                //if any errors then the result is false and assigning the error list
                validationDetail.Result = false;
                validationDetail.ErrorList = errorList;
            }
            else
            {
                //if any errors then the result is true and returns the validation detail object.
                validationDetail.Result = true;
            }
            return validationDetail;
        }

        /// <summary>
        /// get Customers invoices by status
        /// </summary>
        /// <param name="accountNumbers"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public List<CustomerInvoiceList> GetInvoices(List<string> accountNumbers, string status = null)
        {
            // Get Invoices
            InvoiceRepository invoiceRepository = new InvoiceRepository(_iInvoiceRepository);

            List<CustomerInvoiceList> allCustomerInvoiceList = new List<CustomerInvoiceList>();
            //Looping each account number
            foreach (string eachAccountNumber in accountNumbers)
            {
                //check if InvoiceAccount or OrderAccount  are empty
                if (!(string.IsNullOrEmpty(eachAccountNumber))) // eachAccountNumber is empty or not
                {
                    CustomerInvoiceList eachCustomerInvoiceList = new CustomerInvoiceList();
                    eachCustomerInvoiceList.AccountNumber = eachAccountNumber;
                    eachCustomerInvoiceList.InvoiceDetailsList = (invoiceRepository.GetInvoices(eachAccountNumber, status));
                    // eachCustomerInvoiceList.InvoiceList = (invoiceRepository.GetInvoices(eachAccountNumber));
                    allCustomerInvoiceList.Add(eachCustomerInvoiceList);
                }
            }
            return allCustomerInvoiceList;
        }

        /// <summary>
        /// get invoices by billableable item id
        /// </summary>
        /// <param name="accountNumbers"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public List<CustomerInvoiceListDetails> GetInvoiceDetails(List<string> BillabaleItemIDList)
        {
            // Get Invoices
            InvoiceRepository invoiceRepository = new InvoiceRepository(_iInvoiceRepository);
            List<CustomerInvoiceListDetails> allCustomerInvoiceList = new List<CustomerInvoiceListDetails>();
            allCustomerInvoiceList = invoiceRepository.GetInvoiceDetails(BillabaleItemIDList);
            return allCustomerInvoiceList;
        }

        /// <summary>
        /// Get Invoice documents based on customer ids - optional 
        /// </summary>
        /// <returns></returns>
        public List<InvoiceDocumentsList> GetInvoiceDocuments(string customerId, List<string> invoiceIDsList = null)
        {
            // creating object for invoice repository
            InvoiceRepository invoiceRepository = new InvoiceRepository(_iInvoiceRepository);

            //Invoice Documents List for all customers 
            List<InvoiceDocumentsList> invoiceDocumentsList = new List<InvoiceDocumentsList>();

            string folderpath = invoiceRepository.GetInvoiceFolder();

            if (string.IsNullOrEmpty(folderpath))
            {
                return invoiceDocumentsList;
            }
            //////Looping each customers
            ////foreach (string eachAccountNumber in customerId)
            ////{
            //check if InvoiceAccount or OrderAccount  are empty
            if (!(string.IsNullOrEmpty(customerId))) // eachAccountNumber is empty or not
            {
                //create InvoiceDocumentsList for each customr

                //each customer will have either multiple/single document(s) in InvoiceDocumentsListEachCustomer object 
                var invoiceDocumentsListEachCustomer = invoiceRepository.GetInvoiceDocument(folderpath, customerId, invoiceIDsList);
                invoiceDocumentsListEachCustomer.CustomerID = customerId;
                //Storing InvoiceDocumentsListEachCustomer into list for single/multiple customers(s)
                invoiceDocumentsList.Add(invoiceDocumentsListEachCustomer);
            }
            //// }
            return invoiceDocumentsList;
        }

        /// <summary>
        /// Accepts invoices to cancel and returns the validation details
        /// </summary>
        /// <param name="invoiceList"></param>
        /// <returns></returns>
        public ValidationDetail CancellInvoices(List<InvoiceCancellation> InvoiceCancellationList)
        {
            //getting error list if any errors
            List<string> errorList = new List<string>();
            // talks about the overall action result
            ValidationDetail validationDetail = new ValidationDetail();
            //uses for each and every Invoice insert or update
            ValidationDetail validation = new ValidationDetail();
            // Creating a Invoice repository object to call and cancel invoices
            InvoiceRepository invoiceRepository = new InvoiceRepository(_iInvoiceRepository);

            //Looping each Invoice 
            foreach (var eachInvoiceItem in InvoiceCancellationList)
            {
                //check if Invoice ID is empty 
                if (!(string.IsNullOrEmpty(eachInvoiceItem.BillableItemID)))
                {
                    //Actual calling to the repository
                    validation = invoiceRepository.CancelInvoice(eachInvoiceItem);

                    //true for success & false for failure
                    if (validation.Result == false && validation.ErrorList.Any())
                    {
                        //Addding the error in the error list
                        // errorList.Add("There was some error while saving this item . A notification is sent to the administrator.");
                        errorList.Add(validation.ErrorList[0].ToString());
                    }
                }
                else
                    errorList.Add("Billable Item ID(Invoice id) should not be empty.");
            }
            //finally if any errors then the result will be false
            if (errorList.Count != 0)
            {
                //if any errors then the result is false and assigning the error list
                validationDetail.Result = false;
                validationDetail.ErrorList = errorList;
            }
            else
            {
                //if any errors then the result is true and returns the validation detail object.
                validationDetail.Result = true;
            }
            return validationDetail;
        }
    }
}
