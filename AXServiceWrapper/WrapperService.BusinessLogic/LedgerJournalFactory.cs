﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/19/2014
'Description: 
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using WrapperService.BusinessEntities;
using WrapperService.DataAccessRepository;
using WrapperService.DataAccessRepository.Interfaces;

namespace WrapperService.BusinessLogic
{

    public class LedgerJournalFactory
    {
        private readonly ILedgerJournalRepository _iLedgerJournalRepository;

        public LedgerJournalFactory()
        {
        }

        public LedgerJournalFactory(ILedgerJournalRepository ledgerJournalRepository)
        {
            _iLedgerJournalRepository = ledgerJournalRepository;
        }

        /// <summary>
        /// Create or Update LedgerJournals list and returns the validation details
        /// </summary>
        /// <param name="ledgerJournalList"></param>
        /// <returns></returns>
        public ValidationDetail CreatePayment(List<LedgerJournal> ledgerJournalList)
        {
            //getting error list if any errors
            List<string> errorList = new List<string>();
            // talks about the overall action result
            ValidationDetail validationDetail = new ValidationDetail();
            //uses for each and every LedgerJournal insert or update
            ValidationDetail validation = new ValidationDetail();
            // Creating a LedgerJournal repository object to call create LedgerJournal
            LedgerJournalRepository ledgerJournalRepository = new LedgerJournalRepository(_iLedgerJournalRepository);

           
            //Looping each LedgerJournal 
            foreach (LedgerJournal eachLedgerJournal in ledgerJournalList)
            {
                ////check if LedgerJournalAccount or OrderAccount  are empty
                // if (!(string.IsNullOrEmpty(eachLedgerJournal))) // LedgerJournalAccount is Customer account number.
                // {
               
                    //Actual calling to the repository
                    validation = ledgerJournalRepository.CreatePayment(eachLedgerJournal);

                    //true for success & false for failure
                    if (validation.Result == false && validation.ErrorList.Any())
                    {
                        //Addding the error in the error list
                        // errorList.Add("There was some error while saving this item . A notification is sent to the administrator.");
                        errorList.Add(validation.ErrorList[0].ToString());
                    }
                //}
                //else
                //    errorList.Add("Ledger Journal Account or Order Account should not be empty.");
            }
            //finally if any errors then the result will be false
            if (errorList.Count != 0)
            {
                //if any errors then the result is false and assigning the error list
                validationDetail.Result = false;
                validationDetail.ErrorList = errorList;
            }
            else
            {
                //if any errors then the result is true and returns the validation detail object.
                validationDetail.Result = true;
            }
            return validationDetail;
        }

    }
}
