﻿/******************************************************************************************
'Created By     : ANANTHAKUMAR Subramani
'Created Date   : 12/06/2014
'Description    :  Functional test for Customer management ( create/update/Disable Customer operations)
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using WrapperService.BusinessLogic;
using WrapperService.DataContracts;
using WrapperService.MessageContracts;
using WrapperService.ServiceImplementation.Translators;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using TestContext = Microsoft.VisualStudio.TestTools.UnitTesting.TestContext;

namespace WrapperTestProject
{


    /// <summary>
    ///This is a test class for CustomerTest and is intended
    ///to contain all CustomerTest Unit Tests
    ///</summary>
    [TestFixture()]
    public class CustomerFunctionalTest
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        [TestInitialize()]
        public void Initialize()
        {

        }

        // Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void CustomerTestCleanup()
        {
            //Call Disable Customer 
            DeletePermanantly();
        }

        #region " Positive Test cases"


        /// <summary>
        /// Generates a random string with the given length
        /// </summary>
        /// <param name="size">Size of the string</param>
        /// <returns>Random string</returns>
        private string RandomAccountNumber(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();

            for (int i = 0; i < size; i++)
            {
                builder.Append(Convert.ToInt32(random.Next(9)));
            }
            return builder.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private void DeletePermanantly()
        {
            //File path
            const string path = @"E:\VSUSA\Projects\AXWrapper\AXServiceWrapper\WrapperUnitTest\WrapperUnitTest\Files\AccountNumberstoDelete.xml";

            List<string> accountNumbers = new List<string>();
            XmlTextReader reader = new XmlTextReader(path);
            //read the account numbers
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    //read only node type as text 
                    case XmlNodeType.Text: //Display the text in each element.
                        accountNumbers.Add(reader.Value);
                        break;
                }
            }

            #region "Disable Customer"

            CustomerFactory customerFactory = new CustomerFactory();

            var disableCustomerRequest = new DisableCustomerRequest();
            var disableCustomerResponse = new DisableCustomerResponse();
            disableCustomerRequest.AccountNumbers = accountNumbers;
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail = ValidationDetailTranslator.TranslateToDataContract( customerFactory.DisableCustomer(disableCustomerRequest.AccountNumbers));
            disableCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            var isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = accountNumbers;

            var isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(false, validationDetailRead.Result);

            #endregion "Disable Customer"
        }

        #region " Create and Delete Positive tests"
        /// <summary>
        /// Create a cutomer with single address
        /// </summary>
        [Test()]
        public void CreateCustomerNoAddressNoContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);
            //  DeletePermanantly();
            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "NoAddressNoContact",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;

            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }
        [Test()]
        public void CreateCustomerNoAddressNoContactOrg()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);
            //  DeletePermanantly();
            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Organization",
                OrganizationName="ventech Corp."
            };

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;

            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

      
        /// <summary>
        ///     Create Customer without Currency 
        /// </summary>
        [Test()]
        public void CreateCustomerWithoutCurrency()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "SingleAddress",
                MiddleName = "",
                // Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;

            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }
        /// <summary>
        /// Create Customer Single address no contact
        /// </summary>
        [Test()]
        public void CreateCustomerSingleAddressNoContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "SingleAddressnoContact",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Address objFirstAddress = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Location Name",
                Locator = "",
                Roles = "DELIVERY",
                State = "OH",
                Street = "1st street",
                //Type = "EMAIL",
                //TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        /// Create Customer Single address no contact with account is Disable when create
        /// </summary>
        [Test()]
        public void CreateCustomerSingleAddressNoContactwithDisableMode()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "SingleAddressnoContact",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                 NameSequence = "FirstLast",
                CustomerType = "Person",
          
                IsDisable=true
            };

            //Address data
            Address objFirstAddress = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Location Name",
                Locator = "",
                Roles = "DELIVERY",
                State = "OH",
                Street = "1st street",
                //Type = "EMAIL",
                //TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }


        /// <summary>
        /// Create Customer Single address no contact with account is Disable when create
        /// </summary>
        [Test()]
        public void CreateCustomerSingleAddrNoContactwithEnableAccount()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "SingleAddressnoContact",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                IsDisable = false,
                CustomerType = "Person"
            };

            //Address data
            Address objFirstAddress = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Location Name",
                Locator = "",
                Roles = "DELIVERY",
                State = "OH",
                Street = "1st street",
                //Type = "EMAIL",
                //TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Customer multiple address no contact
        /// </summary>
        [Test()]
        public void CreateCustomerMultipleAddressNoContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "MultipleAd",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "OfficeAddress",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress1);
            addNewCustomer.AddressList.Add(objFirstAddress2);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Customer no address single contact
        /// </summary>
        [Test()]
        public void CreateCustomerNoAddressSingleContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "SingleAddress",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "Phone",
                Locator = "Home",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            addNewCustomer.ContactList = new List<Contact>();
            //Add the address in address list of customer object.
            addNewCustomer.ContactList.Add(objContact1);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Customer no address multiple contact
        /// </summary>
        [Test()]
        public void CreateCustomerNoAddressMultipleContacts()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "NoAddressMultipleContacts",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            //Address data
            Contact objContact2 = new Contact
            {
                ACADISID = "124",
                IsPrimary = false,
                ContactName = "6141231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            addNewCustomer.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer.ContactList.Add(objContact1);
            addNewCustomer.ContactList.Add(objContact2);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Customer single address single contact
        /// </summary>
        [Test()]
        public void CreateCustomerSingleAddressSingleContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "SingleAddress",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objAddress1);

            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };


            addNewCustomer.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer.ContactList.Add(objContact1);


            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }


        /// <summary>
        ///Create Customer  Single Address Multiple Contacts
        /// </summary>
        [Test()]
        public void CreateCustomerSingleAddressMultipleContacts()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "multi contacts",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objAddress1);

            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            //contact data
            Contact objContact2 = new Contact
            {
                ACADISID = "124",
                IsPrimary = false,
                ContactName = "6141231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            addNewCustomer.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer.ContactList.Add(objContact1);
            addNewCustomer.ContactList.Add(objContact2);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }


        /// <summary>
        /// Create Customer Multiple Addresses with Multiple Contacts
        /// </summary>
        [Test()]
        public void CreateCustomerMultipleAddressMultipleContacts()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "Multiple",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objAddress1);
            addNewCustomer.AddressList.Add(objAddress2);

            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            //contact data
            Contact objContact2 = new Contact
            {
                ACADISID = "124",
                IsPrimary = false,
                ContactName = "6141231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            addNewCustomer.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer.ContactList.Add(objContact1);
            addNewCustomer.ContactList.Add(objContact2);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Customer Single address Single contact
        /// </summary>
        [Test()]
        public void CreateCustomerMultipleAddressSingleContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "SingleAddressnoContact",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "OfficeAddress",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress1);
            addNewCustomer.AddressList.Add(objFirstAddress2);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }



        /// <summary>
        ///     Create Customer Multiple Addresses with Multiple Contacts
        /// </summary>
        [Test()]
        public void CreateMultipleCustomerMultipleAddressSingleContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "Multiple",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };
            //Creating Account number
            string accountNumber2 = RandomAccountNumber(9);

            Customer addNewCustomer2 = new Customer
            {
                AccountNumber = accountNumber2,
                FirstName = "CreateCustomer",
                LastName = "Multiple",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            string accountNumber3 = RandomAccountNumber(9);

            Customer addNewCustomer3 = new Customer
            {
                AccountNumber = accountNumber3,
                FirstName = "CreateCustomer",
                LastName = "Multiple",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };
            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objAddress1);
            addNewCustomer.AddressList.Add(objAddress2);


            addNewCustomer2.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer2.AddressList.Add(objAddress1);
            addNewCustomer2.AddressList.Add(objAddress2);


            addNewCustomer3.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer3.AddressList.Add(objAddress1);
            addNewCustomer3.AddressList.Add(objAddress2);
            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            ////contact data
            //Contact objContact2 = new Contact
            //{
            //    ACADISID = "124",
            //    IsPrimary = false,
            //    ContactName = "6141231234",
            //    Locator = "Phone",
            //    Roles = "HOME",
            //    Type = "EMAIL",
            //    TypeSpecified = true,
            //};
            addNewCustomer.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer.ContactList.Add(objContact1);

            addNewCustomer2.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer2.ContactList.Add(objContact1);


            addNewCustomer3.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer3.ContactList.Add(objContact1);


            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Customer multiple address no contact
        /// </summary>
        [Test()]
        public void CreateMultipleCustomersSingleAddressNoContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "MultipleCustomer",
                LastName = "Single",
                MiddleName = "1",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            string accountNumber2 = RandomAccountNumber(9);
            //   Create a new Customer, 
            Customer addNewCustomer2 = new Customer
            {
                AccountNumber = accountNumber2,
                FirstName = "CreateCustomer",
                LastName = "single",
                MiddleName = "2",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "OfficeAddress",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };

            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress1);
            addNewCustomer.AddressList.Add(objFirstAddress2);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);
            customerList.Add(addNewCustomer2);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }
        #region "Updated for Customer type - organization - Feb-23-2015"
        /// <summary>
        ///     Create Customer without Currency 
        /// </summary>
        [Test()]
        public void CreateCustomerWithoutCurrency_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
               
                Group = "STUDENTS",
                NameAlias = "",
                NameSequence = "FirstLast",
                CustomerType = "Organization",
                OrganizationName = "Ventech Solutions INC"
            };

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;

            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }
        /// <summary>
        /// Create Customer Single address no contact
        /// </summary>
        [Test()]
        public void CreateCustomerSingleAddressNoContact_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "",
                NameSequence = "FirstLast",
                CustomerType = "Organization",
                OrganizationName = "Ventech Solutions INC"
            };

            //Address data
            Address objFirstAddress = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Location Name",
                Locator = "",
                Roles = "DELIVERY",
                State = "OH",
                Street = "1st street",
                //Type = "EMAIL",
                //TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        /// Create Customer Single address no contact with account is Disable when create
        /// </summary>
        [Test()]
        public void CreateCustomerSingleAddressNoContactwithDisableMode_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
              Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "",
                NameSequence = "FirstLast",
                CustomerType = "Organization",
                OrganizationName="Ventech Solutions INC",

                IsDisable = true
            };

            //Address data
            Address objFirstAddress = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Location Name",
                Locator = "",
                Roles = "DELIVERY",
                State = "OH",
                Street = "1st street",
                //Type = "EMAIL",
                //TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }


        /// <summary>
        /// Create Customer Single address no contact with account is Disable when create
        /// </summary>
        [Test()]
        public void CreateCustomerSingleAddrNoContactwithEnableAccount_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "",
                NameSequence = "FirstLast",
                CustomerType = "Organization",
                OrganizationName="Ventech Solutions INC",
                IsDisable = false 
            };

            //Address data
            Address objFirstAddress = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Location Name",
                Locator = "",
                Roles = "DELIVERY",
                State = "OH",
                Street = "1st street",
                //Type = "EMAIL",
                //TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Customer multiple address no contact
        /// </summary>
        [Test()]
        public void CreateCustomerMultipleAddressNoContact_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "",
                NameSequence = "FirstLast",
                CustomerType = "Organization",
                OrganizationName = "Ventech Solutions INC"
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "OfficeAddress",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress1);
            addNewCustomer.AddressList.Add(objFirstAddress2);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Customer no address single contact
        /// </summary>
        [Test()]
        public void CreateCustomerNoAddressSingleContact_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "",
                NameSequence = "FirstLast",
                CustomerType = "Organization",
                OrganizationName = "Ventech Solutions INC"
            };

            //Address data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "Phone",
                Locator = "Home",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            addNewCustomer.ContactList = new List<Contact>();
            //Add the address in address list of customer object.
            addNewCustomer.ContactList.Add(objContact1);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Customer no address multiple contact
        /// </summary>
        [Test()]
        public void CreateCustomerNoAddressMultipleContacts_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "",
                NameSequence = "FirstLast",
                CustomerType = "Organization",
                OrganizationName = "Ventech Solutions INC"
            };

            //Address data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            //Address data
            Contact objContact2 = new Contact
            {
                ACADISID = "124",
                IsPrimary = false,
                ContactName = "6141231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            addNewCustomer.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer.ContactList.Add(objContact1);
            addNewCustomer.ContactList.Add(objContact2);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Customer single address single contact
        /// </summary>
        [Test()]
        public void CreateCustomerSingleAddressSingleContact_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "",
                NameSequence = "FirstLast",
                CustomerType = "Organization",
                OrganizationName = "Ventech Solutions INC"
            };

            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objAddress1);

            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };


            addNewCustomer.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer.ContactList.Add(objContact1);


            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }


        /// <summary>
        ///Create Customer  Single Address Multiple Contacts
        /// </summary>
        [Test()]
        public void CreateCustomerSingleAddressMultipleContacts_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "",
                NameSequence = "FirstLast",
                CustomerType = "Organization",
                OrganizationName = "Ventech Solutions INC"
            };

            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objAddress1);

            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            //contact data
            Contact objContact2 = new Contact
            {
                ACADISID = "124",
                IsPrimary = false,
                ContactName = "6141231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            addNewCustomer.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer.ContactList.Add(objContact1);
            addNewCustomer.ContactList.Add(objContact2);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }


        /// <summary>
        /// Create Customer Multiple Addresses with Multiple Contacts
        /// </summary>
        [Test()]
        public void CreateCustomerMultipleAddressMultipleContacts_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "",
                NameSequence = "FirstLast",
                CustomerType = "Organization",
                OrganizationName = "Ventech Solutions INC"
            };

            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objAddress1);
            addNewCustomer.AddressList.Add(objAddress2);

            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            //contact data
            Contact objContact2 = new Contact
            {
                ACADISID = "124",
                IsPrimary = false,
                ContactName = "6141231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            addNewCustomer.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer.ContactList.Add(objContact1);
            addNewCustomer.ContactList.Add(objContact2);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Customer Single address Single contact
        /// </summary>
        [Test()]
        public void CreateCustomerMultipleAddressSingleContact_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "",
                NameSequence = "FirstLast",
                CustomerType = "Organization",
                OrganizationName = "Ventech Solutions INC"
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "OfficeAddress",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress1);
            addNewCustomer.AddressList.Add(objFirstAddress2);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }



        /// <summary>
        ///     Create Customer Multiple Addresses with Multiple Contacts
        /// </summary>
        [Test()]
        public void CreateMultipleCustomerMultipleAddressSingleContact_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "",
                NameSequence = "FirstLast",
                CustomerType = "Organization",
                OrganizationName = "Ventech Solutions INC"
            };
            //Creating Account number
            string accountNumber2 = RandomAccountNumber(9);

            Customer addNewCustomer2 = new Customer
            {
                AccountNumber = accountNumber2,
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "",
                NameSequence = "FirstLast",
                CustomerType = "Organization",
                OrganizationName = "Ventech Solutions INC2"
            };

            string accountNumber3 = RandomAccountNumber(9);

            Customer addNewCustomer3 = new Customer
            {
                AccountNumber = accountNumber3,
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "",
                NameSequence = "FirstLast",
                CustomerType = "Organization",
                OrganizationName = "Ventech Solutions INC3"
            };
            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objAddress1);
            addNewCustomer.AddressList.Add(objAddress2);


            addNewCustomer2.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer2.AddressList.Add(objAddress1);
            addNewCustomer2.AddressList.Add(objAddress2);


            addNewCustomer3.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer3.AddressList.Add(objAddress1);
            addNewCustomer3.AddressList.Add(objAddress2);
            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            ////contact data
            //Contact objContact2 = new Contact
            //{
            //    ACADISID = "124",
            //    IsPrimary = false,
            //    ContactName = "6141231234",
            //    Locator = "Phone",
            //    Roles = "HOME",
            //    Type = "EMAIL",
            //    TypeSpecified = true,
            //};
            addNewCustomer.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer.ContactList.Add(objContact1);

            addNewCustomer2.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer2.ContactList.Add(objContact1);


            addNewCustomer3.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer3.ContactList.Add(objContact1);


            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Customer multiple address no contact
        /// </summary>
        [Test()]
        public void CreateMultipleCustomersSingleAddressNoContact_Organization()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "",
                NameSequence = "FirstLast",
                CustomerType = "Organization",
                OrganizationName = "Ventech Solutions INC 1"
            };

            string accountNumber2 = RandomAccountNumber(9);
            //   Create a new Customer, 
            Customer addNewCustomer2 = new Customer
            {
                AccountNumber = accountNumber2,
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "",
                NameSequence = "FirstLast",
                CustomerType = "Organization",
                OrganizationName="Ventech Solutions INC2"
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "OfficeAddress",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };

            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress1);
            addNewCustomer.AddressList.Add(objFirstAddress2);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);
            customerList.Add(addNewCustomer2);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }
        #endregion ""Updated for Customer type - organization - Feb-23-2015"
        #endregion " Create Positive tests"

        #region " Update Positive tests"
        /// <summary>
        /// Create a cutomer with single address
        /// </summary>
        [Test()]
        public void UpdateCustomerNoAddressNoContact()
        {
            //Create 
            string accountNumber = "010556607";

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "NoAddressNoContact",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast"
                ,
                IsDisable = true,
                CustomerType = "Person"
            };


            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            #region " Create Customer"
            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;
            #endregion " Create Customer"

            #region " Update"
            addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "Updated FName",
                LastName = "Updated LName",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Assigning current object
            customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            customerFactory = new CustomerFactory();

            saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            validationDetail =
              ValidationDetailTranslator.TranslateToDataContract(
                  customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;
            #endregion " Update"
            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        /// Create a cutomer with single address
        /// </summary>
        [Test()]
        public void UpdateCustomerNoAddressNoContactwithDisable()
        {
            //Create 
            string accountNumber = "010556607";

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "NoAddressNoContact",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                IsDisable = true,
                CustomerType = "Person"
            };


            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            #region " Create Customer"
            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;
            #endregion " Create Customer"

            #region " Update"
            addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "Updated FName",
                LastName = "Updated LName",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Assigning current object
            customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            customerFactory = new CustomerFactory();

            saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            validationDetail =
              ValidationDetailTranslator.TranslateToDataContract(
                  customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;
            #endregion " Update"
            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

       
        /// <summary>
        /// Create Customer Single address no contact
        /// </summary>
        [Test()]
        public void UpdateCustomerSingleAddressNoContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "SingleAddressnoContact",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Address objFirstAddress = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;


            #region " Update"
            addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "Updated FName",
                LastName = "Updated LName",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Assigning current object
            customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.

            objFirstAddress.City = "Lewis Center";
            addNewCustomer.AddressList.Add(objFirstAddress);

            saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            customerFactory = new CustomerFactory();

            saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            validationDetail =
              ValidationDetailTranslator.TranslateToDataContract(
                  customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;
            #endregion " Update"


            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///Create Customer multiple address no contact
        /// </summary>
        [Test()]
        public void UpdateCustomerMultipleAddressNoContact()
        {
            //Create 
            string accountNumber = "533262100";//RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "MultipleAd",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "OfficeAddress",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress1);
            addNewCustomer.AddressList.Add(objFirstAddress2);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            #region " Update"
            addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "Updated FName",
                LastName = "Updated LName",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Assigning current object
            customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            //Address data
            objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis Center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };

            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress1);
            addNewCustomer.AddressList.Add(objFirstAddress2);

            customerList = new List<Customer>();
            customerList.Add(addNewCustomer);


            saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            customerFactory = new CustomerFactory();

            saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            validationDetail = ValidationDetailTranslator.TranslateToDataContract(customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;
            #endregion " Update"
            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }


        /// <summary>
        ///     Create Customer no address single contact
        /// </summary>
        [Test()]
        public void UpdateCustomerNoAddressSingleContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "SingleAddress",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "Phone",
                Locator = "Home",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            addNewCustomer.ContactList = new List<Contact>();
            //Add the address in address list of customer object.
            addNewCustomer.ContactList.Add(objContact1);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;


            #region " Update"
            addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "Updated FName",
                LastName = "Updated LName",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Assigning current object
            customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "Phone",
                Locator = "1231231234",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            //Address data
            Address objFirstAddress1 = new Address
             {
                 //AddressMethodType = "EMAIL",
                 City = "Lewis Center",
                 CountryRegionID = "USA",
                 IsPrimaryAddress = false,
                 LocationName = "Home",
                 Locator = "",
                 Roles = "HOME",
                 State = "OH",
                 Street = "8491 street",
                 Type = "EMAIL",
                 TypeSpecified = true,
                 ZipCode = "43240"
             };


            //Address data
            Address objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress1);
            addNewCustomer.AddressList.Add(objFirstAddress2);

            addNewCustomer.ContactList = new List<Contact>();
            addNewCustomer.ContactList.Add(objContact1);

            customerList = new List<Customer>();
            customerList.Add(addNewCustomer);


            saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            customerFactory = new CustomerFactory();

            saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            validationDetail =
              ValidationDetailTranslator.TranslateToDataContract(
                  customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;
            #endregion " Update"

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }


        /// <summary>
        ///     Create Customer no address multiple contact
        /// </summary>
        [Test()]
        public void UpdateCustomerNoAddressMultipleContacts()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "Lname",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            //Address data
            Contact objContact2 = new Contact
            {
                ACADISID = "124",
                IsPrimary = false,
                ContactName = "6141231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            addNewCustomer.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer.ContactList.Add(objContact1);
            addNewCustomer.ContactList.Add(objContact2);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            #region " Update"
            addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "Updated FName",
                LastName = "Updated LName",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Assigning current object
            customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "Phone",
                Locator = "1231231234",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis Center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress1);
            addNewCustomer.AddressList.Add(objFirstAddress2);

            addNewCustomer.ContactList = new List<Contact>();
            addNewCustomer.ContactList.Add(objContact1);

            customerList = new List<Customer>();
            customerList.Add(addNewCustomer);


            saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            customerFactory = new CustomerFactory();

            saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            validationDetail =
              ValidationDetailTranslator.TranslateToDataContract(
                  customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;
            #endregion " Update"
            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Create Customer single address single contact
        /// </summary>
        [Test()]
        public void UpdateCustomerSingleAddressSingleContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "SingleAddress",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objAddress1);

            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };


            addNewCustomer.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer.ContactList.Add(objContact1);


            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            #region " Update"
            addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "Updated FName",
                LastName = "Updated LName",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Assigning current object
            customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "Phone",
                Locator = "1231231234",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis Center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress1);

            addNewCustomer.ContactList = new List<Contact>();
            addNewCustomer.ContactList.Add(objContact1);

            customerList = new List<Customer>();
            customerList.Add(addNewCustomer);


            saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            customerFactory = new CustomerFactory();

            saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            validationDetail =
              ValidationDetailTranslator.TranslateToDataContract(
                  customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;
            #endregion " Update"
            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }


        /// <summary>
        ///Update Customer  Single Address Multiple Contacts
        /// </summary>
        [Test()]
        public void UpdateCustomerSingleAddressMultipleContacts()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "multi contacts",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objAddress1);

            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            //contact data
            Contact objContact2 = new Contact
            {
                ACADISID = "124",
                IsPrimary = false,
                ContactName = "6141231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            addNewCustomer.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer.ContactList.Add(objContact1);
            addNewCustomer.ContactList.Add(objContact2);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }


        /// <summary>
        /// Update Customer Multiple Addresses with Multiple Contacts
        /// </summary>
        [Test()]
        public void UpdateCustomerMultipleAddressMultipleContacts()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "Multiple",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objAddress1);
            addNewCustomer.AddressList.Add(objAddress2);

            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            //contact data
            Contact objContact2 = new Contact
            {
                ACADISID = "124",
                IsPrimary = false,
                ContactName = "6141231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            addNewCustomer.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer.ContactList.Add(objContact1);
            addNewCustomer.ContactList.Add(objContact2);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Update Customer Single address Single contact
        /// </summary>
        [Test()]
        public void UpdateCustomerMultipleAddressSingleContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "SingleAddressnoContact",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "OfficeAddress",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress1);
            addNewCustomer.AddressList.Add(objFirstAddress2);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }



        /// <summary>
        ///     Update Customer Multiple Addresses with Multiple Contacts
        /// </summary>
        [Test()]
        public void UpdateMultipleCustomerMultipleAddressSingleContact()
        {
            //Update 
            string accountNumber = RandomAccountNumber(9);

            //   Update a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "Multiple",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };
            //Creating Account number
            string accountNumber2 = RandomAccountNumber(9);

            Customer addNewCustomer2 = new Customer
            {
                AccountNumber = accountNumber2,
                FirstName = "CreateCustomer",
                LastName = "Multiple",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            string accountNumber3 = RandomAccountNumber(9);

            Customer addNewCustomer3 = new Customer
            {
                AccountNumber = accountNumber3,
                FirstName = "CreateCustomer",
                LastName = "Multiple",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };
            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objAddress1);
            addNewCustomer.AddressList.Add(objAddress2);


            addNewCustomer2.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer2.AddressList.Add(objAddress1);
            addNewCustomer2.AddressList.Add(objAddress2);


            addNewCustomer3.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer3.AddressList.Add(objAddress1);
            addNewCustomer3.AddressList.Add(objAddress2);
            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            ////contact data
            //Contact objContact2 = new Contact
            //{
            //    ACADISID = "124",
            //    IsPrimary = false,
            //    ContactName = "6141231234",
            //    Locator = "Phone",
            //    Roles = "HOME",
            //    Type = "EMAIL",
            //    TypeSpecified = true,
            //};
            addNewCustomer.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer.ContactList.Add(objContact1);

            addNewCustomer2.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer2.ContactList.Add(objContact1);


            addNewCustomer3.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer3.ContactList.Add(objContact1);


            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);
            #region " Update"
            addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "Updated FName",
                LastName = "Updated LName",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Assigning current object
            customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "Phone",
                Locator = "1231231234",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis Center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress1);
            addNewCustomer.AddressList.Add(objFirstAddress1);

            addNewCustomer.ContactList = new List<Contact>();
            //Add the address in address list of customer object.

            addNewCustomer.ContactList.Add(objContact1);

            customerList = new List<Customer>();
            customerList.Add(addNewCustomer);


            //Add the address in address list of customer object.
            addNewCustomer2.AddressList.Add(objFirstAddress1);
            addNewCustomer2.AddressList.Add(objFirstAddress1);
            addNewCustomer2.ContactList.Add(objContact1);

            customerList.Add(addNewCustomer2);



            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress1);
            addNewCustomer.ContactList.Add(objContact1);

            customerList.Add(addNewCustomer);


            saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            customerFactory = new CustomerFactory();

            saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            validationDetail =
              ValidationDetailTranslator.TranslateToDataContract(
                  customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;
            #endregion " Update"
            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        /// <summary>
        ///     Update Customer multiple address no contact
        /// </summary>
        [Test()]
        public void UpdateMultipleCustomersSingleAddressNoContact()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "MultipleCustomer",
                LastName = "Single",
                MiddleName = "1",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            string accountNumber2 = RandomAccountNumber(9);
            //   Create a new Customer, 
            Customer addNewCustomer2 = new Customer
            {
                AccountNumber = accountNumber2,
                FirstName = "CreateCustomer",
                LastName = "single",
                MiddleName = "2",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "OfficeAddress",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };

            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress1);
            addNewCustomer.AddressList.Add(objFirstAddress2);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);
            customerList.Add(addNewCustomer2);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            //Check the customer added in there or not
            IsCustomerExistsRequest isCustomerExistsRequest = new IsCustomerExistsRequest();
            //adding account number into accounlt number list
            isCustomerExistsRequest.AccountNumbers = new List<string> { accountNumber };

            IsCustomerExistsResponse isCustomerExistsResponse = new IsCustomerExistsResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            var validationDetailRead =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.IsCustomerExisting(isCustomerExistsRequest.AccountNumbers));
            isCustomerExistsResponse.ValidationDetail = validationDetailRead;

            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetailRead.Result);

            //Store the account number in the xml file for delete in test clean up
            UpdateXml(accountNumber);
        }

        #endregion "Update"
        /// <summary>
        /// Update Xml file with account number tag to delete.
        /// </summary>
        /// <param name="accountNumber"></param>
        public void UpdateXml(string accountNumber)
        {
            XmlDocument doc = new XmlDocument();
            const string path =
                @"E:\VSUSA\Projects\AXWrapper\AXServiceWrapper\WrapperUnitTest\WrapperUnitTest\Files\AccountNumberstoDelete.xml";
            doc.Load(path);
            // Create a new element node.
            XmlNode newElem = doc.CreateNode("element", "accountNumber", "");
            newElem.InnerText = accountNumber;

            XmlElement root = doc.DocumentElement;
            root.AppendChild(newElem);

            //doc.("/AccountNumbers/accountNumber").Attributes["value"].Value = AccountNumber;

            //doc.SelectSingleNode("/AccountNumbers/accountNumber").Attributes["value"].Value = AccountNumber;

            //foreach (XmlNode node1 in node.ChildNodes)
            //    foreach (XmlNode node2 in node1.ChildNodes)
            //        if (node2.Name == "price")
            //        {
            //            Decimal price = Decimal.Parse(node2.InnerText);
            //            // Increase all the book prices by 20%
            //            String newprice = ((Decimal)price * (new Decimal(1.20))).ToString("#.00");
            //            Console.WriteLine("Old Price = " + node2.InnerText + "\tNew price = " + newprice);
            //            node2.InnerText = newprice;
            //        }
            try
            {
                doc.Save(path);

            }
            catch (Exception)
            {
                
            }
           
        }

        /// <summary>
        ///Disable Customers
        /// </summary>
        [Test()]
        public void disableCustomerMultiple()
        {

            //Create 
            string accountNumber1 = "777777820";
            string accountNumber2 = "777777823";

            List<string> accountNumberList = new List<string>();
            accountNumberList.Add(accountNumber1);
            accountNumberList.Add(accountNumber2);

            DisableCustomerRequest disableCustomerRequest = new DisableCustomerRequest { AccountNumbers = accountNumberList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            DisableCustomerResponse disableCustomerResponse = new DisableCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail = ValidationDetailTranslator.TranslateToDataContract(
                customerFactory.DisableCustomer(disableCustomerRequest.AccountNumbers));
            disableCustomerResponse.ValidationDetail = validationDetail;
            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetail.Result);
            //Store the account number in the xml file for delete in test clean up
            // UpdateXml(accountNumber);
        }

        /// <summary>
        ///Disable Customer
        /// </summary>
        [Test()]
        public void disableCustomerSingle()
        {

            //Create 
            string accountNumber1 = "065527648";

            List<string> accountNumberList = new List<string>();
            accountNumberList.Add(accountNumber1);

            DisableCustomerRequest disableCustomerRequest = new DisableCustomerRequest { AccountNumbers = accountNumberList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            DisableCustomerResponse disableCustomerResponse = new DisableCustomerResponse();
            //Calling Disable Customer method
            ValidationDetail validationDetail = ValidationDetailTranslator.TranslateToDataContract(customerFactory.DisableCustomer(disableCustomerRequest.AccountNumbers));
            disableCustomerResponse.ValidationDetail = validationDetail;
            //Expected should be true - if customer added
            Assert.AreEqual(true, validationDetail.Result);
        }

        #endregion " Positive Test cases"


        #region " Negative Test cases"

        #region "Create"
        /// <summary>
        /// Create Customer Single address no contact  here Group name is empty
        /// </summary>
        [Test()]
        public void CreateCustomertWithoutAccountNumber()
        {
            //Account nunmber is empty 
            string accountNumber = string.Empty;

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "SingleAddressnoContact",
                MiddleName = "",
                Currency = "USD",
                Group = "",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Address objFirstAddress = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;

            string expectedError = "Account Number should not be empty.";

            Assert.AreEqual(expectedError, validationDetail.ErrorList[0].ToString());    //Expected should be true - if customer added

        }
        /// <summary>
        /// Trying to crerate a customer without group
        /// </summary>
        [Test()]
        public void CreateCustomertwithoutGroup()
        {
            //Create account number
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "SingleAddressnoContact",
                MiddleName = "",
                Currency = "USD",
                Group = "",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Address objFirstAddress = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;

            const string expectedError = "Field 'Customer group' must be filled in.";
            bool isError = false;
            if (validationDetail.ErrorList != null)
                isError = (validationDetail.ErrorList[0].StartsWith(expectedError));

            //Expected should be true - if customer added
            Assert.AreEqual(true, isError);

        }

        /// <summary>
        ///     Create Customer Adding customer without Country region
        /// </summary>
        [Test()]
        public void CreateCustomerAddAddressWithoutCountryRegion()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "MultipleAd",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                //   CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "OfficeAddress",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };

            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress1);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;

            const string errorCountryRegion = "Check country/region";
            bool isErrorOnCountryRegion = false;
            if (validationDetail.ErrorList.Any())
                isErrorOnCountryRegion = (validationDetail.ErrorList[0].Contains(errorCountryRegion));

            //Expected should be true - country region is missing
            Assert.AreEqual(true, isErrorOnCountryRegion);

            const string errorState = "State not found";
            bool isErrorState = false;
            if (validationDetail.ErrorList.Any())
                isErrorState = (validationDetail.ErrorList[0].Contains(errorState));

            //Expected should be true - state is missing
            Assert.AreEqual(true, isErrorState);
        }

        /// <summary>
        ///     Create Customer single address single contact
        /// </summary>
        [Test()]
        public void CreateCustomerSingleAddressWithOutRole()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "SingleAddress",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Adding address data without adding Roles
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                // Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objAddress1);

            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };


            addNewCustomer.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer.ContactList.Add(objContact1);


            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;


            const string expectedErrorMsg = "The field Type is blank or the relation record is missing";
            bool isErrorState = false;
            if (validationDetail.ErrorList.Any())
                isErrorState = (validationDetail.ErrorList[0].Contains(expectedErrorMsg));

            //Expected should be true - state is missing
            Assert.AreEqual(true, isErrorState);
        }


        /// <summary>
        ///Create Customer- adding contacts withoiout roles
        /// </summary>
        [Test()]
        public void CreateCustomerContactsWithoutRoles()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "multi contacts",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objAddress1);

            //contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "1231231234",
                Locator = "Phone",
                //Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            //contact data
            Contact objContact2 = new Contact
            {
                ACADISID = "124",
                IsPrimary = false,
                ContactName = "6141231234",
                Locator = "Phone",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            addNewCustomer.ContactList = new List<Contact>();
            //Add the address in contact list of customer object.
            addNewCustomer.ContactList.Add(objContact1);
            addNewCustomer.ContactList.Add(objContact2);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;

            const string expectedErrorMsg = "The field Type is blank or the relation record is missing";
            bool isErrorState = false;
            if (validationDetail.ErrorList.Any())
                isErrorState = (validationDetail.ErrorList[0].Contains(expectedErrorMsg));

            //Expected should be true - Role type is missing
            Assert.AreEqual(true, isErrorState);
        }


        /// <summary>
        /// Create Customer Add address with Invalid state - state should be 2 digits. Im tryinig to give "OHIO" as value
        /// </summary>
        [Test()]
        public void CreateCustomerAddAddressWithInvalidState()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "Multiple",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Address objAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "EMAIL",
                Locator = "",
                Roles = "HOME",
                State = "OHIO",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };



            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objAddress1);

            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;

            const string expectedErrorMsg = "State not found.";
            bool isErrorState = false;
            if (validationDetail.ErrorList.Any())
                isErrorState = (validationDetail.ErrorList[0].Contains(expectedErrorMsg));

            //Expected should be true - state is missing
            Assert.AreEqual(true, isErrorState);
        }

        #endregion "Create"
        #region "delete"
        /// <summary>
        ///Disable Customer Account Invalid
        /// </summary>
        [Test()]
        public void disableCustomerInvalidAccountNumber()
        {

            //Create 
            const string accountNumber1 = "777777810121";

            List<string> accountNumberList = new List<string>();
            accountNumberList.Add(accountNumber1);

            DisableCustomerRequest disableCustomerRequest = new DisableCustomerRequest { AccountNumbers = accountNumberList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            DisableCustomerResponse disableCustomerResponse = new DisableCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.DisableCustomer(disableCustomerRequest.AccountNumbers));
            disableCustomerResponse.ValidationDetail = validationDetail;

            //Expected should be false - if customer added
            Assert.AreEqual(false, validationDetail.Result);

            //Store the account number in the xml file for delete in test clean up
            // UpdateXml(accountNumber);
        }

        /// <summary>
        ///Disable Customer Without Account Number
        /// </summary>
        [Test()]
        public void disableCustomerWithoutAccountNumber()
        {
            //Create 
            const string accountNumber1 = "";

            List<string> accountNumberList = new List<string>();
            accountNumberList.Add(accountNumber1);

            DisableCustomerRequest disableCustomerRequest = new DisableCustomerRequest { AccountNumbers = accountNumberList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            DisableCustomerResponse disableCustomerResponse = new DisableCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.DisableCustomer(disableCustomerRequest.AccountNumbers));
            disableCustomerResponse.ValidationDetail = validationDetail;

            //Expected should be true - if customer added
            Assert.AreEqual(false, validationDetail.Result);

            //Store the account number in the xml file for delete in test clean up
            // UpdateXml(accountNumber);
        }
        #endregion "delete"

        /// <summary>
        ///Trying to create Customer but no Ax service is down
        /// </summary>
        [Test()]
        public void CreateCustomerAxServiceDown()
        {
            //Create 
            string accountNumber = RandomAccountNumber(9);

            //   Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = accountNumber,
                FirstName = "CreateCustomer",
                LastName = "MultipleAd",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast",
                CustomerType = "Person"
            };

            //Address data
            Address objFirstAddress1 = new Address
            {
                //AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };


            //Address data
            Address objFirstAddress2 = new Address
            {
                ////AddressMethodType = "EMAIL",
                City = "Lewis center",
                CountryRegionID = "USA",
                IsPrimaryAddress = false,
                LocationName = "Home Address 2",
                Locator = "",
                Roles = "HOME",
                State = "OH",
                Street = "8491 oak village",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress1);
            addNewCustomer.AddressList.Add(objFirstAddress2);


            //Contact data
            Contact objContact1 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "Phone 1",
                Locator = "Home-Hall",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };

            Contact objContact2 = new Contact
            {
                ACADISID = "123",
                IsPrimary = false,
                ContactName = "Phone",
                Locator = "Home-Dining",
                Roles = "HOME",
                Type = "EMAIL",
                TypeSpecified = true,
            };
            addNewCustomer.ContactList = new List<Contact>();
            //Add the address in address list of customer object.
            addNewCustomer.ContactList.Add(objContact1);
            addNewCustomer.ContactList.Add(objContact2);
            List<Customer> customerList = new List<Customer>();
            customerList.Add(addNewCustomer);

            SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            ValidationDetail validationDetail =
                ValidationDetailTranslator.TranslateToDataContract(
                    customerFactory.SaveCustomer(CustomerTranslator.TranslateToEntities(saveCustomerRequest.Customer)));
            saveCustomerResponse.ValidationDetail = validationDetail;

            const string errorMessage = "There was no endpoint listening at ";
            bool isError = false;
            if (validationDetail.ErrorList.Any())
                isError = (validationDetail.ErrorList[0].Contains(errorMessage));

            //Expected should be true - there are no end point
            Assert.AreEqual(true, isError);

        }

        #endregion " Negative Test cases"

        /// <summary>
        ///Get Customer Data without Acocunt Numbers
        /// </summary>
        [Test()]
        public void GetCustomerwithNoAccountNumber()
        {
            //Create 
            List<string> accountNumbers = new List<string>();


            var GetCustomerRequest = new GetCustomerRequest { AccountNumbersList = accountNumbers };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            GetCustomerResponse GetCustomerResponse = new GetCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            string actualResult=     customerFactory.GetCustomer(GetCustomerRequest.AccountNumbersList);

            const string errorMessage = "Account Number should not be empty.";
            bool isError = errorMessage == actualResult;

            //Expected should be true -  bez no account number
            Assert.AreEqual(true, isError);

        }
        /// <summary>
        ///Get Customer Data with multiple Acocunt Numbers
        /// </summary>
        [Test()]
        public void GetCustomerWithInvalidAccountNumber()
        {
            //Create 
            List<string> accountNumbers = new List<string>();
            accountNumbers.Add("ababababa");

            var GetCustomerRequest = new GetCustomerRequest { AccountNumbersList = accountNumbers };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            GetCustomerResponse GetCustomerResponse = new GetCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            GetCustomerResponse.XMLString = customerFactory.GetCustomer(GetCustomerRequest.AccountNumbersList);

           const string dataStartsWith = "<ClientDefinedQuery";
           bool isdata = GetCustomerResponse.XMLString.StartsWith(dataStartsWith);

            //Expected should be true -  bez no account number
            Assert.AreEqual(true, isdata);

        }
        /// <summary>
        ///Get Customer Data with one Acocunt Number
        /// </summary>
        [Test()]
        public void GetCustomerWithOneAccountNumber()
        {
            //Create 
            List<string> accountNumbers = new List<string>();
            accountNumbers.Add("777777818");

            var GetCustomerRequest = new GetCustomerRequest { AccountNumbersList = accountNumbers };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            GetCustomerResponse GetCustomerResponse = new GetCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            GetCustomerResponse.XMLString  = customerFactory.GetCustomer(GetCustomerRequest.AccountNumbersList);

            const string dataStartsWith = "<ClientDefinedQuery";
            bool isdata = GetCustomerResponse.XMLString.StartsWith(dataStartsWith);

            //Expected should be true -  bez no account number
            Assert.AreEqual(true, isdata);

        }
        /// <summary>
        ///Get Customer Data with multiple Acocunt Numbers
        /// </summary>
        [Test()]
        public void GetCustomerWithOneAccountNumbers()
        {
            //Create 
            List<string> accountNumbers = new List<string>();
            accountNumbers.Add("777777818");
            accountNumbers.Add("777777790");

            var GetCustomerRequest = new GetCustomerRequest { AccountNumbersList = accountNumbers };
            //Create customer factory object
            CustomerFactory customerFactory = new CustomerFactory();

            GetCustomerResponse GetCustomerResponse = new GetCustomerResponse();
            //Translates the data contracts to business entities, pass it to repository and retrieve the validation details
            GetCustomerResponse.XMLString = customerFactory.GetCustomer(GetCustomerRequest.AccountNumbersList);

            const string dataStartsWith = "<ClientDefinedQuery";
            bool isdata = GetCustomerResponse.XMLString.StartsWith(dataStartsWith);

            //Expected should be true -  bez no account number
            Assert.AreEqual(true, isdata);

        }
    }
}