﻿/******************************************************************************************
'Created By     : ANANTHAKUMAR Subramani
'Created Date   : 12/05/2014
'Description    :  Unit Test project for Customer management
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using WrapperService.BusinessEntities;
using WrapperService.DataAccessRepository.Interfaces;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using TestContext = Microsoft.VisualStudio.TestTools.UnitTesting.TestContext;

// used for mocking for objects
//using Customer = WrapperService.BusinessEntities.Customer;

namespace WrapperTestProject
{
    /// <summary>
    ///This is a test class for CustomerTest and is intended
    ///to contain all CustomerTest Unit Tests
    ///</summary>
    [TestFixture()]
    public class CustomerTest
    {
        private TestContext _testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return _testContextInstance;
            }
            set
            {
                _testContextInstance = value;
            }
        }

        /// <summary>
        /// Create a new  Customer with out address
        /// </summary>
        [Test]
        public void CreateNewCustomerwithoutAddress()
        {
            // Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = "777777777",
                FirstName = "Anantha",
                LastName = "Subramani",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast"
            };

            //// Assign the addnewwCustomer in expected,
            Customer expectedResult = addNewCustomer;

            //Customer actualResult = new  Customer();

            //Customer Repository 
            Mock<ICustomerRepository> mockCustomerRepository = new Mock<ICustomerRepository>();
            ValidationDetail actualValidationResult = new ValidationDetail
            {
                Result = true
            };

            //Setup the repository insert method
            mockCustomerRepository.Setup(method => method.InsertCustomer(addNewCustomer)).Returns(actualValidationResult);

            //
            ICustomerRepository value = mockCustomerRepository.Object; // pass mock instance to interface

            actualValidationResult = value.InsertCustomer(addNewCustomer); // call mock to get data that is in database

            ValidationDetail expectedValidationResult = new ValidationDetail
            {
                Result = true
            };
            Assert.IsNotNull(expectedResult); // test everything is OK that this stage
            Assert.AreEqual(actualValidationResult.Result, expectedValidationResult.Result); // test everything is OK that this stage

            Assert.IsNotNull(expectedResult.AccountNumber); // check if null
        }

        ///unit test for create a successful customer.
        [Test]
        public void CreateCustomerWithAddress()
        {
            // Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = "111111111",
                FirstName = "Anantha",
                LastName = "Subramani",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "AKS",
                NameSequence = "FirstLast"
            };


            //Address data
            Address objFirstAddress = new Address
            {
               // AddressMethodType = "EMAIL",
                City = "Columbus",
                CountryRegionID = "",
                IsPrimaryAddress = true,
                LocationName = "EMAIL",
                Locator = "HOME",
                Roles = "",
                State = "OH",
                Street = "1st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress);

            // Assign the addnewwCustomer in expected,
            Customer expectedResult = addNewCustomer;

            //Customer actualResult = new  Customer();

            //Customer Repository 
            Mock<ICustomerRepository> mockCustomerRepository = new Mock<ICustomerRepository>();
            ValidationDetail actualValidationResult = new ValidationDetail
            {
                Result = true
            };
           

            //Setup the repository insert method
            mockCustomerRepository.Setup(method => method.InsertCustomer(addNewCustomer)).Returns(actualValidationResult);

            //
            ICustomerRepository value = mockCustomerRepository.Object; // pass mock instance to interface

            actualValidationResult = value.InsertCustomer(addNewCustomer); // call mock to get data that is in database

            ValidationDetail expectedValidationResult = new ValidationDetail
            {
                Result = true
            };

            Assert.IsNotNull(expectedResult); // test everything is OK that this stage
            Assert.AreEqual(actualValidationResult.Result, expectedValidationResult.Result); // test everything is OK that this stage

            Assert.IsNotNull(expectedResult.AccountNumber); // check if null
        }


        /// <summary>
        /// Create a Update  Customer with out address
        /// </summary>
        [Test]
        public void UpdateCustomerwithoutAddress()
        {
            // Create a new Customer, 
            Customer updateCustomer = new Customer
            {
                AccountNumber = "777777778",
                FirstName = "Ananathakumar",
                LastName = "Subramani",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "Anantha",
                NameSequence = "FirstLast"
            };

            //// Assign the addnewwCustomer in expected,
            Customer expectedResult = updateCustomer;

            //Customer actualResult = new  Customer();

            //mocking Customer Repository 
            Mock<ICustomerRepository> mockCustomerRepository = new Mock<ICustomerRepository>();
           
            // creating Expected and Actual objects
             ValidationDetail actualValidationResult = new ValidationDetail
            {
                Result = true
            };
          

            //Setup the repository insert method
            mockCustomerRepository.Setup(method => method.UpdateCustomer(updateCustomer)).Returns(actualValidationResult);

            //
            ICustomerRepository value = mockCustomerRepository.Object; // pass mock instance to interface

            //Calling update customer
            actualValidationResult = value.UpdateCustomer(updateCustomer); // call mock to get data that is in database

            ValidationDetail expectedValidationResult = new ValidationDetail
            {
                Result = true
            };

            Assert.IsNotNull(expectedResult); // test everything is OK that this stage
            Assert.AreEqual(actualValidationResult.Result, expectedValidationResult.Result); // test everything is OK that this stage

            Assert.IsNotNull(expectedResult.AccountNumber); // check if null
        }
       

        /// <summary>
        /// unit test for Update a customer.
        /// </summary>
        [Test]
        public void UpdateCustomerWithAddress()
        {
            // Create a new Customer, 
            Customer addNewCustomer = new Customer
            {
                AccountNumber = "777777777",
                FirstName = "ANAND",
                LastName = "S",
                MiddleName = "",
                Currency = "USD",
                Group = "STUDENTS",
                NameAlias = "KSA",
                NameSequence = "LastFirst"
            };


            //Address data
            Address objFirstAddress = new Address
            {
                //AddressMethodType = "Fax",
                City = "Columbus",
                CountryRegionID = "",
                IsPrimaryAddress = true,
                LocationName = "Fax",
                Locator = "HOME",
                Roles = "",
                State = "OH",
                Street = "11st street",
                Type = "EMAIL",
                TypeSpecified = true,
                ZipCode = "43240"
            };
            addNewCustomer.AddressList = new List<Address>();
            //Add the address in address list of customer object.
            addNewCustomer.AddressList.Add(objFirstAddress);

            // Assign the addnewwCustomer in expected,
            Customer expectedResult = addNewCustomer;

            //Customer actualResult = new  Customer();

            //Customer Repository 
            Mock<ICustomerRepository> mockCustomerRepository = new Mock<ICustomerRepository>();
            ValidationDetail actualValidationResult = new ValidationDetail
            {
                Result = true
            };


            //Setup the repository insert method
            mockCustomerRepository.Setup(method => method.UpdateCustomer(addNewCustomer)).Returns(actualValidationResult);

            //
            ICustomerRepository value = mockCustomerRepository.Object; // pass mock instance to interface

            actualValidationResult = value.UpdateCustomer(addNewCustomer); // call mock to get data that is in database

            ValidationDetail expectedValidationResult = new ValidationDetail
            {
                Result = true
            };

            Assert.IsNotNull(expectedResult); // test everything is OK that this stage
            Assert.AreEqual(actualValidationResult.Result, expectedValidationResult.Result); // test everything is OK that this stage

            Assert.IsNotNull(expectedResult.AccountNumber); // check if null
        }

        /// <summary>
        /// unit test for Update a customer.
        /// </summary>
        [Test]
        public void GetCustomers()
        {
            string accountNumber = "777777777";

              
            //Customer Repository 
            Mock<ICustomerRepository> mockCustomerRepository = new Mock<ICustomerRepository>();
            //ValidationDetail actualValidationResult = new ValidationDetail
            //{
            //    Result = true
            //};
            string expectedXmlData = "";

            //Setup the repository insert method
            mockCustomerRepository.Setup(method => method.GetCustomer(accountNumber)).Returns(expectedXmlData);

            ////
             ICustomerRepository value = mockCustomerRepository.Object; // pass mock instance to interface

             string actualResult = value.GetCustomer(accountNumber); // call mock to get data that is in database

             Assert.AreEqual(actualResult, expectedXmlData); // test everything is OK that this stage
        }    
        ///unit test for create a successful customer.
        [Test]
        public void disableCustomerTest()
        {
            // Create a new Customer, 
            string accountnumber = "777777777";

            //Customer Repository 
            Mock<ICustomerRepository> mockCustomerRepository = new Mock<ICustomerRepository>();
            ValidationDetail actualValidationResult = new ValidationDetail
            {
                Result = true
            };
            ValidationDetail expectedValidationResult = new ValidationDetail
            {
                Result = true
            };

            //Setup the repository insert method
            mockCustomerRepository.Setup(method => method.DisableCustomer(accountnumber)).Returns(actualValidationResult);

            ICustomerRepository value = mockCustomerRepository.Object; // pass mock instance to interface

            actualValidationResult = value.DisableCustomer(accountnumber); // call mock to get data that is in database
            Assert.IsNotNull(expectedValidationResult); // test everything is OK that this stage
            Assert.AreEqual(actualValidationResult.Result, expectedValidationResult.Result); // test everything is OK that this stage
        }

    }
}
