﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 04/07/2015
'Description: To cancell invoices
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using System.Collections.Generic;
using WrapperService.BusinessEntities.Interfaces;

namespace WrapperService.BusinessEntities
{
    public class InvoiceCancellation : IInvoiceCancellation
    {
        /// <summary>
        /// Billable Item ID
        /// </summary>
        public string BillableItemID { get; set; }

        /// <summary>
        /// Cancellation Date
        /// </summary>
        public DateTime CancellationDate { get; set; }

        /// <summary>
        /// Cancellation Id
        /// </summary>
        public string CancellationID { get; set; }

        /// <summary>
        /// Reason for cancellation
        /// </summary>
        public string Reason { get; set; }

    }
}