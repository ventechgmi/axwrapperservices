﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/19/2014
'Description: Ledger Journal Class
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System.Collections.Generic;
using WrapperService.BusinessEntities.Interfaces;

namespace WrapperService.BusinessEntities
{
    public class LedgerJournal : ILedgerJournal
    {
        /// <summary>
        /// Journal Name from COnfig file
        /// </summary>
        public string JournalName { get; set; }

        /// <summary>
        /// Description 
        /// </summary>
        public string Description { get; set; }

        //public DefaultDimension DefaultDimension { get; set; } //Not needed
        /// <summary>
        /// Ledger jounal transaction list
        /// </summary>
        public List<LedgerJournalTransaction> LedgerJournalTransactionList { get; set; }
    }
}
