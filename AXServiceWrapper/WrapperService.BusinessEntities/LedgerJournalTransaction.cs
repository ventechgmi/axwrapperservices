﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/19/2014
'Description: Ledger Journal Transaction Class
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System;
using WrapperService.BusinessEntities.Interfaces;

namespace WrapperService.BusinessEntities
{
    public class LedgerJournalTransaction : ILedgerJournalTransaction
    {
        /// <summary>
        /// Transaction date
        /// </summary>
        public DateTime? TransactionDate { get; set; }

        // public CustPaymJournalWCF.AxdEnum_LedgerJournalACType? AccountType { get; set; }
        /// <summary>
        /// Account Type 
        /// </summary>
        public string AccountType { get; set; }
        /// <summary>
        /// Ledger dimension
        /// </summary>
        public LedgerDimension LedgerDimension { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Debit 
        /// </summary>
        public decimal? Debit { get; set; }

        /// <summary>
        /// Credit
        /// </summary>
        public decimal? Credit { get; set; }

        //  public CustPaymJournalWCF.AxdEnum_LedgerJournalACType? OffsetAccountType { get; set; }
        /// <summary>
        /// Offset Account Type
        /// </summary>
        public string OffsetAccountType { get; set; }

        /// <summary>
        /// Offset Ledger Dimension
        /// </summary>
        public LedgerDimension OffsetLedgerDimension { get; set; }

        /// <summary>
        /// Invoice Id
        /// </summary>
        public string InvoiceId { get; set; }

        //public DateTime? DueDate { get; set; } //Not needed
        // public List<DefaultDimension> DefaultDimension { get; set; } //Not needed
        // public List<DefaultDimension> OffsetDefaultDimension { get; set; } //Not needed

        /// <summary>
        /// Company Name
        /// </summary>
        public string Company { get; set; } //SFM

        /// <summary>
        /// Offset Company Name
        /// </summary>
        public string OffsetCompany { get; set; } //SMF

        /// <summary>
        /// Payment Mode
        /// </summary>
        public string PaymentMode { get; set; } //CreditCard

        /// <summary>
        /// Payment Id
        /// </summary>
        public string PaymentId { get; set; }

        /// <summary>
        /// Payment Notes
        /// </summary>
        public string PaymentNotes { get; set; } //Random note

        //Added ARCode in ledger creation - Jan-22-2015 - starts here 
        /// <summary>
        /// ARCode
        /// </summary>
        public string ARCode { get; set; }
        /// <summary>
        /// Paymen tConfirmation Number
        /// </summary>
        public string PaymentConfirmationNumber { get; set; }

    }
}
