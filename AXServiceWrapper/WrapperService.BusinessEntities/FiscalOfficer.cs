﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 09/15/2015
'Description: Fiscal officer class
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using WrapperService.BusinessEntities.Interfaces;

namespace WrapperService.BusinessEntities
{
    public class FiscalOfficer : IFiscalOfficer
    {
        /// <summary>
        /// Fiscal Officer Account ID
        /// </summary>
        public string AccountID { get; set; }
        /// <summary>
        /// Fiscal officer First Name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Fiscal officer Last name
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Fiscal officer Middle name
        /// </summary>
        public string MiddleName { get; set; }
        /// <summary>
        /// Fiscal officer  name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Fiscal officer Suffix
        /// </summary>
        public string Suffix { get; set; }
        
        /// <summary>
        /// Type is either Secretary or Fiscal
        /// </summary>
        public string Type { get; set; }
    }
}
