﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/03/2014
'Description: 
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using WrapperService.BusinessEntities.Interfaces;

namespace WrapperService.BusinessEntities
{
    public class Customer : ICustomer
    {
        /// <summary>
        /// Handles running unique account number
        /// </summary>
        public string AccountNumber { get; set; }
        /// <summary>
        /// Customer first name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Customer middle Name
        /// </summary>
        public string MiddleName { get; set; }
        /// Customer Last Name
        public string LastName { get; set; }
        /// <summary>
        /// Customer Name Sequence which means to show the first name last name combination  like  - FirstLast, LastFirst - Its a configurable item in the web.config
        /// </summary>
        public string NameSequence { get; set; }
        /// <summary>
        /// Customer Alais Name
        /// </summary>
        public string NameAlias { get; set; }

        /// <summary>
        /// Curreny Type 
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Customer Group
        /// </summary>
        public string Group { get; set; }

        public List<Address> AddressList { get; set; }

        /// <summary>
        /// Contact info view for a customer - added Dec-12-2014
        /// </summary>
        public List<Contact> ContactList { get; set; }

        //Added for enable or disable the customer - Jan-23-2015
        /// <summary>
        /// IsDisable
        /// </summary>
        public bool IsDisable { get; set; }


        //Added By Ananthakumar - Feb-23-2015 Reason: Customertype , Suffix - starts here
        /// <summary>
        /// Customertype is like Person , Organization etc
        /// </summary>
        public string CustomerType { get; set; }
        /// <summary>
        /// Suffix
        /// </summary>
        public string Suffix { get; set; }
        /// <summary>
        /// OrganizationName
        /// </summary>
        public string OrganizationName { get; set; }

        //Added By Ananthakumar - Feb-23-2015 Reason: Customertype , Suffix - ends here       


        /// <summary>
        /// AS 400 ID
        /// </summary>
        public string AS400ID { get; set; }

        /// for VFFD  changes - Starts here
        /// is participating 
        public bool IsParticipating { get; set; }
        /// <summary>
        /// Property valuation
        /// </summary>
        public decimal? PropertyValuation { get; set; }
        
        /// <summary>
        /// Fiscal Officer 
        /// </summary>
        public FiscalOfficer FiscalOfficer { get; set; }

        /// <summary>
        /// Secretary 
        /// </summary>
        public Secretary Secretary { get; set; }
         
  
        /// <summary>
        /// Fund Billing Address 
        /// </summary>
        public Address FundBillingAddress { get; set; }
        /// for VFFD  changes - ends here


        /// <summary>
        /// VendorID
        /// </summary>
        public string VendorID { get; set; }

    }
}
