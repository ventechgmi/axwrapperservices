﻿/******************************************************************************************
'Created By: ANANTHAKUMAR S
'Created Date: 01/06/2014
'Description:
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using WrapperService.BusinessEntities.Interfaces;
namespace WrapperService.BusinessEntities
{
    public class InvoiceDocument : IInvoiceDocument
    {
        /// <summary>
        /// Invoice ID
        /// </summary>
        public string InvoiceID { get; set; }
        /// <summary>
        /// FileName
        /// </summary>
        public string FileName { get; set; }
        ///// <summary>
        ///// File Extension
        ///// </summary>
        //public string FileExtension { get; set; }

        /// <summary>
        /// Server Path
        /// </summary>
        public string FileServerPath { get; set; }
        ///// <summary>
        ///// Size of the file
        ///// </summary>
        //public Int32 FileSize { get; set; }

        ///// <summary>
        ///// File Contents Base 64
        ///// </summary>
        //public string FileContentsBase64 { get; set; }
        /// <summary>
        /// Total Bytes
        /// </summary>
        public byte[] FileContentsByte { get; set; }

        public bool Status { get; set; }
        public string ErrorMessage { get; set; }
    }
}
