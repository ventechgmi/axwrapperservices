﻿/******************************************************************************************
'Created By: ANANTHAKUMAR S
'Created Date: 12/03/2014
'Description:
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WrapperService.BusinessEntities.Interfaces;

namespace WrapperService.BusinessEntities
{
    /// <summary>
    /// Validationdetail object is used to return final output.
    /// ID will have an identity value after creation
    /// Result will have either true or false for the action
    /// ErrorList collection returns list of errors 
    /// ErrorType will return the type of error
    /// </summary>
    public class ValidationDetail : IValidationDetail
    {
        /// <summary>
        /// ID will have an identity value after creation
        /// </summary>
        public Int32 Id { get; set; }
        /// <summary>
        /// Result will have either true or false for the action
        /// </summary>
        public bool Result { get; set; }
        /// <summary>
        /// Returns list of errors 
        /// </summary>
        public List<string> ErrorList { get; set; }
        //This would contain business validations
        public Int32 ErrorType { get; set; }
        public string Number { get; set; }
    }
}