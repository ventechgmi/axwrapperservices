﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 12/31/2014
'Description: Invoice related properties
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;

namespace WrapperService.BusinessEntities
{
    public class CustomerInvoiceList
    {
        /// <summary>
        /// Customer Reference
        /// </summary>
        public string AccountNumber { get; set; }
       
        /// <summary>
        /// Invoice  list
        /// </summary>
        public List<InvoiceDetails> InvoiceDetailsList { get; set; }

    }
}
