﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 12/17/2014
'Description: Invoice related properties
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using WrapperService.BusinessEntities.Interfaces;

namespace WrapperService.BusinessEntities
{
    public class DefaultDimension : IDefaultDimension
    {
        /// <summary>
        /// Dimension name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// value
        /// </summary>
        public string Value { get; set; }
    }
}
