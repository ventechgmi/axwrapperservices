﻿/******************************************************************************************
'Created By: ANANTHAKUMAR S
'Created Date: 12/03/2014
'Description:
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WrapperService.BusinessEntities.Interfaces;

namespace WrapperService.BusinessEntities
{
    /// <summary>
    /// XMLData object is used to return final output.
    /// Result will have either true or false for the action
    /// ErrorList collection returns list of errors 
    /// XMLString will return the XML data
    /// </summary>
    public class XMLData  
    {
        /// <summary>
        /// Result will have either true or false for the action
        /// </summary>
        public bool Result { get; set; }
        /// <summary>
        /// Returns list of errors 
        /// </summary>
        public List<string> ErrorList { get; set; }

        public string XMLString{ get; set; }
    }
}