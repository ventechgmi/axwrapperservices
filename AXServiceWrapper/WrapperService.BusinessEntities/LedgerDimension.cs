﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/19/2014
'Description: Ledger Dimension Class
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using WrapperService.BusinessEntities.Interfaces;

namespace WrapperService.BusinessEntities
{
    public class LedgerDimension : ILedgerDimension
    {
        /// <summary>
        /// Account Name
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        public string DisplayValue { get; set; }

        public List<DefaultDimension> DefaultDimensionList { get; set; }
    }
}
