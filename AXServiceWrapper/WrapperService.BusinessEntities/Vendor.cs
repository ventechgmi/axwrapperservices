﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/03/2014
'Description: 
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using WrapperService.BusinessEntities.Interfaces;

namespace WrapperService.BusinessEntities
{
    public class Vendor : IVendor
    {
        /// <summary>
        /// Handles running unique account number
        /// </summary>
        public string AccountNumber { get; set; }
        /// <summary>
        /// Vendor first name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Vendor middle Name
        /// </summary>
        public string MiddleName { get; set; }
        /// Vendor Last Name
        public string LastName { get; set; }

        /// <summary>
        /// Curreny Type 
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Vendor Group
        /// </summary>
        public string Group { get; set; }

        public List<Address> AddressList { get; set; }

        /// <summary>
        /// Contact info view for a Vendor - added Dec-12-2014
        /// </summary>
        public List<Contact> ContactList { get; set; }
    }
}
