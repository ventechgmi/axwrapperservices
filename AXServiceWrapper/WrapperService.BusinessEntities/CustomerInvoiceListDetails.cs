﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 12/31/2014
'Description: Invoice related properties
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;

namespace WrapperService.BusinessEntities
{
    public class CustomerInvoiceListDetails 
    {
        /// <summary>
        /// BillableItemId
        /// </summary>
        public string BillableItemId { get; set; }

        public string InvoiceAccount { get; set; }
        public string InvoiceAmount { get; set; }
        public string InvoiceId { get; set; }
        public string InvoiceDate { get; set; }
        public string DueDate { get; set; }
        /// <summary>
        /// Paymnet Status
        /// </summary>
        public string PaymentStatus { get; set; }
        public string InvoiceStatus { get; set; }

        public bool Status { get; set; }
        public string ErrorMessage { get; set; }

    }
}
