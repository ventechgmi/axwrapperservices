﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/12/2014
'Description: contact Class for Customer
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using WrapperService.BusinessEntities.Interfaces;

namespace WrapperService.BusinessEntities
{
    public class Contact : IContact
    {
        /// <summary>
        /// It is an ACADIS id
        /// </summary>
        public string ACADISID { get; set; }

        /// <summary>
        /// This is the location name in DIRParty contact view
        /// </summary>
        public string ContactName { get; set; }

        /// <summary>
        /// Customer delivery location - Home,Delivery -value will be like Home  or  Home:Delivery
        /// </summary>
        public string Roles { get; set; }

        /// <summary>
        /// If it is 0 or empty ,the type is Email
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Type Specified
        /// </summary>
        public bool TypeSpecified { get; set; }

        /// <summary>
        /// Phone Number or email or mail etc.
        /// </summary>
        public string Locator { get; set; }
        /// <summary>
        /// Is Primary Address
        /// </summary>
        public bool IsPrimary { get; set; }

        public string Extension { get; set; } //Added By Ananthakumar - Feb-23-2015 Reason: Added for phone Extension

    }
}
