﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 12/17/2014
'Description: Invoice related properties
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using System.Collections.Generic;
using WrapperService.BusinessEntities.Interfaces;

namespace WrapperService.BusinessEntities
{
    public class InvoiceDetails : IInvoiceDetails
    {
        public string InvoiceAccount { get; set; }
        public string InvoiceAmount { get; set; }
        public string RemainingAmount { get; set; }
        public string InvoiceId { get; set; }
        public string InvoiceDate { get; set; }
        public string DueDate { get; set; }
        /// <summary>
        /// Paymnet Status
        /// </summary>
        public string PaymentStatus { get; set; }
        public string InvoiceStatus { get; set; }


        public bool Status { get; set; }

        public string ErrorMessage { get; set; }

        //public string CustGroup { get; set; }
        
        //public string RefNum { get; set; }
        
        //public string SalesId { get; set; }
        
        //public string OrderAccount { get; set; }
        
        //public string InvoiceAccount { get; set; }
        
        //public string InvoiceDate { get; set; }
        
        //public string DueDate { get; set; }
        
        //public string CashDisc { get; set; }
        
        //public string CashDiscDate { get; set; }
        
        //public string Qty { get; set; }
        
        //public string Volume { get; set; }
        
        //public string Weight { get; set; }
        
        //public string SumLineDisc { get; set; }
        
        //public string SalesBalance { get; set; }
        
        //public string EndDisc { get; set; }
        
        //public string InvoiceAmount { get; set; }
        
        //public string CurrencyCode { get; set; }
        
        //public string ExchRate { get; set; }
        
        //public string InvoiceId { get; set; }
        
        //public string LedgerVoucher { get; set; }
        
        //public string Updated { get; set; }
        
        //public string OnAccountAmount { get; set; }
        
        //public string TaxPrintOnInvoice { get; set; }
        
        //public string Listcode { get; set; }
        
        //public string DocumentNum { get; set; }
        
        //public string DocumentDate { get; set; }
        
        //public string CashDiscPercent { get; set; }
        
        //public string IntrastatDispatch { get; set; }
        
        //public string DeliveryName { get; set; }
        
        //public string EnterpriseNumber { get; set; }
        
        //public string PurchaseOrder { get; set; }
        
        //public string DlvTerm { get; set; }
        
        //public string DlvMode { get; set; }
        
        //public string Payment { get; set; }
        
        //public string CashDiscCode { get; set; }
        
        //public string InvoiceRoundOff { get; set; }
        
        //public string SumMarkup { get; set; }
        
        //public string CovStatus { get; set; }
        
        //public string ReturnItemNum { get; set; }
        
        //public string PostingProfile { get; set; }
        
        //public string Backorder { get; set; }
        
        //public string Prepayment { get; set; }
        
        //public string TaxGroup { get; set; }
        
        //public string TaxItemGroup { get; set; }
        
        //public string TaxSpecifyByLine { get; set; }
        
        //public string EInvoiceLineSpecific { get; set; }
        
        //public string OneTimeCustomer { get; set; }
        
        //public string PaymentSched { get; set; }
        
        //public string SumTax { get; set; }
        
        //public string SalesType { get; set; }
        
        //public string EInvoiceAccountCode { get; set; }
        
        //public string InterCompanyPosted { get; set; }
        
        //public string ParmId { get; set; }
        
        //public string ReturnReasonCodeId { get; set; }
        
        //public string EUSalesList { get; set; }
        
        //public string ExchRateSecondary { get; set; }
        
        //public string Triangulation { get; set; }
        
        //public string CustomerRef { get; set; }
        
        //public string VATNum { get; set; }
        
        //public string numberSequenceGroup { get; set; }
        
        //public string InclTax { get; set; }
        
        //public string Log { get; set; }
        
        //public string PaymDayId { get; set; }
        
        //public string InvoicingName { get; set; }
        
        //public string GiroType { get; set; }
        
        //public string ContactPersonId { get; set; }
        
        //public string SalesOriginId { get; set; }
        
        //public string BillofLadingId { get; set; }
        
        //public string inventLocationId { get; set; }
        
        //public string FixedDueDate { get; set; }
        
        //public string InvoiceAmountMST { get; set; }
        
        //public string InvoiceRoundOffMST { get; set; }
        
        //public string SumMarkupMST { get; set; }
        
        //public string SumLineDiscMST { get; set; }
        
        //public string EndDiscMST { get; set; }
        
        //public string SalesBalanceMST { get; set; }
        
        //public string SUmTaxMST { get; set; }
        
        //public string PrintMgmtSiteId { get; set; }
        
        //public string ReturnStatus { get; set; }
        
        //public string InterCompanyCompanyId { get; set; }
        
        //public string InterCompanyPurchId { get; set; }
        
        //public string PrintedOriginals { get; set; }
        
        //public string Proforma { get; set; }
        
        //public string RCSalesList_UK { get; set; }
        
        //public string ReverseCharge_UK { get; set; }
        
        //public string DeliveryPostalAddress { get; set; }
        
        //public string InvoicePostalAddress { get; set; }
        
        //public string SourceDocumentHeader { get; set; }
        
        //public string DefaultDimension { get; set; }
        
        //public string BankLCExportLine { get; set; }
        
        //public string WorkerSalesTaker { get; set; }
        
        //public string ReversedRecId { get; set; }
        
        //public string ReceiptDateConfirmed_ES { get; set; }
        
        //public string PaymId { get; set; }
        
        //public string TaxInvoiceSalesId { get; set; }
        
        //public string IntrastatFulfillmentDate_HU { get; set; }
        
        //public string CashDiscBaseDate { get; set; }
        
        //public string IsCorrection { get; set; }
        
        //public string ReasonTableRef { get; set; }
        
        //public string SourceDocumentLine { get; set; }
        
        //public string TransportationDocument { get; set; }
        
        //public string DirectDebitmandate { get; set; }
        
        //public string modifiedDateTime { get; set; }
        
        //public string createdDateTime { get; set; }
        
        //public string dEL_createdTime { get; set; }
        
        //public string createdBy { get; set; }
        
        //public string dataAreaId { get; set; }
        
        //public string recVersion { get; set; }
        
        //public string Partition { get; set; }
        
        //public string RecId { get; set; } 
    }
}
