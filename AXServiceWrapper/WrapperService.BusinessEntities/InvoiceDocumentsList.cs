﻿/******************************************************************************************
'Created By: ANANTHAKUMAR S
'Created Date: 01/06/2014
'Description:
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;

namespace WrapperService.BusinessEntities
{
    public class InvoiceDocumentsList
    {
        /// <summary>
        /// Customer ID
        /// </summary>
        public string CustomerID { get; set; }
      
        /// <summary>
        /// Invoice Line list
        /// </summary>
        public List<InvoiceDocument> InvoiceDocsList { get; set; }

    

    }
}
