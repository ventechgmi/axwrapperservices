﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 12/03/2014
'Description: Address Class
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using WrapperService.BusinessEntities.Interfaces;

namespace WrapperService.BusinessEntities
{
    public class Address : IAddress
    {

        /// <summary>
        /// It is an ACADIS id
        /// </summary>
        public string ACADISID { get; set; }

        /// <summary>
        /// Location Name - Email
        /// </summary>
        public string LocationName { get; set; }

        /// <summary>
        /// Customer Street Name
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// Customer City Name
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Customer State
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Country Region ID
        /// </summary>
        public string CountryRegionID { get; set; }

        /// <summary>
        /// Country Zipcode
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// Customer delivery location - Home,Delivery -value will be like Home  or  Home:Delivery
        /// </summary>
        public string Roles { get; set; }
    /// <summary>
        /// Address Type Method - Email/Phone/Mail
        /// </summary>
        //public string AddressMethodType { get; set; }

        /// <summary>
        /// If it is 0 or empty ,the type is Email
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Type Specified
        /// </summary>
        public bool TypeSpecified { get; set; }

        /// <summary>
        /// Phone Number or email or mail etc.
        /// </summary>
        public string Locator { get; set; }
    
        /// <summary>
        /// Is Primary Address
        /// </summary>
      public  bool IsPrimaryAddress { get; set; }

      //Added By Ananthakumar - Feb-23-2015 Reason: Line2 - starts here
        /// <summary>
      /// Customer Street Name2 (Line2)
      /// </summary>
      public string Street2 { get; set; }
        //Added By Ananthakumar - Feb-23-2015 Reason: Line2 - ends here
     

    }
}
