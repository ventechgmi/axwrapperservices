﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 12/17/2014
'Description: Invoice related properties
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using System.Collections.Generic;
using WrapperService.BusinessEntities.Interfaces;

namespace WrapperService.BusinessEntities
{
    public class Invoice : IInvoice
    {
        /// <summary>
        /// Customer Reference
        /// </summary>
        public string CustomerReference { get; set; }
        /// <summary>
        /// Invoice date
        /// </summary>
        public DateTime? InvoiceDate { get; set; }
        /// <summary>
        /// Order Account
        /// </summary>
        public string OrderAccount { get; set; }
        /// <summary>
        /// Invoice Account
        /// </summary>
        public string InvoiceAccount { get; set; }
        /// <summary>
        /// Invoice Id
        /// </summary>
        public string InvoiceId { get; set; }

        /// <summary>
        /// Currency code
        /// </summary>
        public string CurrencyCode { get; set; }
      
        /// <summary>
        /// Purchase order form number
        /// </summary>
        public string PurchaseOrderFormNumber { get; set; }
        /// <summary>
        /// Letter Date
        /// </summary>
        public DateTime? LetterDate { get; set; }
        /// <summary>
        /// Invoice Line list
        /// </summary>
        public List<InvoiceLine> InvoiceLineList { get; set; }
        /// <summary>
        /// Invoice default dimension list
        /// </summary>
        public List<DefaultDimension> InvoiceDefaultDimensionList;

        /// <summary>
        /// Added AutoPost
        /// </summary>
        public bool IsAutoPost { get; set; }
    }
}
