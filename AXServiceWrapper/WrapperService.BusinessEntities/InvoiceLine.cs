﻿/******************************************************************************************
'Created By: ANANTHAKUMAR SUBRAMANI
'Created Date: 12/17/2014
'Description: Invoice related properties
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.Collections.Generic;
using WrapperService.BusinessEntities.Interfaces;

namespace WrapperService.BusinessEntities
{
    public class InvoiceLine : IInvoiceLine
    {
        /// <summary>
        /// invoice line description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Invoice amount
        /// </summary>
        public decimal? Amount { get; set; }

        /// <summary>
        /// Invoice main account
        /// </summary>
        public string MainAccount { get; set; }
        /// <summary>
        /// Invoice ARCode
        /// </summary>
        public string ARCode { get; set; }

        /// <summary>
        /// Transaction
        /// </summary>
        public int? ARTrans { get; set; }

        /// <summary>
        /// INvoice comments
        /// </summary>
        public string InvoiceComments { get; set; }
        /// <summary>
        /// Invoice default dimension list
        /// </summary>
        public List<DefaultDimension> InvoiceDefaultDimensionList;

        // Adding two fields for unit price,quantity - Starts here

        /// <summary>
        /// Total Amount
        /// </summary>
        public decimal? UnitPrice { get; set; }

        /// <summary>
        /// Quantity
        /// </summary>
        public int Quantity { get; set; }
        // Adding two fields for unit price,quantity - ends here


        //Added billable id for cancellation
        /// <summary>
        /// billable id for cancellation
        /// </summary>
        public string VEN_BillableItemId { get; set; }

        
    }
}
