﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AXFundsService
{

    [Serializable()]
    [System.Xml.Serialization.XmlRoot("FundCollection")]
    public class FundCollection
    {
        public string EmailSubject { get; set; }

        public string EmailMessage { get; set; }

        [XmlArray("FundDetails")]
        [XmlArrayItem("FundDetail", typeof(FundDetail))]
        public FundDetail[] FundDetail { get; set; }
    }

    [Serializable()]
    public class FundDetail
    {
        [System.Xml.Serialization.XmlElement("Name")]
        public string Name { get; set; }

        [System.Xml.Serialization.XmlElement("AccountID")]
        public string AccountID { get; set; }

        [System.Xml.Serialization.XmlElement("Limit")]
        public decimal Limit { get; set; }

        [System.Xml.Serialization.XmlElement("Email")]
        public string Email { get; set; }

        [System.Xml.Serialization.XmlElement("Timing")]
        public int Timing { get; set; }

        public string EmailSubject { get; set; }

        public string EmailMessage { get; set; }

    }


   
}
