﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 07/23/2015
'Description: Service creates a daily log file, Send email when fund balance reaches less then the limit
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using System.Xml;
using EncryptandDecrypt;
using log4net;
using log4net.Config;
using AXFundsService.FundServiceReference;
using System.Net.Mail;
using System.Xml.Serialization;
using System.IO;
using AXFundsService;


namespace AXWrapperWindowsService
{
    public partial class AXFundsService : ServiceBase
    {

        #region "Constants"

        #endregion "Constants"

        #region "variables"

        /// <summary>
        /// service user id   to access invoice service
        /// </summary>
        private string serviceUserid;

        /// <summary>
        /// Service password to access invoice service
        /// </summary>
        private string servicePassword;

        /// <summary>
        /// To prevent multiple execution of timer events
        /// </summary>
        private volatile bool isTimerExecuting;

        //Uses to tracking the actions 
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        //Timer for poll the data periodically
        private System.Timers.Timer timer;


        //This statement is used to set interval to 1 minute (= 60,000 milliseconds) - 1 sec = 1000
        private double interval = 1000;

        /// <summary>
        /// mail server 
        /// </summary>
        
        string mailServer;
       /// <summary>
       /// Smtp user id
       /// </summary>
        string SMTPUID;

        /// <summary>
        /// smtp password
        /// </summary>
        string SMTPPWD;

        /// <summary>
        /// from address
        /// </summary>
        string FromAddress;

        /// <summary>
        /// mail server port
        /// </summary>
        string port;

        /// <summary>
        /// enable ssl for mail server
        /// </summary>
        string EnableSSL;

        string Email;
        #endregion "variables"

        #region "Service Events"
        /// <summary>
        /// Windows service on start set log4 net configuration, timer
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            log.Info("Enter OnStart() Windows Service Started");
             ////Debugger.Launch();;


            //This statement is used to set interval to 1 minute (= 60,000 milliseconds) - 1 sec = 1000
            //interval = Convert.ToDouble(ConfigurationManager.AppSettings["ServiceInterval"]);

            //user id and password from config file and decrypting 
            serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
            servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

            mailServer = System.Configuration.ConfigurationManager.AppSettings["SMTP.Server"];
            SMTPUID = System.Configuration.ConfigurationManager.AppSettings["SMTP.UID"];
            SMTPPWD = System.Configuration.ConfigurationManager.AppSettings["SMTP.Password"];
            FromAddress = System.Configuration.ConfigurationManager.AppSettings["SMTP.FromAddress"];
            port = System.Configuration.ConfigurationManager.AppSettings["SMTP.Port"];
            EnableSSL = System.Configuration.ConfigurationManager.AppSettings["SMTP.EnableSSL"];


             //List of funds account from app config
            //Email = ConfigurationManager.AppSettings["AdminEmail"];


            ////if timer is running stop when stop the service
            //if (timer != null)
            //    timer.Stop();

            XmlConfigurator.Configure();

            //SetTimer();

            GetFunds();

            // KeepAlive to prevent garbage collection from occurring before the method ends. 
            GC.KeepAlive(timer);

            log.Info("Exit OnStart()");
        }
        private void GetFunds()
        {
            try
            {
                FundCollection FundCollections = null;
                
                 string path = Convert.ToString(ConfigurationManager.AppSettings["FundConfigFile"]);

                XmlSerializer serializer = new XmlSerializer(typeof(FundCollection));

                StreamReader reader = new StreamReader(path);
                FundCollections = (FundCollection)serializer.Deserialize(reader);
                reader.Close();

                log.Debug("GetFunds() after read the xml");

               List<System.Timers.Timer> _timers = new List<System.Timers.Timer>();

               foreach (FundDetail fund in FundCollections.FundDetail)
               {
                   fund.EmailSubject = FundCollections.EmailSubject;
                   fund.EmailMessage = FundCollections.EmailMessage;
                 
                   log.Debug("GetFunds() Before read the xml");

                   //convert timing - minutes into milli seconds
                   long interval = (long)TimeSpan.FromMinutes(fund.Timing).TotalMilliseconds;

                    var tempTimer = new System.Timers.Timer();
                    tempTimer.AutoReset = true;

                   //Assign interval
                    tempTimer.Interval = interval;
                    
                   //set event 
                    tempTimer.Elapsed += (sender, args) => TimerTick(fund);
                    tempTimer.Start();

               }
            }
            catch (Exception ex)
            {
                log.Error("ERROR GetFunds(): " + ex.Message);
            }
            log.Info("Exit GetFunds()");
            // TimerTick(new FundConfig() { Name = "fund 1", Email = "sananthakumar@ventechsolutions.com", Query = "select * from CUSTAGINGLEGALENTITY", Limit = 20000 });
        }

        #region "Email"
        /// <summary>
        /// send email
        /// </summary>
        /// <param name="emails"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        private void SendEmail(string emails, string subject, string body)
        {
            try
            {
                log.Debug("Enter: SendMail()");

                //  string ToAddress = System.Configuration.ConfigurationManager.AppSettings["AdminEmail"];

                if (!string.IsNullOrEmpty(emails))
                {
                    #region "Mail body"
                    StringBuilder mailbody = new StringBuilder();
                    mailbody.Append(@"<html><meta http-equiv=""Content-Type"" content=""text/html; charset=us-ascii""><center><table style=""border:5px solid #cccccc; width:790px"" border=""0"" cellpadding=""0"" cellspacing=""0"">");
                    mailbody.Append(@"<tbody><tr><td colspan=""2"" style=""padding:0px;background:#cbc3a7;"" align=""left""></td></tr><tr><td colspan=""2"" style=""text-align:left; padding:0pt 0px;""><table border=""0"" cellpadding=""0"" cellspacing=""0"" height=""400px"" width=""790px""><tr>");
                    mailbody.Append(@"<td style=""text-align:justify; padding-left:20px; font-family:Calibri;""><span><br />");
                    mailbody.Append(body);
                    //mailbody.Append(@"</span></td></tr></table></td></tr><tr><td style=""padding-left:20px; ""><hr style=""margin:0 ;  width:760px; height:6px; color:#ccc""></td></tr><tr><td colspan=""2"" style=""padding:0px;""><table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%""><tbody><tr><td style=""padding-left:20px; font-family:verdana; font-size:9px;""><span>We respect your privacy and we do not share your personal information.</span></td></tr></tbody></table></td></tr></tbody></table></center></html>");
                    mailbody.Append(@"</span></td></tr></table></td></tr></table></html>");
                    #endregion "Mail body"

                    #region "Mail server details"


                    MailMessage mail = new MailMessage();

                    mail.From = new MailAddress(FromAddress);
                    mail.IsBodyHtml = true;

                    mail.Priority = MailPriority.High;

                    List<string> emaillist = emails.Split(';').ToList();

                    foreach (var emailid in emaillist)
                    {
                        if (!string.IsNullOrEmpty(emailid))
                            mail.To.Add(emailid);
                    }

                    mail.Subject = subject;

                    mail.Body = mailbody.ToString();

                    SmtpClient SmtpServer = new SmtpClient(mailServer);
                    if (!string.IsNullOrEmpty(SMTPPWD))
                        SmtpServer.Credentials = new System.Net.NetworkCredential(SMTPUID, SMTPPWD);

                    if (!string.IsNullOrEmpty(port))
                        SmtpServer.Port = Convert.ToInt32(port);

                    if (!string.IsNullOrEmpty(EnableSSL))
                        SmtpServer.EnableSsl = Convert.ToBoolean(EnableSSL); ;

                    log.Debug("Enter: SendEmail " + subject);
                    SmtpServer.Send(mail);
                    log.Info(string.Format("Mail Sent successfully: subject: {0} ; email address: {1} ", subject, emails));
                    log.Debug(string.Format("Mail Sent successfully: subject: {0} email address: {1} ", subject, emails));

                    #endregion "Mail server details"
                }
            }
            catch (Exception ex)
            {
                log.Error(string.Format("ERROR SendMail(): email address: {0} ; subject: {1} Error :{2}  ", emails, subject, ex.Message));
            }
            log.Debug("Exit: SendMail()");
        }

        #endregion "Email"

        /// <summary>
        /// Service stop event
        /// </summary>
        protected override void OnStop()
        {
            //if timer is running stop when stop the service
            if (timer != null)
                timer.Stop();

            log.Info("Windows Service Stopped");
        }
        #endregion "Service Events"

        #region "Timer Events"
        ///// <summary>
        ///// Set the timer to schedule specific interval
        ///// </summary>
        //private void SetTimer()
        //{
        //    if (timer == null)
        //    {
        //        timer = new System.Timers.Timer();
        //        timer.AutoReset = true;
        //        timer.Interval = interval;
        //        //set event 
        //        timer.Elapsed += new System.Timers.ElapsedEventHandler(TimerElapsedEvent);
        //        timer.Start();
        //    }
        //}
        ///// <summary>
        ///// Timer elapsed event , this will call and execute the process
        ///// </summary>
        ///// <param name="source"></param>
        ///// <param name="e"></param>
        //private void TimerElapsedEvent(object source, System.Timers.ElapsedEventArgs e)
        //{

        //    log.Info("Enter TimerElapsedEvent()");

        //    if (isTimerExecuting)
        //        return;

        //    isTimerExecuting = true;
        //    VEN_FundBalanceServiceClient client = new VEN_FundBalanceServiceClient();

        //    try
        //    {
        //        //// implemented credentials starts
        //        client.ClientCredentials.UserName.UserName = serviceUserid;
        //        client.ClientCredentials.UserName.Password = servicePassword;
        //        client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;

        //        //List of funds account from app config
        //        List<string> AccountIdList = ConfigurationManager.AppSettings["FundAccountIDs"].ToString().Split(';').ToList();
        //        string Limit = ConfigurationManager.AppSettings["FundAccountLimit"];
        //        string Email = ConfigurationManager.AppSettings["AdminEmail"];

        //        //StringBuilder sbBody = new StringBuilder();
        //        //sbBody.Append("Fund Name: ");
        //        //sbBody.Append(fund.Name);
        //        //sbBody.AppendLine("Email : ");
        //        //sbBody.Append(fund.Email);

        //        //sbBody.AppendLine(" Threshold Limit : ");
        //        //sbBody.Append(fund.Limit);

        //        foreach (var AccountId in AccountIdList)
        //        {
        //            // cmd.CommandType = CommandType.Text;
        //            decimal balance = client.GetFundBalance(null, AccountId);

        //            string sBody = "Fund balance reached less than the limit.";

        //            if (balance < Convert.ToDecimal(Limit))
        //            {
        //                //todo send email
        //                log.Debug("TimerTick(): balance:" + balance);
        //                SendEmail(Email, "Fund Threshold", sBody);
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error TimerElapsedEvent() " + ex.Message);
        //    }
        //    finally
        //    {
        //        isTimerExecuting = false;
        //        log.Info("Exit  TimerElapsedEvent()");
        //        client.Close();
        //    }
        //}

/// <summary>
/// timer tick event
/// </summary>
/// <param name="StateObj"></param>
        //public void TimerTick(object StateObj,string subject,string body)
        public void TimerTick(object StateObj)
        {
            log.Debug("Enter TimerTick()");
            FundDetail fund = (FundDetail)StateObj;
            ////Debugger.Launch();;
            string subject = fund.EmailSubject;
            string body = fund.EmailMessage;
           
            body = body.Replace("#Name#", fund.Name).Replace("#Limit#", string.Format("{0:C}",fund.Limit));
             
            VEN_FundBalanceServiceClient client = new VEN_FundBalanceServiceClient();

            try
            {
                //// implemented credentials starts
                client.ClientCredentials.UserName.UserName = serviceUserid;
                client.ClientCredentials.UserName.Password = servicePassword;
                client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;
                 
                log.Debug("TimerTick() Before call AX service GetFundBalance()");
                decimal balance = client.GetFundBalance(null, fund.AccountID);
                log.Debug("TimerTick() after call AX service GetFundBalance()");

                body = body.Replace("#CurrentLimit#", string.Format("{0:C}", balance));

                   // string sBody = "Fund balance reached less than the limit.";

                    if (balance < fund.Limit)
                    {
                        //todo send email
                       log.Debug("TimerTick(): balance:" + balance);
                       SendEmail(fund.Email,subject , body);
                      log.Debug("TimerTick(): balance:" + balance);
                    }

                    log.Debug("Exit TimerTick():");
            }
            catch (Exception ex)
            {
                log.Debug(string.Format("Error TimerTick() Error Data: {0} ; Error: {1}", body, ex.Message));
                log.Error(string.Format("Error TimerTick() Error Data: {0} ; Error: {1}", body, ex.Message));
            }
            finally
            {
                isTimerExecuting = false;
                log.Info("Exit  TimerTick()");
                client.Close();
            }
        }
        #endregion "Timer Events"

    }
}