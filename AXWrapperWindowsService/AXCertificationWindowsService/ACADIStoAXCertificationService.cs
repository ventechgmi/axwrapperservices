﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 02/12/2015
'Description: Service creates a daily log file, Poll billableParties from ACADIS service and create/update Vendors in AX
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.ServiceProcess;
using System.Text;
using System.Xml;
using AcadisDataServices.Client;
using AcadisDataServices.Client.Accounting;
using AcadisDataServices.Client.Certification;
using AXWrapperWindowsService.VendorServiceReference;
using log4net;
using log4net.Config;
//using AXWrapperWindowsService.CustomerServiceReference;
using AXWrapperWindowsService.LedgerJournalService;
using System.Data;
using AXWrapperWindowsService.QueryService;
using System.Data.OleDb;
using System.IO;
using AXWrapperWindowsService.CustomerServiceReference;
using AXWrapperWindowsService.VendorPaymentServiceReference;
namespace AXWrapperWindowsService
{
    public partial class ACADIStoAXVendorService : ServiceBase
    {

        #region "Constants"

          //type is Certifications 
        const string type = "BillableParties";

        #endregion "Constants"

        #region "variables"
        //Page size default to 1000
        int PAGE_SIZE = 1000;
        long invoiceNumber = 0;
        /// <summary>
        /// service user id   to access invoice service
        /// </summary>
        private string serviceUserid;

        /// <summary>
        /// Service password to access invoice service
        /// </summary>
        private string servicePassword;

        /// <summary>
        /// To prevent multiple execution of timer events
        /// </summary>
        private volatile bool isTimerExecuting;

        //Uses to tracking the actions 
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        //Timer for poll the data periodically
        private System.Timers.Timer timer;

        //This statement is used to set interval to 1 minute (= 60,000 milliseconds) - 1 sec = 1000
        private double interval = 1000;

        //Payment offset account
        string OffSetAccount = string.Empty;

        string VFFDInvoiceJournalName = string.Empty;
        #endregion "variables"

        #region "Service Events"
        /// <summary>
        /// Windows service on start set log4 net configuration, timer
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {

            //user id and password from config file and decrypting 
            serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
            servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

            //retrieve the page size from config
            PAGE_SIZE = Convert.ToInt32(Convert.ToString(ConfigurationManager.AppSettings["PAGE_SIZE"]));

            //This statement is used to set interval to 1 minute (= 60,000 milliseconds) - 1 sec = 1000
            interval = Convert.ToDouble(ConfigurationManager.AppSettings["ServiceInterval"]);

            OffSetAccount = Convert.ToString(ConfigurationManager.AppSettings["VFFDOffsetDimensionDependent"]);

            VFFDInvoiceJournalName = Convert.ToString(ConfigurationManager.AppSettings["VFFDInvoiceJournalName"]);

            //if timer is running stop when stop the service
            if (timer != null)
                timer.Stop();
            XmlConfigurator.Configure();
           // Debugger.Launch();
            Log.Debug("Enter OnStart() Windows Service Started on " + DateTime.Now.ToString());

            //Sert timer
            SetTimer();

            // KeepAlive to prevent garbage collection from occurring before the method ends. 
            GC.KeepAlive(timer);
        }

        /// <summary>
        /// Service stop event
        /// </summary>
        protected override void OnStop()
        {

            //if timer is running stop when stop the service
            if (timer != null)
                timer.Stop();

            Log.Debug("Windows Service Stopped on " + DateTime.Now.ToString());
        }
        #endregion "Service Events"

        #region "Timer Events"
        /// <summary>
        /// Set the timer to schedule specific interval
        /// </summary>
        private void SetTimer()
        {
            if (timer == null)
            {
                timer = new System.Timers.Timer();
                timer.AutoReset = true;
                interval = (long)TimeSpan.FromMinutes(interval).TotalMilliseconds;
                // interval = 100;
                timer.Interval = interval;
                //set event 
                timer.Elapsed += new System.Timers.ElapsedEventHandler(TimerElapsedEvent);
                timer.Start();
            }
        }
        /// <summary>
        /// Timer elapsed event , this will call and execute the process
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void TimerElapsedEvent(object source, System.Timers.ElapsedEventArgs e)
        {

            Log.Debug("Enter TimerElapsedEvent()");

            if (isTimerExecuting)
                return;

            isTimerExecuting = true;

            try
            {
                //user id and password from config file and decrypting 
                bool isTesting = Convert.ToBoolean(ConfigurationManager.AppSettings["IsExcelTesting"]);
                if (isTesting)
                {
                    System.Data.DataTable dt = GenerateTestData();
                    List<CertificationSearchResult> FinalList = new List<CertificationSearchResult>();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        var result = new CertificationSearchResult();
                        result.BillableAccountID = Convert.ToInt32(dt.Rows[i]["BillableAccountID"]);
                        result.CertificationName = Convert.ToString(dt.Rows[i]["CertificationName"]);
                        result.CertificationStatus = Convert.ToString(dt.Rows[i]["CertificationStatus"]);
                        result.IsActiveStatus = Convert.ToBoolean(dt.Rows[i]["IsActiveStatus"]);
                        result.OtherNumberID = Convert.ToString(dt.Rows[i]["OtherNumberID"]);
                        if (!(string.IsNullOrEmpty(Convert.ToString(dt.Rows[i]["ExpirationDate"]))))
                            result.ExpirationDate = Convert.ToDateTime(dt.Rows[i]["ExpirationDate"]);
                        FinalList.Add(result);
                    }

                    ProcessCertifications(FinalList);
                    Log.Error("ProcessCertifications(FinalList) Done");
                }
                else
                    MigrateCertifications();
            }

            catch (Exception ex)
            {
                Log.Error("Error TimerElapsedEvent() " + ex.Message);
            }
            finally
            {
                isTimerExecuting = false;
                Log.Debug("Exit  TimerElapsedEvent()");
            }
        }

        #endregion "Timer Events"

        #region "XML file Get and Update"
        /// <returns></returns>

        /// <summary>
        /// Read Last updated value from xml file for each type "Certifications , BillingItems, paid invoices ets...use this..
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<string> GetCertifications()
        {
            Log.Debug("Enter GetCertifications():  ");
            string path = Convert.ToString(ConfigurationManager.AppSettings["ListofCertifications"]);
            List<string> list = new List<string>();
            try
            {
                //File path

                if (!string.IsNullOrEmpty(path))
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(path);
                    Log.Debug("Enter GetCertifications(): XMLpath= " + path);

                    //var nodes = xmlDoc.SelectNodes("/MostRecentUpdateInstant/BillableParties");
                    var nodes = xmlDoc.SelectNodes("/Certifications/Name");
                    Log.Debug("Exit GetCertifications():  ");
                    // Selecting node using XPath syntax
                    XmlNodeList idNodes = xmlDoc.SelectNodes("Certifications/Name");
                    // Filling the list
                    foreach (XmlNode node in idNodes)
                        list.Add(node.InnerText);
                }
                else
                {
                    Log.Error("ERROR: GetCertifications(): " + path + " File Not found ");
                    this.Stop();
                    return list;
                }
            }
            catch (Exception ex)
            {
                Log.Error("ERROR: GetCertifications(): File" + path + " File Not found " + ex.Message);
                this.Stop();
                return list;
            }
            return list;
        }
        #endregion "XML file Get and Update"

        #region "Certifications"

        /// <summary>
        /// Poll Billabale parties from ACADIS and call AX Wrapper service to SAVE Vendor
        /// </summary>
        private void MigrateCertifications()
        {

            try
            {
                Log.Debug("Enter MigrateCertifications():  " + DateTime.Now.ToString());
                #region""BillableParties"

                //TimeSpan BILLABLE_PARTIES_POLLING_INTERVAL = TimeSpan.FromSeconds(10);

                // setup ACADIS Configuration
                var config = new AcadisDataServicesClientConfiguration(ConfigurationManager.AppSettings);
                var client = new AccountingClient(config);

                var certificationClient = new CertificationSearchClient(config);

                List<string> list = GetCertifications();

                List<CertificationSearchResult> FinalList = new List<CertificationSearchResult>();
                foreach (string item in list)
                {
                    try
                    {

                        var criteria = new CertificationSearchRequest
                        {
                            CertificationName = item.Contains("VFFD Deceased Fire") ? "VFFD Deceased Firefighter" : item,
                            ResultsPerPage = PAGE_SIZE,
                            IncludeActive = true,
                            IncludeInactive = false,
                        };

                        Log.Debug("Enter MigrateCertifications(): before calling Search");
                        var searchResults = certificationClient.Search(criteria);
                        FinalList.AddRange(searchResults.Results);

                        do
                        {
                            //ProcessCertifications(searchResults);
                            Log.Debug("Enter MigrateCertifications(): after calling Search");
                            searchResults = certificationClient.GetNextPage(criteria, searchResults);
                        } while (searchResults != null);

                    }
                    catch (Exception Exception)
                    {
                        Log.Error(string.Format("Error MigrateCertification() : {0} Name: {1}", item.ToString(), Exception.Message));
                    }
                }


                Log.Debug("Exit MigrateCertifications()");

                #endregion""BillableParties"

                ProcessCertifications(FinalList);

            }
            catch (Exception ex)
            {
                Log.Error("Error MigrateCertification() " + ex.Message);
            }
            Log.Info("Stopping the Service... ");
            this.Stop();
        }

        /// <summary>
        /// Process Certifications and call AX wrapper serive to create/update Vendor
        /// </summary>
        /// <param name="billableParties"></param>
        /// <param name="lastUpdateId"></param>
        //  private void ProcessCertifications(CertificationSearchResponse results)
        private void ProcessCertifications(List<CertificationSearchResult> FinalList)
        {

            Log.Debug("Enter ProcessCertifications()");

            //Group Certifications by OtherNumberId (Claim NUmber)
            //For each group
            //All but dependent get vendor creation (may create vendor for each) -- Data from customer AX record
            //If vendor not exists then mark to add intital payout to list (item "inital payout") 
            //Create payment journal with (guardian/survivor/all but dependent) as receiptent id
            //Add line item in journal for each item in group with prices looked up from AX

            var claimNumbers = FinalList.Select(s => s.OtherNumberID).Distinct();
            bool createIntitalPremium;
            bool survivorClaim = false;

            foreach (string claim in claimNumbers)
            {
                Log.Info("Claim:" + claim);
                var claims = FinalList.Where(r => r.OtherNumberID == claim).ToList();
                // var survivorGuardian = claims.Where(r => r.CertificationName.ToUpper().Contains("SURVIVOR") || r.CertificationName.ToUpper().Contains("GUARDIAN")).FirstOrDefault();
                var survivorGuardian = claims.FirstOrDefault(r => r.CertificationName.ToUpper().Contains("SURVIVOR") || r.CertificationName.ToUpper().Contains("GUARDIAN"));
                var paymentAccount = survivorGuardian != null ? survivorGuardian.BillableAccountID : claims.First().BillableAccountID;

                //var vendorID = SetupVendor(paymentAccount, out createIntitalPremium);

                //  AddPayments(claims, vendorID, createIntitalPremium);
                //  var name = claims.FirstOrDefault(r => r.CertificationName.ToUpper());
                string OOffsetAccount = Convert.ToString(ConfigurationManager.AppSettings["VFFDOffsetDimensionDependent"]);

                var survivor = claims.FirstOrDefault(r => r.CertificationName.ToUpper().Contains("SURVIVOR"));
                survivorClaim = (survivor != null);
                var guard = claims.FirstOrDefault(r => r.CertificationName.ToUpper().Contains("GUARDIAN"));
                var disablilty = claims.FirstOrDefault(r => r.CertificationName.ToUpper().Contains("DISABILITY"));
                var firefighter = claims.FirstOrDefault(r => r.CertificationName.ToUpper().Contains("SURVIVOR"));
                var dependent = claims.FirstOrDefault(r => r.CertificationName.ToUpper().Contains("SURVIVOR"));

                if (disablilty != null)
                    OOffsetAccount = Convert.ToString(ConfigurationManager.AppSettings["VFFDOffsetDimensionDisblity"]);
                else if (survivor != null)
                    OOffsetAccount = Convert.ToString(ConfigurationManager.AppSettings["VFFDOffsetDimensionSurvivor"]);
                else if (guard != null)
                    OOffsetAccount = Convert.ToString(ConfigurationManager.AppSettings["VFFDOffsetDimensionGuardian"]);
                else if (firefighter != null)
                    OOffsetAccount = Convert.ToString(ConfigurationManager.AppSettings["VFFDOffsetDimensionFirefighter"]);

                //Log.Info("survivorGuardian:" + survivorGuardian);
                //Log.Info("paymentAccount:" + paymentAccount);
                //Log.Info("survivor:" + survivor);
                //Log.Info("guard:" + guard);
                //Log.Info("disablilty:" + disablilty);
                //Log.Info("dependent:" + dependent);
                //Log.Info("OOffsetAccount:" + OOffsetAccount);

                var vendorID = SetupVendor(paymentAccount, survivorClaim, out createIntitalPremium);
                AddInvoiceJournal(claims, vendorID, OOffsetAccount, VFFDInvoiceJournalName, createIntitalPremium); ;

            }
        }
        private void AddInvoiceJournal(List<CertificationSearchResult> claims, string vendorID, string OOffsetAccount, string VFFDInvoiceJournalName, bool createInitialPremium)
        {


            StringBuilder sbErrorData = new StringBuilder();

            // Set up Ledger Journal Client

            VEN_VendorPaymentsListServiceClient client = new VEN_VendorPaymentsListServiceClient();
            var sbErrorFormat = new StringBuilder();
            try
            {
                invoiceNumber = GetMostRecentValue();

                #region "Create"

                //// implemented credentials starts

                serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

                if (client.ClientCredentials != null)
                {
                    client.ClientCredentials.UserName.UserName = serviceUserid;
                    client.ClientCredentials.UserName.Password = servicePassword;
                    client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;
                }

            

                //  VEN_VendorInvoiceContract[] obj1 = new VEN_VendorInvoiceContract[3];
                List<VEN_VendorInvoiceContract> VEN_VendorInvoiceContractList = new List<VEN_VendorInvoiceContract>();

                Log.Info("Query Beneficiary");
                DataSet dsBeneficiary = QueryGetBeneficiary();
                System.Data.DataTable dtBeneficiary = dsBeneficiary.Tables[0];
                Log.Info("Beneficiary Table Loaded");

                    #region "For loop for payments"
                //looping invoices for making ledger transaction list
                foreach (CertificationSearchResult eachCertifications in claims)
                {
                    if (eachCertifications == null)
                    {
                        Log.Info("Certification  error:empty: INV:" + invoiceNumber);
                        foreach (var vin in VEN_VendorInvoiceContractList)
                        {
                            Log.Info("VIN:" + vin.acc + "-" + vin.amount + "_" + vin.JournalName + "_" + vin.offsetDimension + "_" + vin.invoice + "_" + sbErrorFormat + "_" + createInitialPremium);
                        }
                        continue;
                    }
                    #region "Error Data"

                    eachCertifications.CertificationName =
                        eachCertifications.CertificationName.Contains("VFFD Deceased Fire")
                            ? "VFFD Deceased Fire fighter"
                            : eachCertifications.CertificationName;
                           
                    sbErrorFormat.Append("BillableAccountID: " + Convert.ToString(eachCertifications.BillableAccountID) + " VENDORID: " + vendorID);
                    sbErrorFormat.Append("; CertificationName: " + eachCertifications.CertificationName);
                    sbErrorFormat.Append("; CertificationStatus: " + eachCertifications.CertificationStatus);
                    sbErrorFormat.Append("; ExpirationDate: " + eachCertifications.ExpirationDate);
                    sbErrorFormat.Append("; IsActiveStatus: " + eachCertifications.IsActiveStatus.ToString());
                    sbErrorFormat.Append("; OtherNumberID: " + Convert.ToString(eachCertifications.OtherNumberID));
                    sbErrorFormat.Append("; InvoiceNUmber: " + Convert.ToString(invoiceNumber));
                    Log.Info(sbErrorFormat.ToString());
                    #endregion "Error Data"


                    VEN_VendorInvoiceContract VEN_VendorInvoiceContractTemp = new VEN_VendorInvoiceContract();

                    var amount = (from brow in dtBeneficiary.AsEnumerable() where brow.Field<string>("Beneficiary") == eachCertifications.CertificationName select brow["Amount"].ToString()).First();

                    VEN_VendorInvoiceContractTemp.acc = vendorID;
                    VEN_VendorInvoiceContractTemp.description = eachCertifications.CertificationName;
                    // VEN_VendorInvoiceContract.description = "Father";
                    VEN_VendorInvoiceContractTemp.JournalName = VFFDInvoiceJournalName;
                    VEN_VendorInvoiceContractTemp.offsetDimension = OOffsetAccount;
                    VEN_VendorInvoiceContractTemp.amount = Convert.ToDecimal(amount);
                    //   VEN_VendorInvoiceContractTemp.invoice = "002";
                    //  VEN_VendorInvoiceContractTemp.invoice = eachCertifications.BillableAccountID.ToString();
                    VEN_VendorInvoiceContractTemp.invoice = (invoiceNumber).ToString();
                    invoiceNumber = invoiceNumber + 1;
                    sbErrorFormat.Append("; OtherNumberID: " + Convert.ToString(eachCertifications.OtherNumberID));

                    if (VEN_VendorInvoiceContractTemp.amount > 0)
                    {
                        VEN_VendorInvoiceContractList.Add(VEN_VendorInvoiceContractTemp);

                    }
                    else
                    {
                        sbErrorFormat.Append("; Amount is zero this record will not be in the list");

                    }

                }

                //create intital premium
                if (createInitialPremium)
                {

                    VEN_VendorInvoiceContract VEN_VendorInvoiceContractTemp = new VEN_VendorInvoiceContract();

                    var amount = (from brow in dtBeneficiary.AsEnumerable() where brow.Field<string>("Beneficiary") == "Initial Premium Amount" select brow["Amount"].ToString()).First();

                    VEN_VendorInvoiceContractTemp.acc = vendorID;
                    VEN_VendorInvoiceContractTemp.description = "Initial Premium Amount";
                    // VEN_VendorInvoiceContract.description = "Father";
                    VEN_VendorInvoiceContractTemp.JournalName = VFFDInvoiceJournalName;
                    VEN_VendorInvoiceContractTemp.offsetDimension = OOffsetAccount;
                    VEN_VendorInvoiceContractTemp.amount = Convert.ToDecimal(amount);
                    //   VEN_VendorInvoiceContractTemp.invoice = "002";
                    //  VEN_VendorInvoiceContractTemp.invoice = eachCertifications.BillableAccountID.ToString();
                    VEN_VendorInvoiceContractTemp.invoice = (invoiceNumber).ToString();

                    sbErrorFormat.Append("; OtherNumberID: " + "Initial Premium Amount");

                    if (VEN_VendorInvoiceContractTemp.amount > 0)
                    {
                        VEN_VendorInvoiceContractList.Add(VEN_VendorInvoiceContractTemp);

                    }
                    else
                    {
                        sbErrorFormat.Append("; Amount is zero this record will not be in the list");

                    }


                }

                #endregion "For loop for payments"
                //assign journal list
                foreach (var vin in VEN_VendorInvoiceContractList)
                {
                    Log.Debug("VIN:" + vin.acc + "-" + vin.amount + "_" + vin.JournalName + "_" + vin.offsetDimension + "_" + vin.invoice + "_" + sbErrorFormat);
                }
                
                System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender1, certificate, chain, sslPolicyErrors) => true); 


                string result = client.CreateInvoiceJournals(null, VEN_VendorInvoiceContractList);
 
                Log.Info(string.Format("Certification payment created Successfully by windows service for Payments(PaymentsID) {0} and Result: {1}", vendorID,result));
              
                #endregion

                UpdateMostRecentValue(invoiceNumber);
            }
            catch (System.ServiceProcess.TimeoutException exception)
            {
                Log.Error(string.Format("Error from AX: CreateInvoice(): {0} , Error Data:{1} ", exception.Message, sbErrorFormat.ToString()));
                Log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }
            catch (CommunicationException exception)
            {
                Log.Error(string.Format("Error from AX: CreateInvoice(): {0} , Error Data:{1} ", exception.Message, sbErrorFormat.ToString()));
                Log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }
            catch (Exception ex)
            {
                Log.Error("Error:CreatewNewPayment() : " + ex.Message + " ; payment created failed by windows service for Payments(PaymentsID)=" + vendorID);
                Log.Error(string.Format("Error from AX: CreateInvoice(): {0} , Error Data:{1} ", ex.Message, sbErrorFormat.ToString()));
                Log.Error("ERROR DATA: " + sbErrorData.ToString());
                Log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }
            finally
            {
                Log.Info("Finally AddInvoice Journal: finalizing... ");
            }

        }

        private string SetupVendor(int paymentAccount, bool survivorClaim, out bool createIntitalPremium)
        {
            string vendorID = "";
            string queryVendorId = paymentAccount.ToString();
            createIntitalPremium = false;
            List<string> paymentAccountList = new List<string>();

            try
            {

                DataSet dataSet;

                QueryService.Paging paging = null;

                QueryService.QueryServiceClient client = new QueryService.QueryServiceClient();
 
                serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

                if (client.ClientCredentials != null)
                {

                    client.ClientCredentials.UserName.UserName = serviceUserid;
                    client.ClientCredentials.UserName.Password = servicePassword;
                    client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;
    }


                dataSet = client.ExecuteStaticQuery("VendorListBasic", ref paging);
                DataTable dtVendors = dataSet.Tables[0];
                var results = from vRow in dtVendors.AsEnumerable()
                              where vRow.Field<string>("AccountNum") == queryVendorId
                              select vRow;
                if (!results.Any())
                {

                    Customer Customer = QueryGetCustomer(paymentAccount.ToString());
                    if (!string.IsNullOrEmpty(Customer.AccountNumber))
                    {

                        //Fill out vendor with data
                        //   Create a new Vendor, 
                        DateTime ststart = DateTime.Now;

                        var addNewVendor = new Vendor
                        {

                            FirstName = Customer.FirstName,
                            MiddleName = Customer.MiddleName,
                            LastName = Customer.LastName,
                            AccountNumber = paymentAccount.ToString(),
                            Group = "AP",
                            Currency = "USD"
                        };

                        AddVendor(addNewVendor);
                        vendorID = paymentAccount.ToString();

                        //rules for inital premium - if we create the vendor they are new and get intial premium
                        createIntitalPremium = survivorClaim;

                        Customer.VendorID = paymentAccount.ToString();

                    }
                    else
                    {
                        // Log.Error(string.Format("Error SetupVendor(): VendorCREATE Error ({0})  Error: No Data/ data table", paymentAccount));
                        Log.Error(string.Format("Error SetupVendor(): VendorCREATE Error ({0})  Error: Account {0} is not in AX ", paymentAccount));
                        Log.Info(string.Format("Error SetupVendor(): VendorCREATE Error ({0})  Error: Account {0} is not in AX ", paymentAccount));
                        Log.Error("-------------------------------------------------------------------------------------------");

                    }
                }
                vendorID = paymentAccount.ToString();
            }
            catch (Exception ex) //create vendor
            {
                Log.Error(string.Format("Error SetupVendor(): VendorCREATE Error ({0})  Error: {1}  ", paymentAccount, ex.Message));
                Log.Error("-------------------------------------------------------------------------------------------");
            }
            return vendorID;
        }

        private string AddVendor(Vendor addNewVendor)
        {
            string vendorID = addNewVendor.AccountNumber;
            StringBuilder sbErrorData = new StringBuilder();
            Log.Debug("Enter ProcessCertifications() : Added Vendor");

            addNewVendor.AddressList = new List<VendorServiceReference.Address>();
            ////Add the address in address list of Vendor object.

            List<Vendor> VendorList = new List<Vendor>();
            VendorList.Add(addNewVendor);

            #region " Call AX wrapper - to create Vendor"
            // Set up Vendor client
            VendorClient vclient = new VendorClient();
            try
            {

                vclient.ClientCredentials.UserName.UserName = serviceUserid;
                vclient.ClientCredentials.UserName.Password = servicePassword;
                vclient.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;

                //Call AX wrapper service to save Vendor information
                SaveVendorRequest saveVendorRequest = new SaveVendorRequest { Vendor = VendorList };
                //Create Vendor factory object
                foreach (var v in VendorList)
                {
                    Log.Info(v.AccountNumber + " " + v.FirstName + " " + v.LastName + "-" + v.Group + "-" + v.Currency);

                }

                Log.Info("Enter ProcessCertifications(): Before calling client.SaveVendor(saveVendorRequest)");
                System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender1, certificate, chain, sslPolicyErrors) => true); 

                //Call save Vendor
                SaveVendorResponse saveVendorResponse = vclient.SaveVendor(saveVendorRequest);

                Log.Info("Enter ProcessCertifications(): After calling client.SaveVendor(saveVendorRequest)");

                if (saveVendorResponse.ValidationDetail.Result)
                {
                    //Process validation results
                }
                else
                {
                    Log.Error(string.Format("Error SAVEVendor(): BillableAccountID ={0} ERROR: {1} {2} ", addNewVendor.AccountNumber, saveVendorResponse.ValidationDetail.ErrorList[0].ToString()));
                }

            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Error ProcessCertifications(): BillableAccountID ({0})  Error: {1}  ", addNewVendor.AccountNumber, ex.Message));
                Log.Error("-------------------------------------------------------------------------------------------");
            }
            #endregion " Call AX wrparrer - to create Vendor"



            Log.Info("Exit ProcessCertifications()");

            return vendorID;


        #endregion "Certifications"

        }

        private void AddPayments(List<CertificationSearchResult> claims, string vendorID, bool createInitialPremium)
        {


            StringBuilder sbErrorData = new StringBuilder();

            // Set up Ledger Journal Client
            LedgerJournalClient client = new LedgerJournalClient();
            try
            {
                #region "Create"

                //// implemented credentials starts

                if (client.ClientCredentials != null)
                {
                    client.ClientCredentials.UserName.UserName = serviceUserid;
                    client.ClientCredentials.UserName.Password = servicePassword;
                    client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;
                }

                #region  "Payemnt line items"

                List<LedgerJournal> journalTableList = new List<LedgerJournal>();
                List<LedgerJournalTransaction> journalLineList = new List<LedgerJournalTransaction>();


                //get the claim from AX
                //add item to PayJournal
                journalLineList.AddRange(GetLedgerJournalTransactionData(claims, vendorID, createInitialPremium, ref  sbErrorData));

                journalTableList.Add(new LedgerJournal()
                {
                    JournalName = string.Empty, // journal Name will be set in the service itself from configuartion
                    LedgerJournalTransactionList = journalLineList
                });
                #endregion  "Payemnt line items"


                //assign journal list
                CreatePaymentRequest createPaymentRequest = new CreatePaymentRequest
                {
                    LedgerJournalList = journalTableList
                };

    System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender1, certificate, chain, sslPolicyErrors) => true); 


                //Create Payment - call service
                CreatePaymentResponse createPaymentResponse = new CreatePaymentResponse
                {
                    ValidationDetail = client.CreatePayment(createPaymentRequest.LedgerJournalList)
                };

                //Check success 
                if (createPaymentResponse.ValidationDetail.Result)
                {
                    Log.Info("Certification payment created Successfully by windows service for Payments(PaymentsID)=" + vendorID);
                }
                else // error in wrapper
                {
                    var errorMessage = createPaymentResponse.ValidationDetail.ErrorList[0].ToString();
                    Log.Info(errorMessage);
                    Log.Error("Error:CreatewNewPayment() : " + errorMessage + " ; payment created failed by windows service for Payments(PaymentsID)=" + vendorID);
                    Log.Error("ERROR DATA: " + sbErrorData.ToString());
                }
                #endregion
            }
            catch (System.ServiceProcess.TimeoutException exception)
            {
                Log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                Log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }
            catch (CommunicationException exception)
            {
                Log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                Log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }
            catch (Exception ex)
            {
                Log.Error("Error:CreatewNewPayment() : " + ex.Message + " ; payment created failed by windows service for Payments(PaymentsID)=" + vendorID);
                Log.Error("ERROR DATA: " + sbErrorData.ToString());
                Log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }
            finally
            {
                Log.Info("Enter CreatewNewPayment(): after loop lastUpdate=");
            }

        }

        public List<LedgerJournalTransaction> GetLedgerJournalTransactionData(List<CertificationSearchResult> claims, string vendorID, bool createIntialPremium, ref  StringBuilder errorData)
        {
            #region "constants"
            const string offsetAccountType = "Ledger";
            const string AccountType = "Pay";
            #endregion "constants"

            List<LedgerJournalTransaction> ledgerJournalTransactionList = new List<LedgerJournalTransaction>();

            DataSet dsBeneficiary = QueryGetBeneficiary();
            System.Data.DataTable dtBeneficiary = dsBeneficiary.Tables[0];

            //getting payment type from the payments object
            //var paymentType = null;// Payments.PaymentType.Value.ToString();

            //getting submiotted date from the payments object
            string submittedDate = DateTime.Now.ToString();

            #region "For loop for payments"
            //looping invoices for making ledger transaction list
            foreach (CertificationSearchResult eachCertifications in claims)
            {
                #region "Error Data"

                var sbErrorFormat = new StringBuilder();
                sbErrorFormat.Append("BillableAccountID: " + Convert.ToString(eachCertifications.BillableAccountID) + " VENDORID: " + vendorID);
                sbErrorFormat.Append("; CertificationName: " + eachCertifications.CertificationName);
                sbErrorFormat.Append("; CertificationStatus: " + eachCertifications.CertificationStatus);
                sbErrorFormat.Append("; ExpirationDate: " + eachCertifications.ExpirationDate);
                sbErrorFormat.Append("; IsActiveStatus: " + eachCertifications.IsActiveStatus.ToString());
                sbErrorFormat.Append("; OtherNumberID: " + Convert.ToString(eachCertifications.OtherNumberID));

                #endregion "Error Data"

                LedgerJournalTransaction objLedgerJournalTransaction = new LedgerJournalTransaction();

                objLedgerJournalTransaction.Description = eachCertifications.CertificationName;

                objLedgerJournalTransaction.AccountType = AccountType;

                objLedgerJournalTransaction.PaymentId = vendorID;

                //objLedgerJournalTransaction.PaymentConfirmationNumber = Payments.ConfirmationNumber; //added payment confirmation #

                //objLedgerJournalTransaction.PaymentMode = paymentType;
                var amount = (from brow in dtBeneficiary.AsEnumerable() where brow.Field<string>("Beneficiary") == eachCertifications.CertificationName select brow["Amount"].ToString()).First();

                objLedgerJournalTransaction.Debit = Convert.ToDecimal(amount);

                objLedgerJournalTransaction.OffsetAccountType = offsetAccountType;

                objLedgerJournalTransaction.Credit = 0;

                //if (!string.IsNullOrEmpty(Amount))
                //    objLedgerJournalTransaction.Credit = Convert.ToDecimal(Amount);
                //else
                //    objLedgerJournalTransaction.Credit = 0;

                string billableAccountId = vendorID;

                objLedgerJournalTransaction.LedgerDimension = new LedgerDimension() { Account = vendorID, DisplayValue = vendorID };

                string _displayValue = OffSetAccount;

                ////since Ledger dimension default to AX - on  Gustavo-Apr-30

                objLedgerJournalTransaction.OffsetLedgerDimension = new LedgerDimension()
                {
                    Account = _displayValue, // "200102", //PaymentCustomerAccount,//"1359476",
                    DisplayValue = _displayValue
                    //DisplayValue = PaymentCustomerAccount,//"1359476"
                    //Values = offsetDefaultDimension 200102
                    //Account = "200102", //PaymentCustomerAccount,//"1359476",
                    //DisplayValue = "200102"
                };

                ledgerJournalTransactionList.Add(objLedgerJournalTransaction);
            }
            #endregion "For loop for payments"

            if (createIntialPremium)
            {
                LedgerJournalTransaction objLedgerJournalTransaction = new LedgerJournalTransaction();

                objLedgerJournalTransaction.Description = "INITIAL PREMIUM NAME";

                objLedgerJournalTransaction.AccountType = AccountType;

                objLedgerJournalTransaction.PaymentId = vendorID;

                //objLedgerJournalTransaction.PaymentConfirmationNumber = Payments.ConfirmationNumber; //added payment confirmation #

                //objLedgerJournalTransaction.PaymentMode = paymentType;
                var amount = (from brow in dtBeneficiary.AsEnumerable() where brow.Field<string>("Beneficiary") == "Intitial Premium" select brow["Amount"].ToString()).First();

                objLedgerJournalTransaction.Debit = Convert.ToDecimal(amount);

                objLedgerJournalTransaction.OffsetAccountType = offsetAccountType;

                objLedgerJournalTransaction.Credit = 0;

                //if (!string.IsNullOrEmpty(Amount))
                //    objLedgerJournalTransaction.Credit = Convert.ToDecimal(Amount);
                //else
                //    objLedgerJournalTransaction.Credit = 0;

                string billableAccountId = vendorID;

                objLedgerJournalTransaction.LedgerDimension = new LedgerDimension() { Account = vendorID, DisplayValue = vendorID };

                string _displayValue = OffSetAccount;

                ////since Ledger dimension default to AX - on  Gustavo-Apr-30

                objLedgerJournalTransaction.OffsetLedgerDimension = new LedgerDimension()
                {
                    Account = _displayValue, // "200102", //PaymentCustomerAccount,//"1359476",
                    DisplayValue = _displayValue
                    //DisplayValue = PaymentCustomerAccount,//"1359476"
                    //Values = offsetDefaultDimension 200102
                    //Account = "200102", //PaymentCustomerAccount,//"1359476",
                    //DisplayValue = "200102"
                };

                ledgerJournalTransactionList.Add(objLedgerJournalTransaction);
            }

            //}
            return ledgerJournalTransactionList;
        }
        private Customer QueryGetCustomer(string accountNumber)
        {

            CustomerClient client = new CustomerClient();
            try
            {
                List<string> accountnumberList = new List<string>();
                accountnumberList.Add(accountNumber);
                // assign the account numbers
                GetCustomerObjectRequest GetCustomerObjectRequest = new GetCustomerObjectRequest
                {
                    AccountNumbersList = accountnumberList
                };
                GetCustomerObjectResponse GetCustomerObjectResponse = new GetCustomerObjectResponse();


                serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

                if (client.ClientCredentials != null)
                {
                    client.ClientCredentials.UserName.UserName = serviceUserid;
                    client.ClientCredentials.UserName.Password = servicePassword;
                    client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;
                }
                System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender1, certificate, chain, sslPolicyErrors) => true); 

                GetCustomerObjectResponse = client.GetCustomerObject(GetCustomerObjectRequest);

                return GetCustomerObjectResponse.Customer;
                //  if (GetCustomerObjectResponse.Result)
            }

            catch (Exception ex)
            {

                Log.Error("Error:QueryGetCustomer() accountNumbers: " + accountNumber + " ERROR: " + ex.Message + accountNumber);
                Log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
                return new Customer();
            }
        }
        #region "Commented"
        //        private DataSet QueryGetCustomer(string accountNumbers)
        //        {
        //            DataSet dataSet;

        //            Paging paging = null;

        //            QueryMetadata query;

        //            QueryDataSourceMetadata customerDataSource;

        //            query = new QueryMetadata();

        //            // Set the properties of the query."grantFD

        //            query.QueryType = QueryType.Join;


        //            query.DataSources = new QueryDataSourceMetadata[1];


        //            // Set the properties of the Customers data source.


        //            customerDataSource = new QueryDataSourceMetadata();

        //            customerDataSource.Name = "Customers";
        //            customerDataSource.Enabled = true;
        //            customerDataSource.Table = "CustTable";

        //            //Add the data source to the query.
        //            query.DataSources[0] = customerDataSource;

        //            // Setting DynamicFieldList property to false so I can specify only a few fields
        //            customerDataSource.DynamicFieldList = true;

        //            //query.OrderByFields[0] = sort;

        //            QueryDataRangeMetadata rangeData = new QueryDataRangeMetadata();

        //            rangeData.Name = "AccountNum";
        //            rangeData.FieldName = "AccountNum";
        //            rangeData.TableName = "CustTable"; ;
        //            rangeData.Value = accountNumbers;

        //            rangeData.RangeStatus = RangeStatus.Open;
        //            rangeData.Enabled = true;

        //            customerDataSource.Ranges = new QueryDataRangeMetadata[1];
        //            customerDataSource.Ranges[0] = rangeData;


        //            // implemented credentials starts
        //            // implemented credentials starts
        //            string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
        //            string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

        //            QueryServiceClient client1 = new QueryServiceClient();
        //            if (client1.ChannelFactory.Credentials != null)
        //            {
        //                client1.ChannelFactory.Credentials.UserName.UserName = serviceUserid;
        //                client1.ChannelFactory.Credentials.UserName.Password = servicePassword;
        //                //    log.Info("GetCustomer() entered into credentials");
        //            }
        //            // implemented credentials ends

        //            // log.Debug("GetCustomer() Before Client call");
        //            dataSet = client1.ExecuteQuery(query, ref paging);

        //            client1.Close();
        //            return dataSet;

        ////            dataSet = null;
        ////            var client = new CustomerClient();

        ////            List<string> accountnumberList = new List<string>() ;
        ////            accountnumberList.Add(accountNumbers);

        ////            // assign the account numbers
        ////            GetCustomerRequest GetCustomerRequest = new GetCustomerRequest
        ////            {
        ////                AccountNumbersList = accountnumberList
        ////            };
        ////            GetCustomerResponse GetCustomerResponse = new GetCustomerResponse();
        ////            GetCustomerResponse = client.GetCustomer(GetCustomerRequest);

        ////            if (GetCustomerResponse.Result)
        ////            {

        //////                string xml = @"<?xml version=""1.0"" encoding=""utf-8""?>
        //////                           
        //////<ClientDefinedQuery>
        //////  <Customers>
        //////    <AccountNum>99</AccountNum>
        //////    <InvoiceAccount />
        //////    <CustGroup>STUDENTS</CustGroup>
        //////    <LineDisc />
        //////    <PaymTermId>NET30</PaymTermId>
        //////    <CashDisc />
        //////    <Currency>USD</Currency>
        //////    <InterCompanyAutoCreateOrders>0</InterCompanyAutoCreateOrders>
        //////    <SalesGroup />
        //////    <Blocked>0</Blocked>
        //////    <OneTimeCustomer>0</OneTimeCustomer>
        //////    <AccountStatement>0</AccountStatement>
        //////    <CreditMax>0</CreditMax>
        //////    <MandatoryCreditLimit>0</MandatoryCreditLimit>
        //////    <VendAccount />
        //////    <PriceGroup />
        //////    <MultiLineDisc />
        //////    <EndDisc />
        //////    <VATNum />
        //////    <InventLocation />
        //////    <DlvTerm />
        //////    <DlvMode />
        //////    <MarkupGroup />
        //////    <ClearingPeriod />
        //////    <FreightZone />
        //////    <CreditRating />
        //////    <TaxGroup />
        //////    <StatisticsGroup />
        //////    <PaymMode />
        //////    <CommissionGroup />
        //////    <BankAccount />
        //////    <PaymSched />
        //////    <ContactPersonId />
        //////    <InvoiceAddress>0</InvoiceAddress>
        //////    <OurAccountNum />
        //////    <SalesPoolId />
        //////    <InclTax>0</InclTax>
        //////    <CustItemGroupId />
        //////    <numberSequenceGroup />
        //////    <PaymDayId />
        //////    <LineOfBusinessId />
        //////    <DestinationCodeId />
        //////    <GiroType>0</GiroType>
        //////    <SuppItemGroupId />
        //////    <GiroTypeInterestNote>0</GiroTypeInterestNote>
        //////    <TaxLicenseNum />
        //////    <WebSalesOrderDisplay>0</WebSalesOrderDisplay>
        //////    <PaymSpec />
        //////    <BankCentralBankPurposeText />
        //////    <BankCentralBankPurposeCode />
        //////    <InterCompanyAllowIndirectCreation>0</InterCompanyAllowIndirectCreation>
        //////    <PackMaterialFeeLicenseNum />
        //////    <TaxBorderNumber_FI />
        //////    <EinvoiceEANNum />
        //////    <FiscalCode />
        //////    <DlvReason />
        //////    <ForecastDMPInclude>0</ForecastDMPInclude>
        //////    <GiroTypeCollectionletter>0</GiroTypeCollectionletter>
        //////    <SalesCalendarId />
        //////    <CustClassificationId>No</CustClassificationId>
        //////    <InterCompanyDirectDelivery>0</InterCompanyDirectDelivery>
        //////    <EnterpriseNumber />
        //////    <ShipCarrierAccount />
        //////    <GiroTypeProjInvoice>0</GiroTypeProjInvoice>
        //////    <InventSiteId />
        //////    <OrderEntryDeadlineGroupId />
        //////    <ShipCarrierId />
        //////    <ShipCarrierFuelSurcharge>0</ShipCarrierFuelSurcharge>
        //////    <ShipCarrierBlindShipment>0</ShipCarrierBlindShipment>
        //////    <ShipCarrierAccountCode />
        //////    <GiroTypeFreeTextInvoice>0</GiroTypeFreeTextInvoice>
        //////    <SyncEntityId>00000000-0000-0000-0000-000000000000</SyncEntityId>
        //////    <SyncVersion>0</SyncVersion>
        //////    <Memo />
        //////    <SalesDistrictId />
        //////    <SegmentId />
        //////    <SubsegmentId />
        //////    <RFIDItemTagging>0</RFIDItemTagging>
        //////    <RFIDCaseTagging>0</RFIDCaseTagging>
        //////    <RFIDPalletTagging>0</RFIDPalletTagging>
        //////    <CompanyChainId />
        //////    <CompanyIdSiret />
        //////    <Party>5637925343</Party>
        //////    <IdentificationNumber />
        //////    <PartyCountry />
        //////    <PartyState />
        //////    <OrgId />
        //////    <PaymIdType />
        //////    <FactoringAccount />
        //////    <DefaultDimension>0</DefaultDimension>
        //////    <CustExcludeCollectionFee>0</CustExcludeCollectionFee>
        //////    <CustExcludeInterestCharges>0</CustExcludeInterestCharges>
        //////    <CompanyNAFCode>0</CompanyNAFCode>
        //////    <BankCustPaymIdTable>0</BankCustPaymIdTable>
        //////    <GiroTypeAccountStatement>0</GiroTypeAccountStatement>
        //////    <MainContactWorker>0</MainContactWorker>
        //////    <CreditCardAddressVerification>0</CreditCardAddressVerification>
        //////    <CreditCardCVC>0</CreditCardCVC>
        //////    <CreditCardAddressVerificationVoid>0</CreditCardAddressVerificationVoid>
        //////    <CreditCardAddressVerificationLevel>0</CreditCardAddressVerificationLevel>
        //////    <CompanyType_MX>0</CompanyType_MX>
        //////    <Rfc_MX />
        //////    <Curp_MX />
        //////    <StateInscription_MX />
        //////    <ResidenceForeignCountryRegionId_IT />
        //////    <BirthCountyCode_IT />
        //////    <BirthDate_IT>1900-01-01T00:00:00</BirthDate_IT>
        //////    <BirthPlace_IT />
        //////    <EInvoice>0</EInvoice>
        //////    <CCMNum_BR />
        //////    <CNPJCPFNum_BR />
        //////    <PBACustGroupId />
        //////    <IENum_BR />
        //////    <SuframaNumber_BR />
        //////    <Suframa_BR>0</Suframa_BR>
        //////    <CustFinalUser_BR>0</CustFinalUser_BR>
        //////    <InterestCode_BR />
        //////    <FineCode_BR />
        //////    <SuframaPISCOFINS_BR>0</SuframaPISCOFINS_BR>
        //////    <TaxWithholdCalculate_TH>0</TaxWithholdCalculate_TH>
        //////    <TaxWithholdGroup_TH />
        //////    <ConsDay_JP>0</ConsDay_JP>
        //////    <NIT_BR />
        //////    <INSSCEI_BR />
        //////    <CNAE_BR />
        //////    <ICMSContributor_BR>0</ICMSContributor_BR>
        //////    <ServiceCodeOnDlvAddress_BR>0</ServiceCodeOnDlvAddress_BR>
        //////    <InventProfileType_RU>0</InventProfileType_RU>
        //////    <InventProfileId_RU />
        //////    <TaxWithholdCalculate_IN>0</TaxWithholdCalculate_IN>
        //////    <UnitedVATInvoice_LT>0</UnitedVATInvoice_LT>
        //////    <EnterpriseCode />
        //////    <CommercialRegisterSection />
        //////    <CommercialRegisterInsetNumber />
        //////    <CommercialRegister />
        //////    <RegNum_W />
        //////    <IsResident_LV>0</IsResident_LV>
        //////    <IntBank_LV />
        //////    <PaymentReference_EE />
        //////    <PackageDepositExcempt_PL>0</PackageDepositExcempt_PL>
        //////    <FedNonFedIndicator>0</FedNonFedIndicator>
        //////    <IRS1099CIndicator>0</IRS1099CIndicator>
        //////    <AgencyLocationCode />
        //////    <FederalComments />
        //////    <usePurchRequest>0</usePurchRequest>
        //////    <CashDiscBaseDays>0</CashDiscBaseDays>
        //////    <CustTradingPartnerCode>0</CustTradingPartnerCode>
        //////    <ExportSales_PL>0</ExportSales_PL>
        //////    <FiscalDocType_PL>0</FiscalDocType_PL>
        //////    <ForeignResident_RU>0</ForeignResident_RU>
        //////    <GenerateIncomingFiscalDocument_BR>0</GenerateIncomingFiscalDocument_BR>
        //////    <InvoicePostingType_RU>0</InvoicePostingType_RU>
        //////    <IssuerCountry_HU />
        //////    <LvPaymTransCodes>0</LvPaymTransCodes>
        //////    <MandatoryVatDate_PL>0</MandatoryVatDate_PL>
        //////    <PassportNo_HU />
        //////    <PdsCustRebateGroupId />
        //////    <PdsFreightAccrued>0</PdsFreightAccrued>
        //////    <PdsRebateTMAGroup />
        //////    <TaxPeriodPaymentCode_PL />
        //////    <UseCashDisc>0</UseCashDisc>
        //////    <Affiliated_RU>0</Affiliated_RU>
        //////    <DAXIntegrationId>a815dff3-1ead-44e9-afa4-e652c068859c</DAXIntegrationId>
        //////    <DefaultDirectDebitMandate>0</DefaultDirectDebitMandate>
        //////    <EntryCertificateRequired_W>0</EntryCertificateRequired_W>
        //////    <IssueOwnEntryCertificate_W>0</IssueOwnEntryCertificate_W>
        //////    <AS400_ID>VFFD-155-182298509-SB</AS400_ID>
        //////    <VFFDPropertyValuation>0</VFFDPropertyValuation>
        //////    <FiscalOfficerAccountID />
        //////    <FiscalOfficerFirstName />
        //////    <FiscalOfficerLastName />
        //////    <FiscalOfficerMiddleName />
        //////    <FiscalOfficerName />
        //////    <FiscalOfficerSuffix />
        //////    <SecretaryAccountID />
        //////    <SecretaryName />
        //////    <SecretaryFirstName />
        //////    <SecretaryLastName />
        //////    <SecretaryMiddleName />
        //////    <SecretarySuffix />
        //////    <modifiedDateTime>2015-11-12T14:47:45Z</modifiedDateTime>
        //////    <modifiedBy>wfexc</modifiedBy>
        //////    <createdDateTime>2015-11-12T14:47:45Z</createdDateTime>
        //////    <dataAreaId>sfm</dataAreaId>
        //////    <recVersion>1</recVersion>
        //////    <Partition>5637144576</Partition>
        //////    <RecId>5637911843</RecId>
        //////  </Customers>
        //////</ClientDefinedQuery>";
        //////                DataSet ds = new DataSet();
        //////                ds.ReadXml(new XmlTextReader(new StringReader(xml)));
        ////                StringBuilder XMLString = new StringBuilder();

        ////                XMLString.Append(@"<?xml version=""1.0"" encoding=""utf-8""?>\r\n");
        ////                XMLString.AppendLine(@"\n");
        ////                XMLString.Append(GetCustomerResponse.XMLString.ToString());
        ////                dataSet.ReadXml(XMLString.ToString());
        ////            }
        ////            client.Close();
        ////            return dataSet;

        //        }
        #endregion "Commented"
        private DataSet QueryGetBeneficiary()
        {
            DataSet dataSet;

            Paging paging = null;

            QueryMetadata query;

            QueryDataSourceMetadata benefitDataSource;

            query = new QueryMetadata();

            // Set the properties of the query.

            query.QueryType = QueryType.Join;


            query.DataSources = new QueryDataSourceMetadata[1];


            // Set the properties of the Customers data source.


            benefitDataSource = new QueryDataSourceMetadata();

            benefitDataSource.Name = "VEN_Beneficiary";
            benefitDataSource.Enabled = true;
            benefitDataSource.Table = "VEN_beneficairyAmountTable";

            //Add the data source to the query.
            query.DataSources[0] = benefitDataSource;

            // Setting DynamicFieldList property to false so I can specify only a few fields
            benefitDataSource.DynamicFieldList = true;

            //query.OrderByFields[0] = sort;

            //  QueryDataRangeMetadata rangeData = new QueryDataRangeMetadata();

            //rangeData.Name = "AccountNum";
            //rangeData.FieldName = "AccountNum";
            //rangeData.TableName = "CustTable"; ;
            //rangeData.Value = accountNumbers;

            //rangeData.RangeStatus = RangeStatus.Open;
            //rangeData.Enabled = true;

            //benefitDataSource.Ranges = new QueryDataRangeMetadata[1];
            //benefitDataSource.Ranges[0] = rangeData;


            // implemented credentials starts
            // implemented credentials starts
            string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
            string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));
           
            QueryServiceClient client = new QueryServiceClient();
       if (client.ClientCredentials != null)
            {

                client.ClientCredentials.UserName.UserName = serviceUserid;
                client.ClientCredentials.UserName.Password = servicePassword;
                client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;
         }

            //dataSet = client.ExecuteQuery(query, ref paging); client.Close();


            dataSet = client.ExecuteStaticQuery("VEN_Beneficiary", ref paging);
            Log.Info("client.ExecuteStaticQuery(VEN_Beneficiary, ref paging);");

            // dataSet = client.ExecuteStaticQuery(query, ref paging); 
            client.Close();
            return dataSet;
        }

        private System.Data.DataTable GenerateExcelData()
        {
            Log.Debug("Enter GenerateExcelData ");

            //OleDbConnection oledbConn;
            try
            {
                string path = Convert.ToString(ConfigurationManager.AppSettings["ExcelFile"]);

                /* connection string  to work with excel file. HDR=Yes - indicates 
                   that the first row contains columnnames, not data. HDR=No - indicates 
                   the opposite. "IMEX=1;" tells the driver to always read "intermixed" 
                   (numbers, dates, strings etc) data columns as text. 
                Note that this option might affect excel sheet write access negative. */

                OleDbConnection oledbConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';");
                oledbConn.Open();
                OleDbCommand cmd = new OleDbCommand(); ;
                OleDbDataAdapter oleda = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                // passing list to drop-down list

                // selecting distinct list of Slno 
                cmd.Connection = oledbConn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT BillableAccountID, CertificationName, CertificationStatus, ExpirationDate,IsActiveStatus, OtherNumberID FROM [Sheet1$]";
                oleda = new OleDbDataAdapter(cmd);
                oleda.Fill(ds, "Certifications");
                oledbConn.Close();
                return ds.Tables[0];

            }
            // need to catch possible exceptions
            catch (Exception ex)
            {
                return (new System.Data.DataTable());
            }

        }// close of method GemerateExceLData

        private System.Data.DataTable GenerateTestData()
        {
            Log.Debug("Enter GenerateTestData ");

            //OleDbConnection oledbConn;
            try
            {
                DataSet dataSet = new DataSet();
                DataTable dataTable = new DataTable("Certification");
                dataTable.Columns.Add("BillableAccountID", typeof(string));
                dataTable.Columns.Add("CertificationName", typeof(string));
                dataTable.Columns.Add("CertificationStatus", typeof(string));
                dataTable.Columns.Add("ExpirationDate", typeof(string));
                dataTable.Columns.Add("IsActiveStatus", typeof(string));
                dataTable.Columns.Add("OtherNumberID", typeof(string));
                dataSet.Tables.Add(dataTable);

                //string xmlData = "<Certifications><Certification><BillableAccountID>851556</BillableAccountID>";
                //xmlData=xmlData+"<CertificationName>VFFD Deceased Firefighter</CertificationName>";
                //xmlData = xmlData + "<CertificationStatus>Active</CertificationStatus><ExpirationDate></ExpirationDate>";
                //xmlData = xmlData + "<IsActiveStatus>TRUE</IsActiveStatus><OtherNumberID>103</OtherNumberID></Certification>";
                //xmlData=xmlData+"</Certifications>";

                //    StringReader xmlSR = new System.IO.StringReader(xmlData);

                //  dataSet.ReadXml(xmlSR, XmlReadMode.IgnoreSchema);

                string path = Convert.ToString(ConfigurationManager.AppSettings["TestFile"]);
                dataSet.ReadXml(path);

                // TextReader txtReader = new StringReader(path);
                //  XmlReader reader = new XmlTextReader(txtReader);
                //  dataSet.ReadXml(reader);

                Log.Debug("Exit GenerateTestData ");

                return dataSet.Tables[0];

            }
            // need to catch possible exceptions
            catch (Exception ex)
            {
                Log.Error("Error GenerateTestData " + ex.Message);

                return (new System.Data.DataTable());
            }

        }// close of method GemerateExceLData


        #region "XML file Get and Update"
        /// <summary>
        /// Update the Last updated value in xml file for each type "Billable parties , BillingItems, paid invoices ets...use this..
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public void UpdateMostRecentValue(long value)
        {
            try
            {
                //File path
                string path = Convert.ToString(ConfigurationManager.AppSettings["MostRecentValueTrackingFile"]);
                Log.Debug(string.Format("Enter UpdateMostRecentValue(): Type= {0}, lastUpdate= {1}", type, value));
                if (!string.IsNullOrEmpty(path))
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(path);

                    //var nodes = xmlDoc.SelectNodes("/MostRecentUpdateInstant/BillableParties");
                    var nodes = xmlDoc.SelectNodes("/MostRecentUpdateInstant/InvoiceNumber");

                    foreach (XmlNode ch1 in nodes)
                    {
                        if (ch1 != null)
                        {
                            ch1.InnerText = Convert.ToString(value);
                            Log.Debug(string.Format("Enter UpdateMostRecentValue(): Before save Type= {0}, lastUpdate= {1}", type, value));
                            xmlDoc.Save(path);
                            Log.Debug(string.Format("Enter UpdateMostRecentValue(): After save Type= {0}, lastUpdate= {1}", type, value));

                            break;
                        }
                    }
                }
                else
                {
                    Log.Error(string.Format("Error UpdateMostRecentValue(): File ( {0} ) not found", path));
                    this.OnStop();
                }
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Error UpdateMostRecentValue():{0}", ex.Message));
                this.OnStop();
            }

        }

        /// <summary>
        /// Read Last updated value from xml file for each type "Billable parties , BillingItems, paid invoices ets...use this..
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public long GetMostRecentValue()
        {
            Log.Debug("Enter GetMostRecentValue(" + type + "):  ");
            string path = Convert.ToString(ConfigurationManager.AppSettings["MostRecentValueTrackingFile"]);

            long _value = -1;
            try
            {
                //File path

                if (!string.IsNullOrEmpty(path))
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(path);
                    Log.Debug(string.Format("Enter GetMostRecentValue(): XMLpath= {0}", path));

                    //var nodes = xmlDoc.SelectNodes("/MostRecentUpdateInstant/BillableParties");
                    var nodes = xmlDoc.SelectNodes("/MostRecentUpdateInstant/InvoiceNumber");
                    Log.Debug("Exit GetMostRecentValue(" + type + "):  ");
                    _value = (from XmlNode ch1 in nodes
                              where ch1 != null
                              where !string.IsNullOrEmpty(ch1.InnerText)
                              select Convert.ToInt64(ch1.InnerText)).FirstOrDefault();
                }
                else
                {
                    Log.Error(string.Format("Error GetMostRecentValue(): File ( {0} ) not found", path));
                    this.Stop();
                    return -1;
                }
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Error UpdateMostRecentValue(): File ( {0} ) not found", path));
                //stop the service
                this.Stop();
                return -1;
            }
            return _value;
        }
        #endregion "XML file Get and Update"
    }
}