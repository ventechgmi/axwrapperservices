﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 05/25/2015
'Description: delete the daily log files from the specified directories. Audit log creates nearly 200 MB per day. So this servie deletes the files after two days.
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.ServiceProcess;

namespace AXWrapperFileDeleteService
{
    public partial class AXWrapperFileDelete : ServiceBase
    {
        //Uses to tracking the actions 
        //  private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public AXWrapperFileDelete()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            ////Debugger.Launch();;
            DeleteFiles();
        }

        protected override void OnStop()
        {
        }
        /// <summary>
        /// Deleting the audit files after some particular days
        /// </summary>
        public void DeleteFiles()
        {
            try
            {
                string customerfiles = Convert.ToString(ConfigurationManager.AppSettings["Customer.Files"]);
                double noofDays = Convert.ToDouble(ConfigurationManager.AppSettings["NoofDays"]);

                //Get all log files
                var files = new DirectoryInfo(customerfiles).GetFiles("*.log");
                
                //Looping all log files
                foreach (var file in files)
                {
                    //check the no. of days
                    if (DateTime.UtcNow - file.CreationTimeUtc > TimeSpan.FromDays(noofDays))
                    {
                        
                        //delete file
                        File.Delete(file.FullName);
                    }
                }


                // log.Info("Entry: DelteFiles() ");
            }
            catch (Exception ex1)
            {
                // log.Info("Exit: DelteFiles(): " + ex1.Message.ToString());
            }
        }
    }
}
