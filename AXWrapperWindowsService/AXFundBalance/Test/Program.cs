﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AXFundBalance;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            FundConfig hold = new FundConfig()
            {
                Email = "bdavid@ventechsolutions.com",
                Limit = 2500,
                Name = "Test Fund",
                Query = "select * from MyTable",
                Timing = 60
            };

            List<FundConfig> myTest = new List<FundConfig>();
            myTest.Add(hold);

            System.Xml.Serialization.XmlSerializer writer =
                        new System.Xml.Serialization.XmlSerializer(typeof(List<FundConfig>));

            System.IO.StreamWriter file = new System.IO.StreamWriter("FundConfig.xml");
            writer.Serialize(file, myTest);
            file.Close();

        }
    }
}
