﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Xml.Serialization;
using log4net;
using log4net.Config;
using System.Configuration;
using System.Net.Mail;
using System.Reflection;

namespace AXFundBalance
{
    public partial class AXFundService : ServiceBase
    {
        private SqlConnection con = null;
        //Uses to tracking the actions 
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        //Timer for poll the data periodically
        private System.Timers.Timer timer;

        public AXFundService()
        {

            InitializeComponent();

            this.ServiceName = "AX Fund Balance";
            this.EventLog.Log = "Application";

            try
            {
                //if timer is running stop when stop the service
                //if (timer != null)
                //    timer.Stop();

                XmlConfigurator.Configure();
                
                log.Info("Enter OnStart() Windows Service Started on " + DateTime.Now.ToString());


                con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["AxConnectionString"].ConnectionString);
                string path = Convert.ToString(ConfigurationManager.AppSettings["FundConfigFile"]);
               
                XmlSerializer serializer = new XmlSerializer(typeof(List<FundConfig>));

                StreamReader reader = new StreamReader(path);
                List<FundConfig> funds = (List<FundConfig>)serializer.Deserialize(reader);
                reader.Close();
                Debugger.Launch();

                TimerTick(new FundConfig() { Name="fund 1",Email="sananthakumar@ventechsolutions.com", Query = "select * from CUSTAGINGLEGALENTITY" , Limit=20000});

                //List<System.Threading.Timer> _timers = new List<System.Threading.Timer>();
                //foreach (FundConfig fund in funds)
                //{
                //    long inter = (long)TimeSpan.FromMinutes(fund.Timing).TotalMilliseconds;
                //    _timers.Add(new System.Threading.Timer(TimerTick, fund, inter, inter));
                //}
            }
            catch (Exception ex)
            {
                log.Error("Error OnStart() " + ex.Message);
            }
        }

        ///// <summary>
        ///// Set the timer to schedule specific interval
        ///// </summary>
        //private void SetTimer()
        //{
        //    if (timer == null)
        //    {
        //        timer = new System.Timers.Timer();
        //        timer.AutoReset = true;
        //        timer.Interval = interval;
        //        //set event 
        //        timer.Elapsed += new System.Timers.ElapsedEventHandler(TimerElapsedEvent);
        //        timer.Start();
        //    }
        //}
        public void TimerTick(object StateObj)
        {
            FundConfig fund = (FundConfig)StateObj;
            Debugger.Launch();
            con.Open();
              
            using (SqlCommand cmd = new SqlCommand(fund.Query, con))
            {
               // cmd.CommandType = CommandType.Text;

                 SqlDataReader reader = cmd.ExecuteReader();

                reader.Read();
                decimal balance = reader.GetDecimal(0);
                StringBuilder sbBody=new StringBuilder();
                sbBody.Append("Fund Name: ");
                sbBody.Append(fund.Name);
                sbBody.AppendLine("Email : ");
                sbBody.Append(fund.Email);

                sbBody.AppendLine(" Threshold Limit : ");
                sbBody.Append(fund.Limit);
              
                if (balance < fund.Limit)
                {
                    //todo send email
                    SendEmail(fund.Email,"Fund Threshold", sbBody.ToString());
                }

            }

        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
        }

        /// <summary>
        /// Service stop event
        /// </summary>
        protected override void OnStop()
        {

            //if timer is running stop when stop the service
            if (timer != null)
                timer.Stop();
            base.OnStop();
            log.Info("Windows Service Stopped on " + DateTime.Now.ToString());
        }

         #region "Email"
        private void SendEmail(string emails, string subject, string body)
        {
            try
            {

                log.Info("Enter: SendMail getting users emails");   //Get the defaultNamingContext from RootDSE
               
              //  string ToAddress = System.Configuration.ConfigurationManager.AppSettings["AdminEmail"];

                if (!string.IsNullOrEmpty(emails))
                {
                    if (emails.EndsWith(","))
                        emails = emails.Substring(0, emails.Length - 1);

                    StringBuilder mailbody = new StringBuilder();
                    mailbody.Append(@"<html><meta http-equiv=""Content-Type"" content=""text/html; charset=us-ascii""><center><table style=""border:5px solid #cccccc; width:790px"" border=""0"" cellpadding=""0"" cellspacing=""0"">");
                    mailbody.Append(@"<tbody><tr><td colspan=""2"" style=""padding:0px;background:#cbc3a7;"" align=""left""></td></tr><tr><td colspan=""2"" style=""text-align:left; padding:0pt 0px;""><table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""790px""><tr>");
                    mailbody.Append(@"<td style=""text-align:justify; padding-left:20px; font-family:Calibri;""><span><br />");
                    mailbody.Append(body);
                    //mailbody.Append(@"</span></td></tr></table></td></tr><tr><td style=""padding-left:20px; ""><hr style=""margin:0 ;  width:760px; height:6px; color:#ccc""></td></tr><tr><td colspan=""2"" style=""padding:0px;""><table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%""><tbody><tr><td style=""padding-left:20px; font-family:verdana; font-size:9px;""><span>We respect your privacy and we do not share your personal information.</span></td></tr></tbody></table></td></tr></tbody></table></center></html>");
                    mailbody.Append(@"</span></td></tr></table></td></tr></table></html>");

                    string mailserver = System.Configuration.ConfigurationManager.AppSettings["SMTP.Server"];
                    string SMTPUID = System.Configuration.ConfigurationManager.AppSettings["SMTP.UID"];
                    string SMTPPWD = System.Configuration.ConfigurationManager.AppSettings["SMTP.Password"];
                    string FromAddress = System.Configuration.ConfigurationManager.AppSettings["SMTP.FromAddress"];
                    string port = System.Configuration.ConfigurationManager.AppSettings["SMTP.Port"];
                    string EnableSSL = System.Configuration.ConfigurationManager.AppSettings["SMTP.EnableSSL"];
                    
                    //if (ToAddress != "")
                    //    emails += "," + ToAddress;

                    MailMessage mail = new MailMessage();

                    mail.From = new MailAddress(FromAddress);
                    mail.IsBodyHtml = true;
                    //  mail.Priority = MailPriority.High;
                    mail.To.Add(emails);
                    // mail.To.Add(ToAddress);
                    mail.Subject = subject;// "New Docket Cover Sheet created and waiting for approval";
                    mail.Body = mailbody.ToString();// "New Docket Cover Sheet created and waiting for approval";

                    SmtpClient SmtpServer = new SmtpClient(mailserver);
                    if (!string.IsNullOrEmpty(SMTPPWD))
                        SmtpServer.Credentials = new System.Net.NetworkCredential(SMTPUID, SMTPPWD);
                    
                    if (!string.IsNullOrEmpty(port))
                        SmtpServer.Port = Convert.ToInt32(port);

                    if (!string.IsNullOrEmpty(EnableSSL))
                        SmtpServer.EnableSsl = Convert.ToBoolean(EnableSSL); ;

                    log.Info("Enter: SendEmail " + subject);
                    SmtpServer.Send(mail);
                    log.Info("Mail Sent successfully: " + subject + "for the following emails: " + emails);
                }
            }
            catch (Exception ex)
            {
                log.Error(subject + "   " + ex.Message.ToString());
              //  log4net.Config.XmlConfigurator.Configure();
              //  log.Info(subject + "   " + ex.Message.ToString());
            }
        }

        #endregion "Email"
    }
}
