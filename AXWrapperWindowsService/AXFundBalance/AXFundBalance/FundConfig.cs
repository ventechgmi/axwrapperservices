﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace AXFundBalance
{
    [Serializable()]
    public class FundConfig 
    {
        public string Name { get; set; }
        public string Query { get; set; }
        public decimal Limit { get; set; }
        public string Email { get; set; }
        public int Timing { get; set; }

    }
}
