﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 02/12/2015
'Description: Service creates a daily log file, Poll Invoicable Items  from ACADIS service and update in AX
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------


'******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using System.Xml;
using AcadisDataServices.Client;
using AcadisDataServices.Client.Accounting;
using AXWrapperWindowsService.CustomerServiceReference;
using AXWrapperWindowsService.InvoiceServiceReference;
using EncryptandDecrypt;
using log4net;
using log4net.Config;

namespace AXWrapperWindowsService
{
    public partial class ACADIStoAXInvoiceService : ServiceBase
    {
        const string CurrencyCode = "USD";

        #region "variables"
        //Page size default to 1000
        int PAGE_SIZE = 1000;

        //Page size default to 1000
        int CustomerCheckingCount = 10;

        /// <summary>
        /// service user id   to access invoice service
        /// </summary>
        private string serviceUserid;

        /// <summary>
        /// Service password to access invoice service
        /// </summary>
        private string servicePassword;

        /// <summary>
        /// To prevent multiple execution of timer events
        /// </summary>
        private volatile bool isTimerExecuting;

        //Uses to tracking the actions 
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        //Timer for poll the data periodically
        private System.Timers.Timer timer;

        //This statement is used to set interval to 1 minute (= 60,000 milliseconds) - 1 sec = 1000
        private double interval = 1000;

        #endregion "variables"

        #region "Service Events"
        /// <summary>
        /// Windows service on start set log4 net configuration, timer
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            Debugger.Launch();;

            //user id and password from config file and decrypting 
            serviceUserid = GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
            servicePassword = GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

            //customer checking count 
            CustomerCheckingCount = Convert.ToInt32(ConfigurationManager.AppSettings["CustomerCheckingCount"]);

            //retrieve the page size from config
            PAGE_SIZE = Convert.ToInt32(Convert.ToString(ConfigurationManager.AppSettings["PAGE_SIZE"]));

            //This statement is used to set interval to 1 minute (= 60,000 milliseconds) - 1 sec = 1000
            interval = Convert.ToDouble(ConfigurationManager.AppSettings["ServiceInterval"]);
          
         //   LoopingTime = Convert.ToDouble(ConfigurationManager.AppSettings["ServiceInterval"]);

            //if timer is running stop when stop the service
            if (timer != null)
                timer.Stop();

            XmlConfigurator.Configure();
            Log.Info("Enter OnStart() Windows Service Started");

            //Sert timer
            SetTimer();

            // KeepAlive to prevent garbage collection from occurring before the method ends. 
            GC.KeepAlive(timer);

        }

        /// <summary>
        /// service stop event
        /// </summary>
        protected override void OnStop()
        {

            //if timer is running stop when stop the service
            if (timer != null)
                timer.Stop();

            Log.Info("Windows Service Stopped");
        }
        #endregion "Service Events"

        #region "Timer Events"
        /// <summary>
        /// Set the timer to schedule specific interval
        /// </summary>
        private void SetTimer()
        {
            if (timer == null)
            {
                //retrieve polling interval
                //POLLING_INTERVAL = TimeSpan.FromSeconds(Convert.ToDouble(ConfigurationManager.AppSettings["ServiceInterval"]));


                timer = new Timer();
                timer.AutoReset = true;

                timer.Interval = interval;
                //set event 
                timer.Elapsed += new ElapsedEventHandler(TimerElapsedEvent);
                timer.Start();
            }
        }
        /// <summary>
        /// Timer elapsed event , this will call and execute the process
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void TimerElapsedEvent(object source, ElapsedEventArgs e)
        {

            Log.Debug("Enter TimerElapsedEvent()");

            if (isTimerExecuting)
                return;

            isTimerExecuting = true;

            try
            {
                //Migrate Invoices
                MigrateInvoiceableItems();
            }

            catch (Exception ex)
            {
                Log.Error("Error TimerElapsedEvent() " + ex.Message);
            }
            finally
            {
                isTimerExecuting = false;
                Log.Debug("Exit  TimerElapsedEvent()");
            }
        }
        #endregion "Timer Events"

        #region "XML file Get and Update"
        /// <summary>
        /// Update the Last updated value in xml file for each type "Billable parties , BillingItems, paid invoices ets...use this..
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public void UpdateMostRecentValue(string type, long value)
        {
            try
            {
                //File path
                string path = Convert.ToString(ConfigurationManager.AppSettings["MostRecentValueTrackingFile"]);
                Log.Debug(string.Format("Enter UpdateMostRecentValue(): Type= {0}, lastUpdate= {1}", type , value));
                if (!string.IsNullOrEmpty(path))
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(path);

                    //var nodes = xmlDoc.SelectNodes("/MostRecentUpdateInstant/BillableParties");
                    var nodes = xmlDoc.SelectNodes("/MostRecentUpdateInstant/" + type);

                    foreach (XmlNode ch1 in nodes)
                    {
                        if (ch1 != null)
                        {
                            ch1.InnerText = Convert.ToString(value);
                            Log.Debug(string.Format("Enter UpdateMostRecentValue(): Before save Type= {0}, lastUpdate= {1}", type, value));
                            xmlDoc.Save(path);
                            Log.Debug(string.Format("Enter UpdateMostRecentValue(): After save Type= {0}, lastUpdate= {1}", type, value));
                          
                            break;
                        }
                    }
                }
                else
                {
                    Log.Error(string.Format("Error UpdateMostRecentValue(): File ( {0} ) not found" ,path));
                    this.OnStop();
                }
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Error UpdateMostRecentValue():{0}", ex.Message));
                this.OnStop();
            }

        }

        /// <summary>
        /// Read Last updated value from xml file for each type "Billable parties , BillingItems, paid invoices ets...use this..
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public long GetMostRecentValue(string type)
        {
            Log.Debug("Enter GetMostRecentValue(" + type + "):  ");
            string path = Convert.ToString(ConfigurationManager.AppSettings["MostRecentValueTrackingFile"]);

            long _value = -1;
            try
            {
                //File path

                if (!string.IsNullOrEmpty(path))
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(path);
                    Log.Debug(string.Format("Enter GetMostRecentValue(): XMLpath= {0}" , path));

                    //var nodes = xmlDoc.SelectNodes("/MostRecentUpdateInstant/BillableParties");
                    var nodes = xmlDoc.SelectNodes("/MostRecentUpdateInstant/" + type);
                    Log.Debug("Exit GetMostRecentValue(" + type + "):  ");
                    _value = (from XmlNode ch1 in nodes
                              where ch1 != null
                              where !string.IsNullOrEmpty(ch1.InnerText)
                              select Convert.ToInt64(ch1.InnerText)).FirstOrDefault();
                }
                else
                {
                    Log.Error(string.Format("Error GetMostRecentValue(): File ( {0} ) not found", path));
                    this.Stop();
                    return -1;
                }
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Error UpdateMostRecentValue(): File ( {0} ) not found", path));
                //stop the service
                this.Stop();
                return -1;
            }
            return _value;
        }
        #endregion "XML file Get and Update"

        #region "Invoices"
        /// <summary>
        /// Get invoiceable items from ACADIS
        /// </summary>
        private void MigrateInvoiceableItems()
        {
            // Set up Invoice client
            InvoiceClient client = new InvoiceClient();
            try
            {
                Log.Info("Enter MigrateInvoiceableItems() ");
                var config = new AcadisDataServicesClientConfiguration(ConfigurationManager.AppSettings);
                var accounting = new AccountingClient(config);
                Log.Debug("MigrateInvoiceableItems(): Before calling");

                //type is billable parties 
                const string type = "BillingItems";

                long lastUpdate = GetMostRecentValue(type); ;
                //Warning! Calling the service with lastUpdate == 0 will get ALL billing info
                Log.Debug("Enter MigrateInvoiceableItems(): lastUpdate= " + lastUpdate);

                var invoiceItems = accounting.GetInvoiceableItems(lastUpdate, PAGE_SIZE);

                Log.Debug("MigrateInvoiceableItems(): Successfully got records " + invoiceItems.Items.Count() + " from ACADIS");

                if (invoiceItems.Items.Any())
                {

                    var results = from p in invoiceItems.Items
                                  group p by new { p.BillToID, p.BillDate.Date }
                                      into g
                                      select new { g }; //


                    if (client.ClientCredentials != null)
                    {
                        client.ClientCredentials.UserName.UserName = serviceUserid;
                        client.ClientCredentials.UserName.Password = servicePassword;
                        client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode =
                            X509CertificateValidationMode.None;
                    }

                    #region "Looping results"

                    foreach (var each in results)
                    {

                        //If error happen update this data into the file
                        var errordata = new StringBuilder();

                        //declaring invoice list
                        List<Invoice> invoiceList = new List<Invoice>();
                        //declaring invoice line list
                        List<InvoiceLine> invoiceLineList = new List<InvoiceLine>();

                        int count = 0;
                        string BillToID = string.Empty;
                        try
                        {
                            string lastUpdateInstants = string.Empty;
                            string InvoiceAccount = string.Empty;
                            foreach (var item in each.g)
                            {
                                //update the last update ids for invoice line items
                                if (string.IsNullOrEmpty(lastUpdateInstants))
                                    lastUpdateInstants = item.LastUpdateInstant.ToString();
                                else
                                    lastUpdateInstants += "," + item.LastUpdateInstant;

                                lastUpdate = item.LastUpdateInstant;

                                #region "Create"

                                string customerReference = string.Empty;
                                customerReference = Convert.ToString(item.BillFor);
                                string purchaseOrderFormNumber = string.Empty;
                                //string CustomerReference = "123";
                                //string PurchaseOrderFormNumber = "123";
                                string InvoiceId = string.Empty;

                                string Category = Convert.ToString(item.Category).ToUpper();
                                string ARCode = string.Empty;

                                switch (Category)
                                {
                                    case "CLASS":
                                        ARCode = "C";
                                        break;
                                    case "HOUSING":
                                    case "EVENTHOUSING":
                                    case "CLASSHOUSING":
                                        ARCode = "D";
                                        break;
                                    case "ONLINEEVENT":
                                        ARCode = "O";
                                        break;
                                    case "RENTABLE":
                                        ARCode = "B";
                                        break;
                                }

                                ////Now ARCode will allow only one character in AX so we just taking first characters if ACADIS returns more than one characters.
                                //if (ARCode.Length > 1)
                                //    ARCode = ARCode.Substring(0, 1);

                                //Now Description will allow only 60 characters in AX so we just taking first characters if ACADIS returns more than one characters.
                                //string description = Convert.ToString(item.PeriodStart) + " " +
                                //                     Convert.ToString(item.PeriodEnd) + " " +
                                //                     Convert.ToString(item.Usage) + " " +
                                //                     Convert.ToString(item.Item) + " " + Convert.ToString(item.Event);

                                string description = item.Description;

                                ////Now Description will allow 60 characters in AX so we just taking first characters if ACADIS returns more than one characters.
                                //if (description.Length > 60)
                                //{
                                //    description = description.Substring(0, 60);
                                //    Log.Error("MigrateInvoiceableItems(): description length is more than 60, so limited to 60 , 'Last Update=" +
                                //        item.LastUpdateInstant + "'");
                                //}

                                string id = item.ID.ToString();
                                //adding a description in invoice line list
                                invoiceLineList.Add(new InvoiceLine()
                                {
                                    ARTrans = item.ID,
                                    ARCode = ARCode, // Now ARCode will accept only one character
                                    Quantity = item.Quantity,
                                    UnitPrice = item.UnitPrice,
                                    // MainAccount = "426050",
                                    Amount = item.Amount,
                                    InvoiceDefaultDimensionList = new List<DefaultDimension>(),
                                    //Now Description will allow only 60 characters in AX so we just taking first characters if ACADIS returns more than one characters.
                                    Description = description,
                                    VEN_BillableItemId = id // Added for invoice cancellation
                                });

                                #region "To update the error file if any errors"

                                errordata.Append("");
                                errordata.Append("lastUpdate: ");

                                errordata.Append(lastUpdate.ToString());
                                errordata.Append("; CustomerReference: ");
                                errordata.Append(customerReference);
                                errordata.Append("; Category: ");
                                errordata.Append(Category);
                                errordata.Append("; ARCode: ");
                                errordata.Append(ARCode);
                                errordata.Append("; description: ");
                                errordata.Append(description);
                                errordata.Append("; Quantity: ");
                                errordata.Append(item.Quantity.ToString());
                                errordata.Append("; UnitPrice: ");
                                errordata.Append(item.UnitPrice.ToString());
                                errordata.Append("; Amount: ");
                                errordata.Append(item.Amount.ToString());
                                errordata.Append("; VEN_BillableItemId: ");
                                errordata.Append(id);
                                #endregion "To update the error file if any errors"

                                #region "Invoice account and all common for multiple invoice lines - so update invoice account and billdate once"

                                // since we are looping the invoice lines so taking first invoice and 
                                if (count == 0)
                                {
                                    //string BilltoId = Convert.ToString(item.BillToID);
                                    // set up invoice data
                                    BillToID = Convert.ToString(item.BillToID);

                                    Invoice newInvoice = new Invoice()
                                    {
                                        CustomerReference = customerReference,
                                        InvoiceDate = item.BillDate,
                                        //  OrderAccount = Convert.ToString(item.BillForID),
                                        OrderAccount = BillToID,
                                        // Billing for id - Order account
                                        InvoiceAccount = BillToID,
                                        // billing to id - invoice account
                                        //OrderAccount = "1359476",
                                        //InvoiceAccount = "1359476",
                                        InvoiceId = InvoiceId, // Invoice ID
                                        PurchaseOrderFormNumber = purchaseOrderFormNumber,
                                        CurrencyCode = CurrencyCode,
                                        InvoiceLineList = invoiceLineList,
                                        InvoiceDefaultDimensionList = new List<DefaultDimension>(),
                                        // InvoiceDefaultDimensionList = new List<DefaultDimension>()
                                        IsAutoPost = true
                                        // true for post the invoice when creation and false for non-posting.
                                    };

                                    InvoiceAccount = BillToID;
                                    invoiceList.Add(newInvoice);

                                    errordata.AppendLine(@"InvoiceDate: ");
                                    errordata.Append(item.BillDate);

                                    errordata.Append(@"; OrderAccount: ");
                                    errordata.Append(BillToID);

                                    errordata.Append(@"; InvoiceAccount: ");
                                    errordata.Append(BillToID);
                                }
                                #endregion "Invoice account and all common for multiple invoice lines - so update invoice account and billdate once"

                                count++;

                                #endregion "Create"
                            }
                            //Setup the invoice request with invoice lines
                            var createInvoiceRequest = new CreateInvoiceRequest
                            {
                                Invoice = invoiceList
                            };


                            try
                            {
                                // Set up customer client
                                CustomerClient CustomerClient = new CustomerClient();


                                CustomerClient.ClientCredentials.UserName.UserName = serviceUserid;
                                CustomerClient.ClientCredentials.UserName.Password = servicePassword;
                                CustomerClient.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;

                                List<string> BillToList = new List<string>();
                                BillToList.Add(BillToID);

                                //Call AX wrapper service to verify the customer is exists or not
                                IsCustomerExistsRequest IsCustomerExistsRequest = new IsCustomerExistsRequest
                                {
                                    AccountNumbers = BillToList
                                };

                                #region "checking customer"
                                //checking 100 times to get customer created in AX
                                for (int i = 0; i < CustomerCheckingCount; i++)
                                {
                                    System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender1, certificate, chain, sslPolicyErrors) => true);
                                   
                                    Log.Info("Entered to check Customer");
                                     
                                    IsCustomerExistsResponse IsCustomerExistsResponse =
                                        CustomerClient.IsCustomerExists(IsCustomerExistsRequest);
                                    Log.Info("Exit to check Customer");
                                   
                                     
                                    if (IsCustomerExistsResponse.ValidationDetail.Result)
                                    {
                                        Log.Info(string.Format("Success IsCustomerExists(): True for BillToID={0}", BillToID.ToString()));
                                        Log.Info(errordata);
                                        #region " invoice creation "

                                        System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender1, certificate, chain, sslPolicyErrors) => true);
                                        //Call thre service and assign the response
                                        var createInvoiceResponse = new CreateInvoiceResponse
                                        {
                                            ValidationDetail = client.CreateInvoice(createInvoiceRequest.Invoice)
                                        };

                                        // result will be true for success and false for failure invoices
                                        if (createInvoiceResponse.ValidationDetail.Result)
                                        {
                                            Log.Info(string.Format("Invoice created Successfully. Invoice Account(BillToID)={0} ; lastupdate ids= {1}", InvoiceAccount, lastUpdateInstants));
                                            Log.Info("-------------------------------------------------------------------------------------------");
                                            break;
                                        }
                                        else
                                        {
                                            var errorMessage = createInvoiceResponse.ValidationDetail.ErrorList[0].ToString();
                                            Log.Error(string.Format("Error:CreateInvoice() : {0} ; ERROR Data: {1}", errorMessage, errordata));
                                            Log.Error("-------------------------------------------------------------------------------------------");
                                            break;
                                        }
                                        #endregion " invoice creation "
                                    }
                                    else
                                    {
                                        Log.Error(string.Format("Success IsCustomerExists(): False ; Attempt: {0} for BillToID= {1}", (i + 1).ToString(), BillToID.ToString()));
                                        Log.Error("-------------------------------------------------------------------------------------------");
                                        Log.Error(errordata);
                                    }
                                } //for loop ends here
                                #endregion "checking customer"
                            }
                            catch (System.ServiceProcess.TimeoutException exception)
                            {
                                Log.Error(string.Format("Error CreateInvoice(): False for BillToID={0} Error Data : {1} {2}", BillToID.ToString(), errordata, exception.Message));
                                Log.Error("-------------------------------------------------------------------------------------------");
                                client.Abort();
                            }
                            catch (CommunicationException exception)
                            {
                                Log.Error(string.Format("Error CreateInvoice(): False for BillToID={0} Error Data : {1} {2}", BillToID.ToString(), errordata, exception.Message));
                                Log.Error("-------------------------------------------------------------------------------------------");
                                client.Abort();
                            }
                            catch (Exception exception)
                            {
                                Log.Error(string.Format("Error CreateInvoice(): False for BillToID={0} Error Data : {1} {2}", BillToID.ToString(), errordata, exception.Message));
                                Log.Error("-------------------------------------------------------------------------------------------");
                                client.Abort();
                            }
                        }
                        catch (Exception exception)
                        {
                            Log.Error(string.Format("Error CreateInvoice():  Error Data : {0} {1}", errordata, exception.Message));
                            Log.Error("-------------------------------------------------------------------------------------------");
                        }
                    }

                    #endregion "Looping results"

                    Log.Debug(string.Format("MigrateBillabaleParties(): before loop lastUpdate={0}", lastUpdate));
                    UpdateMostRecentValue(type, lastUpdate);
                    Log.Debug(string.Format("MigrateBillabaleParties(): after Updating lastUpdate={0}", lastUpdate));

                }
                else
                    Log.Debug("Exit MigrateBillabaleParties(): No invoices from ACADIS");
            }
            catch (System.ServiceProcess.TimeoutException exception)
            {
                Log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                Log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }
            catch (CommunicationException exception)
            {
                Log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                Log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Error:CreateInvoice() : {0}", ex.Message));
                Log.Error("-------------------------------------------------------------------------------------------");
                client.Abort();
            }
        }
        #endregion "Invoices"
    }
}