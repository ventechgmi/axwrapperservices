﻿/******************************************************************************************
'Created By     : ANANTHAKUMAR SUBRAMANI
'Created Date   : 01/05/2014
'Description    : Customer Service - get customer info from acdis and pass it to AXwrpper service.
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/

using System.ServiceProcess;

namespace AXWrapperWindowsService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new ACADIStoAXPaymentService() 
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
