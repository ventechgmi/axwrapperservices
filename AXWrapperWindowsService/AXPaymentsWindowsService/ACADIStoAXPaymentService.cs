﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 02/12/2015
'Description: Service creates a daily log file, retrieve Paid Invoices from ACADIS service and create ledger payment in AX 
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'ANANTHAKUMAR Subramani     08/06/15             Added Payment confirmation
'ANANTHAKUMAR Subramani     08/11/15             Added Payment Frequency
'******************************************************************************************/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Reflection;
using System.ServiceModel.Security;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Xml;
using AcadisDataServices.Client;
using AcadisDataServices.Client.Accounting;
using AXWrapperWindowsService.LedgerJournalService;
using log4net;
using log4net.Config;
using Newtonsoft.Json;
using Formatting = System.Xml.Formatting;
using System.Timers;
using System.ServiceModel;

namespace AXWrapperWindowsService
{
    public partial class ACADIStoAXPaymentService : ServiceBase
    {

        #region "constants"
        const string offsetAccountType = "Ledger";
        const string AccountType = "Cust";
        #endregion "constants"

        #region "variables"
        //Page size default to 1000
        int PAGE_SIZE = 1000;

        //Payment offset account
        string OffSetAccount = string.Empty;

        /// <summary>
        /// Payment frequency - 1 for 1 payment per day
        /// </summary>
        int PaymentFrequency=0;

        /// <summary>
        /// service user id   to access invoice service
        /// </summary>
        private string serviceUserid;

        /// <summary>
        /// Service password to access invoice service
        /// </summary>
        private string servicePassword;

        /// <summary>
        /// To prevent multiple execution of timer events
        /// </summary>
        private volatile bool isTimerExecuting;

        //Uses to tracking the actions 
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        //Timer for poll the data periodically
        private System.Timers.Timer timer;

        //This statement is used to set interval to 1 minute (= 60,000 milliseconds) - 1 sec = 1000
        private double interval = 1000;



        #endregion "variables"

        #region "Service Events"
        /// <summary>
        /// Windows service on start set log4 net configuration, timer
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {

            //user id and password from config file and decrypting 
            serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
            servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

            //retrieve the page size from config
            PAGE_SIZE = Convert.ToInt32(Convert.ToString(ConfigurationManager.AppSettings["PAGE_SIZE"]));

            PaymentFrequency = Convert.ToInt32(ConfigurationManager.AppSettings["PaymentFrequency"]);
           
            interval = Convert.ToDouble(ConfigurationManager.AppSettings["ServiceInterval"]);
           
            // convert minutes into milliseconds
            interval = TimeSpan.FromMinutes(interval).TotalMilliseconds;
                
            //Payment default account
            OffSetAccount = Convert.ToString(ConfigurationManager.AppSettings["OffSetAccount"]);

            //if timer is running stop when stop the service
            if (timer != null)
                timer.Stop();

            XmlConfigurator.Configure();
            ////Debugger.Launch();;
            Log.Info("Enter OnStart() Windows Service Started on " + DateTime.Now.ToString());

            //Sert timer
            SetTimer();

            // KeepAlive to prevent garbage collection from occurring before the method ends. 
            GC.KeepAlive(timer);
        }

        /// <summary>
        /// Service stop event
        /// </summary>
        protected override void OnStop()
        {

            //if timer is running stop when stop the service
            if (timer != null)
                timer.Stop();

            Log.Info("Windows Service Stopped on " + DateTime.Now.ToString());
        }
        #endregion "Service Events"

        #region "Timer Events"
        /// <summary>
        /// Set the timer to schedule specific interval
        /// </summary>
        private void SetTimer()
        {
            if (timer == null)
            {
                timer = new System.Timers.Timer();
                timer.AutoReset = true;
                timer.Interval = interval;
                //set event 
                timer.Elapsed += new System.Timers.ElapsedEventHandler(TimerElapsedEvent);
                timer.Start();
            }
        }
        /// <summary>
        /// Timer elapsed event , this will call and execute the process
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void TimerElapsedEvent(object source, System.Timers.ElapsedEventArgs e)
        {

            Log.Info("Enter TimerElapsedEvent()");

            if (isTimerExecuting)
                return;

            isTimerExecuting = true;

            try
            {
                //Migrate Payments
                MigratePayments();
            }

            catch (Exception ex)
            {
                Log.Error("Error TimerElapsedEvent() " + ex.Message);
            }
            finally
            {
                isTimerExecuting = false;
                Log.Info("Exit  TimerElapsedEvent()");
            }
        }

        #endregion "Timer Events"

        #region "XML file Get and Update"
        /// <summary>
        /// Update the Last updated value in xml file for each type "Billable parties , BillingItems, paid invoices ets...use this..
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public void UpdateMostRecentValue(string type, long value)
        {
            try
            {
                //File path
                string path = Convert.ToString(ConfigurationManager.AppSettings["MostRecentValueTrackingFile"]);
                Log.Info("Enter UpdateMostRecentValue(): Type=" + type + " , lastUpdate=" + value);
                if (!string.IsNullOrEmpty(path))
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(path);

                    //var nodes = xmlDoc.SelectNodes("/MostRecentUpdateInstant/BillableParties");
                    var nodes = xmlDoc.SelectNodes("/MostRecentUpdateInstant/" + type);

                    foreach (XmlNode ch1 in nodes)
                    {
                        if (ch1 != null)
                        {
                            ch1.InnerText = Convert.ToString(value);
                            Log.Info("Enter UpdateMostRecentValue(): Before save Type=" + type + " , lastUpdate=" +
                                     value);
                            xmlDoc.Save(path);
                            Log.Info("Exit UpdateMostRecentValue(): After save Type=" + type + " , lastUpdate=" + value);
                            break;
                        }
                    }
                }
                else
                {
                    Log.Error("Enter UpdateMostRecentValue(): File not found" + DateTime.Now.ToString());
                    this.OnStop();
                }
            }
            catch (Exception)
            {
                Log.Error("Enter UpdateMostRecentValue(): File not found" + DateTime.Now.ToString());
                this.OnStop();
            }

        }

        /// <summary>
        /// Read Last updated value from xml file for each type "Billable parties , BillingItems, paid invoices ets...use this..
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public long GetMostRecentValue(string type)
        {
            Log.Info("Enter GetMostRecentValue(" + type + "):  ");
            string path = Convert.ToString(ConfigurationManager.AppSettings["MostRecentValueTrackingFile"]);

            long _value = -1;
            try
            {
                //File path

                if (!string.IsNullOrEmpty(path))
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(path);
                    Log.Info("Enter GetMostRecentValue(" + type + "): XMLpath= " + path);

                    //var nodes = xmlDoc.SelectNodes("/MostRecentUpdateInstant/BillableParties");
                    var nodes = xmlDoc.SelectNodes("/MostRecentUpdateInstant/" + type);
                    Log.Info("Exit GetMostRecentValue(" + type + "):  ");
                    _value = (from XmlNode ch1 in nodes
                              where ch1 != null
                              where !string.IsNullOrEmpty(ch1.InnerText)
                              select Convert.ToInt64(ch1.InnerText)).FirstOrDefault();
                }
                else
                {
                    Log.Error("ERROR: GetMostRecentValue(" + type + "): " + path + " File Not found ");
                    this.Stop();
                    return -1;
                }
            }
            catch (Exception ex)
            {
                Log.Error("ERROR: GetMostRecentValue(" + type + "): File" + path + " File Not found " + ex.Message);
                this.Stop();
                return -1;
            }
            return _value;
        }
        #endregion "XML file Get and Update"

        #region "Payments"
        private void MigratePayments()
        {
            try
            {
                Log.Info("Enter MigratePayments() " + DateTime.Now.ToString());
                var config = new AcadisDataServicesClientConfiguration(ConfigurationManager.AppSettings);
                Log.Info("MigratePayments(): Before calling GetMostRecentValue");

                //type is billable parties 
                const string type = "payments";

                long lastUpdate = GetMostRecentValue(type);
                long originalLastUpdate = lastUpdate;
                //Warning! Calling the service with lastUpdate == 0 will get ALL billing info
                Log.Info("Enter MigratePayments() : lastUpdate= " + lastUpdate);
                var accounting = new AccountingClient(config);

                //Create Payment response and retrieve all payments
                GetPaymentsResponse getPaymentsResponse = accounting.GetPayments(lastUpdate, PAGE_SIZE);

                do
                {
                    lastUpdate = getPaymentsResponse.MostRecentUpdateInstant;

                    //Create payments 
                    CreateNewPayment(getPaymentsResponse);

                    //get next records from ACADIS based on the previous input
                    getPaymentsResponse = accounting.GetNextPage(getPaymentsResponse);
                } while (getPaymentsResponse != null); // checking the response has data or not

                Log.Info("Enter MigratePayments(): after loop lastUpdate=" + lastUpdate);

                //do not update if last update ids are same 
                //if (originalLastUpdate != lastUpdate)
                //    UpdateMostRecentValue(type, lastUpdate);

            }
            catch (Exception ex)
            {
                Log.Error("Error:MigratePayments() : " + ex.Message);
            }
        }

         /// <summary>
        /// Create ledger payments
        /// </summary>
        /// <param name="Payments"></param>
        private void CreateNewPayment(GetPaymentsResponse Payments)
        {
            #region
            string type = "payments";

            
            //looping payments
           // for (int paymentCount = 0; paymentCount < Payments.Payments.Count(); paymentCount++)
            // Create only one payment per day - use frequency ( 1 for 1 payment per day) to loop the payments
           if(Payments.Payments.Count()>0)
           {
            //for (int paymentCount = 0; paymentCount < PaymentFrequency; paymentCount++)
            //{

                StringBuilder sbErrorData = new StringBuilder();

                // Set up Ledger Journal Client
                LedgerJournalClient client = new LedgerJournalClient();
                    try
                    {
                        #region "Create"

                         //// implemented credentials starts

                        if (client.ClientCredentials != null)
                        {
                            client.ClientCredentials.UserName.UserName = serviceUserid;
                            client.ClientCredentials.UserName.Password = servicePassword;
                            client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;
                        }

                        #region  "Payemnt line items"

                        List<LedgerJournal> journalTableList = new List<LedgerJournal>();
                        List<LedgerJournalTransaction> journalLineList = new List<LedgerJournalTransaction>();
                           
                        
                        for (int paymentCount = 0; paymentCount<Payments.Payments.Count(); paymentCount++)
                        {
                            //creating Journal list 
                            journalLineList.AddRange(GetLedgerJournalTransactionData(Payments.Payments[paymentCount], ref  sbErrorData));
                        }

                        journalTableList.Add(new LedgerJournal()
                        {
                            JournalName = string.Empty, // journal Name will be set in the service itself from configuartion
                            LedgerJournalTransactionList = journalLineList
                        });
                        #endregion  "Payemnt line items"
                       

                        //assign journal list
                        CreatePaymentRequest createPaymentRequest = new CreatePaymentRequest
                        {
                            LedgerJournalList = journalTableList
                        };

                        System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender1, certificate, chain, sslPolicyErrors) => true); 


                        //Create Payment - call service
                        CreatePaymentResponse createPaymentResponse = new CreatePaymentResponse
                        {
                            ValidationDetail = client.CreatePayment(createPaymentRequest.LedgerJournalList)
                        };

                        //Check success 
                        if (createPaymentResponse.ValidationDetail.Result)
                        {
                            Log.Info("Payment created Successfully by windows service for Payments(PaymentsID)=" + Payments.Payments[0].ID + "; lastupdate ids=" + Payments.MostRecentUpdateInstant);
                        }
                        else // error in wrapper
                        {
                            var errorMessage = createPaymentResponse.ValidationDetail.ErrorList[0].ToString();
                            Log.Info(errorMessage);
                            Log.Error("Error:CreatewNewPayment() : " + errorMessage + " ; payment created failed by windows service for Payments(PaymentsID)=" + Payments.Payments[0].ID + "; lastupdate ids=" + Payments.MostRecentUpdateInstant);
                            Log.Error("ERROR DATA: " + sbErrorData.ToString());
                        }
                        #endregion
                    }
                    catch (System.ServiceProcess.TimeoutException exception)
                    {
                        Log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                        Log.Error("-------------------------------------------------------------------------------------------");
                        client.Abort();
                    }
                    catch (CommunicationException exception)
                    {
                        Log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                        Log.Error("-------------------------------------------------------------------------------------------");
                        client.Abort();
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Error:CreatewNewPayment() : " + ex.Message + " ; payment created failed by windows service for Payments(PaymentsID)=" + Payments.Payments[0].ID + "; lastupdate ids=" + Payments.MostRecentUpdateInstant);
                        Log.Error("ERROR DATA: " + sbErrorData.ToString());
                        Log.Error("-------------------------------------------------------------------------------------------");
                        client.Abort();
                    }
                    finally
                    {
                        Log.Info("Enter CreatewNewPayment(): after loop lastUpdate=" + Payments.MostRecentUpdateInstant);
                        UpdateMostRecentValue(type, Payments.MostRecentUpdateInstant);
                    }

            }
            #endregion
        }

        /// <summary>
        /// Create ledger payments
        /// </summary>
        /// <param name="Payments"></param>
        private void CreateNewPayment1(GetPaymentsResponse Payments)
        {
            #region
            string type = "payments";

            
            //looping payments
           // for (int paymentCount = 0; paymentCount < Payments.Payments.Count(); paymentCount++)
            // Create only one payment per day - use frequency ( 1 for 1 payment per day) to loop the payments
           // if(Payments.Payments.Count()>0)
            for (int paymentCount = 0; paymentCount < PaymentFrequency; paymentCount++)
            {

                StringBuilder sbErrorData = new StringBuilder();

                // Set up Ledger Journal Client
                LedgerJournalClient client = new LedgerJournalClient();
                    try
                    {
                        #region "Create"

                        #region  "Payemnt line items"

                        List<LedgerJournal> journalTableList = new List<LedgerJournal>();
                        List<LedgerJournalTransaction> journalLineList = new List<LedgerJournalTransaction>();

                        //creating Journal list 
                        journalLineList = GetLedgerJournalTransactionData(Payments.Payments[paymentCount], ref  sbErrorData);
                        journalTableList.Add(new LedgerJournal()
                        {
                            JournalName = string.Empty, // journal Name will be set in the service itself from configuartion
                            LedgerJournalTransactionList = journalLineList
                        });

                        #endregion  "Payemnt line items"
                        //// implemented credentials starts

                        if (client.ClientCredentials != null)
                        {
                            client.ClientCredentials.UserName.UserName = serviceUserid;
                            client.ClientCredentials.UserName.Password = servicePassword;
                            client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;
                        }

                        //assign journal list
                        CreatePaymentRequest createPaymentRequest = new CreatePaymentRequest
                        {
                            LedgerJournalList = journalTableList
                        };
                        System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender1, certificate, chain, sslPolicyErrors) => true); 


                        //Create Payment - call service
                        CreatePaymentResponse createPaymentResponse = new CreatePaymentResponse
                        {
                            ValidationDetail = client.CreatePayment(createPaymentRequest.LedgerJournalList)
                        };

                        //Check success 
                        if (createPaymentResponse.ValidationDetail.Result)
                        {
                            Log.Info("Payment created Successfully by windows service for Payments(PaymentsID)=" + Payments.Payments[0].ID + "; lastupdate ids=" + Payments.MostRecentUpdateInstant);
                        }
                        else // error in wrapper
                        {
                            var errorMessage = createPaymentResponse.ValidationDetail.ErrorList[0].ToString();
                            Log.Info(errorMessage);
                            Log.Error("Error:CreatewNewPayment() : " + errorMessage + " ; payment created failed by windows service for Payments(PaymentsID)=" + Payments.Payments[0].ID + "; lastupdate ids=" + Payments.MostRecentUpdateInstant);
                            Log.Error("ERROR DATA: " + sbErrorData.ToString());
                        }
                        #endregion
                    }
                    catch (System.ServiceProcess.TimeoutException exception)
                    {
                        Log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                        Log.Error("-------------------------------------------------------------------------------------------");
                        client.Abort();
                    }
                    catch (CommunicationException exception)
                    {
                        Log.Error(string.Format("Error CreateInvoice(): {0}", exception.Message));
                        Log.Error("-------------------------------------------------------------------------------------------");
                        client.Abort();
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Error:CreatewNewPayment() : " + ex.Message + " ; payment created failed by windows service for Payments(PaymentsID)=" + Payments.Payments[0].ID + "; lastupdate ids=" + Payments.MostRecentUpdateInstant);
                        Log.Error("ERROR DATA: " + sbErrorData.ToString());
                        Log.Error("-------------------------------------------------------------------------------------------");
                        client.Abort();
                    }
                    finally
                    {
                        Log.Info("Enter CreatewNewPayment(): after loop lastUpdate=" + Payments.MostRecentUpdateInstant);
                        UpdateMostRecentValue(type, Payments.MostRecentUpdateInstant);
                    }

            }
            #endregion
        }

        /// <summary>
        /// Create Ledger journal transaction data
        /// </summary>
        /// <param name="Payments"></param>
        /// <returns></returns>
        public List<LedgerJournalTransaction> GetLedgerJournalTransactionData(Payment Payments, ref  StringBuilder errorData)
        {

            List<LedgerJournalTransaction> ledgerJournalTransactionList = new List<LedgerJournalTransaction>();
            //  for (int paymentsCount = 0; paymentsCount < Payments.Payments.Count(); paymentsCount++)
            //  {
            //getting autorize date from the payments object
            //string AuthorizedDate = Convert.ToString(Payments.AuthorizedDate);

            //getting payment id from the payments object
            int id = Payments.ID;

            //getting payment type from the payments object
            var paymentType = Payments.PaymentType.Value.ToString();

            //getting submiotted date from the payments object
            string submittedDate = Convert.ToString(Payments.SubmittedDate);

            #region "Error data"
            errorData.Append("Payments.ID: ");
            errorData.Append(Payments.ID);

            errorData.Append(";Confirmation #: ");
            errorData.Append(Payments.ConfirmationNumber);

            errorData.Append("; PaymentType: ");

            if (paymentType.ToLower() == "credit")
                paymentType = "CreditCard";
            errorData.Append(paymentType);

            errorData.Append("; SubmittedDate: ");
            errorData.Append(submittedDate);
            #endregion "Error data"

            #region "For loop for payments"
            //looping invoices for making ledger transaction list
            for (int invoiceCount = 0; invoiceCount < Payments.Invoices.Count(); invoiceCount++)
            {
                LedgerJournalTransaction objLedgerJournalTransaction = new LedgerJournalTransaction();

                objLedgerJournalTransaction.Description = string.Empty;

                objLedgerJournalTransaction.AccountType = AccountType;

                objLedgerJournalTransaction.PaymentId = id.ToString();

                 objLedgerJournalTransaction.PaymentConfirmationNumber = Payments.ConfirmationNumber; //added payment confirmation #

                objLedgerJournalTransaction.PaymentMode = paymentType;

                objLedgerJournalTransaction.Debit = 0;

                objLedgerJournalTransaction.OffsetAccountType = offsetAccountType;

                if (!string.IsNullOrEmpty(submittedDate))
                    objLedgerJournalTransaction.TransactionDate = Convert.ToDateTime(submittedDate);

                objLedgerJournalTransaction.InvoiceId = Payments.Invoices[invoiceCount].InvoiceNumber;

                objLedgerJournalTransaction.Credit = Payments.Invoices[invoiceCount].Amount;

                //if (!string.IsNullOrEmpty(Amount))
                //    objLedgerJournalTransaction.Credit = Convert.ToDecimal(Amount);
                //else
                //    objLedgerJournalTransaction.Credit = 0;

                string billableAccountId = Convert.ToString(Payments.Invoices[invoiceCount].BillableAccountId);

                objLedgerJournalTransaction.LedgerDimension = new LedgerDimension() { Account = billableAccountId, DisplayValue = billableAccountId };

                #region "Error data"

                errorData.Append("; AccountType: ");
                errorData.Append(AccountType);

                errorData.Append("; Credit: ");
                errorData.Append(objLedgerJournalTransaction.Credit);

                errorData.Append("; AccountType: ");
                errorData.Append(AccountType);

                errorData.Append("; offsetAccountType: ");
                errorData.Append(offsetAccountType);

                errorData.Append("; InvoiceId: ");
                errorData.Append(objLedgerJournalTransaction.InvoiceId);

                errorData.Append("; billableAccountId: ");
                errorData.Append(billableAccountId);

                #endregion "Error data"

                string _displayValue = OffSetAccount;

                ////since Ledger dimension default to AX - on  Gustavo-Apr-30

                objLedgerJournalTransaction.OffsetLedgerDimension = new LedgerDimension()
                {
                    Account = _displayValue, // "200102", //PaymentCustomerAccount,//"1359476",
                    DisplayValue = _displayValue
                    //DisplayValue = PaymentCustomerAccount,//"1359476"
                    //Values = offsetDefaultDimension 200102
                    //Account = "200102", //PaymentCustomerAccount,//"1359476",
                    //DisplayValue = "200102"
                };

                ledgerJournalTransactionList.Add(objLedgerJournalTransaction);
            }
            #endregion "For loop for payments"
            //}
            return ledgerJournalTransactionList;
        }

        #endregion "Payments"

    }
}