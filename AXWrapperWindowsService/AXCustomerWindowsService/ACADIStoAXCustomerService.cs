﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 02/12/2015
'Description: Service creates a daily log file, Poll billableParties from ACADIS service and create/update customers in AX
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.ServiceProcess;
using System.Text;
using System.Xml;
using AcadisDataServices.Client;
using AcadisDataServices.Client.Accounting;
using AXWrapperWindowsService.CustomerServiceReference;
using log4net;
using log4net.Config;

namespace AXWrapperWindowsService
{
    public partial class ACADIStoAXCustomerService : ServiceBase
    {

        #region "Constants"

        const string GroupCodeSTUDENTS = "STUDENTS";
        const string GroupCodeGrant = "GRANT";
        const string CurrencyCode = "USD";
        const string PhoneType = "PHONE";
        const string PhoneRoles = "Business";
        const string EmailType = "EMAIL";
        const string EmailRoles = "Business";
        //type is billable parties 
        const string type = "BillableParties";

        #endregion "Constants"

        #region "variables"
        //Page size default to 1000
        int PAGE_SIZE = 1000;

        /// <summary>
        /// service user id   to access invoice service
        /// </summary>
        private string serviceUserid;

        /// <summary>
        /// Service password to access invoice service
        /// </summary>
        private string servicePassword;

        /// <summary>
        /// To prevent multiple execution of timer events
        /// </summary>
        private volatile bool isTimerExecuting;

        //Uses to tracking the actions 
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        //Timer for poll the data periodically
        private System.Timers.Timer timer;

        //This statement is used to set interval to 1 minute (= 60,000 milliseconds) - 1 sec = 1000
        private double interval = 1000;



        #endregion "variables"

        #region "Service Events"
        /// <summary>
        /// Windows service on start set log4 net configuration, timer
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {

            //user id and password from config file and decrypting 
            serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
            servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

            //retrieve the page size from config
            PAGE_SIZE = Convert.ToInt32(Convert.ToString(ConfigurationManager.AppSettings["PAGE_SIZE"]));

            //This statement is used to set interval to 1 minute (= 60,000 milliseconds) - 1 sec = 1000
            interval = Convert.ToDouble(ConfigurationManager.AppSettings["ServiceInterval"]);

            //if timer is running stop when stop the service
            if (timer != null)
                timer.Stop();

            XmlConfigurator.Configure();
            ////Debugger.Launch();;
            Log.Debug("Enter OnStart() Windows Service Started on " + DateTime.Now);

            //Sert timer
            SetTimer();

            // KeepAlive to prevent garbage collection from occurring before the method ends. 
            GC.KeepAlive(timer);
        }

        /// <summary>
        /// Service stop event
        /// </summary>
        protected override void OnStop()
        {

            //if timer is running stop when stop the service
            if (timer != null)
                timer.Stop();

            Log.Debug("Windows Service Stopped on " + DateTime.Now);
        }
        #endregion "Service Events"

        #region "Timer Events"
        /// <summary>
        /// Set the timer to schedule specific interval
        /// </summary>
        private void SetTimer()
        {
            if (timer == null)
            {
                timer = new System.Timers.Timer();
                timer.AutoReset = true;
                timer.Interval = interval;
                //set event 
                timer.Elapsed += new System.Timers.ElapsedEventHandler(TimerElapsedEvent);
                timer.Start();
            }
        }
        /// <summary>
        /// Timer elapsed event , this will call and execute the process
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void TimerElapsedEvent(object source, System.Timers.ElapsedEventArgs e)
        {

            Log.Debug("Enter TimerElapsedEvent()");

            if (isTimerExecuting)
                return;

            isTimerExecuting = true;
            try
            {
                //Migrate Customers /billable parties
                MigrateBillabaleParties();
            }
            catch (Exception ex)
            {
                Log.Error("Error TimerElapsedEvent() " + ex.Message);
            }
            finally
            {
                isTimerExecuting = false;
                Log.Debug("Exit  TimerElapsedEvent()");
            }
        }

        #endregion "Timer Events"

        #region "XML file Get and Update"
        /// <summary>
        /// Update the Last updated value in xml file for each type "Billable parties , BillingItems, paid invoices ets...use this..
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public void UpdateMostRecentValue(string type, long value)
        {
            try
            {
                //File path
                string path = Convert.ToString(ConfigurationManager.AppSettings["MostRecentValueTrackingFile"]);
                Log.Debug("Enter UpdateMostRecentValue(): Type=" + type + " , lastUpdate=" + value);
                if (!string.IsNullOrEmpty(path))
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(path);

                    //var nodes = xmlDoc.SelectNodes("/MostRecentUpdateInstant/BillableParties");
                    var nodes = xmlDoc.SelectNodes("/MostRecentUpdateInstant/" + type);

                    foreach (XmlNode ch1 in nodes)
                    {
                        if (ch1 != null)
                        {
                            ch1.InnerText = Convert.ToString(value);
                            Log.Debug("Enter UpdateMostRecentValue(): Before save Type=" + type + " , lastUpdate=" + value);
                            xmlDoc.Save(path);
                            Log.Debug("Enter UpdateMostRecentValue(): After save Type=" + type + " , lastUpdate=" + value);
                            break;
                        }
                    }
                }
                else
                {
                    Log.Error("Enter UpdateMostRecentValue(): File not found" + DateTime.Now);
                    this.OnStop();
                }
            }
            catch (Exception)
            {
                Log.Error("Enter UpdateMostRecentValue(): File not found" + DateTime.Now);
                this.OnStop();
            }

        }

        /// <summary>
        /// Read Last updated value from xml file for each type "Billable parties , BillingItems, paid invoices ets...use this..
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public long GetMostRecentValue(string type)
        {
            Log.Debug("Enter GetMostRecentValue(" + type + "):  ");
            string path = Convert.ToString(ConfigurationManager.AppSettings["MostRecentValueTrackingFile"]);

            long _value = -1;
            try
            {
                //File path

                if (!string.IsNullOrEmpty(path))
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(path);
                    Log.Debug("Enter GetMostRecentValue(" + type + "): XMLpath= " + path);

                    //var nodes = xmlDoc.SelectNodes("/MostRecentUpdateInstant/BillableParties");
                    var nodes = xmlDoc.SelectNodes("/MostRecentUpdateInstant/" + type);
                    Log.Debug("Exit GetMostRecentValue(" + type + "):  ");
                    _value = (from XmlNode ch1 in nodes
                        where ch1 != null
                        where !string.IsNullOrEmpty(ch1.InnerText)
                        select Convert.ToInt64(ch1.InnerText)).FirstOrDefault();
                }
                else
                {
                    Log.Error("ERROR: GetMostRecentValue(" + type + "): " + path + " File Not found ");
                    this.Stop();
                    return -1;
                }
            }
            catch (Exception ex)
            {
                Log.Error("ERROR: GetMostRecentValue(" + type + "): File" + path + " File Not found " + ex.Message);
                this.Stop();
                return -1;
            }
            return _value;
        }

        #endregion "XML file Get and Update"

        #region "Billable Parties"

        /// <summary>
        /// Poll Billabale parties from ACADIS and call AX Wrapper service to SAVE Customer
        /// </summary>
        private void MigrateBillabaleParties()
        {
            Log.Debug("Enter MigrateBillabaleParties():  " + DateTime.Now);
            #region""BillableParties"

            //TimeSpan BILLABLE_PARTIES_POLLING_INTERVAL = TimeSpan.FromSeconds(10);

            // setup ACADIS Configuration
            var config = new AcadisDataServicesClientConfiguration(ConfigurationManager.AppSettings);
            var client = new AccountingClient(config);

            long lastUpdate = GetMostRecentValue(type); ;
            long originallastUpdate = lastUpdate;
            //Warning! Calling the service with lastUpdate == 0 will get ALL billable parties. Ideally, this wouldn't happen more often than necessary. 
            Log.Debug("Enter MigrateBillabaleParties(): lastUpdate= " + lastUpdate);

            // Poll ACADIS service- get billable parties
            GetBillablePartiesResponse billableParties = client.GetBillableParties(lastUpdate, PAGE_SIZE);
            Log.Info("Enter MigrateBillabaleParties(): lastUpdate= " + lastUpdate);
            do
            {
                lastUpdate = billableParties.MostRecentUpdateInstant;


                //Process billable parties 
                if (billableParties.BillableParties.Any())
                    ProcessBillableParty(billableParties.BillableParties, lastUpdate); //

                //call next page based on the last received data
                billableParties = client.GetNextPage(billableParties);

            } while (billableParties != null);

            Log.Info("Enter MigrateBillabaleParties(): lastUpdate= " + lastUpdate);


            Log.Debug("Enter MigrateBillabaleParties(): after loop lastUpdate=" + lastUpdate);

            if (originallastUpdate != lastUpdate)
                UpdateMostRecentValue(type, lastUpdate);

            Log.Debug("Exit MigrateBillabaleParties()");

            #endregion""BillableParties"
        }

        /// <summary>
        /// Process billable parties and call AX wrapper serive to create/update customer
        /// </summary>
        /// <param name="billableParties"></param>
        /// <param name="lastUpdateId"></param>
        private void ProcessBillableParty(BillableParty[] billableParties, long lastUpdateId)
        {

            Log.Debug("Enter ProcessBillableParty()");



            //looping each billable parties
            foreach (var eachBillableParty in billableParties)
            {

                string BillableAccountID = Convert.ToString(eachBillableParty.BillableAccountID);
                string LastUpdateInstant = eachBillableParty.LastUpdateInstant.ToString();
                DateTime ststart = DateTime.Now;

                Log.Info("-------------------------------------------------------------------------------------------");
                Log.Info("LastUpdateInstant ( " + LastUpdateInstant + ") start time=" + ststart);

                //   Create a new Customer, 
                var addNewCustomer = new Customer
                {
                    AccountNumber = BillableAccountID,
                    AS400ID = Convert.ToString(eachBillableParty.ReferenceNumber),
                    FirstName = eachBillableParty.FirstName,
                    LastName = eachBillableParty.LastName,
                    MiddleName = eachBillableParty.MiddleName,
                    Currency = CurrencyCode,
                    Suffix = eachBillableParty.Suffix,
                    IsDisable = false,
                    CustomerType = eachBillableParty.BillablePartyType.ToString(),
                    Group = (eachBillableParty.BillablePartyType.ToString().ToUpper() == "GRANT") ? GroupCodeGrant : GroupCodeSTUDENTS,

                    OrganizationName = eachBillableParty.Name
                };
                addNewCustomer.IsParticipating = false;

                #region "Error Data"
                var sbErrorFormat = new StringBuilder();
                sbErrorFormat.Append("BillableAccountID: " + Convert.ToString(eachBillableParty.BillableAccountID));
                sbErrorFormat.Append("; FirstName: " + eachBillableParty.FirstName);
                sbErrorFormat.Append("; LastName: " + eachBillableParty.LastName);
                sbErrorFormat.Append("; Middlename: " + eachBillableParty.MiddleName);
                sbErrorFormat.Append("; BillablePartyType: " + eachBillableParty.BillablePartyType);
                sbErrorFormat.Append("; Name: " + Convert.ToString(eachBillableParty.Name));
                sbErrorFormat.Append("; Group: " + Convert.ToString(eachBillableParty.BillablePartyType));
                sbErrorFormat.Append("; AS400ID: " + Convert.ToString(eachBillableParty.ReferenceNumber));

                #endregion "Error Data"

             

                Log.Debug("Enter ProcessBillableParty() : Added Customer");

                //Fund Billing Address

                addNewCustomer.AddressList = new List<CustomerServiceReference.Address>();
                ////Add the address in address list of customer object.
                addNewCustomer.AddressList = ConvertCustomerAddressData(eachBillableParty, ref sbErrorFormat);

                Log.Debug("Enter ProcessBillableParty(): Added Addresses count=" + addNewCustomer.AddressList.Count());

                if (eachBillableParty.DependentFundInformation != null)
                {
                    addNewCustomer.FiscalOfficer = new FiscalOfficer();
                    if (eachBillableParty.DependentFundInformation.FiscalOfficerAccountID != null)
                        addNewCustomer.FiscalOfficer.AccountID =
                            Convert.ToString(eachBillableParty.DependentFundInformation.FiscalOfficerAccountID);

                    addNewCustomer.FiscalOfficer.FirstName =eachBillableParty.DependentFundInformation.FiscalOfficerFirstName;
                    addNewCustomer.FiscalOfficer.LastName = eachBillableParty.DependentFundInformation.FiscalOfficerLastName;
                    addNewCustomer.FiscalOfficer.MiddleName =eachBillableParty.DependentFundInformation.FiscalOfficerMiddleName;
                    addNewCustomer.FiscalOfficer.Name = eachBillableParty.DependentFundInformation.FiscalOfficerName;
                    addNewCustomer.FiscalOfficer.Suffix = eachBillableParty.DependentFundInformation.FiscalOfficerSuffix;

                    addNewCustomer.Secretary = new Secretary();
                    if (eachBillableParty.DependentFundInformation.SecretaryAccountID != null)
                        addNewCustomer.Secretary.AccountID =
                            Convert.ToString(eachBillableParty.DependentFundInformation.SecretaryAccountID);


                    addNewCustomer.Secretary.FirstName = eachBillableParty.DependentFundInformation.SecretaryFirstName;
                    addNewCustomer.Secretary.LastName = eachBillableParty.DependentFundInformation.SecretaryLastName;
                    addNewCustomer.Secretary.MiddleName = eachBillableParty.DependentFundInformation.SecretaryMiddleName;
                    addNewCustomer.Secretary.Name = eachBillableParty.DependentFundInformation.SecretaryName;
                    addNewCustomer.Secretary.Suffix = eachBillableParty.DependentFundInformation.SecretarySuffix;

                    addNewCustomer.IsParticipating = eachBillableParty.DependentFundInformation.IsParticipating;
                    if (eachBillableParty.DependentFundInformation.PropertyValuation != null)
                        addNewCustomer.PropertyValuation =
                            Convert.ToDecimal(eachBillableParty.DependentFundInformation.PropertyValuation);

                    addNewCustomer.AddressList.AddRange(
                        ConvertCustomerFundAddress(eachBillableParty.DependentFundInformation.FundBillingAddress,
                            ref sbErrorFormat));

                    #region "Error Data - Fiscal Officer and Secreatary"

                    sbErrorFormat.Append("; Fiscal Officer: FirstName: " + addNewCustomer.FiscalOfficer.FirstName);
                    sbErrorFormat.Append("; Fiscal Officer: LastName: " + addNewCustomer.FiscalOfficer.LastName);
                    sbErrorFormat.Append("; Fiscal Officer: Middlename: " + addNewCustomer.FiscalOfficer.MiddleName);
                    sbErrorFormat.Append("; Fiscal Officer: Name: " + addNewCustomer.FiscalOfficer.Name);
                    sbErrorFormat.Append("; Fiscal Officer: Account ID: " + addNewCustomer.FiscalOfficer.AccountID);
                    sbErrorFormat.Append("; Fiscal Officer: Suffix: " + addNewCustomer.FiscalOfficer.Suffix);

                    sbErrorFormat.Append("; Secretary: FirstName: " + addNewCustomer.Secretary.FirstName);
                    sbErrorFormat.Append("; Secretary: LastName: " + addNewCustomer.Secretary.LastName);
                    sbErrorFormat.Append("; Secretary: Middlename: " + addNewCustomer.Secretary.MiddleName);
                    sbErrorFormat.Append("; Secretary: Name: " + addNewCustomer.Secretary.Name);
                    sbErrorFormat.Append("; Secretary: Account ID: " + addNewCustomer.Secretary.AccountID);
                    sbErrorFormat.Append("; Secretary: Suffix: " + addNewCustomer.Secretary.Suffix);
                    sbErrorFormat.Append("; IsParticipating: " + addNewCustomer.IsParticipating);
                    sbErrorFormat.Append("; PropertyValuation " + addNewCustomer.PropertyValuation);

                    #endregion "Error Data"
                }

                addNewCustomer.ContactList = new List<Contact>();

                addNewCustomer.ContactList = ConvertBPPhoneNumbersToContacts(eachBillableParty, ref sbErrorFormat);

                Log.Debug("Enter ProcessBillableParty(): Added PhoneNumbers List count=" + eachBillableParty.PhoneNumbers.Count());

                addNewCustomer.ContactList.AddRange(ConvertBPEmailAddressToContact(eachBillableParty, ref sbErrorFormat));
                Log.Debug("Enter ProcessBillableParty(): Added Email List count=" + eachBillableParty.EmailAddresses.Count());

                List<Customer> customerList = new List<Customer>();
                customerList.Add(addNewCustomer);
                #region " Call AX wrparrer - to create customer"
                // Set up customer client
                CustomerClient client = new CustomerClient();
                try
                {
                    if (client.ClientCredentials != null)
                    {
                        client.ClientCredentials.UserName.UserName = serviceUserid;
                        client.ClientCredentials.UserName.Password = servicePassword;
                        client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode =
                            X509CertificateValidationMode.None;
                    }

                    //Call AX wrapper service to save customer information
                    SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest {Customer = customerList};
                    //Create customer factory object

                    Log.Info("Enter ProcessBillableParty(): Before calling client.SaveCustomer(saveCustomerRequest)");

                    System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender1, certificate, chain, sslPolicyErrors) => true); 

                    //Call save customer
                    SaveCustomerResponse saveCustomerResponse = client.SaveCustomer(saveCustomerRequest);

                    Log.Info("Enter ProcessBillableParty(): After calling client.SaveCustomer(saveCustomerRequest)");

                    if (saveCustomerResponse.ValidationDetail.Result)
                    {
                        Log.Info("Success SAVECUSTOMER(): lastUpdateId=" + eachBillableParty.LastUpdateInstant);
                        DateTime endtime = DateTime.Now;
                        Log.Info("LastUpdateInstant ( " + LastUpdateInstant + ") end time=" + endtime);
                        Log.Info(
                            string.Format(
                                "Total Duration to save the customer for LastUpdateInstant ({0}) in milli seconds {1}",
                                LastUpdateInstant, (endtime - ststart).TotalMilliseconds));
                        Log.Info(
                            "-------------------------------------------------------------------------------------------");
                    }
                    else
                    {
                        Log.Error(string.Format("Error SAVECUSTOMER(): LastUpdateInstant ={0} ERROR: {1} ",
                            eachBillableParty.LastUpdateInstant, saveCustomerResponse.ValidationDetail.ErrorList[0]));
                        Log.Error("ERROR DATA: " + sbErrorFormat);

                        DateTime endtime = DateTime.Now;
                        Log.Info(string.Format("LastUpdateInstant ({0}) end time={1})", eachBillableParty.LastUpdateInstant,
                            endtime));
                        Log.Info(
                            string.Format(
                                "Total Duration to process the customer for LastUpdateInstant ({0}) in milli seconds {1}",
                                LastUpdateInstant, (endtime - ststart).TotalMilliseconds));
                        Log.Info("-------------------------------------------------------------------------------------------");
                    }

                }
                    //catch (System.ServiceProcess.TimeoutException exception)
                    //{
                    //    Log.Error(string.Format("Error ProcessBillableParty(): LastUpdate Instant ({0})  Error: {1}  ", LastUpdateInstant , exception.Message));
                    //    Log.Error(string.Format("ERROR DATA: {0}" , sbErrorFormat.ToString()));
                    //    Log.Error("-------------------------------------------------------------------------------------------");
                    //    client.Abort();
                    //}
                    //catch (CommunicationException exception)
                    //{
                    //    Log.Error(string.Format("Error ProcessBillableParty(): LastUpdateInstant ({0})  Error: {1}  ", LastUpdateInstant, exception.Message));
                    //    Log.Error(string.Format(" ERROR DATA: {0} ", sbErrorFormat.ToString()));
                    //    Log.Info("-------------------------------------------------------------------------------------------");
                    //    client.Abort();
                    //}
                catch (Exception ex)
                {
                    Log.Error(string.Format("Error ProcessBillableParty(): LastUpdateInstant ({0})  Error: {1}  ",
                        LastUpdateInstant, ex.Message));
                    Log.Error("ERROR DATA: " + sbErrorFormat);
                    Log.Error(
                        "-------------------------------------------------------------------------------------------");
                }

                #endregion " Call AX wrparrer - to create customer"

                UpdateMostRecentValue(type, lastUpdateId);
            }
            Log.Info("Exit ProcessBillableParty()");

        }
        /// <summary>
        /// Convert Billable party address to Customer asddress data
        /// </summary>
        /// <param name="billableParty"></param>
        /// <returns></returns>
        private List<CustomerServiceReference.Address> ConvertCustomerFundAddress(AcadisDataServices.Client.Accounting.Address address, ref StringBuilder sbErrorFormat)
        {
            List<CustomerServiceReference.Address> addressList = new List<CustomerServiceReference.Address>();
            Log.Debug("Enter ConvertCustomerAddressData()");

            // foreach (var eachBillableParty in address)
            //  {
            if (address != null)
            {
                string ACADISID = "1";
                string CountryRegionID = "USA";
                string street = address.Line1 + " " + Convert.ToString(address.Line2);
                //   Create a new Customer, 
                //if there is no street address do not create
                if (street.Trim().Length > 0)
                {
                    var addNewAddress = new CustomerServiceReference.Address
                    {
                        ACADISID = ACADISID,
                        CountryRegionID = CountryRegionID,
                        City = address.City,
                        IsPrimaryAddress = false,
                        LocationName = address.AddressType,
                        // Locator = address.,
                        Roles = "Delivery",
                        Street = street,
                        State = address.State,
                        ZipCode = address.Zip == null ? "" : address.Zip,
                        Type = "Delivery"
                    };
                    addressList.Add(addNewAddress);
                    Log.Debug("Exit ConvertCustomerAddressData()");
                }
                else
                    Log.Debug("No Street Address given , No Address created. Exit ConvertCustomerAddressData()");

                

                #region "If error happened this data will be available in the error log"
                sbErrorFormat.AppendLine();
                sbErrorFormat.Append("ACADISID: ");
                sbErrorFormat.Append(ACADISID);

                sbErrorFormat.Append("; City: ");
                sbErrorFormat.Append(address.City);

                sbErrorFormat.Append("; LocationName: ");
                sbErrorFormat.Append(address.AddressType);
                sbErrorFormat.Append("; Roles: Delivery");
               // sbErrorFormat.Append(address.AddressType);
                sbErrorFormat.Append("; Street: ");
                sbErrorFormat.Append(street);
                sbErrorFormat.Append("; Type: Delivery");
              //  sbErrorFormat.Append(address.AddressType);
                #endregion "If error happened this data will be available in the error log"
            }
            return addressList;
            //  }
        }

        private List<CustomerServiceReference.Address> ConvertCustomerAddressData(BillableParty billableParty, ref StringBuilder sbErrorFormat)
        {
            List<CustomerServiceReference.Address> addressList = new List<CustomerServiceReference.Address>();
            Log.Debug("Enter ConvertCustomerAddressData()");

            // foreach (var eachBillableParty in billableParty.MailingAddress)
            //  {
            if (billableParty.MailingAddress != null)
            {
                string ACADISID = "1";
                string CountryRegionID = "USA";
                string street = billableParty.MailingAddress.Line1 + " " + Convert.ToString(billableParty.MailingAddress.Line2);
                //   Create a new Customer, 
                //if there is no street address do not create
                if (street.Trim().Length > 0)
                {
                    var addNewAddress = new CustomerServiceReference.Address
                    {
                        ACADISID = ACADISID,
                        CountryRegionID = CountryRegionID,
                        City = billableParty.MailingAddress.City,
                        IsPrimaryAddress = true, // mailing address should be primary
                        LocationName = billableParty.MailingAddress.AddressType,
                        // Locator = BillableParty.MailingAddress.,
                        Roles = billableParty.MailingAddress.AddressType,
                        Street = street,
                        State = billableParty.MailingAddress.State,
                        ZipCode = billableParty.MailingAddress.Zip == null ? "" : billableParty.MailingAddress.Zip,
                        Type = billableParty.MailingAddress.AddressType
                    };
                    addressList.Add(addNewAddress);
                    Log.Debug("Exit ConvertCustomerAddressData()");
                }
                else
                    Log.Debug("No Street Address given , No Address created. Exit ConvertCustomerAddressData()");
                #region "If error happened this data will be available in the error log"
                sbErrorFormat.AppendLine();
                sbErrorFormat.Append("ACADISID: ");
                sbErrorFormat.Append(ACADISID);

                sbErrorFormat.Append("; City: ");
                sbErrorFormat.Append(billableParty.MailingAddress.City);

                sbErrorFormat.Append("; LocationName: ");
                sbErrorFormat.Append(billableParty.MailingAddress.AddressType);
                sbErrorFormat.Append("; Roles: ");
                sbErrorFormat.Append(billableParty.MailingAddress.AddressType);
                sbErrorFormat.Append("; Street: ");
                sbErrorFormat.Append(street);
                sbErrorFormat.Append("; Type: ");
                sbErrorFormat.Append(billableParty.MailingAddress.AddressType);
                #endregion "If error happened this data will be available in the error log"
            }
            return addressList;
            //  }
        }

        /// <summary>
        /// Convert Billable party PhoneNumbers To Contact 
        /// </summary>
        /// <param name="billableParty"></param>
        /// <returns></returns>
        private List<CustomerServiceReference.Contact> ConvertBPPhoneNumbersToContacts(BillableParty billableParty, ref StringBuilder sbErrorFormat)
        {
            string ACADISID = "1";

            var ContactList = new List<Contact>();
            foreach (var eachPhoneNumber in billableParty.PhoneNumbers)
            {
                string contactname = eachPhoneNumber.PhoneType;
                string Locator = eachPhoneNumber.Number;

                var addNewContact = new CustomerServiceReference.Contact
                {
                    ACADISID = ACADISID,
                    ContactName = contactname,
                    Roles = PhoneRoles,
                    Locator = Locator,
                    IsPrimary = eachPhoneNumber.IsPrimary,
                    //IsPrimary = false,
                    Type = PhoneType,
                };

                ContactList.Add(addNewContact);

                #region "If error happened this data will be available in the error log"

                sbErrorFormat.AppendLine();
                sbErrorFormat.Append("ACADISID: ");
                sbErrorFormat.Append(ACADISID);

                sbErrorFormat.Append("; ContactName: ");
                sbErrorFormat.Append(contactname);

                sbErrorFormat.Append("; PhoneType: ");
                sbErrorFormat.Append(PhoneType);

                sbErrorFormat.Append("; IsPrimary: ");
                sbErrorFormat.Append(eachPhoneNumber.IsPrimary);


                sbErrorFormat.Append("; Roles: ");
                sbErrorFormat.Append(PhoneRoles);

                sbErrorFormat.Append("; Locator: ");
                sbErrorFormat.Append(Locator);

                #endregion "If error happened this data will be available in the error log"
            }

            return ContactList;
            //return billableParty.PhoneNumbers.Select(eachPhoneNumber => new CustomerServiceReference.Contact
            //{
            //    ACADISID = ACADISID,
            //    ContactName = ,
            //    Roles = "Business",
            //    Locator = eachPhoneNumber.Number.ToString(),
            //    IsPrimary = eachPhoneNumber.IsPrimary,
            //    //IsPrimary = false,
            //    Type = "PHONE"
            //}).ToList();


            //List<CustomerServiceReference.Contact> ContactList =
            //    billableParty.PhoneNumbers.Select(eachPhoneNumber => new CustomerServiceReference.Contact
            //    {
            //        ACADISID = ACADISID,
            //        ContactName = eachPhoneNumber.PhoneType.ToString(),
            //        Roles = "Phone",
            //        Locator = eachPhoneNumber.Number.ToString(),
            //        IsPrimary = eachPhoneNumber.IsPrimary,
            //        Type = eachPhoneNumber.PhoneType.ToString(),
            //    }).ToList();
        }

        /// <summary>
        /// Convert Billable party email addresses To Contact list
        /// </summary>
        /// <param name="billableParty"></param>
        /// <returns></returns>
        private List<CustomerServiceReference.Contact> ConvertBPEmailAddressToContact(BillableParty billableParty, ref StringBuilder sbErrorFormat)
        {
            string ACADISID = "1";

            var contactList = new List<Contact>();

            foreach (var eachEmailAddress in billableParty.EmailAddresses)
            {
                //  string contactName = eachEmailAddress.Address;
                string contactname = "EMAIL";
                string Locator = eachEmailAddress.Address;

                var addNewContact = new CustomerServiceReference.Contact
                {
                    ACADISID = ACADISID,
                    ContactName = contactname,
                    Locator = Locator,
                    Roles = EmailRoles,
                    IsPrimary = eachEmailAddress.IsPrimary,
                    Type = EmailType,
                };
                contactList.Add(addNewContact);

                #region "If error happened this data will be available in the error log"
                sbErrorFormat.AppendLine();
                sbErrorFormat.Append("ACADISID: ");
                sbErrorFormat.Append(ACADISID);

                sbErrorFormat.Append("; ContactName: ");
                sbErrorFormat.Append(contactname);
                sbErrorFormat.Append("; Roles: ");
                sbErrorFormat.Append(EmailRoles);

                sbErrorFormat.Append("; IsPrimary: ");
                sbErrorFormat.Append(eachEmailAddress.IsPrimary);

                sbErrorFormat.Append("; Type: ");
                sbErrorFormat.Append(EmailType);
                #endregion "If error happened this data will be available in the error log"
            }
            return contactList;

            //return billableParty.EmailAddresses.Select(eachEmailAddress => new CustomerServiceReference.Contact
            //{
            //    ACADISID = ACADISID,
            //    ContactName = eachEmailAddress.Address,
            //    Roles = Roles,
            //    IsPrimary = eachEmailAddress.IsPrimary,
            //    //IsPrimary = false,
            //    Type = EmailType,
            //}).ToList();
        }
        #endregion "Billable Parties"
    }
}