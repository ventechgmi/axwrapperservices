﻿/******************************************************************************************
'Created By: ANANTHAKUMAR Subramani
'Created Date: 02/12/2015
'Description: Service creates a daily log file, Poll billableParties,InvoicableItems and Paidinvoices from ACADIS service
'
'
'Change History:
'------------------------------------------------------------------------------------------
'Changed By             Changed Date            Description
'------------------------------------------------------------------------------------------
'
'
'******************************************************************************************/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceModel.Security;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Xml;
using AcadisDataServices.Client;
using AcadisDataServices.Client.Accounting;
using AXWrapperWindowsService.CustomerServiceReference;
using AXWrapperWindowsService.InvoiceServiceReference;
using AXWrapperWindowsService.LedgerJournalService;
using log4net;
using log4net.Config;
using Newtonsoft.Json;
using DefaultDimension = AXWrapperWindowsService.InvoiceServiceReference.DefaultDimension;
using Formatting = System.Xml.Formatting;
using System.Timers;

namespace AXWrapperWindowsService
{
    public partial class ACADIStoAXService : ServiceBase
    {
        const string CurrencyCode = "USD";
        //Page size default to 1000
        int PAGE_SIZE = 1000;
        string serviceUserid = string.Empty;
        string servicePassword = string.Empty;
        // TimeSpan POLLING_INTERVAL = TimeSpan.FromSeconds(10);

        private volatile bool isTimerExecuting;

        //Uses to tracking the actions 
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        
        //Timer for poll the data periodically
        private System.Timers.Timer timer;

        string methodType = string.Empty;

        public ACADIStoAXService()
        {
            InitializeComponent();
           
            //user id and password from config file and decrypting 
            serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
            servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));
            
            //retrieve the page size from config
            PAGE_SIZE = Convert.ToInt32(Convert.ToString(ConfigurationManager.AppSettings["PAGE_SIZE"]));

            methodType = Convert.ToString(ConfigurationManager.AppSettings["MethodType"]);
        }
        
        /// <summary>
        /// Windows service on start set log4 net configuration, timer
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {

            //if timer is running stop when stop the service
            if (timer != null)
                timer.Stop();

            XmlConfigurator.Configure();
            Debugger.Launch();
            Log.Debug("Enter OnStart() Windows Service Started on " + DateTime.Now.ToString());
         
           //Sert timer
           //  SetTimer();

            //////Migrate Billable parties
             //MigrateBillabaleParties();

            //////Migrate Invoices
            //MigrateInvoiceableItems();

            //////Migrate Payments
            //MigratePayments();

            ////// Migrate cancellaed invoices
           // MigrateCancellations();
           
            // KeepAlive to prevent garbage collection from occurring before the method ends. 
            GC.KeepAlive(timer);

        }

       
       /// <summary>
        /// Set the timer to schedule specific interval
        /// </summary>
        private void SetTimer()
        {
            if (timer == null)
            {
                //retrieve polling interval
                //POLLING_INTERVAL = TimeSpan.FromSeconds(Convert.ToDouble(ConfigurationManager.AppSettings["ServiceInterval"]));

                //This statement is used to set interval to 1 minute (= 60,000 milliseconds) - 1 sec = 1000
                double interval = Convert.ToDouble(ConfigurationManager.AppSettings["ServiceInterval"]);

                timer = new System.Timers.Timer();
                timer.AutoReset = true;

                timer.Interval = interval;
                //set event 
                timer.Elapsed += new System.Timers.ElapsedEventHandler(TimerElapsedEvent);
                timer.Start();
            }
        }
        /// <summary>
        /// Timer elapsed event , this will call and execute the process
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void TimerElapsedEvent(object source, System.Timers.ElapsedEventArgs e)
        {

            Log.Debug("Enter TimerElapsedEvent()");

            if (isTimerExecuting)
                return;
            isTimerExecuting = true;

            try
            {

                switch (methodType.ToLower())
                {
                    case "billableparties":
                        //Migrate Billable parties
                        MigrateBillabaleParties();

                        break;
                    case "billingitems":
                        //Migrate Invoices
                        MigrateInvoiceableItems();

                        break;
                    case "payments":
                        //Migrate Payments
                        MigratePayments();

                        break;
                    case "cancellations":
                        // Migrate cancellaed invoices
                        MigrateCancellations();

                        break;
                }
            }

            catch (Exception ex)
            {
                Log.Error("Error TimerElapsedEvent() " + ex.Message);
            }
            finally
            {
                isTimerExecuting = false;
                Log.Debug("Exit  TimerElapsedEvent()");
            }
            

        }

        //protected override void OnStart(string[] args)
        //{
        //    //if timer is running stop when stop the service
        //    if (timer != null)
        //        timer.Stop();

        //    XmlConfigurator.Configure();
        //    Debugger.Launch();
        //    Log.Debug("Enter OnStart() Windows Service Started on " + DateTime.Now.ToString());

        //    //set timer
        //    SetTimer();


        //}

        protected override void OnStop()
        {

            //ContinueTriggering = false;
            //_trigger.Stop();
            Log.Info("Windows Service Stopped on " + DateTime.Now.ToString());
        }

        #region "Billable Parties"

        /// <summary>
        /// Poll Billabale parties from ACADIS and call AX Wrapper service to SAVE Customer
        /// </summary>
        private void MigrateBillabaleParties()
        {
            Log.Debug("Enter MigrateBillabaleParties():  " + DateTime.Now.ToString());
            #region""BillableParties"

            //TimeSpan BILLABLE_PARTIES_POLLING_INTERVAL = TimeSpan.FromSeconds(10);

            // setup ACADIS Configuration
            var config = new AcadisDataServicesClientConfiguration(ConfigurationManager.AppSettings);
            var client = new AccountingClient(config);


            //type is billable parties 
            string type = "BillableParties";

            long lastUpdate = GetMostRecentValue(type); ;
            //Warning! Calling the service with lastUpdate == 0 will get ALL billable
            //parties. Ideally, this wouldn't happen more often than necessary. Implementers
            //should consider storing this somewhere so that we don't request all billable
            //parties when the process restarts.
            Log.Debug("Enter MigrateBillabaleParties(): lastUpdate= " + lastUpdate);


            GetBillablePartiesResponse result;
            do
            {
                result = client.GetBillableParties(lastUpdate, PAGE_SIZE); // Poll ACADIS service- get billable parties
                lastUpdate = result.MostRecentUpdateInstant; // assign last update value from each billable party
                ProcessBillableParty(result.BillableParties, lastUpdate); //
                Log.Debug("Enter MigrateBillabaleParties(): in Looping and processed " + lastUpdate);

            } while (result.BillableParties.Length == PAGE_SIZE);

            Log.Debug("Enter MigrateBillabaleParties(): after loop lastUpdate=" + lastUpdate);
            UpdateMostRecentValue(type, lastUpdate);
            //Thread.Sleep(BILLABLE_PARTIES_POLLING_INTERVAL);

            #endregion""BillableParties"
        }
        /// <summary>
        /// Update the Last updated value in xml file for each type "Billable parties , BillingItems, paid invoices ets...use this..
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public void UpdateMostRecentValue(string type, long value)
        {
            //File path
            string path = Convert.ToString(ConfigurationManager.AppSettings["MostRecentValueTrackingFile"]);
            Log.Debug("Enter UpdateMostRecentValue(): Type=" + type + " , lastUpdate=" + value);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);

            //var nodes = xmlDoc.SelectNodes("/MostRecentUpdateInstant/BillableParties");
            var nodes = xmlDoc.SelectNodes("/MostRecentUpdateInstant/" + type);

            foreach (XmlNode ch1 in nodes)
            {
                if (ch1 != null)
                {
                    ch1.InnerText = Convert.ToString(value);
                    Log.Debug("Enter UpdateMostRecentValue(): Before save Type=" + type + " , lastUpdate=" + value);
                    xmlDoc.Save(path);
                    Log.Debug("Enter UpdateMostRecentValue(): After save Type=" + type + " , lastUpdate=" + value);
                    break;
                }
            }

        }

        /// <summary>
        /// Read Last updated value from xml file for each type "Billable parties , BillingItems, paid invoices ets...use this..
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public long GetMostRecentValue(string type)
        {
            Log.Debug("Enter GetMostRecentValue(" + type + "):  ");

            //File path
            string path = Convert.ToString(ConfigurationManager.AppSettings["MostRecentValueTrackingFile"]);
            // path = @"E:\VSUSA\Projects\AXWrapper\AcadisDataServicesSample_4.8.2.59286\AcadisDataServicesSample\MostRecentValues.xml";

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            Log.Debug("Enter GetMostRecentValue(" + type + "): XMLpath= " + path);

            //var nodes = xmlDoc.SelectNodes("/MostRecentUpdateInstant/BillableParties");
            var nodes = xmlDoc.SelectNodes("/MostRecentUpdateInstant/" + type);
            Log.Debug("Exit GetMostRecentValue(" + type + "):  ");
            return (from XmlNode ch1 in nodes
                    where ch1 != null
                    where !string.IsNullOrEmpty(ch1.InnerText)
                    select Convert.ToInt64(ch1.InnerText)).FirstOrDefault();

        }
        /// <summary>
        /// Process billable parties and call AX wrapper serive to create/update customer
        /// </summary>
        /// <param name="billableParties"></param>
        /// <param name="lastUpdateId"></param>
        private void ProcessBillableParty(BillableParty[] billableParties, long lastUpdateId)
        {
            Log.Debug("Enter ProcessBillableParty()");

            #region "Constants"

            const string groupCode = "STUDENTS";

            #endregion "Constants"

            foreach (var eachBillableParty in billableParties)
            {

                //   Create a new Customer, 
                var addNewCustomer = new Customer
                {
                    AccountNumber = Convert.ToString(eachBillableParty.BillableAccountID),
                    FirstName = eachBillableParty.FirstName,
                    LastName = eachBillableParty.LastName,
                    MiddleName = eachBillableParty.MiddleName,
                    Currency = CurrencyCode,
                    Group = groupCode,
                    Suffix = eachBillableParty.Suffix,
                    IsDisable = false,
                    CustomerType = eachBillableParty.BillablePartyType.ToString(),
                    // CustomerType = "Person",
                    OrganizationName = eachBillableParty.Name
                };

                Log.Debug("Enter ProcessBillableParty() : Added Customer");

                //var sbErrorFormat = new StringBuilder();
                //sbErrorFormat.Append("BillableAccountID=" + Convert.ToString(eachBillableParty.BillableAccountID));
                //sbErrorFormat.Append("\tFirstName=" + eachBillableParty.FirstName);
                //sbErrorFormat.Append("\tLastName=" + eachBillableParty.LastName);
                //sbErrorFormat.Append("\tBillablePartyType=" + eachBillableParty.BillablePartyType.ToString());
                //sbErrorFormat.Append("\tName=" + eachBillableParty.Name);


                addNewCustomer.AddressList = new List<CustomerServiceReference.Address>();
                ////Add the address in address list of customer object.
                addNewCustomer.AddressList = ConvertCustomerAddressData(eachBillableParty);

                Log.Debug("Enter ProcessBillableParty(): Added Addresses count=" + addNewCustomer.AddressList.Count().ToString());

                addNewCustomer.ContactList = new List<Contact>();

                addNewCustomer.ContactList = ConvertBPPhoneNumbersToContacts(eachBillableParty);

                Log.Debug("Enter ProcessBillableParty(): Added PhoneNumbers List count=" + eachBillableParty.PhoneNumbers.Count().ToString());

                addNewCustomer.ContactList.AddRange(ConvertBPEmailAddressToContact(eachBillableParty));
                Log.Debug("Enter ProcessBillableParty(): Added Email List count=" + eachBillableParty.EmailAddresses.Count().ToString());

                List<Customer> customerList = new List<Customer>();
                customerList.Add(addNewCustomer);

                // Set up customer client
                using (CustomerClient client = new CustomerClient())
                    try
                    {
                        //Call AX wrapper service to save customer information
                        SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest { Customer = customerList };
                        //Create customer factory object

                        Log.Debug("Enter ProcessBillableParty(): Before calling client.SaveCustomer(saveCustomerRequest)");
                        Log.Debug("Enter ProcessBillableParty(): Calling AX Wrapper Service SAVECUSTOMER");

                        SaveCustomerResponse saveCustomerResponse = client.SaveCustomer(saveCustomerRequest);
                        Log.Debug("Enter ProcessBillableParty(): After calling client.SaveCustomer(saveCustomerRequest)");

                        if (saveCustomerResponse.ValidationDetail.Result)
                        {
                            Log.Info("Success SAVECUSTOMER(): lastUpdateId=" + eachBillableParty.LastUpdateInstant.ToString());
                        }
                        else
                        {
                            Log.Error("Error SAVECUSTOMER(): lastUpdateId=" + eachBillableParty.LastUpdateInstant + " ERROR: " + saveCustomerResponse.ValidationDetail.ErrorList[0].ToString());
                            //   Log.Error("Error SAVECUSTOMER(): lastUpdateId=" + lastUpdateId + " Record: " + sbErrorFormat.ToString());
                            //  MessageBox.Show(saveCustomerResponse.ValidationDetail.ErrorList[0].ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Error: ProcessBillableParty(): lastUpdateId=" + eachBillableParty.LastUpdateInstant + "ERROR: " + ex.Message.ToString());
                        //  Log.Error("Error SAVECUSTOMER(): lastUpdateId=" + lastUpdateId + " Record: " + sbErrorFormat.ToString());
                    }
            }
            Log.Debug("Exit: ProcessBillableParty()");

        }
        /// <summary>
        /// Convert Billable party address to Customer asddress data
        /// </summary>
        /// <param name="billableParty"></param>
        /// <returns></returns>
        private List<CustomerServiceReference.Address> ConvertCustomerAddressData(BillableParty billableParty)
        {
            List<CustomerServiceReference.Address> addressList = new List<CustomerServiceReference.Address>();
            Log.Debug("Enter ConvertCustomerAddressData()");

            // foreach (var eachBillableParty in billableParty.MailingAddress)
            //  {
            if (billableParty.MailingAddress != null)
            {
                string ACADISID = "1";
                string CountryRegionID = "USA";
                //   Create a new Customer, 
                var addNewAddress = new CustomerServiceReference.Address
                {
                    ACADISID = ACADISID,
                    CountryRegionID = CountryRegionID,
                    City = billableParty.MailingAddress.City,
                    //IsPrimaryAddress = BillableParty.MailingAddress.,
                    LocationName = billableParty.MailingAddress.AddressType,
                    // Locator = BillableParty.MailingAddress.,
                    Roles = billableParty.MailingAddress.AddressType,
                    Street =
                        billableParty.MailingAddress.Line1 + " " + Convert.ToString(billableParty.MailingAddress.Line2),
                    State = billableParty.MailingAddress.State,
                    Type = billableParty.MailingAddress.AddressType
                };
                addressList.Add(addNewAddress);
                Log.Debug("Exit ConvertCustomerAddressData()");
            }
            return addressList;
            //  }
        }

        /// <summary>
        /// Convert Billable party PhoneNumbers To Contact 
        /// </summary>
        /// <param name="billableParty"></param>
        /// <returns></returns>
        private List<CustomerServiceReference.Contact> ConvertBPPhoneNumbersToContacts(BillableParty billableParty)
        {
            string ACADISID = "1";
            //foreach (var eachPhoneNumber in billableParty.PhoneNumbers)
            //{
            //    var addNewContact = new CustomerServiceReference.Contact
            //    {
            //        ACADISID = ACADISID,
            //        ContactName = eachPhoneNumber.Number,
            //        Roles = eachPhoneNumber.PhoneType.ToString(),
            //        Locator = eachPhoneNumber.Number.ToString(),
            //        IsPrimary = eachPhoneNumber.IsPrimary,
            //        Type = eachPhoneNumber.PhoneType.ToString();
            //    };
            //    ContactList.Add(addNewContact);
            //}

            return billableParty.PhoneNumbers.Select(eachPhoneNumber => new CustomerServiceReference.Contact
            {
                ACADISID = ACADISID,
                ContactName = eachPhoneNumber.PhoneType.ToString(),
                Roles = "Business",
                Locator = eachPhoneNumber.Number.ToString(),
                IsPrimary = eachPhoneNumber.IsPrimary,
                //IsPrimary = false,
                Type = "PHONE"
            }).ToList();
            //List<CustomerServiceReference.Contact> ContactList =
            //    billableParty.PhoneNumbers.Select(eachPhoneNumber => new CustomerServiceReference.Contact
            //    {
            //        ACADISID = ACADISID,
            //        ContactName = eachPhoneNumber.PhoneType.ToString(),
            //        Roles = "Phone",
            //        Locator = eachPhoneNumber.Number.ToString(),
            //        IsPrimary = eachPhoneNumber.IsPrimary,
            //        Type = eachPhoneNumber.PhoneType.ToString(),
            //    }).ToList();
        }

        /// <summary>
        /// Convert Billable party email addresses To Contact list
        /// </summary>
        /// <param name="billableParty"></param>
        /// <returns></returns>
        private List<CustomerServiceReference.Contact> ConvertBPEmailAddressToContact(BillableParty billableParty)
        {
            string ACADISID = "1";
            string EmailType = "EMAIL";
            const string Roles = "Business";

            //foreach (var eachEmailAddress in billableParty.EmailAddresses)
            //{
            //    var addNewContact = new CustomerServiceReference.Contact
            //    {
            //        ACADISID = ACADISID,
            //        ContactName = eachEmailAddress.Address,
            //        Roles = Roles,
            //        IsPrimary = eachEmailAddress.IsPrimary,
            //        Type = EmailType,
            //    };
            //    contactList.Add(addNewContact);
            //}

            return billableParty.EmailAddresses.Select(eachEmailAddress => new CustomerServiceReference.Contact
            {
                ACADISID = ACADISID,
                ContactName = eachEmailAddress.Address,
                Roles = Roles,
                IsPrimary = eachEmailAddress.IsPrimary,
                //IsPrimary = false,
                Type = EmailType,
            }).ToList();
        }
        #endregion "Billable Parties"

        #region "Invoices"
        /// <summary>
        /// Get invoiceable items from ACADIS
        /// </summary>
        private void MigrateInvoiceableItems()
        {
            try
            {
                Log.Debug("Enter MigrateInvoiceableItems() " + DateTime.Now.ToString());
                var config = new AcadisDataServicesClientConfiguration(ConfigurationManager.AppSettings);
                var accounting = new AccountingClient(config);
                Log.Debug("MigrateInvoiceableItems(): Before calling");

                //type is billable parties 
                const string type = "BillingItems";

                long lastUpdate = GetMostRecentValue(type); ;
                //Warning! Calling the service with lastUpdate == 0 will get ALL billing info
                Log.Debug("Enter MigrateInvoiceableItems(): lastUpdate= " + lastUpdate);

                var invoiceItems = accounting.GetInvoiceableItems(lastUpdate, PAGE_SIZE);


                Log.Debug("MigrateInvoiceableItems(): Successfully got records " + invoiceItems.Items.Count() + " from ACADIS" + DateTime.Now.ToString());

                //var results = from p in invoiceItems
                //              group p by new { p.BillToID, p.CreationDate }
                //                  into g
                //                  select new { g }; //

                var results = from p in invoiceItems.Items
                              group p by new { p.BillToID, p.BillDate.Date }
                                  into g
                                  select new { g }; //

                // Set up Invoice client
                using (InvoiceClient client = new InvoiceClient())
                {

                    //// implemented credentials starts
                    ////string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
                    //string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

                    client.ClientCredentials.UserName.UserName = serviceUserid;
                    client.ClientCredentials.UserName.Password = servicePassword;
                    client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;


                    foreach (var each in results)
                    {
                        //declaring invoice list
                        List<Invoice> invoiceList = new List<Invoice>();

                        //declaring invoice line list

                        List<InvoiceLine> invoiceLineList = new List<InvoiceLine>();
                        int count = 0;
                        string BillToID = string.Empty;
                        try
                        {
                            string LastUpdateInstants = string.Empty;
                            string InvoiceAccount = string.Empty;
                            foreach (var item in each.g)
                            {
                                if (string.IsNullOrEmpty(LastUpdateInstants))
                                    LastUpdateInstants = item.LastUpdateInstant.ToString();
                                else
                                    LastUpdateInstants += "," + item.LastUpdateInstant;

                                lastUpdate = item.LastUpdateInstant;

                                #region "Create"

                                string customerReference = string.Empty;
                                customerReference = Convert.ToString(item.BillFor);
                                string purchaseOrderFormNumber = string.Empty;
                                //string CustomerReference = "123";
                                //string PurchaseOrderFormNumber = "123";
                                string InvoiceId = string.Empty;

                                string Category = Convert.ToString(item.Category).ToUpper();
                                string ARCode = string.Empty;

                                switch (Category)
                                {
                                    case "CLASS":
                                        ARCode = "C";
                                        break;
                                    case "HOUSING":
                                    case "EVENTHOUSING":
                                    case "CLASSHOUSING":
                                        ARCode = "D";
                                        break;
                                    case "ONLINEEVENT":
                                        ARCode = "O";
                                        break;
                                    case "RENTABLE":
                                        ARCode = "O";
                                        break;
                                }

                                ////Now ARCode will allow only one character in AX so we just taking first characters if ACADIS returns more than one characters.
                                //if (ARCode.Length > 1)
                                //    ARCode = ARCode.Substring(0, 1);

                                //Now Description will allow only 60 characters in AX so we just taking first characters if ACADIS returns more than one characters.
                                //string description = Convert.ToString(item.PeriodStart) + " " +
                                //                     Convert.ToString(item.PeriodEnd) + " " +
                                //                     Convert.ToString(item.Usage) + " " +
                                //                     Convert.ToString(item.Item) + " " + Convert.ToString(item.Event);

                                string description = item.Description;

                                ////Now Description will allow 60 characters in AX so we just taking first characters if ACADIS returns more than one characters.
                                //if (description.Length > 60)
                                //{
                                //    description = description.Substring(0, 60);
                                //    Log.Error("MigrateInvoiceableItems(): description length is more than 60, so limited to 60 , 'Last Update=" +
                                //        item.LastUpdateInstant + "'");
                                //}

                                //adding a description in invoice line list
                                invoiceLineList.Add(new InvoiceLine()
                                {
                                    ARTrans = item.ID,
                                    ARCode = ARCode, // Now ARCode will accept only one character
                                    Quantity = item.Quantity,
                                    UnitPrice = item.UnitPrice,
                                    // MainAccount = "426050",
                                    Amount = item.Amount,
                                    InvoiceDefaultDimensionList = new List<DefaultDimension>(),
                                    //Now Description will allow only 60 characters in AX so we just taking first characters if ACADIS returns more than one characters.
                                    Description = description,
                                    VEN_BillableItemId = item.ID.ToString() // Added for invoice cancellation
                                });

                                // since we are looping the invoice lines so taking first invoice and 
                                if (count == 0)
                                {
                                    // set up invoice data
                                    Invoice newInvoice = new Invoice()
                                    {
                                        CustomerReference = customerReference,
                                        InvoiceDate = item.BillDate,
                                        //  OrderAccount = Convert.ToString(item.BillForID),
                                        OrderAccount = Convert.ToString(item.BillToID),
                                        // Billing for id - Order account
                                        InvoiceAccount = Convert.ToString(item.BillToID),
                                        // billing to id - invoice account
                                        //OrderAccount = "1359476",
                                        //InvoiceAccount = "1359476",
                                        InvoiceId = InvoiceId, // Invoice ID
                                        PurchaseOrderFormNumber = purchaseOrderFormNumber,
                                        CurrencyCode = CurrencyCode,
                                        InvoiceLineList = invoiceLineList,
                                        InvoiceDefaultDimensionList = new List<DefaultDimension>(),
                                        // InvoiceDefaultDimensionList = new List<DefaultDimension>()
                                        IsAutoPost = true
                                        // true for post the invoice when creation and false for non-posting.
                                    };
                                    BillToID =Convert.ToString(item.BillToID);
                                    InvoiceAccount = BillToID;
                                    invoiceList.Add(newInvoice);
                                }
                                count++;

                                #endregion "Create"

                            }
                            //Setup the invoice request with invoice lines
                            var createInvoiceRequest = new CreateInvoiceRequest
                            {
                                Invoice = invoiceList
                            };

                            
                             // Set up customer client
                            using (CustomerClient CustomerClient = new CustomerClient())
                                try
                                {
                                    List<string> BillToList = new List<string>();
                                    BillToList.Add(BillToID);

                                    //Call AX wrapper service to verify the customer is exists or not
                                    IsCustomerExistsRequest IsCustomerExistsRequest = new IsCustomerExistsRequest { AccountNumbers = BillToList };

                                    //checking 5 times to get customer created in AX
                                    for (int i = 0; i < 5; i++)
                                    {
                                        IsCustomerExistsResponse IsCustomerExistsResponse = CustomerClient.IsCustomerExists(IsCustomerExistsRequest);

                                        if (IsCustomerExistsResponse.ValidationDetail.Result)
                                        {
                                            Log.Info("Success IsCustomerExists(): True for BillToID=" + BillToID.ToString());

                                            #region " invoice creation "
                                            //Call thre service and assign the response
                                            var createInvoiceResponse = new CreateInvoiceResponse
                                            {
                                                ValidationDetail = client.CreateInvoice(createInvoiceRequest.Invoice)
                                            };

                                            // result will be true for success and false for failure invoices
                                            if (createInvoiceResponse.ValidationDetail.Result)
                                            {
                                                Log.Info("Invoice created Successfully by windows service for Invoice Account(BillToID)=" + InvoiceAccount + "; lastupdate ids=" + LastUpdateInstants);
                                                break;
                                            }
                                            else
                                            {
                                                var errorMessage = createInvoiceResponse.ValidationDetail.ErrorList[0].ToString();
                                                //Log.Info(errorMessage);
                                                Log.Error("Error:CreateInvoice() : " + errorMessage + " ; Invoice created failed by windows service for Invoice Account(BillToID)=" + InvoiceAccount + "; lastupdate ids=" + LastUpdateInstants);
                                            }
                                            #endregion " invoice creation "

                                        }
                                        else
                                        {
                                            Log.Info("Success IsCustomerExists(): False for BillToID=" + BillToID.ToString());
                                        }
                                    } //for loop ends here
                                }
                                catch (Exception ex1)
                                {
                                    Log.Error("Error IsCustomerExists(): False for BillToID=" + BillToID.ToString() + "  " + ex1.Message);
                                }
                        }
                        catch (Exception ex)
                        {
                            Log.Error("Error:CreateInvoice() : " + ex.Message);
                        }
                    }
                    Log.Debug("Enter MigrateBillabaleParties(): after loop lastUpdate=" + lastUpdate);
                    UpdateMostRecentValue(type, lastUpdate);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error:CreateInvoice() : " + ex.Message);
            }
        }
        #endregion "Invoices"

        #region "Payments"
        private void MigratePayments()
        {
            try
            {
                Log.Debug("Enter MigratePayments() " + DateTime.Now.ToString());
                var config = new AcadisDataServicesClientConfiguration(ConfigurationManager.AppSettings);
                Log.Debug("MigratePayments(): Before calling GetMostRecentValue");

                //type is billable parties 
                const string type = "payments";

                long lastUpdate = GetMostRecentValue(type); ;
                //Warning! Calling the service with lastUpdate == 0 will get ALL billing info
                Log.Debug("Enter MigratePayments() : lastUpdate= " + lastUpdate);
                var accounting = new AccountingClient(config);

                GetPaymentsResponse payments = accounting.GetPayments(lastUpdate, PAGE_SIZE);

                do
                {
                    lastUpdate = payments.MostRecentUpdateInstant;
                    CreateNewPayment(payments);
                    payments = accounting.GetNextPage(payments);
                } while (payments != null);

                Log.Debug("Enter MigratePayments(): after loop lastUpdate=" + lastUpdate);
                UpdateMostRecentValue(type, lastUpdate);

            }
            catch (Exception ex)
            {
                Log.Error("Error:MigratePayments() : " + ex.Message);
            }
        }
        /// <summary>
        /// Create ledger payments
        /// </summary>
        /// <param name="Payments"></param>
        private void CreateNewPayment(GetPaymentsResponse Payments)
        {
            #region
            string type = "payments";

            //Service user id and password
            //string serviceUserid = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.UserId"]));
            // string servicePassword = EncryptandDecrypt.GeneralFunctions.Decrypt(Convert.ToString(ConfigurationManager.AppSettings["ServiceSecurity.Password"]));

            for (int paymentsCount = 0; paymentsCount < Payments.Payments.Count(); paymentsCount++)
            {
                // Set up Ledger Journal Client
                using (LedgerJournalClient client = new LedgerJournalClient())
                    try
                    {
                        #region "Create"

                        #region  "Payemnt line items"

                        List<LedgerJournal> journalTableList = new List<LedgerJournal>();
                        List<LedgerJournalTransaction> journalLineList = new List<LedgerJournalTransaction>();

                        //creating Journal list 
                        journalLineList = GetLedgerJournalTransactionData(Payments.Payments[paymentsCount]);

                        journalTableList.Add(new LedgerJournal()
                        {
                            JournalName = string.Empty, // journal Name will be set in the service itself from configuartion
                            // Description = txtJournalDescription.Text, //"Test Journal AIF",
                            LedgerJournalTransactionList = journalLineList
                        });

                        #endregion  "Payemnt line items"
                        //// implemented credentials starts

                        client.ClientCredentials.UserName.UserName = serviceUserid;
                        client.ClientCredentials.UserName.Password = servicePassword;
                        client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;


                        CreatePaymentRequest createPaymentRequest = new CreatePaymentRequest
                        {
                            LedgerJournalList = journalTableList
                        };
                        //Create Payment factory object

                        CreatePaymentResponse createPaymentResponse = new CreatePaymentResponse();

                        createPaymentResponse.ValidationDetail = client.CreatePayment(createPaymentRequest.LedgerJournalList);

                        if (createPaymentResponse.ValidationDetail.Result)
                        {
                            Log.Info("Payment created Successfully by windows service for Payments(PaymentsID)=" + Payments.Payments[0].ID + "; lastupdate ids=" + Payments.MostRecentUpdateInstant);
                        }
                        else
                        {
                            var errorMessage = createPaymentResponse.ValidationDetail.ErrorList[0].ToString();
                            Log.Info(errorMessage);
                            Log.Error("Error:CreatewNewPayment() : " + errorMessage + " ; payment created failed by windows service for Payments(PaymentsID)=" + Payments.Payments[0].ID + "; lastupdate ids=" + Payments.MostRecentUpdateInstant);
                        }

                        #endregion
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Error:CreatewNewPayment() : " + ex.Message + " ; payment created failed by windows service for Payments(PaymentsID)=" + Payments.Payments[0].ID + "; lastupdate ids=" + Payments.MostRecentUpdateInstant);
                    
                    }
                    finally
                    {
                        Log.Debug("Enter CreatewNewPayment(): after loop lastUpdate=" + Payments.MostRecentUpdateInstant);
                        UpdateMostRecentValue(type, Payments.MostRecentUpdateInstant);

                    }

            }
            #endregion
        }

        public List<LedgerJournalTransaction> GetLedgerJournalTransactionData(Payment Payments)
        {
            string offsetAccountType = "Ledger";
            string AccountType = "Cust";

            List<LedgerJournalTransaction> ledgerJournalTransactionList = new List<LedgerJournalTransaction>();
            //  for (int paymentsCount = 0; paymentsCount < Payments.Payments.Count(); paymentsCount++)
            //  {
            //getting autorize date from the payments object
            string AuthorizedDate = Convert.ToString(Payments.AuthorizedDate);

            //getting submiotted date from the payments object
            string SubmittedDate = Convert.ToString(Payments.SubmittedDate);

            //getting payment id from the payments object
            int ID = Payments.ID;

            //getting payment type from the payments object
            string PaymentType = Payments.PaymentType.Value.ToString();

            //looping invoices for making ledger transaction list
            for (int invoiceCount = 0; invoiceCount < Payments.Invoices.Count(); invoiceCount++)
            {

                LedgerJournalTransaction objLedgerJournalTransaction = new LedgerJournalTransaction();

                objLedgerJournalTransaction.Description = string.Empty;

                objLedgerJournalTransaction.AccountType = AccountType;

                objLedgerJournalTransaction.PaymentId = ID.ToString();

                objLedgerJournalTransaction.PaymentMode = PaymentType;

                objLedgerJournalTransaction.Debit = 0;

                objLedgerJournalTransaction.OffsetAccountType = offsetAccountType;

                string Amount = Convert.ToString(Payments.Invoices[invoiceCount].Amount);
                string InvoiceNumber = Payments.Invoices[invoiceCount].InvoiceNumber;
                string BillableAccountId = Convert.ToString(Payments.Invoices[invoiceCount].BillableAccountId);


                if (!string.IsNullOrEmpty(SubmittedDate))
                    objLedgerJournalTransaction.TransactionDate = Convert.ToDateTime(SubmittedDate);

                objLedgerJournalTransaction.InvoiceId = InvoiceNumber;


                if (!string.IsNullOrEmpty(Amount))
                    objLedgerJournalTransaction.Credit = Convert.ToDecimal(Amount);
                else
                    objLedgerJournalTransaction.Credit = 0;


                objLedgerJournalTransaction.LedgerDimension = new LedgerDimension() { Account = BillableAccountId, DisplayValue = BillableAccountId };

                string _displayValue = BillableAccountId;

                ////since Ledger dimension default to AX - on  Gustavo-Apr-30

                //objLedgerJournalTransaction.OffsetLedgerDimension = new LedgerDimension()
                //{
                //    Account = _displayValue, // "200102", //PaymentCustomerAccount,//"1359476",
                //    DisplayValue = _displayValue
                //    //DisplayValue = PaymentCustomerAccount,//"1359476"
                //    //Values = offsetDefaultDimension 200102
                //    //Account = "200102", //PaymentCustomerAccount,//"1359476",
                //    //DisplayValue = "200102"
                //};

                ledgerJournalTransactionList.Add(objLedgerJournalTransaction);
            }
            //}
            return ledgerJournalTransactionList;
        }

        #endregion "Payments"

        #region "Cancel Invoices"
        private void MigrateCancellations()
        {
            try
            {
                Log.Debug("Enter InvoiceCancellation() " + DateTime.Now.ToString());
                var config = new AcadisDataServicesClientConfiguration(ConfigurationManager.AppSettings);
                Log.Debug("InvoiceCancellation(): Before calling GetMostRecentValue");

                //type is invoice cancellation 
                const string type = "cancellations";

                long lastUpdate = GetMostRecentValue(type);
                //Warning! Calling the service with lastUpdate == 0 will get ALL  info
                Log.Debug("Enter InvoiceCancellation() : lastUpdate= " + lastUpdate);
                var client = new AccountingClient(config);

                GetCancellationsResponse cancellations = client.GetCancellations(lastUpdate, PAGE_SIZE);

                do
                {
                    lastUpdate = cancellations.MostRecentUpdateInstant;
                    ProcessCancellations(cancellations.Items);
                    cancellations = client.GetNextPage(cancellations);
                } while (cancellations != null);

                Log.Debug("Enter InvoiceCancellation(): after loop lastUpdate=" + lastUpdate);
                UpdateMostRecentValue(type, lastUpdate);

            }
            catch (Exception ex)
            {
                Log.Error("Error:InvoiceCancellation() : " + ex.Message);
            }
        }

        private void ProcessCancellations(CancellationItem[] items)
        {
            using (InvoiceClient client = new InvoiceClient())
            {

                //// implemented credentials starts

                client.ClientCredentials.UserName.UserName = serviceUserid;
                client.ClientCredentials.UserName.Password = servicePassword;
                client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;

                //declaring invoice list
                List<InvoiceCancellation> InvoiceCancellationList = new List<InvoiceCancellation>();

                foreach (var eachItem in items)
                {
                    try
                    {// Set up Invoice client

                        //adding a new invoice to cancel
                        InvoiceCancellationList.Add(new InvoiceCancellation()
                        {
                            CancellationDate = eachItem.CancelDateTime,
                            BillableItemID = eachItem.CancelledItem.ID.ToString(),
                            CancellationID = eachItem.ID.ToString(),
                            Reason = eachItem.CancelReason
                        });

                        CancelInvoiceRequest CancelInvoiceRequest = new CancelInvoiceRequest { Invoice = InvoiceCancellationList };

                        //Cancel Invoice response
                        CancelInvoiceResponse CancelInvoiceResponse = new CancelInvoiceResponse();
                        CancelInvoiceResponse.ValidationDetail = client.CancelInvoice(CancelInvoiceRequest.Invoice);

                        if (CancelInvoiceResponse.ValidationDetail.Result)
                        {
                            Log.Info("Invoice cancellation record created successfully by windows service for id=" + eachItem.ID.ToString() + "; lastupdate id=" + eachItem.LastUpdateInstant);
                        }
                        else
                        {
                            var errorMessage = CancelInvoiceResponse.ValidationDetail.ErrorList[0].ToString();
                            Log.Info(errorMessage);
                            Log.Error("Error:ProcessCancellations() : " + errorMessage + " ; cancellation creation failed by windows service for Cancelaation id=" + eachItem.ID.ToString() + "; lastupdate id=" + eachItem.LastUpdateInstant);
                        }

                    } //try scope ends here
                    catch (Exception ex)
                    {
                        Log.Error("Error:ProcessCancellations() : " + ex.Message + " ; cancellation creation failed by windows service for Cancelaation id=" + eachItem.ID.ToString() + "; lastupdate id=" + eachItem.LastUpdateInstant);
                    }
                    finally
                    {
                        Log.Debug("Enter ProcessCancellations(): lastUpdate=" + eachItem.LastUpdateInstant);
                        UpdateMostRecentValue("Cancellations", eachItem.LastUpdateInstant);
                    }
                } //for loop ends here
            } //invoice client scope ends here
        }
        #endregion "Cancel Invoices"
    }
}